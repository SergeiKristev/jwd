-- MySQL Script generated by MySQL Workbench
-- Tue Jul 14 11:07:27 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema doctor_home_visit_service
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema doctor_home_visit_service
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `doctor_home_visit_service` DEFAULT CHARACTER SET utf8 ;
USE `doctor_home_visit_service` ;

-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`appointment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`appointment` (
  `appointment_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `visit_start_time` TIME NOT NULL,
  `visit_end_time` TIME NOT NULL,
  `isFreeTime` TINYINT(1) NOT NULL DEFAULT 1,
  `day_schedule_id` BIGINT(10) NOT NULL,
  PRIMARY KEY (`appointment_id`),
  CONSTRAINT `fk_week_schedule_day_schedule1`
    FOREIGN KEY (`day_schedule_id`)
    REFERENCES `doctor_home_visit_service`.`day_schedule` (`day_schedule_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `doctor_home_visit_service`.`appointment` (`appointment_id` ASC) VISIBLE;

CREATE INDEX `fk_week_schedule_day_schedule1_idx` ON `doctor_home_visit_service`.`appointment` (`day_schedule_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`day_schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`day_schedule` (
  `day_schedule_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `doctor_id` BIGINT(10) NOT NULL,
  `schedule_date` DATE NOT NULL,
  `schedule_start` TIME NOT NULL,
  `schedule_end` TIME NOT NULL,
  PRIMARY KEY (`day_schedule_id`),
  CONSTRAINT `fk_day_schedule_doctor1`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `doctor_home_visit_service`.`doctor` (`doctor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `doctor_home_visit_service`.`day_schedule` (`day_schedule_id` ASC) VISIBLE;

CREATE INDEX `fk_day_schedule_doctor1_idx` ON `doctor_home_visit_service`.`day_schedule` (`doctor_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`doctor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`doctor` (
  `doctor_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_account_id` BIGINT(10) NOT NULL,
  `speciality` VARCHAR(45) NOT NULL,
  `medical_category` VARCHAR(45) NOT NULL,
  `experience` INT NOT NULL,
  PRIMARY KEY (`doctor_id`),
  CONSTRAINT `fk_doctor_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `doctor_home_visit_service`.`user_account` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `doctor_id_UNIQUE` ON `doctor_home_visit_service`.`doctor` (`doctor_id` ASC) VISIBLE;

CREATE INDEX `fk_doctor_user_account1_idx` ON `doctor_home_visit_service`.`doctor` (`user_account_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`icd_10`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`icd_10` (
  `icd10_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(512) NOT NULL,
  `code` VARCHAR(20) NOT NULL,
  `parent_id` BIGINT(10) NULL,
  `parent_code` VARCHAR(20) NULL,
  `node_count` INT NOT NULL DEFAULT 0,
  `additional_info` TEXT NULL,
  PRIMARY KEY (`icd10_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`medical_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`medical_card` (
  `medical_card_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `patient_id` BIGINT(10) NOT NULL,
  `visit_id` BIGINT(10) NOT NULL,
  `symptoms` VARCHAR(500) NOT NULL,
  `medical_examination` VARCHAR(1000) NOT NULL,
  `clinical_diagnosis` VARCHAR(250) NOT NULL,
  `icd_10_id` BIGINT(10) NOT NULL,
  `recommendations` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`medical_card_id`),
  CONSTRAINT `fk_medical_card_visit1`
    FOREIGN KEY (`visit_id`)
    REFERENCES `doctor_home_visit_service`.`visit` (`visit_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_prescription_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `doctor_home_visit_service`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_medical_card_icd_101`
    FOREIGN KEY (`icd_10_id`)
    REFERENCES `doctor_home_visit_service`.`icd_10` (`icd10_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `doctor_home_visit_service`.`medical_card` (`medical_card_id` ASC) VISIBLE;

CREATE INDEX `fk_medical_card_visit1_idx` ON `doctor_home_visit_service`.`medical_card` (`visit_id` ASC) VISIBLE;

CREATE INDEX `fk_prescription_patient1_idx` ON `doctor_home_visit_service`.`medical_card` (`patient_id` ASC) VISIBLE;

CREATE INDEX `fk_medical_card_icd_101_idx` ON `doctor_home_visit_service`.`medical_card` (`icd_10_id` ASC) VISIBLE;

CREATE UNIQUE INDEX `visit_id_UNIQUE` ON `doctor_home_visit_service`.`medical_card` (`visit_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`patient`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`patient` (
  `patient_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_account_id` BIGINT(10) NOT NULL,
  `home_address` VARCHAR(250) NOT NULL,
  `polyclinic` VARCHAR(100) NULL,
  `insurance` VARCHAR(250) NULL,
  PRIMARY KEY (`patient_id`),
  CONSTRAINT `fk_patient_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `doctor_home_visit_service`.`user_account` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_patient_user_account1_idx` ON `doctor_home_visit_service`.`patient` (`user_account_id` ASC) VISIBLE;

CREATE UNIQUE INDEX `id_UNIQUE` ON `doctor_home_visit_service`.`patient` (`patient_id` ASC) VISIBLE;

CREATE UNIQUE INDEX `user_account_id_UNIQUE` ON `doctor_home_visit_service`.`patient` (`user_account_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`tariff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`tariff` (
  `tariff_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `tariff_name` VARCHAR(45) NOT NULL,
  `cost` DECIMAL(3) NOT NULL,
  PRIMARY KEY (`tariff_id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `idtarifа_UNIQUE` ON `doctor_home_visit_service`.`tariff` (`tariff_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`user_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`user_account` (
  `user_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_email` VARCHAR(45) NOT NULL,
  `user_password` VARCHAR(45) NOT NULL,
  `user_phone` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `gender` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `is_active` TINYINT(1) NOT NULL DEFAULT 1,
  `user_role` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `user_id_UNIQUE` ON `doctor_home_visit_service`.`user_account` (`user_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `doctor_home_visit_service`.`visit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `doctor_home_visit_service`.`visit` (
  `visit_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `patient_id` BIGINT(10) NOT NULL,
  `visit_reason` VARCHAR(250) NOT NULL,
  `visit_type` VARCHAR(45) NOT NULL,
  `visit_date` DATETIME NOT NULL,
  `doctor_id` BIGINT(10) NOT NULL,
  `tariff_id` BIGINT(10) NOT NULL,
  `notes` VARCHAR(200) NULL,
  `appointment_status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`visit_id`),
  CONSTRAINT `fk_visit_doctor1`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `doctor_home_visit_service`.`doctor` (`doctor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `doctor_home_visit_service`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_tariff1`
    FOREIGN KEY (`tariff_id`)
    REFERENCES `doctor_home_visit_service`.`tariff` (`tariff_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `doctor_home_visit_service`.`visit` (`visit_id` ASC) VISIBLE;

CREATE INDEX `fk_visit_doctor1_idx` ON `doctor_home_visit_service`.`visit` (`doctor_id` ASC) VISIBLE;

CREATE INDEX `fk_visit_patient1_idx` ON `doctor_home_visit_service`.`visit` (`patient_id` ASC) VISIBLE;

CREATE INDEX `fk_visit_tariff1_idx` ON `doctor_home_visit_service`.`visit` (`tariff_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




-- password for all users - Qwerty1
INSERT INTO `doctor_home_visit_service`.`user_account` (`user_email`, `user_password`, `user_phone`, `first_name`, `last_name`, `gender`, `date_of_birth`, `is_active`, `user_role`) VALUES ('admin@email.com', 'bd462d5d7e7d5f8416515c6b0f3ed640', '8(029)804-85-23', 'Сергей', 'Кристев', 'MALE', '1990-05-30', '1', 'ADMIN');
INSERT INTO `doctor_home_visit_service`.`user_account` (`user_email`, `user_password`, `user_phone`, `first_name`, `last_name`, `gender`, `date_of_birth`, `is_active`, `user_role`) VALUES ('doctor@email.com', 'bd462d5d7e7d5f8416515c6b0f3ed640', '8(033)850-44-34', 'Иван', 'Иванов', 'MALE', '1985-12-23', '1', 'DOCTOR');
INSERT INTO `doctor_home_visit_service`.`user_account` (`user_email`, `user_password`, `user_phone`, `first_name`, `last_name`, `gender`, `date_of_birth`, `is_active`, `user_role`) VALUES ('doctor2@email.com', 'bd462d5d7e7d5f8416515c6b0f3ed640', '8(029)756-66-24', 'Николай', 'Николаев', 'MALE', '1987-03-23', '1', 'DOCTOR');
INSERT INTO `doctor_home_visit_service`.`user_account` (`user_email`, `user_password`, `user_phone`, `first_name`, `last_name`, `gender`, `date_of_birth`, `is_active`, `user_role`) VALUES ('patient@email.com', 'bd462d5d7e7d5f8416515c6b0f3ed640', '8(044)340-55-20', 'Петр', 'Петров', 'MALE', '1988-04-22', '1', 'PATIENT');


INSERT INTO `doctor_home_visit_service`.`doctor` (`user_account_id`, `speciality`, `medical_category`, `experience`) VALUES ('2', 'PEDIATRICIAN', 'FIRST', '6');
INSERT INTO `doctor_home_visit_service`.`doctor` (`user_account_id`, `speciality`, `medical_category`, `experience`) VALUES ('3', 'PEDIATRICIAN', 'FIRST', '10');

INSERT INTO `doctor_home_visit_service`.`patient` (`user_account_id`, `home_address`, `polyclinic`) VALUES ('4', 'ул. Навуковая 6-143', '19');

INSERT INTO `doctor_home_visit_service`.`tariff` (`tariff_name`, `cost`) VALUES ('BASE', '40.0');
INSERT INTO `doctor_home_visit_service`.`tariff` (`tariff_name`, `cost`) VALUES ('NARROW SPECIALIST', '45.0');







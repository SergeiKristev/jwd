<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="commandName" type="by.kristev.doctor.command.CommandName" required="true"
                         description="Command name to submit"/>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<c:choose>
    <c:when test="${securityContext.canExecute(commandName)}">
        <jsp:doBody/>
    </c:when>
    <c:otherwise>
<%--        <fmt:message key="security.not.enough.permission"/>--%>
    </c:otherwise>
</c:choose>



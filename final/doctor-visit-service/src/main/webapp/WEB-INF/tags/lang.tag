<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<ul class="nav-menu">
    <a href="?lang=en">
        <span><fmt:message key="links.lang.en"/></span>
    </a>
    <a href="?lang=ru">
        <span><fmt:message key="links.lang.ru"/></span>
    </a>
</ul>

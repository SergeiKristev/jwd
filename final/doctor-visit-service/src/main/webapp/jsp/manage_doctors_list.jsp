<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.AppointmentStatus" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<!-- Кнопка запуска модального окна -->
<button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1" data-toggle="modal"
        data-target="#registerDoctor">
    <fmt:message key="button.doctor_registration"/>
</button>

<!-- Модальное окно -->
<div class="modal fade" id="registerDoctor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.doctor_registration"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <jsp:include page="registration_doctor.jsp"/>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>
            </div>
        </div>
    </div>
</div>

<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm"><fmt:message key="doctor.id"/></th>
        <th class="th-sm"><fmt:message key="user.firstName"/></th>
        <th class="th-sm"><fmt:message key="user.last_name"/></th>
        <th class="th-sm"><fmt:message key="doctor.speciality"/></th>
        <th class="th-sm"><fmt:message key="doctor.medical_category"/></th>
        <th class="th-sm"><fmt:message key="list.visit.option"/></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty doctorList}">
        <c:forEach items="${doctorList}" var="doctor">
            <tr>
                <td><c:out value="${doctor.doctorId}"/></td>
                <td><c:out value="${doctor.firstName}"/></td>
                <td><c:out value="${doctor.lastName}"/></td>
                <td><fmt:message key="doctor.speciality.${doctor.speciality}"/></td>
                <td><fmt:message key="doctor.category.${doctor.medicalCategory}"/></td>

                <td>

                    <!-- Кнопка запуска модального окна -->
                    <button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                            data-toggle="modal" data-target="#myModal${doctor.doctorId}">
                        <fmt:message key="button.detail"/>
                    </button>

                    <!-- Модальное окно -->
                    <div class="modal fade" id="myModal${doctor.doctorId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.detail"/></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table id="dtUsers" class="table table-striped table-bordered table-sm"
                                           cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="th-sm">#</th>
                                            <th class="th-sm"><fmt:message key="page.doctor.details"/></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><fmt:message key="doctor.id"/></td>
                                            <td><c:out value="${doctor.doctorId}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.firstName"/></td>
                                            <td><c:out value="${doctor.firstName}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.last_name"/></td>
                                            <td><c:out value="${doctor.lastName}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.email"/></td>
                                            <td><c:out value="${doctor.email}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="doctor.speciality"/></td>
                                            <td><fmt:message key="doctor.speciality.${doctor.speciality}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="doctor.medical_category"/></td>
                                            <td><fmt:message key="doctor.category.${doctor.medicalCategory}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="doctor.experience"/></td>
                                            <td><c:out value="${doctor.experience}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.phone"/></td>
                                            <td><c:out value="${doctor.phone}"/></td>
                                        </tr>

                                        </tbody>

                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <security:check commandName="${CommandName.EDIT_DOCTOR}">
                            <form>
                                <input type="hidden" name="_command" value="${CommandName.DISPLAY_EDIT_DOCTOR_FORM}">
                                <input type="hidden" name="doctor.id" value="${doctor.doctorId}">
                                <input type="submit" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                       value="<fmt:message key="button.edit"/>">
                            </form>

                    </security:check>

                    <!-- Кнопка запуска модального окна -->
                    <button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1" data-toggle="modal"
                            data-target="#editSchedule${doctor.doctorId}">
                        <fmt:message key="button.edit_schedule"/>
                    </button>
                    <!-- Модальное окно -->
                    <div class="modal fade" id="editSchedule${doctor.doctorId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel"><fmt:message key="page.edit_schedules"/></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container">
                                        <h1 class="well"><fmt:message key="form.schedule.select_days"/></h1>
                                        <h4 class="well"><fmt:message key="form.visit.doctor"/>: <c:out value="${doctor.lastName}"/></h4>
                                        <div class="col-lg-12 well">
                                            <div class="row">
                                                <form action="${pageContext.request.contextPath}/" method="get">
                                                    <input type="hidden" name="doctor_id" value="${doctor.doctorId}">
                                                    <input type="hidden" name="_command" value="${CommandName.MANAGE_SCHEDULES_LIST}">
                                                    <div class="row">
                                                        <div class="col-sm-12 form-group">
                                                            <label><fmt:message key="form.schedule.date_from"/></label>
                                                            <input class="form-control" type="date" name="date_from" placeholder="<fmt:message key="form.schedule.date_from"/>.." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                                                        </div>

                                                        <div class="col-sm-12 form-group">
                                                            <label><fmt:message key="form.schedule.date_to"/></label>
                                                            <input class="form-control" type="date" name="date_to" placeholder="<fmt:message key="form.schedule.date_to"/>.." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                                                        </div>

                                                        <button type="submit" class="btn btn-lg btn-primary"><fmt:message key="form.registration.submit"/></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>

            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th><fmt:message key="doctor.id"/></th>
    <th><fmt:message key="user.firstName"/></th>
    <th><fmt:message key="user.last_name"/></th>
    <th><fmt:message key="doctor.speciality"/></th>
    <th><fmt:message key="doctor.medical_category"/></th>
    <th><fmt:message key="list.visit.option"/></th>
    </tfoot>
</table>

<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable({
            "order": [[1, "asc"]]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

<script>
    $("#phone").mask("8(999)999-99-99");
</script>




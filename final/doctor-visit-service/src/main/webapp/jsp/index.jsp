<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<div class="container">
    <div class="row">
        <div class="col-lg-5 align-self-center">
            <div class="welcome-img">
                <img src="${pageContext.request.contextPath}/static/images/welcome.png" alt="">
            </div>
        </div>
        <div class="col-lg-7">
            <div class="welcome-text mt-5 mt-lg-0">
                <h2><fmt:message key="page.welcome"/></h2>
                <p class="pt-3"><fmt:message key="page.description.title"/></p>
                <p><fmt:message key="page.description.text"/></p>
                <c:choose>
                    <c:when test="${securityContext.loggedIn}">
                        <a href="?_command=${CommandName.TAKE_APPOINTMENT}" class="template-btn mt-3"><fmt:message key="links.take_appointment"/></a>
                    </c:when>
                    <c:otherwise>
                        <a href="#" class="modal-link template-btn mt-3"><fmt:message key="links.take_appointment"/></a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>


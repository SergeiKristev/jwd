<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.AppointmentStatus" %>

<h4><fmt:message key="form.visit.id"/>: ${medicalCard.visit.getId()},
    <fmt:message
            key="form.visit.date_title"/>: ${medicalCard.visit.getVisitDate().toLocalDate()} ${medicalCard.visit.getVisitDate().toLocalTime()},
    <fmt:message key="form.visit.patient"/>: ${medicalCard.patient.getFirstName()} ${medicalCard.patient.getLastName()},
    <fmt:message
            key="form.visit.doctor"/>: ${medicalCard.visit.getDoctor().getFirstName()} ${medicalCard.visit.getDoctor().getLastName()}</h4>
<table id="dtUsers" class="table table-striped table-bordered table-hover table-sm"
       cellspacing="0" width="100%">

    <thead>
    <tr>
        <th class="th-sm">#</th>
        <th class="th-sm"><fmt:message key="page.medical_card"/></th>

    </tr>
    </thead>
    <tbody>

    <tr>
        <td><fmt:message key="form.medical_card.id"/></td>
        <td><c:out value="${medicalCard.id}"/>
    </tr>
    <tr>
        <td><fmt:message key="list.visit.patient.first_name"/></td>
        <td><c:out value="${medicalCard.patient.getFirstName()}"/></td>
    </tr>
    <tr>
        <td><fmt:message key="list.visit.patient.last_name"/></td>
        <td><c:out value="${medicalCard.patient.getLastName()}"/></td>
    </tr>
    <tr>
        <td><fmt:message key="form.medical_card.symptoms"/></td>
        <td><c:out value="${medicalCard.symptoms}"/></td>
    </tr>
    <tr>
            <td><fmt:message key="form.medical_card.examination"/></td>
            <td><c:out value="${medicalCard.medicalExamination}"/></td>

    </tr>
    <tr>
        <td><fmt:message key="form.medical_card.clinical_diagnosis"/></td>
        <td><c:out value="${medicalCard.clinicalDiagnosis}"/></td>
    </tr>

    <tr>
        <td><fmt:message key="form.medical_card.icd10_diagnosis"/></td>
        <td><c:out value="${medicalCard.icd10Diagnosis.getCode()}"/>: <c:out
                value="${medicalCard.icd10Diagnosis.getName()}"/></td>
    </tr>

    <tr>
        <td><fmt:message key="form.medical_card.recommendations"/></td>
        <td><c:out value="${medicalCard.recommendations}"/></td>
    </tr>

    </tbody>

</table>

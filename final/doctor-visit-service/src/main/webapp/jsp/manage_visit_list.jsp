<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.AppointmentStatus" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm"><fmt:message key="form.visit.id"/></th>
        <th class="th-sm"><fmt:message key="list.visit.date_time"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.last_name"/></th>
        <th class="th-sm"><fmt:message key="list.visit.doctor.last_name"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.home_address"/></th>
        <th class="th-sm"><fmt:message key="form.visit.reason"/></th>
        <th class="th-sm"><fmt:message key="list.visit.status"/></th>
        <th class="th-sm"><fmt:message key="list.visit.option"/></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty visitList}">
        <c:forEach items="${visitList}" var="visit">
            <tr>
                <td><c:out value="${visit.id}"/></td>
                <td><c:out value="${visit.visitDate.toLocalDate()}"/>
                    <c:out value="${visit.visitDate.toLocalTime()}"/></td>
                <td><c:out value="${visit.patient.getLastName()}"/></td>
                <td><c:out value="${visit.doctor.getLastName()}"/></td>
                <td><c:out value="${visit.patient.getHomeAddress()}"/></td>
                <td><fmt:message key="form.visit.reason.${visit.visitReason}"/></td>
                <td><fmt:message key="form.visit.status.${visit.appointmentStatus}"/></td>
                <td>

                    <!-- Кнопка запуска модального окна -->
                    <button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                            data-toggle="modal" data-target="#myModal${visit.id}">
                        <fmt:message key="button.detail"/>
                    </button>

                    <!-- Модальное окно -->
                    <div class="modal fade" id="myModal${visit.id}" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.detail"/></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table id="dtUsers" class="table table-striped table-bordered table-sm"
                                           cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="th-sm">#</th>
                                            <th class="th-sm"><fmt:message key="page.visit_details"/></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><fmt:message key="list.visit.date_time"/></td>
                                            <td><c:out value="${visit.visitDate.toLocalDate()}"/>
                                                <c:out value="${visit.visitDate.toLocalTime()}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="list.visit.patient.first_name"/></td>
                                            <td><c:out value="${visit.patient.getFirstName()}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="list.visit.patient.last_name"/></td>
                                            <td><c:out value="${visit.patient.getLastName()}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="list.visit.patient.phone"/></td>
                                            <td><c:out value="${visit.patient.getPhone()}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="list.visit.patient.home_address"/></td>
                                            <td><c:out value="${visit.patient.getHomeAddress()}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="list.visit.doctor.first_name"/></td>
                                            <td><c:out value="${visit.doctor.getFirstName()}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="list.visit.doctor.last_name"/></td>
                                            <td><c:out value="${visit.doctor.getLastName()}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="list.visit.doctor.speciality"/></td>

                                            <td><fmt:message
                                                    key="doctor.speciality.${visit.doctor.getSpeciality()}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="form.visit.type"/></td>

                                            <td><fmt:message key="form.visit.type.${visit.visitType}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="form.visit.reason"/></td>

                                            <td><fmt:message key="form.visit.reason.${visit.visitReason}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="form.visit.notes"/></td>

                                            <td><c:out value="${visit.notes}"/></td>
                                        </tr>
                                        <tr>
                                            <td><fmt:message key="list.visit.status"/></td>

                                            <td><fmt:message key="form.visit.status.${visit.appointmentStatus}"/></td>
                                        </tr>
                                        <c:if test="${visit.appointmentStatus ne AppointmentStatus.CANCELED}">
                                            <tr>
                                                <td><fmt:message key="list.visit.cost"/></td>
                                                <td><c:out value="${visit.tariff.getCost()}"/> <fmt:message key="currency.byn"/></td>
                                            </tr>
                                        </c:if>


                                        </tbody>

                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message
                                            key="button.close"/></button>
                                </div>
                            </div>
                        </div>
                    </div>


                        <c:if test="${visit.appointmentStatus eq AppointmentStatus.ACCEPTED_BY_ADMINISTRATOR}">
                            <security:check commandName="${CommandName.FILL_MEDICAL_CARD}">
                                <form>
                                    <input type="hidden" name="_command"
                                           value="${CommandName.DISPLAY_MEDICAL_CARD_FORM}">
                                    <input type="hidden" name="visit_id" value="${visit.id}">
                                    <input type="submit"
                                           class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                           value="<fmt:message key="button.fill_medical_card"/>">
                                </form>
                            </security:check>
                        </c:if>


                    <c:choose>
                        <c:when test="${visit.appointmentStatus eq AppointmentStatus.ACCEPTED_BY_ADMINISTRATOR}">
                            <security:check commandName="${CommandName.EDIT_VISIT_FORM}">
                                <c:if test="${visit.appointmentStatus ne AppointmentStatus.CANCELED}">
                                    <form>
                                        <input type="hidden" name="_command"
                                               value="${CommandName.DISPLAY_EDIT_VISIT_FORM}">
                                        <input type="hidden" name="visit_id" value="${visit.id}">
                                        <input type="submit"
                                               class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                               value="<fmt:message key="button.edit"/>">
                                    </form>
                                </c:if>
                            </security:check> </c:when>
                        <c:when test="${visit.appointmentStatus eq AppointmentStatus.WAITING_FOR_CONFIRMATION}">
                            <security:check commandName="${CommandName.EDIT_VISIT_FORM}">
                                <c:if test="${visit.appointmentStatus ne AppointmentStatus.CANCELED}">
                                    <form>
                                        <input type="hidden" name="_command"
                                               value="${CommandName.DISPLAY_EDIT_VISIT_FORM}">
                                        <input type="hidden" name="visit_id" value="${visit.id}">
                                        <input type="submit"
                                               class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                               value="<fmt:message key="button.edit"/>">
                                    </form>
                                </c:if>
                            </security:check> </c:when>
                        <c:otherwise>
                        </c:otherwise>
                    </c:choose>


                    <c:if test="${visit.appointmentStatus eq AppointmentStatus.COMPLETED}">
                        <form>
                            <input type="hidden" name="_command"
                                   value="${CommandName.DISPLAY_MEDICAL_CARD_DETAIL}">
                            <input type="hidden" name="visit_id" value="${visit.id}">
                            <input type="submit"
                                   class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                   value="<fmt:message key="button.medical_card"/>">
                        </form>

                    </c:if>

                </td>

            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th><fmt:message key="form.visit.id"/></th>
    <th><fmt:message key="list.visit.date_time"/></th>
    <th><fmt:message key="list.visit.patient.last_name"/></th>
    <th><fmt:message key="list.visit.doctor.last_name"/></th>
    <th><fmt:message key="list.visit.patient.home_address"/></th>
    <th><fmt:message key="form.visit.reason"/></th>
    <th><fmt:message key="list.visit.status"/></th>
    <th><fmt:message key="list.visit.option"/></th>
    </tfoot>
</table>

<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable({
            "order": [[2, "desc"]]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>





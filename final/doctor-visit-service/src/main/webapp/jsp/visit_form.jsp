<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.VisitReason" %>


<c:if test="${not empty doctorsList}">

    <div class="col-lg-12 well">
        <h3><fmt:message key="form.visit.choose_time"/></h3>
        <h4><fmt:message key="form.visit.date_title"/>: <c:out value="${visitDate}"/> <fmt:message
                key="form.visit.speciality_title"/>: <fmt:message key="doctor.speciality.${doctorsSpeciality}"/></h4>
        <h4><fmt:message key="form.visit.free_doctors"/>:</h4>

        <form action="${pageContext.request.contextPath}/" method="post">
            <input type="hidden" name="_command" value="${CommandName.CREATE_VISIT_COMMAND}">
            <input type="hidden" name="visit_date" value="${visitDate}">
            <div class="row">
                <c:forEach items="${doctorsList}" var="entry">
                    <div class="col-sm-2 align-content-lg-left">
                        <h5>${entry.key.firstName} ${entry.key.lastName}, <fmt:message
                                key="form.visit.doctor_category"/>: <fmt:message
                                key="doctor.category.${entry.key.medicalCategory}"/>,
                            <fmt:message key="form.visit.doctor_experience"/>: ${entry.key.experience} <fmt:message
                                    key="form.visit.year"/></h5>
                    </div>
                    <div class="col-sm-10 align-content-lg-left">
                        <c:forEach items="${entry.value}" var="interval">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary">
                                    <input type="radio" class="appointments" name="appointment_id"
                                           data-id="${entry.key.doctorId}" value="${interval.id}"
                                           autocomplete="off" required>
                                    <c:out value="${interval.name}"/>
                                </label>
                            </div>
                        </c:forEach>
                    </div>
                </c:forEach>
                <input type="hidden" class="doctorId" name="doctor_id" value="">
                <div class="col-sm-12">

                    <div class="form-group">
                        <label><fmt:message key="form.visit.notes"/></label>
                        <textarea name="notes" placeholder="<fmt:message key="form.visit.notes"/>.."
                                  rows="2" class="form-control" maxlength="199"></textarea>
                    </div>
                    <div class="form-group">
                        <label><fmt:message key="form.visit.reason"/></label>
                        <select type="text" name="visit_reason"
                                placeholder="<fmt:message key="form.visit.reason"/>.."
                                class="form-control">
                            <c:forEach items="${VisitReason.values()}" var="reason">
                                <option value="${reason.name()}"><fmt:message
                                        key="form.visit.reason.${reason.name()}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-lg btn-primary"><fmt:message
                    key="button.submit"/></button>
        </form>
    </div>
</c:if>
<c:if test="${empty doctorsList}">
    <fmt:message key="form.visit.no_appointment_available"/>
</c:if>

<script>
    $(".appointments").on("change", function () {
        var currentId = $(this).data("id");
        $(".doctorId").val(currentId);
    });
</script>

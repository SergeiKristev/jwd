<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 6/22/20
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<div class="footer-copyright">
    <div class="container">
        <p><fmt:message key="page.footer"/></p>
    </div>
</div>


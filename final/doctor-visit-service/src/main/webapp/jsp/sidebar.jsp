<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 7/1/20
  Time: 00:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>
<c:if test="${securityContext.loggedIn}">
    <div class="sidebar">
        <ul class="nav-menu">
            <li><a href="?_command=${CommandName.VIEW_PERSONAL_ACCOUNT}"><fmt:message
                    key="links.user.personal_account"/></a></li>

            <li><a href="?_command=${CommandName.DISPLAY_EDIT_USER_FORM}"><fmt:message
                    key="links.user.edit_user_form"/></a></li>

            <security:check commandName="${CommandName.TAKE_APPOINTMENT}">
                <li><a href="?_command=${CommandName.TAKE_APPOINTMENT}"><fmt:message key="links.take_appointment"/></a>
                </li>
            </security:check>

            <security:check commandName="${CommandName.MANAGE_OUTSTANDING_VISITS}">
                <li><a href="?_command=${CommandName.MANAGE_OUTSTANDING_VISITS}"><fmt:message
                        key="links.manage.outstanding_visit_list"/></a></li>
            </security:check>

            <security:check commandName="${CommandName.MANAGE_DOCTORS_OUTSTANDING_VISITS}">
                <li><a href="?_command=${CommandName.MANAGE_DOCTORS_OUTSTANDING_VISITS}"><fmt:message
                        key="links.manage.outstanding_visit_list"/></a></li>
            </security:check>

            <security:check commandName="${CommandName.MANAGE_VISIT_LIST}">
                <li><a href="?_command=${CommandName.MANAGE_VISIT_LIST}"><fmt:message
                        key="links.manage.visit_list"/></a></li>
            </security:check>

            <security:check commandName="${CommandName.VIEW_ALL_VISITS}">
                <li><a href="?_command=${CommandName.VIEW_ALL_VISITS}"><fmt:message key="links.manage.visit_list"/></a>
                </li>
            </security:check>

            <security:check commandName="${CommandName.MANAGE_DOCTORS_LIST}">
                <li><a href="?_command=${CommandName.MANAGE_DOCTORS_LIST}"><fmt:message
                        key="links.manage_doctors_list"/></a></li>
            </security:check>

            <security:check commandName="${CommandName.VIEW_PATIENTS_VISIT_LIST}">
                <li><a href="?_command=${CommandName.VIEW_PATIENTS_VISIT_LIST}"><fmt:message
                        key="links.patients_visit_list"/></a></li>
            </security:check>

            <security:check commandName="${CommandName.MANAGE_PATIENTS_LIST}">
                <li><a href="?_command=${CommandName.MANAGE_PATIENTS_LIST}"><fmt:message key="links.manage_patients_list"/></a></li>
            </security:check>
        </ul>
    </div>
</c:if>


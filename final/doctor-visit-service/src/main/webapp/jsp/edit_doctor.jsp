<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.Gender" %>
<%@ page import="by.kristev.doctor.model.Speciality" %>
<%@ page import="by.kristev.doctor.model.MedicalCategory" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>


<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<div class="container">
    <h1 class="well"><fmt:message key="form.edit_doctor"/></h1>
    <div class="col-lg-12 well">
        <div class="row">
            <form action="${pageContext.request.contextPath}/" method="post">
                <input type="hidden" name="_command" value="${CommandName.EDIT_DOCTOR}">
                <input type="hidden" name="doctor.id" value="${doctor.doctorId}">

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.registration.first_name"/></label>
                            <input type="text" name="user.first_name" value="<c:out value="${doctor.firstName}"/>"
                                   placeholder="<fmt:message key="form.registration.first_name"/>.."
                                   class="form-control" maxlength="30" required>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.registration.last_name"/></label>
                            <input type="text" name="user.last_name" value="<c:out value="${doctor.lastName}"/>"
                                   placeholder="<fmt:message key="form.registration.last_name"/>.." class="form-control"
                                   maxlength="40" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.registration.date_of_birth"/></label>
                            <input class="form-control" type="date" name="user.date_of_birth"
                                   value="<c:out value="${doctor.dateOfBirth}"/>"
                                   placeholder="<fmt:message key="form.registration.date_of_birth"/>.."
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                        </div>

                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.registration.gender"/></label>
                            <select type="text" name="user.gender"
                                    placeholder="<fmt:message key="form.registration.gender"/>.." class="form-control"
                                    required>
                                <option selected="selected" value="<c:out value="${doctor.gender.name()}"/>">
                                    <fmt:message
                                            key="form.gender.${doctor.gender.name()}"/></option>
                                <c:forEach items="${Gender.values()}" var="gender">
                                    <option value="<c:out value="${gender.name()}"/>"><fmt:message
                                            key="form.gender.${gender.name()}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label><fmt:message key="form.registration.speciality"/></label>
                        <select type="text" name="doctor.speciality"
                                placeholder="<fmt:message key="doctor.speciality"/>.." class="form-control" required>
                            <option selected="selected" value="<c:out value="${doctor.speciality.name()}"/>">
                                <fmt:message
                                        key="doctor.speciality.${doctor.speciality.name()}"/></option>
                            <c:forEach items="${Speciality.values()}" var="speciality">
                                <option value="<c:out value="${speciality.name()}"/>"><fmt:message
                                        key="doctor.speciality.${speciality.name()}"/></option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label><fmt:message key="doctor.medical_category"/></label>
                            <select type="text" name="doctor.medical_category"
                                    placeholder="<fmt:message key="doctor.medical_category"/>.." class="form-control">
                                <option selected="selected" value="<c:out value="${doctor.medicalCategory.name()}"/>">
                                    <fmt:message
                                            key="doctor.category.${doctor.medicalCategory.name()}"/></option>
                                <c:forEach items="${MedicalCategory.values()}" var="category">
                                    <option value="<c:out value="${category.name()}"/>"><fmt:message
                                            key="doctor.category.${category.name()}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-12 form-group">
                            <label><fmt:message key="doctor.experience"/></label>
                            <input type="number" min="1" max="70" name="doctor.experience"
                                   value="<c:out value="${doctor.experience}"/>"
                                   placeholder="<fmt:message key="doctor.experience"/>.." class="form-control"
                                   maxlength="45" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label><fmt:message key="form.registration.phone"/></label>
                        <input id="phone" type="text" name="user.phone" value="<c:out value="${doctor.phone}"/>"
                               placeholder="8(XXX) XXX-XX-XX" class="form-control" maxlength="45" required>
                    </div>
                    <div class="form-group">
                        <label><fmt:message key="form.registration.email"/></label>
                        <c:out value="${doctor.email}"/>"
                    </div>

                    <div class="form-group">
                        <!-- Кнопка запуска модального окна -->
                        <button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                data-toggle="modal" data-target="#myModal${doctor.userId}">
                            <fmt:message key="button.change_password"/>
                        </button>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-primary btn-outline btn-1d btn-sm btn-block"><fmt:message key="button.submit"/></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <security:check commandName="${CommandName.DISABLE_USER}">
        <div class="col-lg-12 well">
            <div class="col-sm-12">
                <div class="row">
                    <form>
                        <div class="form-group">
                            <input type="hidden" name="_command" value="${CommandName.DISABLE_USER}">
                            <input type="hidden" name="user.id" value="${doctor.userId}">
                            <button type="submit" class="btn btn-lg btn-danger btn-outline btn-1d btn-sm btn-block confirm"><fmt:message
                                    key="button.disable"/></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </security:check>
</div>

<!-- Модальное окно -->
<div class="modal fade" id="myModal${doctor.userId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="form.change_password"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="${pageContext.request.contextPath}/" method="post">

                    <h2 class="form-signin-heading"><fmt:message key="form.change_password"/></h2>
                    <input type="hidden" name="_command" value="${CommandName.CHANGE_PASSWORD}">
                    <input type="hidden" name="user.id" value="${doctor.userId}">

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.old_password"/></label>
                        <input  class="form-control" type="Password" name="user.old_password"
                               placeholder="<fmt:message key="form.registration.old_password"/>.." class="form-control"
                               maxlength="16" required>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.new_password"/></label>
                        <input class="form-control" type="Password" name="user.new_password"
                               placeholder="<fmt:message key="form.registration.new_password"/>.." class="form-control"
                               maxlength="32" required>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.confirm_new_password"/></label>
                        <input class="form-control" type="Password" name="user.confirm_password"
                               placeholder="<fmt:message key="form.registration.confirm_new_password"/>.."
                               class="form-control" maxlength="32" required>
                    </div>

                    <button class="btn btn-lg btn-primary btn-outline btn-1d btn-sm btn-block" type="submit"><fmt:message
                            key="button.change_password"/></button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message
                        key="button.close"/></button>
            </div>
        </div>
    </div>
</div>

<!-- Скрипт, привязывающий событие click, открывающее модальное окно, к элементам, имеющим класс .btn -->
<script>
    $(document).ready(function () {
        //при нажатию на любую кнопку, имеющую класс .btn
        $(".modal-link").click(function () {
            //открыть модальное окно с id="myModal"
            $("#myModal${doctor.userId}").modal('show');
        });
    });
</script>

<script>
    $("#phone").mask("8(999)999-99-99");
</script>

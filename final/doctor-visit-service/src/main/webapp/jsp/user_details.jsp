<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>


<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<!-- Кнопка запуска модального окна -->
<button type="button" class="fcbtn btn btn-info btn-outline btn-1d btn-sm btn-block my-1" data-toggle="modal" data-target="#myModal">
    <fmt:message key="button.detail"/>
</button>

<!-- Модальное окно -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.detail"/></h4>
            </div>
            <div class="modal-body">
                <table id="dtUsers" class="table table-striped table-bordered table-sm"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">#</th>
                        <th class="th-sm"><fmt:message key="page.visit_details"/></th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><fmt:message key="user."/></td>
                        <td><c:out value="${visit.visitDate.toLocalDate()}"/>
                            <c:out value="${visit.visitDate.toLocalTime()}"/></td>
                    </tr>


                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


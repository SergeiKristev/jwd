<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>


<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm"><fmt:message key="form.visit.id"/></th>
        <th class="th-sm"><fmt:message key="list.visit.date_time"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.first_name"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.last_name"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.home_address"/></th>
        <th class="th-sm"><fmt:message key="list.visit.patient.phone"/></th>
        <th class="th-sm"><fmt:message key="form.visit.reason"/></th>
        <th class="th-sm"><fmt:message key="list.visit.status"/></th>
        <th class="th-sm"><fmt:message key="list.visit.option"/></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty visitList}">
        <c:forEach items="${visitList}" var="visit">
            <tr>
                <td><c:out value="${visit.id}"/></td>
                <td><c:out value="${visit.visitDate}"/></td>
                <td><c:out value="${visit.patient.getFirstName()}"/></td>
                <td><c:out value="${visit.patient.getLastName()}"/></td>
                <td><c:out value="${visit.patient.getHomeAddress()}"/></td>
                <td><c:out value="${visit.patient.getPhone()}"/></td>
                <td><fmt:message key="form.visit.reason.${visit.visitReason}"/></td>
                <td><fmt:message key="form.visit.status.${visit.appointmentStatus}"/></td>
                <td>
                    <form>
                        <input type="hidden" name="_command" value="${CommandName.VIEW_VISIT_DETAILS}">
                        <input type="hidden" name="visit_id" value="${visit.id}">
                        <input type="submit" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm"
                               value="<fmt:message key="button.detail"/>">
                    </form>
                </td>

            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th><fmt:message key="form.visit.id"/></th>
    <th><fmt:message key="list.visit.date_time"/></th>
    <th><fmt:message key="list.visit.patient.first_name"/></th>
    <th><fmt:message key="list.visit.patient.last_name"/></th>
    <th><fmt:message key="list.visit.patient.home_address"/></th>
    <th><fmt:message key="list.visit.patient.phone"/></th>
    <th><fmt:message key="form.visit.reason"/></th>
    <th><fmt:message key="list.visit.status"/></th>
    <th><fmt:message key="list.visit.option"/></th>
    </tfoot>
</table>

<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable({
            "order": [[ 2, "desc" ]]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>



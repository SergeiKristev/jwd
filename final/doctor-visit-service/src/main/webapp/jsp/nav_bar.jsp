<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>
<ul class="nav-menu">
    <li><a href="?_command=${CommandName.INDEX}"><fmt:message key="links.home"/></a></li>
    <li><a href="${pageContext.request.contextPath}/doctors.html"><fmt:message key="links.doctors"/></a></li>
    <li><a href="${pageContext.request.contextPath}/contact.html"><fmt:message key="links.contacts"/></a></li>
    <c:choose>
        <c:when test="${securityContext.loggedIn}">
            <security:check commandName="${CommandName.VIEW_PERSONAL_ACCOUNT}">
                <li><a href="?_command=${CommandName.VIEW_PERSONAL_ACCOUNT}"><fmt:message
                        key="links.user.personal_account"/></a></li>
            </security:check>
            <li><a href="?_command=${CommandName.LOG_OUT}"><fmt:message key="links.logout"/></a></li>
        </c:when>
        <c:otherwise>
            <li><a href="#" class="modal-link"><fmt:message key="links.login"/></a></li>
        </c:otherwise>
    </c:choose>
</ul>

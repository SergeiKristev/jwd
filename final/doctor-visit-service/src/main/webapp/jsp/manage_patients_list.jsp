<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.AppointmentStatus" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>

<jsp:useBean id="securityContext" scope="application"
             class="by.kristev.doctor.SecurityContext"/>

<!-- Кнопка запуска модального окна -->
<button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1" data-toggle="modal"
        data-target="#registerPatient">
    <fmt:message key="button.patient_registration"/>
</button>

<!-- Модальное окно -->
<div class="modal fade" id="registerPatient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.patient_registration"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <jsp:include page="registration_patient.jsp"/>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>
            </div>
        </div>
    </div>
</div>

<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm"><fmt:message key="patient.id"/></th>
        <th class="th-sm"><fmt:message key="user.firstName"/></th>
        <th class="th-sm"><fmt:message key="user.last_name"/></th>
        <th class="th-sm"><fmt:message key="user.phone"/></th>
        <th class="th-sm"><fmt:message key="patient.home_address"/></th>
        <th class="th-sm"><fmt:message key="list.visit.option"/></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty patientList}">
        <c:forEach items="${patientList}" var="patient">
            <tr>
                <td><c:out value="${patient.patientId}"/></td>
                <td><c:out value="${patient.firstName}"/></td>
                <td><c:out value="${patient.lastName}"/></td>
                <td><c:out value="${patient.phone}"/></td>
                <td><c:out value="${patient.homeAddress}"/></td>

                <td>

                    <!-- Кнопка запуска модального окна -->
                    <button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                            data-toggle="modal" data-target="#myModal${patient.patientId}">
                        <fmt:message key="button.detail"/>
                    </button>

                    <!-- Модальное окно -->
                    <div class="modal fade" id="myModal${patient.patientId}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.detail"/></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table id="dtUsers" class="table table-striped table-bordered table-sm"
                                           cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="th-sm">#</th>
                                            <th class="th-sm"><fmt:message key="page.user.details"/><th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><fmt:message key="patient.id"/></td>
                                            <td><c:out value="${patient.patientId}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.firstName"/></td>
                                            <td><c:out value="${patient.firstName}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.last_name"/></td>
                                            <td><c:out value="${patient.lastName}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.email"/></td>
                                            <td><c:out value="${patient.email}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="patient.home_address"/></td>
                                            <td><c:out value="${patient.homeAddress}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="user.phone"/></td>
                                            <td><c:out value="${patient.phone}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="patient.polyclinic"/></td>
                                            <td><c:out value="${patient.polyclinic}"/></td>
                                        </tr>

                                        <tr>
                                            <td><fmt:message key="patient.insurance"/></td>
                                            <td><c:out value="${patient.insurance}"/></td>
                                        </tr>

                                        </tbody>

                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>

                                </div>
                            </div>
                        </div>
                    </div>

                    <security:check commandName="${CommandName.EDIT_PATIENT}">
                        <form>
                            <input type="hidden" name="_command" value="${CommandName.DISPLAY_EDIT_PATIENT_FORM}">
                            <input type="hidden" name="patient.id" value="${patient.patientId}">
                            <input type="submit" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
                                   value="<fmt:message key="button.edit"/>">
                        </form>

                    </security:check>

                </td>

            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th><fmt:message key="patient.id"/></th>
    <th><fmt:message key="user.firstName"/></th>
    <th><fmt:message key="user.last_name"/></th>
    <th><fmt:message key="user.phone"/></th>
    <th><fmt:message key="patient.home_address"/></th>
    <th><fmt:message key="list.visit.option"/></th>
    </tfoot>
</table>

<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable({
            "order": [[1, "asc"]]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

<script>
    $("#phone").mask("8(999)999-99-99");
</script>




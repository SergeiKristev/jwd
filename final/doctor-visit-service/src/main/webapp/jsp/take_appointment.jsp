<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.Speciality" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>


<security:check commandName="${CommandName.VIEW_PATIENTS_VISIT_LIST}">
    <form action="${pageContext.request.contextPath}/" method="post">
        <input type="hidden" name="_command" value="${CommandName.VISIT_DISPLAY_COMMAND}">
        <div class="row">
            <div class="col-sm-6 form-group">
                <label><fmt:message key="form.registration.date_of_visit"/></label>
                <input class="form-control" type="date" name="visit_date"
                       placeholder="<fmt:message key="form.registration.date_of_visit"/>.."
                       onfocus="this.placeholder = ''"
                       onblur="this.placeholder = 'Date'" required>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 form-group">
                <label><fmt:message key="form.visit.doctor_speciality"/></label>
                <select type="text" name="doctor.speciality"
                        placeholder="<fmt:message key="form.visit.doctor_speciality"/>.."
                        class="form-control">
                    <c:forEach items="${Speciality.values()}" var="speciality">
                        <option value="${speciality.name()}"><fmt:message
                                key="doctor.speciality.${speciality.name()}"/></option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary"><fmt:message key="button.submit"/></button>
        </div>
    </form>
</security:check>

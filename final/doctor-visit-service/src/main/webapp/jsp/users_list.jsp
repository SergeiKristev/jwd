<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>



<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm">Id</th>
        <th class="th-sm">Type</th>
        <th class="th-sm">First Name</th>
        <th class="th-sm">Last Name</th>
        <th class="th-sm">Phone</th>
        <th class="th-sm">Options</th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty usersList}">
        <c:forEach items="${usersList}" var="user">
            <tr>
                <td><c:out value="${user.userId}"/></td>
                <td><c:out value="${user.userRole}"/></td>
                <td><c:out value="${user.firstName}"/></td>
                <td><c:out value="${user.lastName}"/></td>
                <td><c:out value="${user.phone}"/></td>
                <td>
                    <form>
                        <input type="hidden" name="_command" value="${CommandName.VIEW_USER_DETAILS}">
                        <input type="hidden" name="userId" value="${user.userId}">
                        <input type="submit" class="fcbtn btn btn-info btn-outline btn-1d btn-sm" value="<fmt:message key="form.users.detail"/>">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th>Id</th>
    <th>Type</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Phone</th>
    <th>Options</th>
    </tfoot>
</table>

<li><a href="?_command=${CommandName.VISIT_DISPLAY_COMMAND}">Create visit</a></li>
<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable();
        $('.dataTables_length').addClass('bs-select');
    });
</script>


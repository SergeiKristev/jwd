<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.VisitType" %>
<%@ page import="by.kristev.doctor.model.VisitReason" %>
<%@ page import="by.kristev.doctor.model.AppointmentStatus" %>

<div class="col-lg-8 well">
    <h4><fmt:message key="form.visit.id"/>: ${visit.id},
        <fmt:message key="form.visit.date_title"/>: ${visit.visitDate.toLocalDate()} ${visit.visitDate.toLocalTime()},
        <fmt:message key="form.visit.patient"/>: ${visit.patient.getFirstName()} ${visit.patient.getLastName()},
        <fmt:message key="form.visit.address"/>: ${visit.patient.getHomeAddress()},
        <fmt:message key="form.visit.phone"/>: ${visit.patient.getPhone()},
        <fmt:message key="form.visit.doctor"/>: ${visit.doctor.getFirstName()} ${visit.doctor.getLastName()}</h4>
    <form action="${pageContext.request.contextPath}/" method="post">
        <input type="hidden" name="_command" value="${CommandName.EDIT_VISIT_FORM}">
        <input type="hidden" name="visit_id" value="${visit.id}">
        <div class="form-group">
            <label><fmt:message key="form.visit.notes"/></label>
            <textarea name="notes" placeholder="<fmt:message key="form.visit.notes"/>.."
                      rows="2" class="form-control" maxlength="200"><c:out value="${visit.notes}"/></textarea>
        </div>
        <div class="form-group">
            <label><fmt:message key="form.visit.reason"/></label>
            <select type="text" name="visit_reason"
                    placeholder="<fmt:message key="form.visit.reason"/>.."
                    class="form-control" required>
                <c:forEach items="${VisitReason.values()}" var="reason">
                    <option value="${reason.name()}"><fmt:message
                            key="form.visit.reason.${reason.name()}"/></option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <label><fmt:message key="form.visit.type"/></label>
            <select type="text" name="visit_type"
                    placeholder="<fmt:message key="form.visit.type"/>.."
                    class="form-control" required>
                <c:forEach items="${VisitType.values()}" var="type">
                    <option value="${type.name()}"><fmt:message
                            key="form.visit.type.${type.name()}"/></option>
                </c:forEach>
            </select>
        </div>

        <div class="form-group">
            <label><fmt:message key="form.visit.status"/></label>
            <select type="text" name="appointment_status"
                    placeholder="<fmt:message key="form.visit.status"/>.."
                    class="form-control" required>
                <option value="${AppointmentStatus.WAITING_FOR_CONFIRMATION}"><fmt:message
                        key="form.visit.status.WAITING_FOR_CONFIRMATION"/></option>
                <option value="${AppointmentStatus.ACCEPTED_BY_ADMINISTRATOR}"><fmt:message
                        key="form.visit.status.ACCEPTED_BY_ADMINISTRATOR"/></option>
                <option value="${AppointmentStatus.CANCELED}"><fmt:message
                        key="form.visit.status.CANCELED"/></option>
            </select>
        </div>

        <button type="submit" class="btn btn-lg btn-primary"><fmt:message
                key="button.submit"/></button>
    </form>
</div>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.Gender" %>
<%@ page import="by.kristev.doctor.model.Speciality" %>
<%@ page import="by.kristev.doctor.model.MedicalCategory" %>


<div class="container">
    <h1 class="well"><fmt:message key="form.registration.name"/></h1>
    <div class="col-lg-12 well">
        <div class="row">
            <form action="${pageContext.request.contextPath}/" method="post">
                <input type="hidden" name="_command" value="${CommandName.REGISTER_NEW_DOCTOR}">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.first_name"/></label>
                            <input type="text"  class="form-control"  name="user.first_name" value="<c:out value="${user.firstName}"/>" placeholder="<fmt:message key="form.registration.first_name"/>.." class="form-control"  maxlength="30" required>
                        </div>
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.last_name"/></label>
                            <input type="text" class="form-control"  name="user.last_name" value="<c:out value="${user.lastName}"/>" placeholder="<fmt:message key="form.registration.last_name"/>.." class="form-control" maxlength="40" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.date_of_birth"/></label>
                            <input class="form-control" type="date" name="user.date_of_birth" value="<c:out value="${user.dateOfBirth}"/>" placeholder="<fmt:message key="form.registration.date_of_birth"/>.." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                        </div>

                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.gender"/></label>
                            <select type="text" name="user.gender" placeholder="<fmt:message key="form.registration.gender"/>.." class="form-control" required>
                                <c:if test="${user.gender.name()} not empty">
                                    <option selected="selected" value="<c:out value="${user.gender.name()}"/>"><fmt:message
                                            key="form.registration.gender.${user.gender.name()}"/></option>
                                </c:if>
                                <c:forEach items="${Gender.values()}" var="gender">
                                    <option value="<c:out value="${gender.name()}"/>"><fmt:message
                                            key="form.gender.${gender.name()}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.speciality"/></label>
                        <select type="text" name="doctor.speciality" placeholder="<fmt:message key="doctor.speciality"/>.." class="form-control" required>
                            <c:if test="${doctor.speciality.name()} not empty">
                                <option selected="selected" value="<c:out value="${doctor.speciality.name()}"/>"><fmt:message
                                        key="doctor.speciality.${doctor.speciality.name()}"/></option>
                            </c:if>
                            <c:forEach items="${Speciality.values()}" var="speciality">
                                <option value="<c:out value="${speciality.name()}"/>"><fmt:message
                                        key="doctor.speciality.${speciality.name()}"/></option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group required">
                            <label class="control-label"><fmt:message key="doctor.medical_category"/></label>
                            <select type="text" name="doctor.medical_category" placeholder="<fmt:message key="doctor.medical_category"/>.." class="form-control" required>
                                <c:if test="${doctor.medicalCategory.name()} not empty">
                                    <option selected="selected" value="<c:out value="${doctor.medicalCategory.name()}"/>"><fmt:message
                                            key="doctor.category.${doctor.medicalCategory.name()}"/></option>
                                </c:if>
                                <c:forEach items="${MedicalCategory.values()}" var="category">
                                    <option value="<c:out value="${category.name()}"/>"><fmt:message
                                            key="doctor.category.${category.name()}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-12 form-group required">
                            <label class="control-label"><fmt:message key="doctor.experience"/></label>
                            <input class="form-control" type="number" min="1" max="70"  name="doctor.experience" value="<c:out value="${doctor.experience}"/>" placeholder="<fmt:message key="doctor.experience"/>.." class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.phone"/></label>
                        <input class="form-control"  id="phone" type="text" name="user.phone" value="<c:out value="${user.phone}"/>" placeholder="8(XXX) XXX-XX-XX" class="form-control" maxlength="45" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.email"/></label>
                        <input class="form-control"  type="text" name="user.email" value="<c:out value="${user.email}"/>" placeholder="<fmt:message key="form.registration.email"/>.." class="form-control" maxlength="45" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.password"/></label>
                        <input  class="form-control"  type="Password" name="user.password" placeholder="<fmt:message key="form.registration.password"/>.." class="form-control" maxlength="32" required>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.confirm_password"/></label>
                        <input class="form-control"  type="Password" name="user.confirm_password" placeholder="<fmt:message key="form.registration.confirm_password"/>.." class="form-control" maxlength="32" required>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary"><fmt:message key="form.registration.submit"/></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $("#phone").mask("8(999)999-99-99");
</script>
<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 6/12/2020
  Time: 12:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <jsp:include page="meta.jsp"/>
</head>
<body>
<!-- Preloader Starts -->
<div class="preloader">
    <div class="spinner"></div>
</div>
<!-- Preloader End -->
<!-- Header Area Starts -->
<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
    </c:otherwise>
</c:choose>

<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>

<header class="header-area">
    <div>
        <lang:lang></lang:lang>
    </div>
    <jsp:include page="header.jsp"/>
</header>
<!-- Header Area End -->

<!-- Banner Area Starts -->

<section class="banner-area other-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <fmt:message key="${pageName}"/>
            </div>
        </div>
    </div>
</section>
<!-- Banner Area End -->

<!-- Welcome Area Starts -->
<!-- Модальное окно -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="links.login"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <jsp:include page="log_in_form.jsp"/>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message key="button.close"/></button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3 p-5 align-content-lg-left">
        <jsp:include page="sidebar.jsp"/>

    </div>
    <div class="col-sm-9 p-5 align-content-lg-center">
        <c:choose>
            <c:when test="${not empty validationResult}">
                <c:forEach items="${validationResult}" var="message">
                    <div id="message" class="alert alert-danger alert-dismissible fade show" role="alert">
                        <div id="allert"><fmt:message key="${message}"/></div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div id="message" class="alert alert-success alert-dismissible fade show" role="alert">
                    <div id="allert"><fmt:message key="message.${pageContext.request.getParameter(\"message\")}"/></div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:otherwise>
        </c:choose>
        <jsp:include page="${viewName}.jsp"/>
    </div>
</div>
<!-- Welcome Area End -->

<!-- Footer Area Starts -->
<footer class="footer-area section-padding">
    <jsp:include page="footer.jsp"/>
</footer>

<!-- Footer Area End -->
<!-- Скрипт, привязывающий событие click, открывающее модальное окно, к элементам, имеющим класс .btn -->
<script>
    $(document).ready(function(){
        //при нажатию на любую кнопку, имеющую класс .btn
        $(".modal-link").click(function() {
            //открыть модальное окно с id="myModal"
            $("#myModal").modal('show');
        });
    });
</script>

<script>
    function extractGet(name) {
        var result = window.location.search.match(new RegExp(name + '=([^&=]+)'));
        return result ? decodeURIComponent(result[1].replace(/\+/g, " ")) : false;
    }
</script>

<script>
    if (!extractGet("message") && !'${validationResult}') {
        $(".alert").alert('close')
    }
</script>

<script>
    $(function() {
        $('.confirm').click(function() {
            return window.confirm('<fmt:message key="message.are_you_sure"/>');
        });
    });
</script>

<style>
    .form-group.required .control-label:before{
        color: red;
        content: "*";
        position: absolute;
        margin-left: -15px;
    }
</style>

</body>
</html>


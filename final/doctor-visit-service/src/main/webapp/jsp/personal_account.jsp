<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 7/1/20
  Time: 12:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.Speciality" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>


<div>
    <h1><fmt:message key="page.personal_account.hello"/> ${user_first_name}</h1>
</div>


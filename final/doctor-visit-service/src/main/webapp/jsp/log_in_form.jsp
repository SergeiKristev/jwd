<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>


<form class="modal-body mx-3" action="${pageContext.request.contextPath}/" method="post">
    <h2 class="form-signin-heading"><fmt:message key="form.login.title"/></h2>
    <input type="hidden" name="_command" value="${CommandName.LOG_IN_SAVE}">
    <input type="text" class="form-control mb-3" name="user.login_name"
           placeholder="<fmt:message key="form.registration.email"/>" maxlength="45" required="" autofocus=""/>
    <input type="password" class="form-control mb-3" name="user.password"
           placeholder="<fmt:message key="form.registration.password"/>" maxlength="45" required=""/>

    <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="button.login"/></button>
    <label class="mt-3"><a href="?_command=${CommandName.REGISTER_NEW_PATIENT_DISPLAY}"><fmt:message
            key="links.patient.register"/></a></label>
</form>


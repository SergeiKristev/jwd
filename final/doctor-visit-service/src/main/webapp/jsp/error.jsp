<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 6/12/2020
  Time: 12:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <jsp:include page="meta.jsp"/>
</head>
<body>
<!-- Preloader Starts -->
<div class="preloader">
    <div class="spinner"></div>
</div>
<!-- Preloader End -->
<!-- Header Area Starts -->
<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
    </c:otherwise>
</c:choose>

<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>

<header class="header-area">
    <div>
        <lang:lang></lang:lang>
    </div>
    <jsp:include page="header.jsp"/>
</header>
<!-- Header Area End -->

<!-- Banner Area Starts -->

<section class="banner-area other-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <fmt:message key="page.error"/>
        </div>
    </div>
</section>
<!-- Banner Area End -->

<!-- Welcome Area Starts -->

<section>
    <div class="col-sm-12 p-5 align-content-lg-center">
        <h1>Opps...</h1>
        <table class="alert-danger" width="100%" border="1">
            <tr valign="top">
                <td width="40%"><b class="alert-danger">Error:</b></td>
                <td>${pageContext.exception}</td>
            </tr>
            <tr valign="top">
                <td><b class="alert-danger">URI:</b></td>
                <td>${pageContext.errorData.requestURI}</td>
            </tr>
            <tr valign="top">
                <td><b class="alert-danger">Status code:</b></td>
                <td>${pageContext.errorData.statusCode}</td>
            </tr>

            <tr valign="top">
                <td><b class="alert-danger">Cause:</b></td>
                <td>${pageContext.exception.cause}</td>
            </tr>

        </table>
    </div>
</section>
<!-- Welcome Area End -->

<!-- Footer Area Starts -->
<footer class="footer-area section-padding">
    <jsp:include page="footer.jsp"/>
</footer>
<!-- Footer Area End -->
</body>
</html>



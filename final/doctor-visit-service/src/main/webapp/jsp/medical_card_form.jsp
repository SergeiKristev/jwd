<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>


<div class="col-lg-8 well">

    <h4><fmt:message key="form.visit.id"/>: ${visit.id},
        <fmt:message key="form.visit.date_title"/>: ${visit.visitDate.toLocalDate()} ${visit.visitDate.toLocalTime()},
        <fmt:message key="form.visit.patient"/>: ${visit.patient.getFirstName()} ${visit.patient.getLastName()},
        <fmt:message key="form.visit.address"/>: ${visit.patient.getHomeAddress()},
        <fmt:message key="form.visit.phone"/>: ${visit.patient.getPhone()},
        <fmt:message key="form.visit.doctor"/>: ${visit.doctor.getFirstName()} ${visit.doctor.getLastName()}</h4>
    <form action="${pageContext.request.contextPath}/" method="post">
        <input type="hidden" name="_command" value="${CommandName.FILL_MEDICAL_CARD}">
        <input type="hidden" name="visit_id" value="${visit.id}">

        <div class="form-group required">
            <label class="control-label"><fmt:message key="form.medical_card.symptoms"/></label>
            <textarea name="symptoms" placeholder="<fmt:message key="form.medical_card.symptoms"/>.."
                      rows="3" class="form-control" maxlength="500" required><c:out value="${medicalCard.symptoms}"/></textarea>
        </div>

        <div class="form-group required">
            <label class="control-label"><fmt:message key="form.medical_card.examination"/></label>
            <textarea name="examination" placeholder="<fmt:message key="form.medical_card.examination"/>.."
                      rows="5" class="form-control" maxlength="1000" required><c:out value="${medicalCard.medicalExamination}"/></textarea>
        </div>

        <div class="form-group required">
            <label class="control-label"><fmt:message key="form.medical_card.clinical_diagnosis"/></label>
            <textarea name="clinical_diagnosis"
                      placeholder="<fmt:message key="form.medical_card.clinical_diagnosis"/>.."
                      rows="3" class="form-control" maxlength="250" required><c:out value="${medicalCard.clinicalDiagnosis}"/></textarea>
        </div>

        <div class="form-group required">
            <label class="control-label"><fmt:message key="form.medical_card.icd10_code"/></label>
            <input type="text" name="icd10_diagnosis" pattern="^[A-Z]{1}[0-9]{2}[.]{1}[0-9]{1}$" placeholder="<fmt:message key="form.medical_card.icd10_code"/>"
                   class="form-control" required>${medicalCard.icd10Diagnosis.getCode()}
            <h5><fmt:message key="form.medical_card.icd10_diagnosis"/>:<c:out value="${medicalCard.icd10Diagnosis.getName()}"/></h5>
        </div>

        <div class="form-group required">
            <label class="control-label"><fmt:message key="form.medical_card.recommendations"/></label>
            <textarea name="recommendations" placeholder="<fmt:message key="form.medical_card.recommendations"/>.."
                      rows="3" class="form-control" maxlength="1000" required><c:out value="${medicalCard.recommendations}"/></textarea>
        </div>


        <form>
            <input type="submit" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm"
                   value="<fmt:message key="button.fill_medical_card"/>">
        </form>
    </form>
</div>



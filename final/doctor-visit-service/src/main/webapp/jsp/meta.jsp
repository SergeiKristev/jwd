<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 6/22/20
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Required Meta Tags -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- Page Title -->
<title>Abstract medical centre</title>

<!-- Favicon -->
<link rel="shortcut icon" href="${pageContext.request.contextPath}/static/images/logo/favicon.png" type="image/x-icon">


<!-- CSS Files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/animate-3.7.0.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/font-awesome-4.7.0.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-4.1.3.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/owl-carousel.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/jquery.datetimepicker.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/linearicons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/sort.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/datatables.min.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/alerts.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.css">




<!-- Javascript -->
<script src="${pageContext.request.contextPath}/static/js/allerts.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/jquery-2.2.4.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/bootstrap-4.1.3.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/wow.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/owl-carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/jquery.datetimepicker.full.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/jquery.nice-select.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor/superfish.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/main.js"></script>
<script src="${pageContext.request.contextPath}/static/js/datatables.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.maskedinput.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/notyf@3/notyf.min.js"></script>



<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ page import="by.kristev.doctor.model.Gender" %>


<div class="container">
    <h1 class="well"><fmt:message key="form.registration.name"/></h1>
    <div class="col-lg-12 well">
        <div class="row">
            <form action="${pageContext.request.contextPath}/" method="post">
                <input type="hidden" name="_command" value="${CommandName.REGISTER_NEW_PATIENT}">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.first_name"/></label>
                            <input type="text" class="form-control" name="user.first_name" value="<c:out value="${user.firstName}"/>" placeholder="<fmt:message key="form.registration.first_name"/>.." class="form-control" maxlength="30" required>
                        </div>
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.last_name"/></label>
                            <input type="text" class="form-control" name="user.last_name" value="<c:out value="${user.lastName}"/>" placeholder="<fmt:message key="form.registration.last_name"/>.." class="form-control" maxlength="40" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.date_of_birth"/></label>
                            <input class="form-control" class="form-control" type="date" name="user.date_of_birth" value="<c:out value="${user.dateOfBirth}"/>" placeholder="<fmt:message key="form.registration.date_of_birth"/>.." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                        </div>

                        <div class="col-sm-6 form-group required">
                            <label class="control-label"><fmt:message key="form.registration.gender"/></label>
                            <select type="text" name="user.gender" placeholder="<fmt:message key="form.registration.gender"/>.." class="form-control" required>
                                <c:if test="${user.gender.name()} not empty">
                                    <option selected="selected" value="<c:out value="${user.gender.name()}"/>"><fmt:message
                                            key="form.registration.gender.${user.gender.name()}"/></option>
                                </c:if>
                                <c:forEach items="${Gender.values()}" var="gender">
                                    <option value="<c:out value="${gender.name()}"/>"><fmt:message
                                            key="form.gender.${gender.name()}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.home_address"/></label>
                        <textarea name="patient.home_address" value="<c:out value="${patient.homeAddress}"/>" placeholder="<fmt:message key="form.registration.home_address"/>" rows="2" class="form-control" minlength="10" maxlength="250" required><c:out value="${patient.homeAddress}"/></textarea>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label><fmt:message key="form.registration.polyclinic"/></label>
                            <input type="text" class="form-control" name="patient.polyclinic" value="<c:out value="${patient.polyclinic}"/>" placeholder="<fmt:message key="form.registration.polyclinic"/>.." class="form-control" maxlength="100">
                        </div>
                        <div class="col-sm-12 form-group">
                            <label><fmt:message key="form.registration.insurance"/></label>
                            <input type="text"  class="form-control" name="patient.insurance" value="<c:out value="${patient.insurance}"/>" placeholder="<fmt:message key="form.registration.insurance"/>.." class="form-control" maxlength="250">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.phone"/></label>
                        <input id="phone" type="text" class="form-control" name="user.phone" value="<c:out value="${user.phone}"/>" placeholder="8(XXX) XXX-XX-XX" class="form-control" maxlength="45" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.email"/></label>
                        <input type="text" name="user.email"  class="form-control" value="<c:out value="${user.email}"/>" placeholder="<fmt:message key="form.registration.email"/>.." class="form-control" maxlength="45" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.password"/></label>
                        <input type="Password" name="user.password"  class="form-control" placeholder="<fmt:message key="form.registration.password"/>.." class="form-control" maxlength="32" required>
                    </div>

                    <div class="form-group required">
                        <label class="control-label"><fmt:message key="form.registration.confirm_password"/></label>
                        <input type="Password" name="user.confirm_password" class="form-control" placeholder="<fmt:message key="form.registration.confirm_password"/>.." class="form-control" maxlength="32" required>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary"><fmt:message key="form.registration.submit"/></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $("#phone").mask("8(999)999-99-99");
</script>


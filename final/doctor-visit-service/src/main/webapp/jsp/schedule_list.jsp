<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="by.kristev.doctor.command.CommandName" %>
<%@ taglib prefix="security" tagdir="/WEB-INF/tags/security" %>


<h3><fmt:message key="doctor.id"/>: <c:out value="${doctor.doctorId}"/>
    <fmt:message key="user.last_name"/>: <c:out value="${doctor.lastName}"/></h3>
<h3><fmt:message key="form.schedule"/> <fmt:message key="form.schedule.date_from"/>: <c:out value="${date_from}"/>
    <fmt:message key="form.schedule.date_to"/>: <c:out value="${date_to}"/></h3>


<!-- Кнопка запуска модального окна -->
<button type="button" class="fcbtn btn btn-primary btn-outline btn-1d btn-sm btn-block my-1"
        data-toggle="modal" data-target="#editSchedule">
    <fmt:message key="button.add_duty"/>
</button>

<!-- Модальное окно -->
<div class="modal fade" id="editSchedule" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="button.edit"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <form action="${pageContext.request.contextPath}/" method="post">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label><fmt:message key="form.schedule_date"/></label>
                            <input class="form-control" type="date" name="schedule_date"
                                   placeholder="<fmt:message key="form.schedule_date"/>.."
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date'" required>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.schedule.date_from"/></label>
                            <select type="text" name="schedule_start"
                                    placeholder="<fmt:message key="form.schedule.date_from"/>.."
                                    class="form-control" required>

                                <option selected="selected"
                                        value="<c:out value="${schedule.scheduleStartTime}"/>:00">
                                    <c:out value="${schedule.scheduleStartTime}"/></option>
                                <option value="08:00:00">8:00</option>
                                <option value="09:00:00">9:00</option>
                                <option value="10:00:00">10:00</option>
                                <option value="11:00:00">11:00</option>
                                <option value="12:00:00">12:00</option>
                                <option value="13:00:00">13:00</option>
                                <option value="14:00:00">14:00</option>
                                <option value="15:00:00">15:00</option>
                                <option value="16:00:00">16:00</option>
                                <option value="17:00:00">17:00</option>
                                <option value="18:00:00">18:00</option>
                                <option value="19:00:00">19:00</option>
                                <option value="20:00:00">20:00</option>

                            </select>
                        </div>

                        <div class="col-sm-6 form-group">
                            <label><fmt:message key="form.schedule.date_to"/></label>
                            <select type="text" name="schedule_end"
                                    placeholder="<fmt:message key="form.schedule.date_to"/>.."
                                    class="form-control" required>
                                <option selected="selected"
                                        value="<c:out value="${schedule.scheduleEndTime}"/>:00">
                                    <c:out value="${schedule.scheduleEndTime}"/></option>
                                <option value="10:00:00">10:00</option>
                                <option value="11:00:00">11:00</option>
                                <option value="12:00:00">12:00</option>
                                <option value="13:00:00">13:00</option>
                                <option value="14:00:00">14:00</option>
                                <option value="15:00:00">15:00</option>
                                <option value="16:00:00">16:00</option>
                                <option value="17:00:00">17:00</option>
                                <option value="18:00:00">18:00</option>
                                <option value="19:00:00">19:00</option>
                                <option value="20:00:00">20:00</option>
                                <option value="21:00:00">21:00</option>
                                <option value="22:00:00">22:00</option>

                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="_command" value="${CommandName.ADD_SCHEDULE}">
                    <input type="hidden" name="doctor_id" value="${doctor.doctorId}">
                    <input type="hidden" name="date_from" value="${date_from}">
                    <input type="hidden" name="date_to" value="${date_to}">
                    <button type="submit" class="btn btn-lg btn-primary"><fmt:message
                            key="form.registration.submit"/></button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><fmt:message
                        key="button.close"/></button>

            </div>
        </div>
    </div>
</div>

<table id="dtUsers" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm"><fmt:message key="schedule.id"/></th>
        <th class="th-sm"><fmt:message key="schedule.date"/></th>
        <th class="th-sm"><fmt:message key="schedule.time_start"/></th>
        <th class="th-sm"><fmt:message key="schedule.time_end"/></th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${not empty dayScheduleList}">
        <c:forEach items="${dayScheduleList}" var="schedule">
            <tr>
                <td><c:out value="${schedule.id}"/></td>
                <td><c:out value="${schedule.scheduleDate}"/></td>
                <td><c:out value="${schedule.scheduleStartTime}"/></td>
                <td><c:out value="${schedule.scheduleEndTime}"/></td>
            </tr>
        </c:forEach>
    </c:if>
    </tbody>
    <tfoot>
    <th><fmt:message key="schedule.id"/></th>
    <th><fmt:message key="schedule.date"/></th>
    <th><fmt:message key="schedule.time_start"/></th>
    <th><fmt:message key="schedule.time_end"/></th>
    </tfoot>
</table>

<script>
    $(document).ready(function () {
        $('#dtUsers').DataTable({
            "order": [[1, "asc"]]
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>




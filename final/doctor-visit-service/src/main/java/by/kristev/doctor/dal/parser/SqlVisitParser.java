package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SqlVisitParser implements EntityParser<Visit> {

    private static final String VISIT_ID = "visit_id";
    private static final String PATIENT_ID = "patient_id";
    private static final String VISIT_REASON = "visit_reason";
    private static final String VISIT_TYPE = "visit_type";
    private static final String VISIT_DATE = "visit_date";
    private static final String DOCTOR_ID = "doctor_id";
    private static final String TARIFF_ID = "tariff_id";
    private static final String NOTES = "notes";
    private static final String APPOINTMENT_STATUS = "appointment_status";

    @Override
    public Visit createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long visitId = resultSet.getLong(VISIT_ID);
        Long patientId = resultSet.getLong(PATIENT_ID);
        VisitReason visitReason = VisitReason.valueOf(resultSet.getString(VISIT_REASON));
        VisitType visitType = VisitType.valueOf(resultSet.getString(VISIT_TYPE));
        LocalDateTime visitDate = LocalDateTime.parse(resultSet.getString(VISIT_DATE), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Long doctorId = resultSet.getLong(DOCTOR_ID);
        Long tariffId = resultSet.getLong(TARIFF_ID);
        String notes = resultSet.getString(NOTES);
        AppointmentStatus appointmentStatus = AppointmentStatus.valueOf(resultSet.getString(APPOINTMENT_STATUS));
        Patient patient = new Patient();
        Doctor doctor = new Doctor();
        Tariff tariff = new Tariff();
        patient.setPatientId(patientId);
        doctor.setDoctorId(doctorId);
        tariff.setId(tariffId);
        return new Visit(visitId, patient, visitReason, visitType, visitDate, doctor, tariff, notes, appointmentStatus);
    }

    @Override
    public PreparedStatement parseEntityToStatement(Visit entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setLong(++i, entity.getPatient().getPatientId());
        statement.setString(++i, entity.getVisitReason().name());
        statement.setString(++i, entity.getVisitType().name());
        statement.setString(++i, entity.getVisitDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        statement.setLong(++i, entity.getDoctor().getDoctorId());
        statement.setLong(++i, entity.getTariff().getId());
        statement.setString(++i, entity.getNotes());
        statement.setString(++i, entity.getAppointmentStatus().name());
        return statement;
    }
}

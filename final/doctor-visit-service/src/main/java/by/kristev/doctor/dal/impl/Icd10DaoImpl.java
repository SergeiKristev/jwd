package by.kristev.doctor.dal.impl;

import by.kristev.doctor.dal.Icd10Dao;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Icd10;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Icd10DaoImpl implements Icd10Dao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.icd_10;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.icd_10 where icd10_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`icd_10` (`name`, `code`, `parent_id`, `parent_code`, `node_count`, `additional_info`) VALUES (?, ?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`icd_10` SET `name` = ?, `code` = ?, `parent_id` = ?, `parent_code` = ?, `node_count` = ?, `additional_info` = ? WHERE (`icd10_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM `doctor_home_visit_service`.`icd_10` WHERE (`icd10_id` = ?);";
    private static final String SELECT_BY_CODE = "SELECT * FROM doctor_home_visit_service.icd_10 WHERE code like ?;";


    private static final Logger LOGGER = LogManager.getLogger(Icd10DaoImpl.class);

    private final ConnectionManager connectionManager;
    private final EntityParser<Icd10> icd10EntityParser;

    public Icd10DaoImpl(ConnectionManager connectionManager, EntityParser<Icd10> icd10EntityParser) {
        this.connectionManager = connectionManager;
        this.icd10EntityParser = icd10EntityParser;
    }

    @Override
    public Long save(Icd10 icd10) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            icd10EntityParser.parseEntityToStatement(icd10, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    icd10.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating icd10 failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return icd10.getId();
    }

    @Override
    public boolean update(Icd10 icd10) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            icd10EntityParser.parseEntityToStatement(icd10, updateStmt);
            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, icd10.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(Icd10 icd10) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, icd10.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

    }

    @Override
    public Icd10 getById(Long id) throws DAOException {

        AtomicReference<Icd10> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Icd10 icd10 = icd10EntityParser.createEntityFromResultSet(resultSet);
                    result.set(icd10);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();

    }

    @Override
    public List<Icd10> findAll() throws DAOException {
        List<Icd10> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Icd10 icd10 = icd10EntityParser.createEntityFromResultSet(resultSet);
                    result.add(icd10);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Icd10 getIcd10byCode(String code) throws DAOException {
        AtomicReference<Icd10> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_CODE)) {
            selectStmt.setString(1, code);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Icd10 icd10 = icd10EntityParser.createEntityFromResultSet(resultSet);
                    result.set(icd10);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting icd10 failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();    }
}

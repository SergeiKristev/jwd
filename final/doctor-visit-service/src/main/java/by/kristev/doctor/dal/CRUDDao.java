package by.kristev.doctor.dal;

import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;

import java.sql.SQLException;
import java.util.List;

public interface CRUDDao<ENTITY, KEY> {

    KEY save(ENTITY entity) throws DAOException;

    boolean update(ENTITY entity) throws DAOException;

    boolean delete(ENTITY entity) throws DAOException;

    ENTITY getById(KEY id) throws DAOException;

    List<ENTITY> findAll() throws DAOException;
}

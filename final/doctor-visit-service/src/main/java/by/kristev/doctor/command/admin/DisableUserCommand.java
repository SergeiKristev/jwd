package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import by.kristev.doctor.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the disable user command
 *
 * @see Command,UserService
 */
public class DisableUserCommand implements Command {

    private final UserService userService;

    /**
     * Initializing of the command
     *
     * @param userService - service of the user objects
     */
    public DisableUserCommand(UserService userService) {
        this.userService = userService;
    }


    /**
     * Executes user's disable.
     *
     * The disabled user is not displayed in the list of patients and doctors management.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to a page with a list of all doctors or all patient's
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long userId = Long.parseLong(request.getParameter("user.id"));

        User user = userService.getUserById(userId);
        userService.disableUser(userId);

        if (user.getUserRole().equals(UserRole.DOCTOR)) {
            return "redirect:?_command=" + CommandName.MANAGE_DOCTORS_LIST + "&message=user_disabled";

        } else {
            return "redirect:?_command=" + CommandName.MANAGE_PATIENTS_LIST + "&message=user_disabled";
        }
    }
}

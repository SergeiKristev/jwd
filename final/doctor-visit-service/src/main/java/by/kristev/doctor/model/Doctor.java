package by.kristev.doctor.model;

import java.time.LocalDate;
import java.util.Objects;

public class Doctor extends User {

    Long doctorId;
    private Speciality speciality;
    private MedicalCategory medicalCategory;
    private int experience;

    public Doctor() {
    }

    public Doctor(Long doctorId, Speciality speciality, MedicalCategory medicalCategory, int experience) {
        this.doctorId = doctorId;
        this.speciality = speciality;
        this.medicalCategory = medicalCategory;
        this.experience = experience;
    }

    public Doctor(Long userId, String email, String password, String phone, String firstName, String lastName, Gender gender, LocalDate dateOfBirth, boolean isActive, UserRole userRole, Long doctorId, Speciality speciality, MedicalCategory medicalCategory, int experience) {
        super(userId, email, password, phone, firstName, lastName, gender, dateOfBirth, isActive, userRole);
        this.doctorId = doctorId;
        this.speciality = speciality;
        this.medicalCategory = medicalCategory;
        this.experience = experience;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public MedicalCategory getMedicalCategory() {
        return medicalCategory;
    }

    public void setMedicalCategory(MedicalCategory medicalCategory) {
        this.medicalCategory = medicalCategory;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return experience == doctor.experience &&
                Objects.equals(doctorId, doctor.doctorId) &&
                speciality == doctor.speciality &&
                medicalCategory == doctor.medicalCategory;
    }

    @Override
    public int hashCode() {
        return Objects.hash(doctorId, speciality, medicalCategory, experience);
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId=" + doctorId +
                ", speciality=" + speciality +
                ", medicalCategory=" + medicalCategory +
                ", experience=" + experience +
                "} " + super.toString();
    }
}

package by.kristev.doctor.validation;

import java.util.List;

public interface EntityValidator<E> {

    List<String> validate(E entity);

}

package by.kristev.doctor.validation;

import by.kristev.doctor.model.DaySchedule;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ScheduleValidator implements EntityValidator<DaySchedule> {

    private static ScheduleValidator instance;

    public static ScheduleValidator getInstance() {

        if (instance == null) {
            instance = new ScheduleValidator();
        }
        return instance;
    }

    private ScheduleValidator() {

    }

    @Override
    public List<String> validate(DaySchedule entity) {

        List<String> validationResult = new ArrayList<>(3);
        if (entity.getScheduleDate() == null || entity.getScheduleDate().isBefore(LocalDate.now())) {
            validationResult.add("validation.schedule.date");
        }
        if (entity.getScheduleStartTime() == null) {
            validationResult.add("validation.schedule.time_start");
        }

        if (entity.getScheduleEndTime() == null || entity.getScheduleEndTime().isBefore(entity.getScheduleStartTime().plusHours(2))) {
            validationResult.add("validation.schedule.time_end");
        }

        return validationResult;
    }
}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.AppointmentStatus;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.model.VisitReason;
import by.kristev.doctor.model.VisitType;
import by.kristev.doctor.service.VisitService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;

/**
 * A class of the visit's data editing command
 *
 * @see Command,VisitService
 */
public class EditVisitFormCommand implements Command {

    private final VisitService visitService;

    /**
     * Initializing of the command
     *
     * @param visitService - service of the visit objects
     */
    public EditVisitFormCommand(VisitService visitService) {
        this.visitService = visitService;
    }

    /**
     * We get the visit ID from the request, and we get the visit object via the VisitService by visit ID.
     * If there is a visit status <i>CANCELED</i> we call the <i>releaseAppointment</i> method of the <i>visitService</i>,
     * after which this time will be free for booking.
     * After that we redirect occurs to the personal account page.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to a page with a list of outstanding visits.
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Long visitId = Long.parseLong(request.getParameter("visit_id"));
        Visit visit = visitService.getVisitById(visitId);

        String notes = request.getParameter("notes");
        VisitReason visitReason = VisitReason.valueOf(request.getParameter("visit_reason"));
        VisitType visitType = VisitType.valueOf(request.getParameter("visit_type"));
        AppointmentStatus status = AppointmentStatus.valueOf(request.getParameter("appointment_status"));

        visit.setNotes(notes);
        visit.setVisitReason(visitReason);
        visit.setVisitType(visitType);
        visit.setAppointmentStatus(status);

        if (AppointmentStatus.CANCELED.equals(status)) {
            Long doctorId = visit.getDoctor().getDoctorId();
            LocalTime visitStartTime = visit.getVisitDate().toLocalTime();
            visitService.releaseAppointment(doctorId, visitStartTime);
        }

        visitService.updateVisit(visit);

        return "redirect:?_command=" + CommandName.MANAGE_OUTSTANDING_VISITS + "&message=visit_edited";
    }
}

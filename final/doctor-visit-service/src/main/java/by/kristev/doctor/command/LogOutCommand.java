package by.kristev.doctor.command;

import by.kristev.doctor.SecurityContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogOutCommand implements Command{
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SecurityContext.getInstance().logout(request.getRequestedSessionId());
        request.setAttribute("pageName", "message.logout");
        return "index";
    }
}

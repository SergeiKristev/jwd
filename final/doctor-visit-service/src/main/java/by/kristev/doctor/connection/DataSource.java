package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;

public interface DataSource {

    Connection getConnection() throws ConnectionException;

    void close();

}

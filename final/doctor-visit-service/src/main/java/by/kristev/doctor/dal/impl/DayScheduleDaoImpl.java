package by.kristev.doctor.dal.impl;

import by.kristev.doctor.dal.DayScheduleDao;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.DaySchedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DayScheduleDaoImpl implements DayScheduleDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.day_schedule;";
    private static final String SELECT_BY_DATE_INTERVAL_QUERY = "SELECT * FROM doctor_home_visit_service.day_schedule where doctor_id = ? and schedule_date between ? and ?;";
    private static final String SELECT_BY_DOCTOR_ID_AND_DATE = "SELECT * FROM doctor_home_visit_service.day_schedule where doctor_id = ? and schedule_date = ?;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.day_schedule where day_schedule_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`day_schedule` (`doctor_id`, `schedule_date`, `schedule_start`, `schedule_end`) VALUES (?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`day_schedule` SET `doctor_id` = ?, `schedule_date` = ?, `schedule_start` = ?, `schedule_end` = ? WHERE (`day_schedule_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM `doctor_home_visit_service`.`day_schedule` WHERE (`day_schedule_id` = ?);";

    private static final Logger LOGGER = LogManager.getLogger(DayScheduleDaoImpl.class);
    private final ConnectionManager connectionManager;
    private final EntityParser<DaySchedule> dayScheduleParser;

    public DayScheduleDaoImpl(ConnectionManager connectionManager, EntityParser<DaySchedule> dayScheduleParser) {
        this.connectionManager = connectionManager;
        this.dayScheduleParser = dayScheduleParser;
    }

    @Override
    public Long save(DaySchedule daySchedule) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            dayScheduleParser.parseEntityToStatement(daySchedule, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    daySchedule.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating day schedule failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return daySchedule.getId();
    }

    @Override
    public boolean update(DaySchedule daySchedule) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            dayScheduleParser.parseEntityToStatement(daySchedule, updateStmt);

            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, daySchedule.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(DaySchedule daySchedule) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, daySchedule.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public DaySchedule getById(Long id) throws DAOException {
        AtomicReference<DaySchedule> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    DaySchedule daySchedule = dayScheduleParser.createEntityFromResultSet(resultSet);
                    result.set(daySchedule);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<DaySchedule> findAll() throws DAOException {
        List<DaySchedule> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    DaySchedule daySchedule = dayScheduleParser.createEntityFromResultSet(resultSet);
                    result.add(daySchedule);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public List<DaySchedule> getDayScheduleList(Long doctorId, LocalDate from, LocalDate to) throws DAOException {
        List<DaySchedule> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_DATE_INTERVAL_QUERY)) {
            selectStmt.setLong(1, doctorId);
            selectStmt.setString(2, from.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            selectStmt.setString(3, to.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    DaySchedule daySchedule = dayScheduleParser.createEntityFromResultSet(resultSet);
                    result.add(daySchedule);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;    }

    @Override
    public boolean isFreeDate(Long doctorId, LocalDate dutyDate) throws DAOException {
        AtomicReference<DaySchedule> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_DOCTOR_ID_AND_DATE)) {
            selectStmt.setLong(1, doctorId);
            selectStmt.setString(2, dutyDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    DaySchedule daySchedule = dayScheduleParser.createEntityFromResultSet(resultSet);
                    result.set(daySchedule);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting day schedule failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get() == null;
    }
}

package by.kristev.doctor.command.patient;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the displaying the take appointment form command
 *
 * @see Command
 */
public class TakeAppointmentCommand implements Command {

    /**
     * Displaying the the take appointment form
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        request.setAttribute("pageName", "page.create_visit");
        return "take_appointment";
    }
}

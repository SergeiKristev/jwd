package by.kristev.doctor.validation;

import by.kristev.doctor.model.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientValidator implements EntityValidator<Patient> {

    private static PatientValidator instance;

    public static PatientValidator getInstance() {

        if (instance == null) {
            instance = new PatientValidator();
        }
        return instance;
    }

    private PatientValidator() {

    }

    @Override
    public List<String> validate(Patient patient) {
        List<String> validationResult = new ArrayList<>(3);
        if (patient.getPolyclinic() != null && patient.getPolyclinic().length() > 100) {
            validationResult.add("validation.patient.polyclinic");
        }
        if (patient.getHomeAddress().length() < 10 || patient.getHomeAddress().length() > 250) {
            validationResult.add("validation.patient.home_address");
        }

        if (patient.getInsurance() != null && patient.getInsurance().length() > 250) {
            validationResult.add("validation.patient.insurance");
        }

        return validationResult;
    }
}

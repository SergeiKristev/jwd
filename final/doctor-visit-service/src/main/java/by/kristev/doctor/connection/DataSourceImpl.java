package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;

public class DataSourceImpl implements DataSource {

    private final ConnectionPool connectionPool;

    public DataSourceImpl() {
        connectionPool = ConnectionPoolImpl.getInstance();
    }

    @Override
    public Connection getConnection() throws ConnectionException {
        return connectionPool.getConnection();
    }

    @Override
    public void close() {
        connectionPool.closeAll();
    }
}

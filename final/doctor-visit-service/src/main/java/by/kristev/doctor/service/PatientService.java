package by.kristev.doctor.service;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Patient;

import java.util.List;

public interface PatientService {

    List<Patient> findAllPatients() throws ServiceException;
    boolean registerPatient(Patient patient, Long userId) throws ServiceException;
    Patient getPatientById(Long patientId) throws ServiceException;
    boolean deletePatient(Long patientId) throws ServiceException;
    boolean update(Patient patient) throws ServiceException;
    Patient getPatientByUserId(Long userId) throws ServiceException;

}

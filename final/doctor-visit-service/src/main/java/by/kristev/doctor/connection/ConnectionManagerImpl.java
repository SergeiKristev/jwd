package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManagerImpl implements ConnectionManager{

    private final DataSource dataSource;
    private final TransactionManager transactionManager;

    public ConnectionManagerImpl(DataSource dataSource, TransactionManager transactionManager) {
        this.dataSource = dataSource;
        this.transactionManager = transactionManager;
    }

    @Override
    public Connection getConnection() throws ConnectionException, SQLException {
        Connection connection = transactionManager.getConnection();
        return connection != null ? connection : dataSource.getConnection();
    }

}

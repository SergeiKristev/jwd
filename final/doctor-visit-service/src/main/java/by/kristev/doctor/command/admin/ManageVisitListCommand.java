package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A class for displaying the list of all visits.
 *
 * @see Command,VisitService
 */
public class ManageVisitListCommand implements Command {

    private final VisitService visitService;

    /**
     * Initializing of the command
     *
     * @param visitService - service of the visit objects
     */
    public ManageVisitListCommand(VisitService visitService) {
        this.visitService = visitService;
    }

    /**
     * Displaying the list of all visits
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        List<Visit> visitList = visitService.findAllVisits();
        request.setAttribute("visitList", visitList);
        request.setAttribute("pageName", "page.visit_list");
        return "manage_visit_list";
    }
}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Appointment;
import by.kristev.doctor.model.DaySchedule;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.validation.ScheduleValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * A class of the schedule creation command
 *
 * @see Command,DoctorService,ScheduleValidator,Appointment
 */
public class AddScheduleCommand implements Command {

    private final DoctorService doctorService;
    private final ScheduleValidator scheduleValidator;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     */
    public AddScheduleCommand(DoctorService doctorService) {

        this.scheduleValidator = ScheduleValidator.getInstance();
        this.doctorService = doctorService;
    }

    /**
     * Executes adding day's duty.
     * We read the doctor's ID and duty date from the request.
     * If the duty is already assigned on the specified date - add a string to the <i>validationResult</i> object
     * with a message about the busy duty and redirect to the duty search page.
     * If this date is available, we perform input data validation. If validation is successful,
     * we create 1-hour <i>Appointment</i> objects within the specified time interval and redirect to the duty search page.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to the page with the duty schedule. Name of the jsp view.
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Long doctorId = Long.parseLong(request.getParameter("doctor_id"));
        Doctor doctor = doctorService.getDoctorById(doctorId);
        LocalDate scheduleDate = LocalDate.parse(request.getParameter("schedule_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate from = LocalDate.parse(request.getParameter("date_from"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate to = LocalDate.parse(request.getParameter("date_to"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        List<String> validationResult = new ArrayList<>(4);

        if (doctorService.isFreeDutyDate(doctorId, scheduleDate)) {

            LocalTime scheduleStartTime = LocalTime.parse(request.getParameter("schedule_start"), DateTimeFormatter.ofPattern("HH:mm:ss"));
            LocalTime scheduleEndTime = LocalTime.parse(request.getParameter("schedule_end"), DateTimeFormatter.ofPattern("HH:mm:ss"));
            DaySchedule daySchedule = new DaySchedule();
            daySchedule.setDoctor(doctor);
            daySchedule.setScheduleDate(scheduleDate);
            daySchedule.setScheduleStartTime(scheduleStartTime);
            daySchedule.setScheduleEndTime(scheduleEndTime);

            validationResult.addAll(scheduleValidator.validate(daySchedule));

            if (validationResult.isEmpty()) {
                doctorService.createDaySchedule(daySchedule);

                LocalTime currentAppointmentTime = scheduleStartTime;
                while (currentAppointmentTime.isBefore(scheduleEndTime)) {
                    Appointment appointment = new Appointment();
                    appointment.setDaySchedules(daySchedule);
                    appointment.setVisitStartTime(currentAppointmentTime);
                    appointment.setVisitEndTime(currentAppointmentTime.plusHours(1));
                    appointment.setName(appointment.getVisitStartTime().toString() + "-" + appointment.getVisitEndTime());
                    appointment.setFreeTime(true);
                    doctorService.createAppointment(appointment);
                    currentAppointmentTime = currentAppointmentTime.plusHours(1);
                }
                return "redirect:?_command=" + CommandName.MANAGE_SCHEDULES_LIST + "&doctor_id=" + doctorId + "&date_from=" + from + "&date_to=" + to + "&message=duty_added";
            }
        } else {
            validationResult.add("validation.schedule.reserved");
        }
        request.setAttribute("doctor", doctor);
        request.setAttribute("doctor_id", doctorId);
        request.setAttribute("date_from", from);
        request.setAttribute("date_to", to);
        request.setAttribute("pageName", "page.schedules_list");
        request.setAttribute("validationResult", validationResult);
        return "schedule_list";
    }
}

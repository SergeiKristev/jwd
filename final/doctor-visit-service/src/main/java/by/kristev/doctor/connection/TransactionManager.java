package by.kristev.doctor.connection;


import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;

public interface TransactionManager {

    Connection getConnection();

    void beginTransaction() throws ConnectionException;

    void rollbackTransaction() throws ConnectionException;

    void commitTransaction() throws ConnectionException;

    boolean isEmpty() throws ConnectionException;


}

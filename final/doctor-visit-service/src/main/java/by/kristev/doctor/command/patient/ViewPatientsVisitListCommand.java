package by.kristev.doctor.command.patient;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.PatientService;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A class for displaying the patient's list of all visits.
 *
 * @see Command,VisitService, DoctorService
 */
public class ViewPatientsVisitListCommand implements Command {

    private final PatientService patientService;
    private final VisitService visitService;

    /**
     * Initializing of the command
     *
     * @param patientService - service of the patient objects
     * @param visitService - service of the visit objects
     */
    public ViewPatientsVisitListCommand(PatientService patientService, VisitService visitService) {
        this.patientService = patientService;
        this.visitService = visitService;
    }

    /**
     * Displaying the patient's list of all visits.
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ServiceException {

        User currentUser = SecurityContext.getInstance().getCurrentUser();
        Patient patient = patientService.getPatientByUserId(currentUser.getUserId());
        List<Visit> visitList = visitService.findVisitsByPatientId(patient.getPatientId());
        request.setAttribute("visitList", visitList);
        request.setAttribute("pageName", "page.visit_list");
        return "manage_visit_list";
    }
}

package by.kristev.doctor.model;

public enum VisitReason {

    INFECTION_DISEASE,
    CARDIOLOGY_DISEASE,
    NEUROLOGY_DISEASE;
}

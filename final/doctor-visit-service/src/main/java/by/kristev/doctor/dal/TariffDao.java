package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Tariff;

import java.sql.SQLException;

public interface TariffDao extends CRUDDao<Tariff, Long> {
    Tariff getByTariffName(String tariffName) throws DAOException;
}

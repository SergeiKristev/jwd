package by.kristev.doctor;

import by.kristev.doctor.command.*;
import by.kristev.doctor.command.admin.*;
import by.kristev.doctor.command.doctor.DisplayMedicalCardFormCommand;
import by.kristev.doctor.command.doctor.FillingMedicalCardCommand;
import by.kristev.doctor.command.doctor.MangeDoctorsOutstandingVisitsCommand;
import by.kristev.doctor.command.doctor.ViewAllVisitsCommand;
import by.kristev.doctor.command.patient.*;
import by.kristev.doctor.dal.*;
import by.kristev.doctor.connection.*;
import by.kristev.doctor.dal.impl.*;
import by.kristev.doctor.dal.parser.*;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext {
    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);
    private static final AtomicBoolean IS_INSTANCE_EXIST = new AtomicBoolean(false);
    private static final Lock INSTANCE_LOCK = new ReentrantLock();

    private static ApplicationContext instance;

    private final Map<Class<?>, Object> beans = new HashMap<>();


    private ApplicationContext() {
    }

    public static ApplicationContext getInstance() {
        if (!IS_INSTANCE_EXIST.get()) {
            INSTANCE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ApplicationContext();
                    IS_INSTANCE_EXIST.set(true);
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    static <T> InvocationHandler createTransactionalInvocationHandler(TransactionManager tr, T service) {
        return (proxy, method, args) -> {

            Method declaredMethod = service.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
            if (method.isAnnotationPresent(Transactional.class)
                    || declaredMethod.isAnnotationPresent(Transactional.class)) {
                tr.beginTransaction();
                try {
                    Object result = method.invoke(service, args);
                    tr.commitTransaction();
                    return result;
                } catch (Exception e) {
                    LOGGER.error("Transaction will be rolled back, exception is occurred " +
                            "during methodName={} execution", method.getName());
                    tr.rollbackTransaction();
                    throw e;
                }
            } else {
                return method.invoke(service, args);
            }
        };
    }

    static <T> T createProxy(ClassLoader classLoader, InvocationHandler handler, Class<T>... toBeProxied) {
        return (T) Proxy.newProxyInstance(classLoader, toBeProxied, handler);
    }


    public void initialize() {

        //init DB connection
        DataSource dataSource = new DataSourceImpl();

        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        ConnectionManager connectionManager = new ConnectionManagerImpl(dataSource, transactionManager);

        //init EntityParsers foe DAO
        EntityParser<User> sqlUserParser = new SqlUserParser();
        EntityParser<Patient> sqlPatientParser = new SqlPatientParser();
        EntityParser<Doctor> sqlDoctorParser = new SqlDoctorParser();
        EntityParser<Visit> sqlVisitParser = new SqlVisitParser();
        EntityParser<Tariff> sqlTariffParser = new SqlTariffParser();
        EntityParser<MedicalCard> sqlMedicalCardParser = new SqlMedicalCardParser();
        EntityParser<Icd10> sqlIcd10Parser = new SqlIcd10Parser();
        EntityParser<DaySchedule> sqlDayScheduleParser = new SqlDayScheduleParser();
        EntityParser<Appointment> sqlAppointmentParser = new SqlAppointmentParser();

        //init DAO
        UserDao userDao = new UserDaoImpl(connectionManager, sqlUserParser);
        PatientDao patientDao = new PatientDaoImpl(connectionManager, sqlPatientParser);
        DoctorDao doctorDao = new DoctorDaoImpl(connectionManager, sqlDoctorParser);
        VisitDao visitDao = new VisitDaoImpl(connectionManager, sqlVisitParser);
        TariffDao tariffDao = new TariffDaoImpl(connectionManager, sqlTariffParser);
        MedicalCardDao medicalCardDao = new MedicalCardDaoImpl(connectionManager, sqlMedicalCardParser);
        Icd10Dao icd10Dao = new Icd10DaoImpl(connectionManager, sqlIcd10Parser);
        DayScheduleDao dayScheduleDao = new DayScheduleDaoImpl(connectionManager, sqlDayScheduleParser);
        AppointmentDao appointmentDao = new AppointmentDaoImpl(connectionManager, sqlAppointmentParser);

        LOGGER.info("Init DAO");

        //init Services
        UserService userService = new UserServiceImpl(userDao);
        PatientService patientService = new PatientServiceImpl(userDao, patientDao);
        DoctorService doctorService = new DoctorServiceImpl(userDao, doctorDao, dayScheduleDao, appointmentDao);
        VisitService visitService = new VisitServiceImpl(patientDao, doctorDao, visitDao, tariffDao, appointmentDao);
        MedicalCardService medicalCardService = new MedicalCardServiceImpl(patientDao, medicalCardDao, icd10Dao);

        //init service's proxy
        InvocationHandler userServiceHandler = createTransactionalInvocationHandler
                (transactionManager, userService);
        UserService userProxyService = createProxy(getClass().getClassLoader(),
                userServiceHandler, UserService.class);

        InvocationHandler patientServiceHandler = createTransactionalInvocationHandler
                (transactionManager, patientService);
        PatientService patientProxyService = createProxy(getClass().getClassLoader(),
                patientServiceHandler, PatientService.class);

        InvocationHandler doctorServiceHandler = createTransactionalInvocationHandler
                (transactionManager, doctorService);
        DoctorService doctorProxyService = createProxy(getClass().getClassLoader(),
                doctorServiceHandler, DoctorService.class);

        InvocationHandler visitServiceHandler = createTransactionalInvocationHandler
                (transactionManager, visitService);
        VisitService visitProxyService = createProxy(getClass().getClassLoader(),
                visitServiceHandler, VisitService.class);

        InvocationHandler medicalCardServiceHandler = createTransactionalInvocationHandler
                (transactionManager, medicalCardService);
        MedicalCardService medicalCardProxyService = createProxy(getClass().getClassLoader(),
                medicalCardServiceHandler, MedicalCardService.class);

        LOGGER.info("Init services");

        //init commands
        Command indexCommand = new IndexCommand();
        Command logInCommand = new LogInSaveCommand(userProxyService);
        Command logOutCommand = new LogOutCommand();
        Command disableUserCommand = new DisableUserCommand(userProxyService);
        Command registerNewPatientDisplayCommand = new RegisterNewPatientDisplayCommand();
        Command registerNewPatientCommand = new RegisterNewPatientCommand(userProxyService, patientProxyService);
        Command registerNewDoctorCommand = new RegisterNewDoctorCommand(userProxyService, doctorProxyService);
        Command displayEditDoctorCommand = new DisplayEditDoctorFormCommand(doctorProxyService);
        Command displayEditPatientCommand = new DisplayEditPatientFormCommand(patientProxyService);
        Command displayEditUserCommand = new DisplayEditUserFormCommand(patientProxyService, doctorProxyService);
        Command editDoctorCommand = new EditDoctorCommand(doctorProxyService);
        Command editPatientCommand = new EditPatientCommand(patientProxyService);
        Command visitDisplayCommand = new VisitDisplayCommand(doctorProxyService, visitProxyService);
        Command viewVisitDetailsCommand = new ViewVisitDetailsCommand(visitProxyService);
        Command createVisitCommand = new CreateVisitCommand(patientProxyService, doctorProxyService, visitProxyService);
        Command viewPersonalAccountCommand = new ViewPersonalAccountCommand();
        Command viewPatientsVisitListCommand = new ViewPatientsVisitListCommand(patientProxyService, visitProxyService);
        Command manageVisitListCommand = new ManageVisitListCommand(visitProxyService);
        Command manageOutstandingVisitsCommand = new ManageOutstandingVisits(visitProxyService);
        Command manageDoctorsOutstandingVisitsCommand = new MangeDoctorsOutstandingVisitsCommand(visitProxyService, doctorProxyService);
        Command viewAllVisitsCommand = new ViewAllVisitsCommand(visitProxyService, doctorProxyService);
        Command displayEditVisitFormCommand = new DisplayEditVisitFormCommand(visitProxyService);
        Command editVisitFormCommand = new EditVisitFormCommand(visitProxyService);
        Command takeAppointmentCommand = new TakeAppointmentCommand();
        Command displayMedicalCardFormCommand = new DisplayMedicalCardFormCommand(visitProxyService);
        Command fillMedicalCardCommand = new FillingMedicalCardCommand(visitProxyService, medicalCardProxyService);
        Command medicalCardDetailCommand = new MedicalCardDetailCommand(medicalCardProxyService, visitProxyService);
        Command manegeDoctorsListCommand = new ManageDoctorsListCommand(doctorProxyService);
        Command manegePatientsListCommand = new ManagePatientsListCommand(patientProxyService);
        Command manageSchedulesListCommand = new ManageSchedulesListCommand(doctorProxyService);
        Command addScheduleCommand = new AddScheduleCommand(doctorProxyService);
        Command changePasswordCommand = new ChangePasswordCommand(userProxyService);

        //register commands
        AppCommandProvider appCommandProvider = new AppCommandProviderImpl();
        appCommandProvider.register(CommandName.INDEX, indexCommand);
        appCommandProvider.register(CommandName.LOG_IN_SAVE, logInCommand);
        appCommandProvider.register(CommandName.LOG_OUT, logOutCommand);
        appCommandProvider.register(CommandName.DISABLE_USER, disableUserCommand);
        appCommandProvider.register(CommandName.REGISTER_NEW_PATIENT_DISPLAY, registerNewPatientDisplayCommand);
        appCommandProvider.register(CommandName.REGISTER_NEW_PATIENT, registerNewPatientCommand);
        appCommandProvider.register(CommandName.REGISTER_NEW_DOCTOR, registerNewDoctorCommand);
        appCommandProvider.register(CommandName.DISPLAY_EDIT_DOCTOR_FORM, displayEditDoctorCommand);
        appCommandProvider.register(CommandName.DISPLAY_EDIT_PATIENT_FORM, displayEditPatientCommand);
        appCommandProvider.register(CommandName.DISPLAY_EDIT_USER_FORM, displayEditUserCommand);
        appCommandProvider.register(CommandName.EDIT_DOCTOR, editDoctorCommand);
        appCommandProvider.register(CommandName.EDIT_PATIENT, editPatientCommand);
        appCommandProvider.register(CommandName.VISIT_DISPLAY_COMMAND, visitDisplayCommand);
        appCommandProvider.register(CommandName.VIEW_VISIT_DETAILS, viewVisitDetailsCommand);
        appCommandProvider.register(CommandName.CREATE_VISIT_COMMAND, createVisitCommand);
        appCommandProvider.register(CommandName.VIEW_PERSONAL_ACCOUNT, viewPersonalAccountCommand);
        appCommandProvider.register(CommandName.VIEW_PATIENTS_VISIT_LIST, viewPatientsVisitListCommand);
        appCommandProvider.register(CommandName.MANAGE_VISIT_LIST, manageVisitListCommand);
        appCommandProvider.register(CommandName.MANAGE_OUTSTANDING_VISITS, manageOutstandingVisitsCommand);
        appCommandProvider.register(CommandName.MANAGE_DOCTORS_OUTSTANDING_VISITS, manageDoctorsOutstandingVisitsCommand);
        appCommandProvider.register(CommandName.VIEW_ALL_VISITS, viewAllVisitsCommand);
        appCommandProvider.register(CommandName.DISPLAY_EDIT_VISIT_FORM, displayEditVisitFormCommand);
        appCommandProvider.register(CommandName.EDIT_VISIT_FORM, editVisitFormCommand);
        appCommandProvider.register(CommandName.TAKE_APPOINTMENT, takeAppointmentCommand);
        appCommandProvider.register(CommandName.FILL_MEDICAL_CARD, fillMedicalCardCommand);
        appCommandProvider.register(CommandName.DISPLAY_MEDICAL_CARD_FORM, displayMedicalCardFormCommand);
        appCommandProvider.register(CommandName.DISPLAY_MEDICAL_CARD_DETAIL, medicalCardDetailCommand);
        appCommandProvider.register(CommandName.MANAGE_DOCTORS_LIST, manegeDoctorsListCommand);
        appCommandProvider.register(CommandName.MANAGE_PATIENTS_LIST, manegePatientsListCommand);
        appCommandProvider.register(CommandName.MANAGE_SCHEDULES_LIST, manageSchedulesListCommand);
        appCommandProvider.register(CommandName.ADD_SCHEDULE, addScheduleCommand);
        appCommandProvider.register(CommandName.CHANGE_PASSWORD, changePasswordCommand);

        LOGGER.info("Registry commands");

        beans.put(DataSource.class, dataSource);
        beans.put(ConnectionManager.class, connectionManager);
        beans.put(TransactionManager.class, transactionManager);
        beans.put(UserDao.class, userDao);
        beans.put(PatientDao.class, patientDao);
        beans.put(DoctorDao.class, doctorDao);
        beans.put(VisitDao.class, visitDao);
        beans.put(TariffDao.class, tariffDao);
        beans.put(MedicalCardDao.class, medicalCardDao);
        beans.put(Icd10Dao.class, icd10Dao);
        beans.put(DayScheduleDao.class, dayScheduleDao);
        beans.put(AppointmentDao.class, appointmentDao);

        beans.put(UserService.class, userProxyService);
        beans.put(PatientService.class, patientProxyService);
        beans.put(DoctorService.class, doctorProxyService);
        beans.put(VisitService.class, visitProxyService);
        beans.put(MedicalCardService.class, medicalCardProxyService);
        beans.put(AppCommandProvider.class, appCommandProvider);


    }

    public void destroy() {
        ApplicationContext context = getInstance();
        DataSource dataSource = context.getBean(DataSource.class);
        dataSource.close();
        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}

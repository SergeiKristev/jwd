package by.kristev.doctor.model;

public enum VisitType {

    URGENT,
    REPEATED,
    SCHEDULED_VISIT;
}

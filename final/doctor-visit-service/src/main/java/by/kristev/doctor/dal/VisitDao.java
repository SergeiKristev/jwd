package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Visit;
import java.util.List;

public interface VisitDao extends CRUDDao<Visit, Long> {

    List<Visit> findVisitsByPatientId (Long patientId) throws DAOException;
    List<Visit> findVisitsByAppointmentStatusName (String appointmentStatus) throws DAOException;
    List<Visit> selectAllOutstandingVisits () throws DAOException;
    List<Visit> selectAllOutstandingVisitsByDoctorId (Long doctorId) throws DAOException;
    List<Visit> selectAllVisitsByDoctorId (Long doctorId) throws DAOException;
    boolean updateAppointmentStatus(Long id) throws DAOException;

}

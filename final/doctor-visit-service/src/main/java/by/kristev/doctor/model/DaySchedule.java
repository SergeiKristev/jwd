package by.kristev.doctor.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class DaySchedule {

    private Long id;
    private Doctor doctor;
    private LocalDate scheduleDate;
    private LocalTime scheduleStartTime;
    private LocalTime scheduleEndTime;


    public DaySchedule() {
    }

    public DaySchedule(Long id, Doctor doctor, LocalDate scheduleDate, LocalTime scheduleStartTime, LocalTime scheduleEndTime) {
        this.id = id;
        this.doctor = doctor;
        this.scheduleDate = scheduleDate;
        this.scheduleStartTime = scheduleStartTime;
        this.scheduleEndTime = scheduleEndTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalTime getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(LocalTime scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public LocalTime getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(LocalTime scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DaySchedule that = (DaySchedule) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(scheduleDate, that.scheduleDate) &&
                Objects.equals(scheduleStartTime, that.scheduleStartTime) &&
                Objects.equals(scheduleEndTime, that.scheduleEndTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, doctor, scheduleDate, scheduleStartTime, scheduleEndTime);
    }

    @Override
    public String toString() {
        return "DaySchedule{" +
                "id=" + id +
                ", doctor=" + doctor +
                ", scheduleDate=" + scheduleDate +
                ", scheduleStartTime=" + scheduleStartTime +
                ", scheduleEndTime=" + scheduleEndTime +
                '}';
    }
}

package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static by.kristev.doctor.ApplicationConstants.*;

public class ConnectionPoolImpl implements ConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPoolImpl.class);

    private final String jdbcUrl;
    private final String user;
    private final String password;
    private final int poolCapacity;

    private static final AtomicBoolean isInstanceExist = new AtomicBoolean(false);
    private final Lock connectionLock = new ReentrantLock();
    private final Condition emptyPool = connectionLock.newCondition();
    private static ConnectionPoolImpl instance;
    private static final Lock instanceLock = new ReentrantLock();

    private final LinkedList<Connection> availableConnections = new LinkedList<>();
    private final LinkedList<Connection> usedConnections = new LinkedList<>();

    private ConnectionPoolImpl() {

        try (InputStream resourceAsStream = ConnectionPoolImpl.class.getClassLoader().
                getResourceAsStream(DATASOURCE_PROPERTIES_PATH)) {
            Properties prop = new Properties();
            prop.load(resourceAsStream);
            String driverClass = prop.getProperty(DATASOURCE_DRIVER);
            jdbcUrl = prop.getProperty(DATASOURCE_URL);
            this.user = prop.getProperty(DATASOURCE_USER);
            this.password = prop.getProperty(DATASOURCE_PASSWORD);
            this.poolCapacity = Integer.parseInt(prop.getProperty(DATASOURCE_POOL_SIZE));

            initDriver(driverClass);
        } catch (IOException ioException) {
            LOGGER.error("Properties didn't load");
            throw new IllegalStateException(ioException.getMessage());
        }
    }

    public static ConnectionPoolImpl getInstance() {
        if (!isInstanceExist.get()) {
            instanceLock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPoolImpl();
                    isInstanceExist.set(true);
                }
            } finally {
                instanceLock.unlock();
            }
        }
        return instance;
    }



    @Override
    public Connection getConnection() throws ConnectionException {
        connectionLock.lock();
        Connection proxyConnection = null;
        try {

            if (availableConnections.isEmpty() && usedConnections.size() == poolCapacity) {
                emptyPool.await();
            }
            if (availableConnections.isEmpty() && usedConnections.size() < poolCapacity) {

                Connection connection = DriverManager.getConnection(jdbcUrl, user, password);

                availableConnections.add(connection);
            } else if (availableConnections.isEmpty()) {
                throw new IllegalStateException("Get Maximum pool size was reached");
            }
            Connection connection = availableConnections.removeFirst();
            usedConnections.add(connection);
            proxyConnection = createProxyConnection(connection);
        } catch (SQLException | InterruptedException e) {
            LOGGER.error("Connection didn't create", e);
            throw new ConnectionException(e.getMessage(), e);
        } finally {
            connectionLock.unlock();
        }
        return proxyConnection;
    }

    private void initDriver(String driverClass) {
        try {
            Class.forName(driverClass);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
            throw new IllegalStateException("Driver cannot be found", e);
        }
    }

    private Connection createProxyConnection(Connection connection) {

        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    void releaseConnection(Connection connection) {
        try {
            connectionLock.lock();
            if (availableConnections.size() >= poolCapacity) {
                throw new IllegalStateException("Release Maximum pool size was reached");
            }
            usedConnections.remove(connection);
            availableConnections.add(connection);
            emptyPool.signal();
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            connectionLock.unlock();
        }
    }


    @Override
    public int getPoolSize() {
        AtomicInteger poolSize = new AtomicInteger(availableConnections.size()
                + usedConnections.size());
        return poolSize.get();
    }

    @Override
    public void closeAll() {
        usedConnections.forEach(this::releaseConnection);
        for (Connection c : availableConnections) {
            try {
                c.close();
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        availableConnections.clear();
        LOGGER.info("close all");
    }

}

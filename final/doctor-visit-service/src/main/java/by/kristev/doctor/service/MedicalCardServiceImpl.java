package by.kristev.doctor.service;

import by.kristev.doctor.dal.*;
import by.kristev.doctor.connection.Transactional;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;

public class MedicalCardServiceImpl implements MedicalCardService {

    private static final Logger LOGGER = LogManager.getLogger(MedicalCardServiceImpl.class);
    private final PatientDao patientDao;
    private final MedicalCardDao medicalCardDao;
    private final Icd10Dao icd10Dao;

    public MedicalCardServiceImpl(PatientDao patientDao, MedicalCardDao medicalCardDao, Icd10Dao icd10Dao) {
        this.patientDao = patientDao;
        this.medicalCardDao = medicalCardDao;
        this.icd10Dao = icd10Dao;
    }

    @Transactional
    @Override
    public Long fillMedicalCard(MedicalCard medicalCard) throws ServiceException {
        try {
            return medicalCardDao.save(medicalCard);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Medical card create exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Icd10 getIcd10byCode(String code) throws ServiceException {
        try {
            return icd10Dao.getIcd10byCode(code);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Icd10 code finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public MedicalCard getMedicalCardByVisitId(Long visitId) throws ServiceException {
        try {
            MedicalCard medicalCard = medicalCardDao.getMedicalCardByVisitId(visitId);
            Patient patient = patientDao.getById(medicalCard.getPatient().getPatientId());
            Icd10 icd10 = icd10Dao.getById(medicalCard.getIcd10Diagnosis().getId());
            medicalCard.setPatient(patient);
            medicalCard.setIcd10Diagnosis(icd10);
            return medicalCard;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Medical card getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }
}

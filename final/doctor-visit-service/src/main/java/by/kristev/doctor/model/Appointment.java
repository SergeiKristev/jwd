package by.kristev.doctor.model;

import java.time.LocalTime;
import java.util.Objects;

public class Appointment {

    private Long id;
    private String name;
    private LocalTime visitStartTime;
    private LocalTime visitEndTime;
    private boolean isFreeTime;
    private DaySchedule daySchedules;

    public Appointment() {
    }

    public Appointment(Long id, String name, LocalTime visitStartTime, LocalTime visitEndTime, boolean isFreeTime, DaySchedule daySchedules) {
        this.id = id;
        this.name = name;
        this.visitStartTime = visitStartTime;
        this.visitEndTime = visitEndTime;
        this.isFreeTime = isFreeTime;
        this.daySchedules = daySchedules;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getVisitStartTime() {
        return visitStartTime;
    }

    public void setVisitStartTime(LocalTime visitStartTime) {
        this.visitStartTime = visitStartTime;
    }

    public LocalTime getVisitEndTime() {
        return visitEndTime;
    }

    public void setVisitEndTime(LocalTime visitEndTime) {
        this.visitEndTime = visitEndTime;
    }

    public boolean isFreeTime() {
        return isFreeTime;
    }

    public void setFreeTime(boolean freeTime) {
        isFreeTime = freeTime;
    }

    public DaySchedule getDaySchedules() {
        return daySchedules;
    }

    public void setDaySchedules(DaySchedule daySchedules) {
        this.daySchedules = daySchedules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment that = (Appointment) o;
        return isFreeTime == that.isFreeTime &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(visitStartTime, that.visitStartTime) &&
                Objects.equals(visitEndTime, that.visitEndTime) &&
                Objects.equals(daySchedules, that.daySchedules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, visitStartTime, visitEndTime, isFreeTime, daySchedules);
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", visitStartTime=" + visitStartTime +
                ", visitEndTime=" + visitEndTime +
                ", isFreeTime=" + isFreeTime +
                ", daySchedules=" + daySchedules +
                '}';
    }
}

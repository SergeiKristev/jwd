package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.service.PatientService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A class for displaying a list of visits to a single patient.
 *
 * @see Command,PatientService
 */
public class ManagePatientsListCommand implements Command {

    private final PatientService patientService;

    /**
     * Initializing of the command
     *
     * @param patientService - service of the patient objects
     */
    public ManagePatientsListCommand(PatientService patientService) {
        this.patientService = patientService;
    }

    /**
     * Displaying the list of patients
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        List<Patient> patientList = patientService.findAllPatients();

        request.setAttribute("patientList", patientList);
        request.setAttribute("pageName", "page.manage_patients_list");
        return "manage_patients_list";    }
}

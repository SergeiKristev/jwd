package by.kristev.doctor.dal.impl;

import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.DoctorDao;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DoctorDaoImpl implements DoctorDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.doctor where user_id = user_account_id and is_active = 1";
    private static final String SELECT_BY_DOCTOR_ID_QUERY = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.doctor where user_id = user_account_id and doctor_id = ?;";
    private static final String SELECT_BY_USER_ID_QUERY = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.doctor where user_id = user_account_id and user_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`doctor` (`user_account_id`, `speciality`, `medical_category`, `experience`) VALUES (?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`doctor` SET `speciality` = ?, `medical_category` = ?, `experience` = ? WHERE (`user_account_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM doctor_home_visit_service.doctor WHERE doctor_id = ?";
    private static final String SELECT_FREE_BY_SPECIALITY_AND_SCHEDULES = "SELECT distinct d.doctor_id FROM doctor_home_visit_service.appointment ap join day_schedule ds \n" +
            "on ap.day_schedule_id = ds.day_schedule_id join doctor d ON ds.doctor_id = d.doctor_id where isFreeTime = '1' and schedule_date = ?\n" +
            "and d.speciality = ?";


    private static final Logger LOGGER = LogManager.getLogger(DoctorDaoImpl.class);
    private final ConnectionManager connectionManager;
    private final EntityParser<Doctor> doctorEntityParser;

    public DoctorDaoImpl(ConnectionManager connectionManager, EntityParser<Doctor> doctorEntityParser) {
        this.connectionManager = connectionManager;
        this.doctorEntityParser = doctorEntityParser;
    }

    @Override
    public Doctor getDoctorByUserId(Long userId) throws DAOException {
        AtomicReference<Doctor> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_USER_ID_QUERY)) {
            selectStmt.setLong(1, userId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Doctor doctor = doctorEntityParser.createEntityFromResultSet(resultSet);
                    result.set(doctor);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public Long registerNewDoctor(Doctor doctor, Long userId) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setLong(++i, userId);
            insertStmt.setString(++i, doctor.getSpeciality().name());
            insertStmt.setString(++i, doctor.getMedicalCategory().name());
            insertStmt.setInt(++i, doctor.getExperience());


            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    generatedKeys.getLong(1);
                } else {
                    LOGGER.error("Creating doctor failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return userId;
    }

    @Override
    public Long save(Doctor doctor) throws DAOException {
        throw new DAOException("Creating doctor failed, no ID obtained.");
    }

    @Override
    public boolean update(Doctor doctor) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            doctorEntityParser.parseEntityToStatement(doctor, updateStmt);
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(Doctor doctor) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, doctor.getDoctorId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public Doctor getById(Long id) throws DAOException {
        AtomicReference<Doctor> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_DOCTOR_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Doctor doctor = doctorEntityParser.createEntityFromResultSet(resultSet);
                    result.set(doctor);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<Doctor> findAll() throws DAOException {
        List<Doctor> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Doctor doctor = doctorEntityParser.createEntityFromResultSet(resultSet);
                    result.add(doctor);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public List<Long> findFreeDoctorsIdBySchedulesAndSpeciality(LocalDate scheduleDate, Speciality speciality) throws
            DAOException {
        List<Long> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_FREE_BY_SPECIALITY_AND_SCHEDULES)) {
            selectStmt.setString(1, scheduleDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            selectStmt.setString(2, speciality.name());
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Long doctorId = resultSet.getLong(1);
                    result.add(doctorId);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding doctor failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }


}

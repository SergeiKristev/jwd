package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.DaySchedule;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.Speciality;

import java.time.LocalDate;
import java.util.List;

public interface DoctorDao extends CRUDDao<Doctor, Long>{

    Doctor getDoctorByUserId(Long userId) throws DAOException;
    Long registerNewDoctor(Doctor doctor, Long userId) throws DAOException;
    List<Long> findFreeDoctorsIdBySchedulesAndSpeciality(LocalDate scheduleDate, Speciality speciality) throws DAOException;
}

package by.kristev.doctor.validation;

import by.kristev.doctor.model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserValidator implements EntityValidator<User> {

    private static UserValidator instance;

    public static UserValidator getInstance() {

        if (instance == null) {
            instance = new UserValidator();
        }
        return instance;
    }

    private UserValidator() {

    }

    @Override
    public List<String> validate(User user) {
        List<String> validationResult = new ArrayList<>(7);
        if (user.getEmail() == null || !user.getEmail().matches("^([\\w\\d.]+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})$")) {
            validationResult.add("validation.user.email");
        }
        if (user.getPassword() == null || !user.getPassword().matches("^((?=.*\\d)(?=.*[a-zA-Z]).{4,32})$")) {
            validationResult.add("validation.user.password");
        }
        if (user.getPhone() != null && !user.getPhone().isEmpty() &&
                !user.getPhone().matches("8\\([0-9]{3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}")) {
            validationResult.add("validation.user.phone");
        }
        if (user.getFirstName() == null || !user.getFirstName().matches("^([\\p{L}\\s]{1,30})$")) {
            validationResult.add("validation.user.first_name");
        }
        if (user.getLastName() == null || !user.getLastName().matches("^([\\p{L}\\s]{1,40})$")) {
            validationResult.add("validation.user.last_name");
        }

        if (user.getGender() == null) {
            validationResult.add("validation.user.gender");
        }

        if (user.getDateOfBirth() == null || user.getDateOfBirth().isAfter(LocalDate.now()) || !user.getDateOfBirth().toString().matches("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])")) {
            validationResult.add("validation.user.date_of_birth");
        }

        if (user.getUserRole() == null) {
            validationResult.add("validation.user.roles");
        }
        return validationResult;
    }
}

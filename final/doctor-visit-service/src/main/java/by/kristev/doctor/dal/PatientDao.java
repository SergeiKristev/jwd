package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Patient;

import java.sql.SQLException;
import java.util.List;

public interface PatientDao extends CRUDDao<Patient, Long> {

    List<Patient> findPatientsByPolyclinic(String polyclinic) throws DAOException;

    Long registerNewPatient(Patient patient, Long userId) throws DAOException;

    Patient getPatientByUserId(Long userId) throws DAOException;


}

package by.kristev.doctor.model;

public enum AppointmentStatus {

    WAITING_FOR_CONFIRMATION,
    ACCEPTED_BY_ADMINISTRATOR,
    COMPLETED,
    CANCELED;
}

package by.kristev.doctor.command.patient;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Gender;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import by.kristev.doctor.service.PatientService;
import by.kristev.doctor.service.UserService;
import by.kristev.doctor.validation.PatientValidator;
import by.kristev.doctor.validation.UserValidator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class of the new doctor registration command
 *
 * @see Command,PatientService,UserService,UserValidator,PatientValidator
 */
public class RegisterNewPatientCommand implements Command {

    private final UserService userService;
    private final PatientService patientService;
    private final UserValidator userValidator;
    private final PatientValidator patientValidator;

    /**
     * Initializing of the command
     *
     * @param userService - service of the user objects
     * @param patientService - service of the patient objects
     */
    public RegisterNewPatientCommand(UserService userService, PatientService patientService) {
        this.userService = userService;
        this.patientService = patientService;
        this.userValidator = UserValidator.getInstance();
        this.patientValidator = PatientValidator.getInstance();
    }

    /**
     * Executes the new patient registration. We get the data needed to create the patient object from the patient
     * register form. After that we perform input data validation. If validation is successful,
     * we create <i>User</i> throw the <i>UserService</i>. Getting the new user's ID. Creating and validating
     * a new patient object. If there is invalid data, we set the attribute of the <i>validationResult</i> object and
     * forward to the patient's registration page. Using the received ID of the new user, we register the patient
     * through the <i>PatientService</i>.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to the page with all patient's list or to the index page. Name of the jsp view.
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ServiceException {

        String email = request.getParameter("user.email");
        String password = request.getParameter("user.password");
        String confirmPassword = request.getParameter("user.confirm_password");
        String phone = request.getParameter("user.phone");
        String firstName = request.getParameter("user.first_name");
        String lastName = request.getParameter("user.last_name");
        Gender gender = Gender.valueOf(request.getParameter("user.gender"));
        LocalDate localDate = LocalDate.parse(request.getParameter("user.date_of_birth"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        UserRole userRole = UserRole.PATIENT;
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGender(gender);
        user.setDateOfBirth(localDate);
        user.setActive(true);
        user.setUserRole(userRole);

        Patient patient = new Patient();
        String homeAddress = request.getParameter("patient.home_address");
        String polyclinic = request.getParameter("patient.polyclinic");
        String insurance = request.getParameter("patient.insurance");
        patient.setHomeAddress(homeAddress);
        patient.setPolyclinic(polyclinic);
        patient.setInsurance(insurance);

        List<String> validationResult = userValidator.validate(user);

        List<User> users = userService.findAllUsers().stream()
                .filter(u -> u.getEmail().equalsIgnoreCase(email))
                .collect(Collectors.toList());

        if (!users.isEmpty()) {
            validationResult.add("validation.user.is_present");
        } else if (!password.equals(confirmPassword)) {
            validationResult.add("validation.user.password_confirm");
        }

        List<String> validationPatientResult = patientValidator.validate(patient);
        validationResult.addAll(validationPatientResult);

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("user", user);
            request.setAttribute("patient", patient);
            request.setAttribute("pageName", "page.patient_register");
            return "registration_patient";
        }

        Long userId = userService.registerUser(user);
        patientService.registerPatient(patient, userId);

        if (SecurityContext.getInstance().isLoggedIn()) {
            return "redirect:?_command=" + CommandName.MANAGE_PATIENTS_LIST + "&message=patient_registered";
        } else {
            return "redirect:?_command=" + CommandName.INDEX + "&message=patient_registered_log_in";
        }
    }
}

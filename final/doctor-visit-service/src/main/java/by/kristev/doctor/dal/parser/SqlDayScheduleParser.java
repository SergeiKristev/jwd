package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.*;

import javax.security.sasl.SaslClient;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SqlDayScheduleParser implements EntityParser<DaySchedule> {

    private static final String DAY_SCHEDULE_ID = "day_schedule_id";
    private static final String DOCTOR_ID = "doctor_id";
    private static final String SCHEDULE_DATE = "schedule_date";
    private static final String SCHEDULE_START = "schedule_start";
    private static final String SCHEDULE_END = "schedule_end";



    @Override
    public DaySchedule createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long scheduleId = resultSet.getLong(DAY_SCHEDULE_ID);
        LocalDate scheduleDate = LocalDate.parse(resultSet.getString(SCHEDULE_DATE), DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Doctor doctor = new Doctor();
        doctor.setDoctorId(resultSet.getLong(DOCTOR_ID));
        LocalTime scheduleStartTime = LocalTime.parse(resultSet.getString(SCHEDULE_START), DateTimeFormatter.ofPattern("HH:mm:ss"));
        LocalTime scheduleEndTime = LocalTime.parse(resultSet.getString(SCHEDULE_END), DateTimeFormatter.ofPattern("HH:mm:ss"));
        return new DaySchedule(scheduleId, doctor, scheduleDate, scheduleStartTime, scheduleEndTime);
    }

    @Override
    public PreparedStatement parseEntityToStatement(DaySchedule entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setLong(++i, entity.getDoctor().getDoctorId());
        statement.setString(++i, entity.getScheduleDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        statement.setString(++i, entity.getScheduleStartTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        statement.setString(++i, entity.getScheduleEndTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

        return statement;
    }
}

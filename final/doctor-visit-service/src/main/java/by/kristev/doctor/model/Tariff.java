package by.kristev.doctor.model;

import java.util.Objects;

public class Tariff {

    private Long id;
    private String tariffName;
    private double cost;

    public Tariff() {
    }

    public Tariff(Long id, String tariffName, double cost) {
        this.id = id;
        this.tariffName = tariffName;
        this.cost = cost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tariff tariff = (Tariff) o;
        return Double.compare(tariff.cost, cost) == 0 &&
                Objects.equals(id, tariff.id) &&
                Objects.equals(tariffName, tariff.tariffName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tariffName, cost);
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", tariffName='" + tariffName + '\'' +
                ", cost=" + cost +
                '}';
    }
}

package by.kristev.doctor.service;

import by.kristev.doctor.dal.PatientDao;
import by.kristev.doctor.dal.UserDao;
import by.kristev.doctor.connection.Transactional;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;
import java.util.List;

public class PatientServiceImpl implements PatientService {

    private static final Logger LOGGER = LogManager.getLogger(PatientServiceImpl.class);
    private final UserDao userDao;
    private final PatientDao patientDao;

    public PatientServiceImpl(UserDao userDao, PatientDao patientDao) {
        this.userDao = userDao;
        this.patientDao = patientDao;
    }

    @Transactional
    @Override
    public boolean registerPatient(Patient patient, Long userId) throws ServiceException {
        try {
            patientDao.registerNewPatient(patient, userId);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User create exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Patient> findAllPatients() throws ServiceException {
        List<Patient> patientList;
        try {
            patientList = patientDao.findAll();
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return patientList;
    }

    @Override
    public Patient getPatientById(Long patientId) throws ServiceException {

        try {
            return patientDao.getById(patientId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Patient getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public boolean deletePatient(Long patientId) throws ServiceException {
        try {
            Patient patient = getPatientById(patientId);
            User user = userDao.getById(patient.getUserId());
            patientDao.delete(patient);
            userDao.delete(user);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Patient delete exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }


    @Transactional
    @Override
    public boolean update(Patient patient) throws ServiceException {
        try {
            userDao.update(patient);
            patientDao.update(patient);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Patient update exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Patient getPatientByUserId(Long userId) throws ServiceException {
        try {
            return patientDao.getPatientByUserId(userId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Patient getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }
}

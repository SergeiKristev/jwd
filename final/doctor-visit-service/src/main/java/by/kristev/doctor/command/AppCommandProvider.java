package by.kristev.doctor.command;

public interface AppCommandProvider {

    void register(CommandName commandName, Command command);

    void remove(CommandName commandName);

    Command get(CommandName commandName);
}

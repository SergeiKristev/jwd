package by.kristev.doctor.dal.impl;

import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.PatientDao;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class PatientDaoImpl implements PatientDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.user_account u join doctor_home_visit_service.patient p on u.user_id = p.user_account_id where u.is_active = 1";
    private static final String SELECT_BY_PATIENT_ID_QUERY = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.patient where user_id = user_account_id and patient_id = ? and is_active = 1;";
    private static final String INSERT_QUERY = "INSERT INTO doctor_home_visit_service.patient (user_account_id, home_address, polyclinic, insurance) VALUES (?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`patient` SET `home_address` = ?, `polyclinic` = ?, `insurance` = ? WHERE (`user_account_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM `doctor_home_visit_service`.`patient` WHERE (`patient_id` = ? );";
    private static final String SELECT_BY_POLYCLINIC = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.patient where user_id = user_account_id AND polyclinic = ?";
    private static final String SELECT_BY_USER_ID_QUERY = "SELECT * FROM doctor_home_visit_service.user_account join doctor_home_visit_service.patient where user_id = user_account_id and user_id = ?;";


    private static final Logger LOGGER = LogManager.getLogger(PatientDaoImpl.class);

    private final ConnectionManager connectionManager;
    private final EntityParser<Patient> patientParser;


    public PatientDaoImpl(ConnectionManager connectionManager, EntityParser<Patient> patientParser) {
        this.connectionManager = connectionManager;
        this.patientParser = patientParser;
    }

    @Override
    public List<Patient> findPatientsByPolyclinic(String polyclinic) throws DAOException {
        List<Patient> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_POLYCLINIC)) {
            selectStmt.setString(1, polyclinic);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Patient patient = patientParser.createEntityFromResultSet(resultSet);
                    result.add(patient);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding patients failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }


    @Override
    public Long registerNewPatient(Patient patient, Long userId) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStmt.setLong(++i, userId);
            insertStmt.setString(++i, patient.getHomeAddress());
            insertStmt.setString(++i, patient.getPolyclinic());
            insertStmt.setString(++i, patient.getInsurance());

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    patient.setPatientId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Register patient failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Register patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return userId;
    }

    @Override
    public Long save(Patient patient) throws DAOException {

        throw new DAOException("Creating patient failed, no ID obtained.");

    }


    @Override
    public boolean update(Patient patient) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            patientParser.parseEntityToStatement(patient, updateStmt);
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }


    @Override
    public boolean delete(Patient patient) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, patient.getPatientId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public Patient getById(Long id) throws DAOException {
        AtomicReference<Patient> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_PATIENT_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Patient patient = patientParser.createEntityFromResultSet(resultSet);
                    result.set(patient);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<Patient> findAll() throws DAOException {
        List<Patient> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Patient patient = patientParser.createEntityFromResultSet(resultSet);
                    result.add(patient);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding patients failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Patient getPatientByUserId(Long userId) throws DAOException {
        AtomicReference<Patient> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_USER_ID_QUERY)) {
            selectStmt.setLong(1, userId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Patient patient = patientParser.createEntityFromResultSet(resultSet);
                    result.set(patient);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting patient failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }
}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the displaying the doctor's data editing command
 *
 * @see Command,DoctorService
 */
public class DisplayEditDoctorFormCommand implements Command {

    private final DoctorService doctorService;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     */
    public DisplayEditDoctorFormCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
    }


    /**
     * Executes displaying the doctor's data editing form.
     *
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long doctorId = Long.parseLong(request.getParameter("doctor.id"));
        Doctor doctor = doctorService.getDoctorById(doctorId);
        request.setAttribute("doctor", doctor);
        request.setAttribute("pageName", "page.edit_doctor");
        return "edit_doctor";
    }
}

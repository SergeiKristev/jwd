package by.kristev.doctor.filter;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.command.CommandUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter(servletNames = {"index"}, filterName = "security")
public class CommandSecurityFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {


    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) request;
        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.setCurrentSessionId(servletRequest.getSession().getId());
        String command = CommandUtil.getCommandFromRequest((HttpServletRequest) request);
        CommandName commandName = CommandName.of(command);
        
        if (securityContext.canExecute(commandName)) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse) response).sendRedirect("?_command=" + CommandName.INDEX.name());
        }
    }

    @Override
    public void destroy() {

    }
}

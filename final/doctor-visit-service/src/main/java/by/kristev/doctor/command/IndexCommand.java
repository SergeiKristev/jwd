package by.kristev.doctor.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexCommand implements Command{

    @Override
    public String process(HttpServletRequest req, HttpServletResponse resp)  {
        req.setAttribute("pageName", "page.index");
        return "index";
    }
}

package by.kristev.doctor.command.patient;

import by.kristev.doctor.command.Command;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the displaying the patient's registration form command
 *
 * @see Command
 */
public class RegisterNewPatientDisplayCommand implements Command {

    /**
     * Displaying the patient's registration form
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("pageName", "page.patient_register");
        return "registration_patient";
    }
}

package by.kristev.doctor.listener;

import by.kristev.doctor.ApplicationContext;
import by.kristev.doctor.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().initialize();
        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.initialize(servletContextEvent.getServletContext());
        servletContextEvent.getServletContext().setAttribute("securityContext", securityContext);
        LOGGER.info("Context was initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info("Context was destroyed");
    }
}

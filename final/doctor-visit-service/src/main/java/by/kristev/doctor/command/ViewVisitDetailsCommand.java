package by.kristev.doctor.command;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewVisitDetailsCommand implements Command {

    private final VisitService visitService;

    public ViewVisitDetailsCommand(VisitService visitService) {
        this.visitService = visitService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long visitId = Long.parseLong(request.getParameter("visit_id"));

        Visit visit = visitService.getVisitById(visitId);
        request.setAttribute("visit", visit);
        request.setAttribute("pageName", "page.visit_details");
        return "visit_details";
    }
}

package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.DaySchedule;
import by.kristev.doctor.model.Appointment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SqlAppointmentParser implements EntityParser<Appointment> {

    private static final String APPOINTMENT_ID = "appointment_id";
    private static final String NAME = "name";
    private static final String VISIT_START_TIME = "visit_start_time";
    private static final String VISIT_END_TIME = "visit_end_time";
    private static final String IS_FREE_TIME = "isFreeTime";
    private static final String DAY_SCHEDULE_ID = "day_schedule_id";

    @Override
    public Appointment createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long timeIntervalId = resultSet.getLong(APPOINTMENT_ID);
        String name = resultSet.getString(NAME);
        LocalTime visitStartTime = LocalTime.parse(resultSet.getString(VISIT_START_TIME), DateTimeFormatter.ofPattern("HH:mm:ss"));
        LocalTime visitEndTime = LocalTime.parse(resultSet.getString(VISIT_END_TIME), DateTimeFormatter.ofPattern("HH:mm:ss"));
        boolean isFreeTime = resultSet.getBoolean(IS_FREE_TIME);
        Long dayScheduleId = resultSet.getLong(DAY_SCHEDULE_ID);
        DaySchedule daySchedule = new DaySchedule();
        daySchedule.setId(dayScheduleId);

        return new Appointment(timeIntervalId, name, visitStartTime, visitEndTime, isFreeTime, daySchedule);
    }

    @Override
    public PreparedStatement parseEntityToStatement(Appointment entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getName());
        statement.setString(++i, entity.getVisitStartTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        statement.setString(++i, entity.getVisitEndTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        statement.setBoolean(++i, entity.isFreeTime());
        statement.setLong(++i, entity.getDaySchedules().getId());

        return statement;
    }
}

package by.kristev.doctor.model;

public enum UserRole {

    ADMIN,
    PATIENT,
    DOCTOR;
}

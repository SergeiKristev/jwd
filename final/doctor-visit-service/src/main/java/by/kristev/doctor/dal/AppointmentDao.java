package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Appointment;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface AppointmentDao extends CRUDDao<Appointment, Long> {
    Long getDoctorIdFromSelectedAppointment(Long appointmentId) throws DAOException;
    List<Appointment> findAppointmentsByDoctorIdAndAppointmentDate(Long doctorId, LocalDate appointmentDate, LocalTime currentTime) throws DAOException;
    boolean changeAppointmentStatus(Long appointmentId, int status) throws DAOException;
    Long getAppointmentIdByDoctorIdAndAppointmentStatTime(Long doctorId, LocalTime startTime) throws DAOException;
    boolean isBusyTime(Long appointmentId) throws DAOException;
}

package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;

public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = LogManager.getLogger(TransactionManagerImpl.class);
    private final ThreadLocal<Connection> currentConnection = new ThreadLocal<>();
    private final DataSource dataSource;

    public TransactionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void beginTransaction() throws ConnectionException {
        if (this.isEmpty()) {
            try {
                Connection connection = dataSource.getConnection();
                currentConnection.set(connection);
                connection.setAutoCommit(false);
                LOGGER.info("Transaction started");
            } catch (SQLException e) {
                LOGGER.error(MessageFormat.format("Transaction didn`t start: {0}", e.getMessage()));
                throw new ConnectionException(e.getMessage(), e);

            }
        }
    }

    @Override
    public void rollbackTransaction() throws ConnectionException {
        try {
            Connection connection = currentConnection.get();
            connection.rollback();
            connection.setAutoCommit(true);
            LOGGER.info("Transaction rollback");
            this.close();
        } catch (SQLException e) {
            LOGGER.error(MessageFormat.format("Rollback error: {0}", e.getMessage()));
            throw new ConnectionException(e.getMessage(), e);
        }
    }

    @Override
    public void commitTransaction() throws ConnectionException {
        try {
            Connection connection = currentConnection.get();
            connection.commit();
            LOGGER.info("Transaction commit");
            this.close();
        } catch (SQLException e) {
            LOGGER.error(MessageFormat.format("Commit transaction error: {0}", e.getMessage()));
            throw new ConnectionException(e.getMessage(), e);
        }
    }

    @Override
    public Connection getConnection() {
        return currentConnection.get();
    }

    private void close() throws ConnectionException {
        try {
            Connection connection = currentConnection.get();
            connection.setAutoCommit(true);
            connection.close();
            currentConnection.remove();
            LOGGER.info("Transaction closed");
        } catch (SQLException e) {
            LOGGER.error(MessageFormat.format("Transaction closing exception{0}", e));
            throw new ConnectionException(e.getMessage(), e);
        }
    }

    @Override
    public boolean isEmpty() {
        Connection connection = currentConnection.get();
        return connection == null;
    }

}

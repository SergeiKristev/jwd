package by.kristev.doctor.validation;

import by.kristev.doctor.model.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorValidator implements EntityValidator<Doctor> {

    private static DoctorValidator instance;

    public static DoctorValidator getInstance() {

        if (instance == null) {
            instance = new DoctorValidator();
        }
        return instance;
    }

    private DoctorValidator() {

    }

    @Override
    public List<String> validate(Doctor entity) {

        List<String> validationResult = new ArrayList<>(3);
        if (entity.getSpeciality() == null) {
            validationResult.add("validation.doctor.speciality");
        }
        if (entity.getMedicalCategory() == null) {
            validationResult.add("validation.doctor.medical_category");
        }

        if (entity.getExperience() <= 0 || entity.getExperience() > 70) {
            validationResult.add("validation.doctor.experience");
        }

        return validationResult;
    }
}

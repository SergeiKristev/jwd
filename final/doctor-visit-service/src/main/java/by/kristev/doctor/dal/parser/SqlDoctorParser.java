package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SqlDoctorParser implements EntityParser<Doctor> {
    private static final String USER_ID = "user_id";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PASSWORD = "user_password";
    private static final String USER_PHONE = "user_phone";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String GENDER = "gender";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String IS_ACTIVE = "is_active";
    private static final String USER_ROLE = "user_role";
    private static final String DOCTOR_ID = "doctor_id";
    private static final String SPECIALITY = "speciality";
    private static final String MEDICAL_CATEGORY = "medical_category";
    private static final String EXPERIENCE = "experience";

    @Override
    public Doctor createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long userId = resultSet.getLong(USER_ID);
        String email = resultSet.getString(USER_EMAIL);
        String password = resultSet.getString(USER_PASSWORD);
        String phone = resultSet.getString(USER_PHONE);
        String firstName = resultSet.getString(FIRST_NAME);
        String lastName = resultSet.getString(LAST_NAME);
        Gender gender = Gender.valueOf(resultSet.getString(GENDER));
        LocalDate dateOfBirth = LocalDate.parse(resultSet.getString(DATE_OF_BIRTH), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        boolean isActive = resultSet.getBoolean(IS_ACTIVE);
        UserRole userRole = UserRole.valueOf(resultSet.getString(USER_ROLE));
        Long doctorId = resultSet.getLong(DOCTOR_ID);
        Speciality speciality = Speciality.valueOf(resultSet.getString(SPECIALITY));
        MedicalCategory medicalCategory = MedicalCategory.valueOf(resultSet.getString(MEDICAL_CATEGORY));
        int experience = resultSet.getInt(EXPERIENCE);

        return new Doctor(userId, email, password, phone, firstName, lastName, gender, dateOfBirth, isActive, userRole, doctorId, speciality, medicalCategory, experience);
    }

    @Override
    public PreparedStatement parseEntityToStatement(Doctor entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getSpeciality().name());
        statement.setString(++i, entity.getMedicalCategory().name());
        statement.setInt(++i, entity.getExperience());
        statement.setLong(++i, entity.getUserId());
        return statement;
    }
}

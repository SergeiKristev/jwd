package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.Gender;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SqlPatientParser implements EntityParser<Patient> {

    private static final String USER_ID = "user_id";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PASSWORD = "user_password";
    private static final String USER_PHONE = "user_phone";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String GENDER = "gender";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String IS_ACTIVE = "is_active";
    private static final String USER_ROLE = "user_role";
    private static final String PATIENT_ID = "patient_id";
    private static final String HOME_ADDRESS = "home_address";
    private static final String POLYCLINIC = "polyclinic";
    private static final String INSURANCE = "insurance";

    @Override
    public Patient createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long userId = resultSet.getLong(USER_ID);
        String email = resultSet.getString(USER_EMAIL);
        String password = resultSet.getString(USER_PASSWORD);
        String phone = resultSet.getString(USER_PHONE);
        String firstName = resultSet.getString(FIRST_NAME);
        String lastName = resultSet.getString(LAST_NAME);
        Gender gender = Gender.valueOf(resultSet.getString(GENDER));
        LocalDate dateOfBirth = LocalDate.parse(resultSet.getString(DATE_OF_BIRTH), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        boolean isActive = resultSet.getBoolean(IS_ACTIVE);
        UserRole userRole = UserRole.valueOf(resultSet.getString(USER_ROLE));
        Long patientId = resultSet.getLong(PATIENT_ID);
        String homeAddress = resultSet.getString(HOME_ADDRESS);
        String polyclinic = resultSet.getString(POLYCLINIC);
        String insurance = resultSet.getString(INSURANCE);
        return new Patient(userId, email, password, phone, firstName, lastName, gender, dateOfBirth, isActive, userRole, patientId, homeAddress, polyclinic, insurance);
    }

        @Override
        public PreparedStatement parseEntityToStatement (Patient entity, PreparedStatement statement) throws
        SQLException {
            int i = 0;
            statement.setString(++i, entity.getHomeAddress());
            statement.setString(++i, entity.getPolyclinic());
            statement.setString(++i, entity.getInsurance());
            statement.setLong(++i, entity.getUserId());
            return statement;
        }
    }

package by.kristev.doctor.dal.impl;

import by.kristev.doctor.dal.AppointmentDao;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Appointment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AppointmentDaoImpl implements AppointmentDao {
    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.appointment;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.appointment where appointment_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`appointment` (`name`, `visit_start_time`, `visit_end_time`, `isFreeTime`, `day_schedule_id`) VALUES (?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`appointment` SET `name` = ?, `visit_start_time` = ?, `visit_end_time` = ?, `isFreeTime` = ?, `day_schedule_id` = ? WHERE (`appointment_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM `doctor_home_visit_service`.`appointment` WHERE (`appointment_id` = ?);";
    private static final String FIND_DOCTOR_ID_BY_APPOINTMENT_ID = "SELECT doctor_id FROM doctor_home_visit_service.appointment ti join day_schedule ds " +
            "on ti.day_schedule_id = ds.day_schedule_id where appointment_id = ?;";
    private static final String FIND_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE = "SELECT appointment_id, name, visit_start_time, visit_end_time, isFreeTime, ap.day_schedule_id FROM doctor_home_visit_service.appointment ap join doctor_home_visit_service.day_schedule ds on ap.day_schedule_id = ds.day_schedule_id where ds.doctor_id = ? \n" +
            "and isFreeTime = '1' \n" +
            "and ds.schedule_date = ? " +
            "and visit_start_time > ?;";
    private static final String CHANGE_APPOINTMENT_STATUS = "UPDATE `doctor_home_visit_service`.`appointment` SET `isFreeTime` = ? WHERE (`appointment_id` = ?);";
    private static final String FIND_APPOINTMENT_ID_BY_DOCTOR_ID_AND_VISIT_START_TIME = "SELECT appointment_id  FROM doctor_home_visit_service.appointment ap join day_schedule ds on ap.day_schedule_id = ds.day_schedule_id where ds.doctor_id = ? and visit_start_time = ?";
    private static final String IS_BUSY_TIME = "SELECT * FROM doctor_home_visit_service.appointment where appointment_id = ? and isFreeTime = 1;";



    private static final Logger LOGGER = LogManager.getLogger(AppointmentDaoImpl.class);
    private final ConnectionManager connectionManager;
    private final EntityParser<Appointment> visitTimeIntervalEntityParser;

    public AppointmentDaoImpl(ConnectionManager connectionManager, EntityParser<Appointment> visitTimeIntervalEntityParser) {
        this.connectionManager = connectionManager;
        this.visitTimeIntervalEntityParser = visitTimeIntervalEntityParser;
    }

    @Override
    public Long save(Appointment appointment) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            visitTimeIntervalEntityParser.parseEntityToStatement(appointment, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    appointment.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating appointment failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return appointment.getId();
    }

    @Override
    public boolean update(Appointment appointment) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            visitTimeIntervalEntityParser.parseEntityToStatement(appointment, updateStmt);

            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, appointment.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(Appointment appointment) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, appointment.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public Appointment getById(Long id) throws DAOException {
        AtomicReference<Appointment> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Appointment appointment = visitTimeIntervalEntityParser.createEntityFromResultSet(resultSet);
                    result.set(appointment);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<Appointment> findAll() throws DAOException {
        List<Appointment> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Appointment appointment = visitTimeIntervalEntityParser.createEntityFromResultSet(resultSet);
                    result.add(appointment);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding appointments user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Long getDoctorIdFromSelectedAppointment(Long appointmentId) throws DAOException {
        AtomicReference<Long> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(FIND_DOCTOR_ID_BY_APPOINTMENT_ID)) {
            selectStmt.setLong(1, appointmentId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Long doctorId = resultSet.getLong(1);
                    result.set(doctorId);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<Appointment> findAppointmentsByDoctorIdAndAppointmentDate(Long doctorId, LocalDate appointmentDate, LocalTime currentTime) throws DAOException {
        List<Appointment> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(FIND_APPOINTMENTS_BY_DOCTOR_ID_AND_DATE)) {
            selectStmt.setLong(1, doctorId);
            selectStmt.setString(2, appointmentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            selectStmt.setString(3, currentTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Appointment appointment = visitTimeIntervalEntityParser.createEntityFromResultSet(resultSet);
                    result.add(appointment);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public boolean changeAppointmentStatus(Long appointmentId, int status) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement stmt = connection.prepareStatement(CHANGE_APPOINTMENT_STATUS)) {
            int i = 0;
            stmt.setInt(++i, status);
            stmt.setLong(++i, appointmentId);
            stmt.executeUpdate();
            return true;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Changing appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public Long getAppointmentIdByDoctorIdAndAppointmentStatTime(Long doctorId, LocalTime startTime) throws DAOException {
        AtomicReference<Long> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(FIND_APPOINTMENT_ID_BY_DOCTOR_ID_AND_VISIT_START_TIME)) {
            selectStmt.setLong(1, doctorId);
            selectStmt.setString(2, startTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Long timeIntervalId = resultSet.getLong(1);
                    result.set(timeIntervalId);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public boolean isBusyTime(Long appointmentId) throws DAOException {
        List<Long> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(IS_BUSY_TIME)) {
            selectStmt.setLong(1, appointmentId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Long appointment = resultSet.getLong(1);
                    result.add(appointment);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting appointment failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.isEmpty();
    }
}

package by.kristev.doctor.command.patient;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.service.PatientService;
import by.kristev.doctor.service.VisitService;
import by.kristev.doctor.validation.VisitValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * A class for creating a new visit.
 *
 * @see Command,VisitService,DoctorService
 */
public class CreateVisitCommand implements Command {

    private final PatientService patientService;
    private final DoctorService doctorService;
    private final VisitService visitService;
    private final VisitValidator visitValidator;

    /**
     * Initializing of the command
     *
     * @param patientService - service of the patient objects
     * @param doctorService - service of the doctor objects
     * @param visitService - service of the visit objects
     */
    public CreateVisitCommand(PatientService patientService, DoctorService doctorService, VisitService visitService) {
        this.patientService = patientService;
        this.doctorService = doctorService;
        this.visitService = visitService;
        this.visitValidator = VisitValidator.getInstance();
    }

    /**
     * Executes the creation a new visit. We get the data needed to create the visit object from the visit form.
     * After that we perform input data validation. If validation is successful,
     * we create <i>Visit</i> object throw the <i>VisitService</i>. If there is invalid data, we set the attribute of
     * the <i>validationResult</i> object and forward to the visit form page.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to the page with patient's visits list. Name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        User currentUser = SecurityContext.getInstance().getCurrentUser();
        Long doctorId = Long.parseLong(request.getParameter("doctor_id"));
        Patient patient = patientService.getPatientByUserId(currentUser.getUserId());
        LocalDate visitDate = LocalDate.parse(request.getParameter("visit_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Long appointmentId = Long.parseLong(request.getParameter("appointment_id"));
        Appointment appointment = visitService.getAppointmentById(appointmentId);
        LocalTime visitStartTime = appointment.getVisitStartTime();
        LocalDateTime visitDateTime = LocalDateTime.of(visitDate, visitStartTime);
        Doctor doctor = doctorService.getDoctorById(doctorId);
        Tariff tariff = visitService.getTariffByName("BASE");
        String notes = request.getParameter("notes");
        VisitReason visitReason = VisitReason.valueOf(request.getParameter("visit_reason"));

        Visit visit = new Visit();
        visit.setPatient(patient);
        visit.setDoctor(doctor);
        visit.setTariff(tariff);
        visit.setVisitType(VisitType.URGENT);
        visit.setVisitReason(visitReason);

        visit.setVisitDate(visitDateTime);
        visit.setNotes(notes);
        visit.setAppointmentStatus(AppointmentStatus.WAITING_FOR_CONFIRMATION);

        List<String> validationResult = visitValidator.validate(visit);

        if (visitService.isBusyAppointment(appointmentId)) {
            validationResult.add("validation.visit.is_busy");
        }

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("doctor_id", doctorId);
            request.setAttribute("pageName", "page.create_visit");
            return "visit_form";
        }

        visitService.createNewVisit(visit, appointmentId);

        request.getSession().removeAttribute("visitDate");
        request.setAttribute("pageName", "page.create_visit");
        return "redirect:?_command=" + CommandName.VIEW_PATIENTS_VISIT_LIST +  "&message=visit_created";
    }
}

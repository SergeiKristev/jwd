package by.kristev.doctor.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Visit {

    private Long id;
    private Patient patient;
    private VisitReason visitReason;
    private VisitType visitType;
    private LocalDateTime visitDate;
    private Doctor doctor;
    private Tariff tariff;
    private String notes;
    private AppointmentStatus appointmentStatus;

    public Visit() {
    }

    public Visit(Long id, Patient patient, VisitReason visitReason, VisitType visitType, LocalDateTime visitDate, Doctor doctor, Tariff tariff, String notes, AppointmentStatus appointmentStatus) {
        this.id = id;
        this.patient = patient;
        this.visitReason = visitReason;
        this.visitType = visitType;
        this.visitDate = visitDate;
        this.doctor = doctor;
        this.tariff = tariff;
        this.notes = notes;
        this.appointmentStatus = appointmentStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public VisitReason getVisitReason() {
        return visitReason;
    }

    public void setVisitReason(VisitReason visitReason) {
        this.visitReason = visitReason;
    }

    public VisitType getVisitType() {
        return visitType;
    }

    public void setVisitType(VisitType visitType) {
        this.visitType = visitType;
    }

    public LocalDateTime getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDateTime visitDate) {
        this.visitDate = visitDate;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return Objects.equals(id, visit.id) &&
                Objects.equals(patient, visit.patient) &&
                visitReason == visit.visitReason &&
                visitType == visit.visitType &&
                Objects.equals(visitDate, visit.visitDate) &&
                Objects.equals(doctor, visit.doctor) &&
                Objects.equals(tariff, visit.tariff) &&
                Objects.equals(notes, visit.notes) &&
                appointmentStatus == visit.appointmentStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, patient, visitReason, visitType, visitDate, doctor, tariff, notes, appointmentStatus);
    }

    @Override
    public String toString() {
        return "Visit{" +
                "id=" + id +
                ", patient=" + patient +
                ", visitReason='" + visitReason + '\'' +
                ", visitType=" + visitType +
                ", visitDate=" + visitDate +
                ", doctor=" + doctor +
                ", tariff=" + tariff +
                ", notes='" + notes + '\'' +
                ", appointmentStatus=" + appointmentStatus +
                '}';
    }


}


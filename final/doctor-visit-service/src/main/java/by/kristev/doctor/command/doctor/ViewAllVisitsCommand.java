package by.kristev.doctor.command.doctor;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.service.VisitService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A class for displaying the list of all visits to a single doctor.
 *
 * @see Command,VisitService,DoctorService
 */
public class ViewAllVisitsCommand implements Command {

    private final VisitService visitService;
    private final DoctorService doctorService;

    /**
     * Initializing of the command
     *
     * @param visitService - service of the visit objects
     * @param doctorService - service of the doctor objects
     */
    public ViewAllVisitsCommand(VisitService visitService, DoctorService doctorService) {
        this.visitService = visitService;
        this.doctorService = doctorService;
    }

    /**
     * Displaying the list of all visits to a single doctor.
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        User currentUser = SecurityContext.getInstance().getCurrentUser();
        Doctor doctor = doctorService.getDoctorByUserId(currentUser.getUserId());
        List<Visit> visitList = visitService.selectAllVisitsByDoctorId(doctor.getDoctorId());
        request.setAttribute("visitList", visitList);
        request.setAttribute("pageName", "page.visit_list");

        return "manage_visit_list";
    }
}

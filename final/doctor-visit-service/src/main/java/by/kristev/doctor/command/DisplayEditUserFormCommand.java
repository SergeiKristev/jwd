package by.kristev.doctor.command;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.service.PatientService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DisplayEditUserFormCommand implements Command {

    private final PatientService patientService;
    private final DoctorService doctorService;

    public DisplayEditUserFormCommand(PatientService patientService, DoctorService doctorService) {
        this.patientService = patientService;
        this.doctorService = doctorService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        User currentUser = SecurityContext.getInstance().getCurrentUser();
        Long userId = currentUser.getUserId();

        if (currentUser.getUserRole().equals(UserRole.PATIENT)) {
            Patient patient = patientService.getPatientByUserId(userId);
            request.setAttribute("patient", patient);
            request.setAttribute("pageName", "page.edit_patient");
            return "edit_patient";
        } else if(currentUser.getUserRole().equals(UserRole.DOCTOR)) {
            Doctor doctor  = doctorService.getDoctorByUserId(userId);
            request.setAttribute("doctor", doctor);
            request.setAttribute("pageName", "page.edit_doctor");
            return "edit_doctor";
        } else {
            //TODO create admin edit page
            request.setAttribute("user_first_name", currentUser.getFirstName());
            request.setAttribute("pageName", "page.personal_account");
            return "personal_account";
        }

    }
}

package by.kristev.doctor.validation;

import by.kristev.doctor.model.MedicalCard;

import java.util.ArrayList;
import java.util.List;

public class MedicalCardValidator implements EntityValidator<MedicalCard> {

    private static MedicalCardValidator instance;

    public static MedicalCardValidator getInstance() {

        if (instance == null) {
            instance = new MedicalCardValidator();
        }
        return instance;
    }

    private MedicalCardValidator() {

    }

    @Override
    public List<String> validate(MedicalCard entity) {
        List<String> validationResult = new ArrayList<>(7);
        if (entity.getPatient() == null) {
            validationResult.add("validation.medical_card.patient");
        }
        if (entity.getVisit() == null) {
            validationResult.add("validation.medical_card.visit");
        }
        if (entity.getSymptoms() == null || entity.getSymptoms().length() == 0 || entity.getSymptoms().length() > 500) {
            validationResult.add("validation.medical_card.symptoms");
        }
        if (entity.getMedicalExamination() == null || entity.getMedicalExamination().length() == 0 || entity.getMedicalExamination().length() > 1000) {
            validationResult.add("validation.medical_card.examination");
        }
        if (entity.getClinicalDiagnosis() == null || entity.getClinicalDiagnosis().length() <= 0 || entity.getClinicalDiagnosis().length() > 250) {
            validationResult.add("validation.medical_card.clinical_diagnosis");
        }

        if (entity.getIcd10Diagnosis() == null || !entity.getIcd10Diagnosis().getCode().matches("^[A-Z]{1}[0-9]{2}[.]{1}[0-9]{1}$")) {
            validationResult.add("validation.medical_card.icd10_diagnosis");
        }

        if (entity.getRecommendations() == null || entity.getRecommendations().length() == 0 || entity.getRecommendations().length() > 1000) {
            validationResult.add("validation.medical_card.recommendations");
        }
        return validationResult;
    }
}

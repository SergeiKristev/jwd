package by.kristev.doctor.dal.impl;

import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.UserDao;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class UserDaoImpl implements UserDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.user_account";
    private static final String SELECT_BY_ID_QUERY = "SELECT *  FROM doctor_home_visit_service.user_account WHERE user_id = ?";
    private static final String SELECT_BY_LOGIN_QUERY = "SELECT *  FROM doctor_home_visit_service.user_account WHERE user_email = ?";
    private static final String INSERT_QUERY = "INSERT INTO doctor_home_visit_service.user_account (user_email, user_password, user_phone, first_name, last_name, gender, date_of_birth, is_active, user_role ) VALUES (?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_QUERY = "UPDATE doctor_home_visit_service.user_account SET user_email=?, user_password=?, user_phone=?, first_name=?, last_name=?, gender=?, date_of_birth=?, is_active=?, user_role =? WHERE user_id = ?";
    private static final String DELETE_QUERY = "DELETE FROM doctor_home_visit_service.user_account WHERE user_id = ?";
    private static final String DISABLE_QUERY = "UPDATE `doctor_home_visit_service`.`user_account` SET `is_active` = '0' WHERE (`user_id` = ?);";
    private static final String FIND_USERS_BY_ROLE_NAME = "SELECT * FROM doctor_home_visit_service.user_account WHERE user_role = ?";
    private static final String ASSIGN_ROLE = "UPDATE doctor_home_visit_service.user_account SET user_role =? WHERE user_id = ?";
    private static final String GET_USER_ROLE = "SELECT user_role FROM doctor_home_visit_service.user_account WHERE user_id = ?";


    private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);
    private final ConnectionManager connectionManager;
    private final EntityParser<User> entityParser;

    public UserDaoImpl(ConnectionManager connectionManager, EntityParser<User> entityParser) {
        this.connectionManager = connectionManager;
        this.entityParser = entityParser;
    }

    @Override
    public Long save(User user) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            entityParser.parseEntityToStatement(user, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setUserId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return user.getUserId();
    }

    @Override
    public boolean update(User user) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            entityParser.parseEntityToStatement(user, updateStmt);
            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, user.getUserId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean disable(Long userId) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DISABLE_QUERY)) {
            updateStmt.setLong(1, userId);
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Disable user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(User user) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, user.getUserId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

    }

    @Override
    public User getById(Long id) throws DAOException {

        AtomicReference<User> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    User user = entityParser.createEntityFromResultSet(resultSet);
                    result.set(user);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting user by id failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();

    }

    @Override
    public List<User> findAll() throws DAOException {
        List<User> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    User user = entityParser.createEntityFromResultSet(resultSet);
                    result.add(user);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding user failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Optional<User> findByLogin(String login) throws DAOException {

        List<User> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            selectStmt.setString(1, login);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    User user = entityParser.createEntityFromResultSet(resultSet);
                    result.add(user);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding user by login failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return result.stream().findFirst();
    }

    public List<User> findUsersByRoleName(UserRole userRoleName) throws DAOException {
        List<User> users = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement stmt = connection.prepareStatement(FIND_USERS_BY_ROLE_NAME)) {
            stmt.setString(1, userRoleName.name());
            try (ResultSet resultSet = stmt.executeQuery()) {
                while (resultSet.next()) {
                    User user = entityParser.createEntityFromResultSet(resultSet);
                    users.add(user);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding user by role name failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return users;
    }


    @Override
    public void assignRole(Long userId, UserRole userRole) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement stmt = connection.prepareStatement(ASSIGN_ROLE)) {
            int i = 0;

            stmt.setString(++i, userRole.name());
            stmt.setLong(++i, userId);
            stmt.executeUpdate();
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Assigning role failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public UserRole getUserRoleById(Long userId) throws DAOException {

        AtomicReference<UserRole> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(GET_USER_ROLE)) {
            selectStmt.setLong(1, userId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    UserRole userRole = UserRole.valueOf(resultSet.getString("user_role"));
                    result.set(userRole);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting user by role name failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

}

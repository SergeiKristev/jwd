package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;

import java.util.List;
import java.util.Optional;

public interface UserDao extends CRUDDao<User, Long> {

    Optional<User> findByLogin(String login) throws DAOException;

    List<User> findUsersByRoleName(UserRole userRoleName) throws DAOException;

    void assignRole(Long userId, UserRole userRole) throws DAOException;

    UserRole getUserRoleById(Long userId) throws DAOException;

    boolean disable(Long userId) throws DAOException;
}

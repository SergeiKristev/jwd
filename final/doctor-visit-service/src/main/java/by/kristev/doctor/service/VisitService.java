package by.kristev.doctor.service;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Tariff;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.model.Appointment;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface VisitService {

    List<Visit> findAllVisits() throws ServiceException;

    Long createNewVisit(Visit visit, Long appointmentId) throws ServiceException;

    List<Appointment> findAppointmentsByDoctorIdAndAppointmentDate(Long doctorId, LocalDate appointmentDate) throws ServiceException;

    Tariff getTariffByName(String tariffName) throws ServiceException;

    Appointment getAppointmentById(Long appointmentId) throws ServiceException;

    boolean isBusyAppointment(Long appointmentId) throws ServiceException;

    List<Visit> findVisitsByPatientId(Long patientId) throws ServiceException;

    Visit getVisitById(Long visitId) throws ServiceException;

    boolean updateVisit(Visit visit) throws ServiceException;

    List<Visit> selectAllOutstandingVisits() throws ServiceException;

    List<Visit> selectAllOutstandingVisitsByDoctorId(Long doctorId) throws ServiceException;

    List<Visit> selectAllVisitsByDoctorId(Long doctorId) throws ServiceException;

    boolean releaseAppointment(Long doctorId, LocalTime startTime) throws ServiceException;

}

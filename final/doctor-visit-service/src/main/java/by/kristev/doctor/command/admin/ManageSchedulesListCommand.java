package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.DaySchedule;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.service.DoctorService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * A class for displaying a list of schedules.
 *
 * @see Command,DoctorService
 */
public class ManageSchedulesListCommand implements Command {
    private final DoctorService doctorService;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     */
    public ManageSchedulesListCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    /**
     *
     * Executes displaying the doctor's duty schedule with the passed doctor's ID in the specified date range
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Long doctorId = Long.parseLong(request.getParameter("doctor_id"));
        Doctor doctor = doctorService.getDoctorById(doctorId);
        LocalDate from = LocalDate.parse(request.getParameter("date_from"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate to = LocalDate.parse(request.getParameter("date_to"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<DaySchedule> dayScheduleList = doctorService.getDayScheduleList(doctorId, from, to);

        request.setAttribute("doctor", doctor);
        request.setAttribute("dayScheduleList", dayScheduleList);
        request.setAttribute("date_from", from);
        request.setAttribute("date_to", to);
        request.setAttribute("doctor_id", doctorId);
        request.setAttribute("pageName", "page.schedules_list");
        return "schedule_list";
    }
}

package by.kristev.doctor.service;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;

import java.time.LocalDate;
import java.util.List;

public interface DoctorService {
    Doctor getDoctorByUserId(Long userId) throws ServiceException;

    List<Doctor> findAllDoctors() throws ServiceException;

    boolean registerDoctor(Doctor doctor, Long userId) throws ServiceException;

    List<Doctor> findAllFreeDoctorsByScheduleAndSpeciality(LocalDate scheduleDate, Speciality speciality) throws ServiceException;

    Doctor getDoctorById(Long doctorId) throws ServiceException;

    boolean deleteDoctor(Long doctorId) throws ServiceException;

    boolean update(Doctor doctor) throws ServiceException;

    Long createDaySchedule(DaySchedule daySchedule) throws ServiceException;

    Long createAppointment(Appointment appointment) throws ServiceException;

    List<DaySchedule> getDayScheduleList(Long doctorId, LocalDate from, LocalDate to) throws ServiceException;

    boolean isFreeDutyDate(Long doctorId, LocalDate dutyDate) throws ServiceException;
}

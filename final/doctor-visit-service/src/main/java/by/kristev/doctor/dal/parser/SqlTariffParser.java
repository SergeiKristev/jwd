package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.Tariff;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlTariffParser implements EntityParser<Tariff> {

    private static final String TARIFF_ID = "tariff_id";
    private static final String TARIFF_NAME = "tariff_name";
    private static final String COST = "cost";

    @Override
    public Tariff createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long tariffId = resultSet.getLong(TARIFF_ID);
        String tariffName = resultSet.getString(TARIFF_NAME);
        double cost = resultSet.getDouble(COST);
        return new Tariff(tariffId, tariffName, cost);
    }

    @Override
    public PreparedStatement parseEntityToStatement(Tariff entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getTariffName());
        statement.setDouble(++i, entity.getCost());

        return statement;
    }
}

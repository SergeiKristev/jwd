package by.kristev.doctor.model;

public enum Speciality {

    PEDIATRICIAN,
    THERAPIST,
    NEUROLOGIST;

}

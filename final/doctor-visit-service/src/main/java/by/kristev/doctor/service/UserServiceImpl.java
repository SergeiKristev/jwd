package by.kristev.doctor.service;

import by.kristev.doctor.dal.UserDao;
import by.kristev.doctor.connection.*;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import by.kristev.doctor.utils.EncryptUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {

        this.userDao = userDao;
    }

    @Override
    public List<User> findAllUsers() throws ServiceException {
        try {
            return userDao.findAll();
        } catch (DAOException e) {
            LOGGER.error("Failed to read users", e);
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public User getUserById(Long id) throws ServiceException {

        try {
            return userDao.getById(id);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            User user = getUserById(id);
            user.setPassword(EncryptUtil.encryptString(user.getPassword()));
            userDao.delete(user);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User delete exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }


    @Transactional
    @Override
    public boolean update(User user) throws ServiceException {
        try {
            user.setPassword(EncryptUtil.encryptString(user.getPassword()));
            userDao.update(user);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User update exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public boolean disableUser(Long userId) throws ServiceException {
        try {
            userDao.disable(userId);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User disable exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public Long registerUser(User user) throws ServiceException {
        Long userId;
        try {
            user.setPassword(EncryptUtil.encryptString(user.getPassword()));
            userId = userDao.save(user);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User register exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return userId;
    }

    @Override
    public List<User> getAllUsersByRole(UserRole userRole) throws ServiceException {
        List<User> userList = new ArrayList<>();
        try {
            userList = userDao.findUsersByRoleName(userRole);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User search exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return userList;
    }

    @Override
    public User findUserByEmail(String email) throws ServiceException {
        try {
            return userDao.findByLogin(email).filter(dto -> dto.getEmail().equals(email)).orElseThrow(IllegalArgumentException::new);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("User search exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }
}

package by.kristev.doctor;

import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SecurityContext {

    private static final Logger LOGGER = LogManager.getLogger(SecurityContext.class);
    private static final SecurityContext SECURITY_CONTEXT = new SecurityContext();
    private final Map<String, User> userMap = new ConcurrentHashMap<>(1000);
    private final ThreadLocal<String> currentSessionIdStorage = new ThreadLocal<>();
    private final Properties properties = new Properties();

    public static SecurityContext getInstance() {
        return SECURITY_CONTEXT;
    }

    public void initialize(ServletContext servletContext) {

        try (InputStream propertyStream = SecurityContext.class.getResourceAsStream("/security.properties")) {
            properties.load(propertyStream);
            LOGGER.info("SecurityContext initialized");
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read security properties", e);
        }
    }

    public String getCurrentSessionId() {
        return currentSessionIdStorage.get();
    }

    public void setCurrentSessionId(String sessionId) {
        currentSessionIdStorage.set(sessionId);
    }

    public void login(User user, String sessionId) {
        userMap.put(sessionId, user);
    }

    public User getCurrentUser() {
        String currentSessionId = getCurrentSessionId();
        return currentSessionId != null ? userMap.get(currentSessionId) : null;
    }

    public boolean canExecute(CommandName commandName) {
        User currentUser = getCurrentUser();
        return canExecute(currentUser, commandName);
    }

    public boolean canExecute(User user, CommandName commandName) {

        String commandToRoles = properties.getProperty("command." + commandName.name());
        List<String> roles = Optional.ofNullable(commandToRoles)
                .map(s -> Arrays.asList(s.split(",")))
                .orElseGet(ArrayList::new);

        return (user != null && roles.stream()
                .anyMatch(role -> role.equalsIgnoreCase(user.getUserRole().name()))) || roles.isEmpty();

    }


    public boolean isLoggedIn() {

        return getCurrentUser() != null;
    }


    public void logout(String sessionId) {

        userMap.remove(sessionId);
    }
}

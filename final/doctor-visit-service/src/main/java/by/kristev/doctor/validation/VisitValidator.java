package by.kristev.doctor.validation;

import by.kristev.doctor.model.Visit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VisitValidator implements EntityValidator<Visit> {

    private static VisitValidator instance;

    public static VisitValidator getInstance() {

        if (instance == null) {
            instance = new VisitValidator();
        }
        return instance;
    }

    private VisitValidator() {

    }

    @Override
    public List<String> validate(Visit entity) {

        List<String> validationResult = new ArrayList<>(3);
        if (entity.getVisitDate().toLocalDate().isBefore(LocalDate.now()) || !entity.getVisitDate().toLocalDate().toString().matches("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])")) {
            validationResult.add("validation.user.date_of_visit");
        }
        if (entity.getNotes() != null && entity.getNotes().length() >= 200) {
            validationResult.add("validation.visit.notes");
        }

        if (entity.getVisitReason() == null) {
            validationResult.add("validation.visit.reason");
        }

        return validationResult;
    }
}

package by.kristev.doctor.command;

import java.util.stream.Stream;

public enum CommandName {

    LOG_IN_SAVE,
    LOG_OUT,
    INDEX,
    DISABLE_USER,
    VIEW_PERSONAL_ACCOUNT,
    REGISTER_NEW_PATIENT_DISPLAY,
    REGISTER_NEW_PATIENT,
    REGISTER_NEW_DOCTOR,
    CHANGE_PASSWORD,
    VISIT_DISPLAY_COMMAND,
    CREATE_VISIT_COMMAND,
    VIEW_PATIENTS_VISIT_LIST,
    VIEW_VISIT_DETAILS,
    DISPLAY_EDIT_VISIT_FORM,
    EDIT_VISIT_FORM,
    MANAGE_OUTSTANDING_VISITS,
    MANAGE_DOCTORS_OUTSTANDING_VISITS,
    VIEW_ALL_VISITS,
    MANAGE_VISIT_LIST,
    MANAGE_DOCTORS_LIST,
    DISPLAY_EDIT_DOCTOR_FORM,
    DISPLAY_EDIT_USER_FORM,
    EDIT_DOCTOR,
    EDIT_PATIENT,
    DISPLAY_EDIT_PATIENT_FORM,
    MANAGE_PATIENTS_LIST,
    FILL_MEDICAL_CARD,
    DISPLAY_MEDICAL_CARD_FORM,
    DISPLAY_MEDICAL_CARD_DETAIL,
    MANAGE_SCHEDULES_LIST,
    ADD_SCHEDULE,
    TAKE_APPOINTMENT;


    public static CommandName of(String commandName) {

        return Stream.of(CommandName.values())
                .filter(c -> c.name().equalsIgnoreCase(commandName))
                .findFirst().orElse(INDEX);
    }
}

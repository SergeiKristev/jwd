package by.kristev.doctor.model;

import java.time.LocalDate;
import java.util.Objects;

public class Patient extends User {

    Long patientId;
    private String homeAddress;
    private String polyclinic;
    private String insurance;

    public Patient() {

    }
    public Patient(Long patientId, String homeAddress, String polyclinic, String insurance) {
        this.patientId = patientId;
        this.homeAddress = homeAddress;
        this.polyclinic = polyclinic;
        this.insurance = insurance;
    }

    public Patient(Long userId, String email, String password, String phone, String firstName, String lastName, Gender gender, LocalDate dateOfBirth, boolean isActive, UserRole userRole, Long patientId, String homeAddress, String polyclinic, String insurance) {
        super(userId, email, password, phone, firstName, lastName, gender, dateOfBirth, isActive, userRole);
        this.patientId = patientId;
        this.homeAddress = homeAddress;
        this.polyclinic = polyclinic;
        this.insurance = insurance;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getPolyclinic() {
        return polyclinic;
    }

    public void setPolyclinic(String polyclinic) {
        this.polyclinic = polyclinic;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(patientId, patient.patientId) &&
                Objects.equals(homeAddress, patient.homeAddress) &&
                Objects.equals(polyclinic, patient.polyclinic) &&
                Objects.equals(insurance, patient.insurance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientId, homeAddress, polyclinic, insurance);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "patientId=" + patientId +
                ", homeAddress='" + homeAddress + '\'' +
                ", polyclinic='" + polyclinic + '\'' +
                ", insurance='" + insurance + '\'' +
                "} " + super.toString();
    }
}

package by.kristev.doctor.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;

public class EncryptUtil {

    private static final Logger LOGGER = LogManager.getLogger(EncryptUtil.class);

    private EncryptUtil() {

    }

    public static String encryptString(String toEncryptString) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(toEncryptString.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(MessageFormat.format("Password encrypt exception: {0}", e.getMessage()));
        }
        return generatedPassword;
    }

}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Doctor;
import by.kristev.doctor.service.DoctorService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * A class for displaying the list of doctors
 *
 * @see Command,DoctorService
 */
public class ManageDoctorsListCommand implements Command {

    private final DoctorService doctorService;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     */
    public ManageDoctorsListCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    /**
     * Executes displaying the doctor's list.
     *
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        List<Doctor> doctorList = doctorService.findAllDoctors();

        request.setAttribute("doctorList", doctorList);
        request.setAttribute("pageName", "page.manage_doctors_list");
        return "manage_doctors_list";
    }
}

package by.kristev.doctor.model;

import java.util.Objects;

public class Icd10 {

    private Long id;
    private String name;
    private String code;
    private Long parentId;
    private String parentCode;
    private int nodeCount;
    private String additionalInfo;

    public Icd10() {
    }

    public Icd10(Long id, String name, String code, Long parentId, String parentCode, int nodeCount, String additionalInfo) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.parentId = parentId;
        this.parentCode = parentCode;
        this.nodeCount = nodeCount;
        this.additionalInfo = additionalInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Icd10 icd10 = (Icd10) o;
        return nodeCount == icd10.nodeCount &&
                Objects.equals(id, icd10.id) &&
                Objects.equals(name, icd10.name) &&
                Objects.equals(code, icd10.code) &&
                Objects.equals(parentId, icd10.parentId) &&
                Objects.equals(parentCode, icd10.parentCode) &&
                Objects.equals(additionalInfo, icd10.additionalInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, parentId, parentCode, nodeCount, additionalInfo);
    }

    @Override
    public String toString() {
        return "Icd10{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", parentId=" + parentId +
                ", parentCode='" + parentCode + '\'' +
                ", nodeCount=" + nodeCount +
                ", additionalInfo='" + additionalInfo + '\'' +
                '}';
    }
}

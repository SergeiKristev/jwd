package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.DaySchedule;

import java.time.LocalDate;
import java.util.List;

public interface DayScheduleDao extends CRUDDao<DaySchedule, Long> {

    List<DaySchedule> getDayScheduleList(Long doctorId, LocalDate from, LocalDate to) throws DAOException;
    boolean isFreeDate(Long doctorId, LocalDate dutyDate) throws DAOException;
}

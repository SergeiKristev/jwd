package by.kristev.doctor.dal.impl;

import by.kristev.doctor.dal.MedicalCardDao;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.MedicalCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class MedicalCardDaoImpl implements MedicalCardDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.medical_card;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.medical_card where medical_card_id = ?;";
    private static final String SELECT_BY_VISIT_ID = "SELECT * FROM doctor_home_visit_service.medical_card where visit_id = ?";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`medical_card` (`patient_id`, `visit_id`, `symptoms`, `medical_examination`, `clinical_diagnosis`, `icd_10_id`, `recommendations`) VALUES (?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`medical_card` SET `patient_id` = ?, `visit_id` = ?, `symptoms` = ?, `medical_examination` = ?, `clinical_diagnosis` = ?, `icd_10_id` = ? WHERE (`medical_card_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM `doctor_home_visit_service`.`medical_card` WHERE (`medical_card_id` = ?);";


    private static final Logger LOGGER = LogManager.getLogger(MedicalCardDaoImpl.class);

    private final ConnectionManager connectionManager;
    private final EntityParser<MedicalCard> medicalCardEntityParser;

    public MedicalCardDaoImpl(ConnectionManager connectionManager, EntityParser<MedicalCard> medicalCardEntityParser) {
        this.connectionManager = connectionManager;
        this.medicalCardEntityParser = medicalCardEntityParser;
    }

    @Override
    public Long save(MedicalCard medicalCard) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            medicalCardEntityParser.parseEntityToStatement(medicalCard, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    medicalCard.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating medical card failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating medical card failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return medicalCard.getId();
    }

    @Override
    public boolean update(MedicalCard medicalCard) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            medicalCardEntityParser.parseEntityToStatement(medicalCard, updateStmt);
            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, medicalCard.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating medical card failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(MedicalCard medicalCard) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, medicalCard.getId());
            return updateStmt.executeUpdate() > 0;
        }catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting medical card failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

    }

    @Override
    public MedicalCard getById(Long id) throws DAOException {

        return getMedicalCard(id, SELECT_BY_ID_QUERY);

    }

    @Override
    public List<MedicalCard> findAll() throws DAOException {
        List<MedicalCard> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    MedicalCard medicalCard = medicalCardEntityParser.createEntityFromResultSet(resultSet);
                    result.add(medicalCard);
                }
            }
        }catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding medical card failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public MedicalCard getMedicalCardByVisitId(Long visitId) throws DAOException {
        return getMedicalCard(visitId, SELECT_BY_VISIT_ID);
    }

    private MedicalCard getMedicalCard(Long visitId, String selectByVisitId) throws DAOException {
        AtomicReference<MedicalCard> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(selectByVisitId)) {
            selectStmt.setLong(1, visitId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    MedicalCard medicalCard = medicalCardEntityParser.createEntityFromResultSet(resultSet);
                    result.set(medicalCard);
                }
            }
        }catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting medical card failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }
}

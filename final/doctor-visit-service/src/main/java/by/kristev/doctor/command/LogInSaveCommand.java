package by.kristev.doctor.command;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.service.UserService;
import by.kristev.doctor.utils.EncryptUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LogInSaveCommand implements Command {

    private final UserService userService;

    public LogInSaveCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        String login = request.getParameter("user.login_name");
        String password = request.getParameter("user.password");
        List<User> users = userService.findAllUsers().stream()
                .filter(u -> u.getEmail().equalsIgnoreCase(login) && u.isActive() && u.getPassword().equals(EncryptUtil.encryptString(password)))
                .collect(Collectors.toList());
        if (users.isEmpty()) {
            List<String> validationResult = new ArrayList<>(2);
            validationResult.add("message.login.invalid");
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("pageName", "links.login");
            return "log_in_form";
        } else {
            User user = users.get(0);
            SecurityContext.getInstance().login(user, request.getSession().getId());
        }
        return "redirect:?_command=" + CommandName.VIEW_PERSONAL_ACCOUNT;
    }
}

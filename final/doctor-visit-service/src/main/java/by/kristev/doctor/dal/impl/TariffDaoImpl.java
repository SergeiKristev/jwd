package by.kristev.doctor.dal.impl;

import by.kristev.doctor.dal.TariffDao;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Tariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class TariffDaoImpl implements TariffDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.tariff;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.tariff where tariff_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`tariff` (`tariff_name`, `cost`) VALUES (?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`tariff` SET `tariff_name` = ?, `cost` = ? WHERE (`tariff_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM doctor_home_visit_service.tariff WHERE tariff_id = ?";
    private static final String SELECT_BY_NAME = "SELECT * FROM doctor_home_visit_service.tariff where tariff_name = ?;";


    private static final Logger LOGGER = LogManager.getLogger(TariffDaoImpl.class);

    private final ConnectionManager connectionManager;
    private final EntityParser<Tariff> tariffEntityParser;

    public TariffDaoImpl(ConnectionManager connectionManager, EntityParser<Tariff> tariffEntityParser) {
        this.connectionManager = connectionManager;
        this.tariffEntityParser = tariffEntityParser;
    }

    @Override
    public Long save(Tariff tariff) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            tariffEntityParser.parseEntityToStatement(tariff, insertStmt);

            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tariff.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating tariff failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

        return tariff.getId();
    }

    @Override
    public boolean update(Tariff tariff) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            tariffEntityParser.parseEntityToStatement(tariff, updateStmt);
            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, tariff.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(Tariff tariff) throws DAOException {

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, tariff.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }

    }

    @Override
    public Tariff getById(Long id) throws DAOException {

        AtomicReference<Tariff> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Tariff tariff = tariffEntityParser.createEntityFromResultSet(resultSet);
                    result.set(tariff);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();

    }

    @Override
    public List<Tariff> findAll() throws DAOException {
        List<Tariff> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Tariff tariff = tariffEntityParser.createEntityFromResultSet(resultSet);
                    result.add(tariff);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Tariff getByTariffName(String tariffName) throws DAOException {
        AtomicReference<Tariff> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_NAME)) {
            selectStmt.setString(1, tariffName);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Tariff tariff = tariffEntityParser.createEntityFromResultSet(resultSet);
                    result.set(tariff);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting tariff failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }
}

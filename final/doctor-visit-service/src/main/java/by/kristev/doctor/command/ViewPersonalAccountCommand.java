package by.kristev.doctor.command;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.model.User;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewPersonalAccountCommand implements Command{

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User currentUser = SecurityContext.getInstance().getCurrentUser();
        request.setAttribute("user_first_name", currentUser.getFirstName());
        request.setAttribute("pageName", "page.personal_account");
        return "personal_account";
    }
}

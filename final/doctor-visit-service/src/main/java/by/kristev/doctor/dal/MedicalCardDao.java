package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.MedicalCard;

public interface MedicalCardDao extends CRUDDao<MedicalCard, Long> {

   MedicalCard getMedicalCardByVisitId(Long visitId) throws DAOException;
}


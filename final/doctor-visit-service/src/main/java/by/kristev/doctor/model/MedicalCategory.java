package by.kristev.doctor.model;

public enum MedicalCategory {

    NO_CATEGORY,
    SECOND,
    FIRST,
    HIGHEST;
}

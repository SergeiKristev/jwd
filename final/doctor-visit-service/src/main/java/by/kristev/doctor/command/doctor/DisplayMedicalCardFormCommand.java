package by.kristev.doctor.command.doctor;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the new doctor registration command
 *
 * @see Command,VisitService
 */
public class DisplayMedicalCardFormCommand implements Command {

    private final VisitService visitService;

    /**
     * Initializing of the command
     *
     * @param visitService - service of the visit objects
     */
    public DisplayMedicalCardFormCommand(VisitService visitService) {
        this.visitService = visitService;
    }

    /**
     * Displaying the medical card form
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Long visitId = Long.parseLong(request.getParameter("visit_id"));
        Visit visit = visitService.getVisitById(visitId);
        request.setAttribute("visit", visit);
        request.setAttribute("pageName", "page.medical_card");

        return "medical_card_form";
    }
}

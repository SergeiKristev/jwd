package by.kristev.doctor.controller;

import by.kristev.doctor.ApplicationContext;
import by.kristev.doctor.command.*;
import by.kristev.doctor.exception.CommandProcessException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;


/**
 * Servlet which receives client's requests.
 * @see ApplicationContext,AppCommandProvider
 */
@WebServlet(urlPatterns = "/", name = "index")
public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static AppCommandProvider commandProvider;


    /**
     * Initializing ApplicationContext
     *
     */
    @Override
    public void init() {
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProvider.class);
    }

    /**
     * Represents an GET request that accepts request and response arguments
     * and returns no result.
     *
     * @param req  HttpServletRequest object that contains all the client's request information.
     * @param resp HttpServletResponse object to return information to the client.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = CommandUtil.getCommandFromRequest(req);
        String viewName = "index";
        try {
            viewName = commandProvider.get(CommandName.of(commandName)).apply(req, resp);
            LOGGER.info(MessageFormat.format("ViewName: {0}", viewName));
            if (viewName.startsWith("redirect:")) {
                String redirect = viewName.replace("redirect:", "");
                resp.sendRedirect(redirect);
            } else {
                req.setAttribute("viewName", viewName);
                req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
            }

        } catch (CommandProcessException exception) {
            LOGGER.error("Command process exception ", exception);
            throw new ServletException(exception);
        } catch (Exception exception) {
            LOGGER.error("Servlet exception ", exception);
            req.setAttribute("viewName", viewName);
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    public void destroy() {

    }

}

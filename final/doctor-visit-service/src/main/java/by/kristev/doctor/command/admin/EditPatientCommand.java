package by.kristev.doctor.command.admin;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.PatientService;
import by.kristev.doctor.validation.PatientValidator;
import by.kristev.doctor.validation.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * A class of the displaying the patient's data editing command
 *
 * @see Command,PatientService,PatientValidator
 */
public class EditPatientCommand implements Command {

    private final PatientService patientService;
    private final UserValidator userValidator;
    private final PatientValidator patientValidator;

    /**
     * Initializing of the command
     *
     * @param patientService - service of the patient objects
     */
    public EditPatientCommand(PatientService patientService) {
        this.patientService = patientService;
        this.userValidator = UserValidator.getInstance();
        this.patientValidator = PatientValidator.getInstance();
    }

    /**
     * Executes the editing patients's data. We get the data needed to create the patient object from the patient data
     * editing form. After that we perform input data validation. If validation is successful,
     * we create <i>Patient</i> object and update the data via the <i>PatientService</i>. If we are logged in as
     * an administrator, a redirect occurs to the page displaying the list of patients. If we are logged in as a patient,
     * a redirect occurs to the personal account page.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to a page with a list of all patients or to the personal account page. Name of the jsp view.
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ServiceException {
        User currentUser = SecurityContext.getInstance().getCurrentUser();

        Long patientId = Long.parseLong(request.getParameter("patient.id"));
        Patient patient = patientService.getPatientById(patientId);
        String phone = request.getParameter("user.phone");
        String firstName = request.getParameter("user.first_name");
        String lastName = request.getParameter("user.last_name");
        Gender gender = Gender.valueOf(request.getParameter("user.gender"));
        LocalDate localDate = LocalDate.parse(request.getParameter("user.date_of_birth"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        String homeAddress = request.getParameter("patient.home_address");
        String polyclinic = request.getParameter("patient.polyclinic");
        String insurance = request.getParameter("patient.insurance");
        patient.setPhone(phone);
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setGender(gender);
        patient.setDateOfBirth(localDate);
        patient.setHomeAddress(homeAddress);
        patient.setPolyclinic(polyclinic);
        patient.setInsurance(insurance);

        List<String> validationResult = userValidator.validate(patient);

        List<String> validationPatientResult = patientValidator.validate(patient);
        validationResult.addAll(validationPatientResult);

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("patient", patient);
            request.setAttribute("pageName", "page.manage_patients_list");
            return "edit_patient";
        }

        patientService.update(patient);

        if (currentUser.getUserRole().equals(UserRole.ADMIN)) {
            return "redirect:?_command=" + CommandName.MANAGE_PATIENTS_LIST + "&message=patient_edited";
        } else {
            return "redirect:?_command=" + CommandName.VIEW_PERSONAL_ACCOUNT + "&message=patient_edited";

        }
    }
}

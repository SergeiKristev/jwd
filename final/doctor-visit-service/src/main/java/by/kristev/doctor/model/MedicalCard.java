package by.kristev.doctor.model;

import java.util.Objects;

public class MedicalCard {

    private Long id;
    private Patient patient;
    private Visit visit;
    private String symptoms;
    private String medicalExamination;
    private String clinicalDiagnosis;
    private Icd10 icd10Diagnosis;
    private String recommendations;

    public MedicalCard() {
    }

    public MedicalCard(Long id, Patient patient, Visit visit, String symptoms, String medicalExamination, String clinicalDiagnosis, Icd10 icd10Diagnosis, String recommendations) {
        this.id = id;
        this.patient = patient;
        this.visit = visit;
        this.symptoms = symptoms;
        this.medicalExamination = medicalExamination;
        this.clinicalDiagnosis = clinicalDiagnosis;
        this.icd10Diagnosis = icd10Diagnosis;
        this.recommendations = recommendations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getMedicalExamination() {
        return medicalExamination;
    }

    public void setMedicalExamination(String medicalExamination) {
        this.medicalExamination = medicalExamination;
    }

    public String getClinicalDiagnosis() {
        return clinicalDiagnosis;
    }

    public void setClinicalDiagnosis(String clinicalDiagnosis) {
        this.clinicalDiagnosis = clinicalDiagnosis;
    }

    public Icd10 getIcd10Diagnosis() {
        return icd10Diagnosis;
    }

    public void setIcd10Diagnosis(Icd10 icd10Diagnosis) {
        this.icd10Diagnosis = icd10Diagnosis;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicalCard that = (MedicalCard) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(visit, that.visit) &&
                Objects.equals(symptoms, that.symptoms) &&
                Objects.equals(medicalExamination, that.medicalExamination) &&
                Objects.equals(clinicalDiagnosis, that.clinicalDiagnosis) &&
                Objects.equals(icd10Diagnosis, that.icd10Diagnosis) &&
                Objects.equals(recommendations, that.recommendations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, patient, visit, symptoms, medicalExamination, clinicalDiagnosis, icd10Diagnosis, recommendations);
    }

    @Override
    public String toString() {
        return "MedicalCard{" +
                "id=" + id +
                ", patient=" + patient +
                ", visit=" + visit +
                ", symptoms='" + symptoms + '\'' +
                ", medicalExamination='" + medicalExamination + '\'' +
                ", clinicalDiagnosis='" + clinicalDiagnosis + '\'' +
                ", icd10Diagnosis=" + icd10Diagnosis +
                ", recommendations='" + recommendations + '\'' +
                '}';
    }
}

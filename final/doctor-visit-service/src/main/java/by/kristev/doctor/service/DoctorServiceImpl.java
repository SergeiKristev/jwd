package by.kristev.doctor.service;

import by.kristev.doctor.dal.AppointmentDao;
import by.kristev.doctor.dal.DayScheduleDao;
import by.kristev.doctor.dal.DoctorDao;
import by.kristev.doctor.dal.UserDao;
import by.kristev.doctor.connection.Transactional;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DoctorServiceImpl implements DoctorService {

    private static final Logger LOGGER = LogManager.getLogger(DoctorServiceImpl.class);
    private final UserDao userDao;
    private final DoctorDao doctorDao;
    private final DayScheduleDao dayScheduleDao;
    private final AppointmentDao appointmentDao;

    public DoctorServiceImpl(UserDao userDao, DoctorDao doctorDao, DayScheduleDao dayScheduleDao, AppointmentDao appointmentDao) {
        this.userDao = userDao;
        this.doctorDao = doctorDao;
        this.dayScheduleDao = dayScheduleDao;
        this.appointmentDao = appointmentDao;
    }

    @Override
    public Doctor getDoctorByUserId(Long userId) throws ServiceException {
        try {
            return doctorDao.getDoctorByUserId(userId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Doctor> findAllDoctors() throws ServiceException {
        List<Doctor> doctorList;
        try {
            doctorList = doctorDao.findAll();

        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return doctorList;
    }

    @Transactional
    @Override
    public boolean registerDoctor(Doctor doctor, Long userId) throws ServiceException {
        try {
            doctorDao.registerNewDoctor(doctor, userId);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor create exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Doctor> findAllFreeDoctorsByScheduleAndSpeciality(LocalDate scheduleDate, Speciality speciality) throws ServiceException {
        List<Doctor> doctorList = new ArrayList<>();
        List<Long> doctorsId;
        try {
            doctorsId = doctorDao.findFreeDoctorsIdBySchedulesAndSpeciality(scheduleDate, speciality);
            for (Long doctorId : doctorsId) {
                Doctor doctor = doctorDao.getById(doctorId);
                doctorList.add(doctor);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return doctorList;
    }

    @Override
    public Doctor getDoctorById(Long doctorId) throws ServiceException {

        try {
            return doctorDao.getById(doctorId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public boolean deleteDoctor(Long doctorId) throws ServiceException {
        try {
            Doctor doctor = getDoctorById(doctorId);
            User user = userDao.getById(doctor.getUserId());
            doctorDao.delete(doctor);
            userDao.delete(user);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor delete exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }


    @Transactional
    @Override
    public boolean update(Doctor doctor) throws ServiceException {
        try {
            userDao.update(doctor);
            doctorDao.update(doctor);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Doctor update exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public Long createDaySchedule(DaySchedule daySchedule) throws ServiceException {
        try {
            return dayScheduleDao.save(daySchedule);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Day schedule save exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<DaySchedule> getDayScheduleList(Long doctorId, LocalDate from, LocalDate to) throws ServiceException {
        List<DaySchedule> dayScheduleList;
        Doctor doctor;
        try {
            doctor = doctorDao.getById(doctorId);
            dayScheduleList = dayScheduleDao.getDayScheduleList(doctorId, from, to);
            for (DaySchedule daySchedule : dayScheduleList) {
                daySchedule.setDoctor(doctor);
            }

        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Schedule list getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return dayScheduleList;
    }

    @Override
    public boolean isFreeDutyDate(Long doctorId, LocalDate dutyDate) throws ServiceException {
        try {
            return dayScheduleDao.isFreeDate(doctorId, dutyDate);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Day schedule exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Transactional
    @Override
    public Long createAppointment(Appointment appointment) throws ServiceException {
        try {
            return appointmentDao.save(appointment);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Appointment create exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

}

package by.kristev.doctor.command;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.service.UserService;
import by.kristev.doctor.utils.EncryptUtil;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChangePasswordCommand implements Command {

    private final UserService userService;

    public ChangePasswordCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long userId = Long.parseLong(request.getParameter("user.id"));
        User user = userService.getUserById(userId);
        String oldPassword = request.getParameter("user.old_password");
        String newPassword = request.getParameter("user.new_password");
        String confirmPassword = request.getParameter("user.confirm_password");

        List<String> validationResult = new ArrayList<>(3);

        if (oldPassword == null || !user.getPassword().equals(EncryptUtil.encryptString(oldPassword))) {
            validationResult.add("validation.user.passwords_not_match");
        } else if (newPassword == null || !newPassword.matches("^((?=.*\\d)(?=.*[a-zA-Z]).{4,32})$")) {
            validationResult.add("validation.user.password");
        } else if (!newPassword.equals(confirmPassword)) {
            validationResult.add("validation.user.password_confirm");
        }

        if (validationResult.isEmpty()) {
            user.setPassword(newPassword);
            userService.update(user);
            return "redirect:?_command=" + CommandName.VIEW_PERSONAL_ACCOUNT + "&message=password_changed";

        } else {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("pageName", "page.personal_account");
            request.setAttribute("user_first_name", user.getFirstName());
            return "personal_account";
        }
    }
}

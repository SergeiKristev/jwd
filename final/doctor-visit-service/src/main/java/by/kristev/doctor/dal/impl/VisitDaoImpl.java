package by.kristev.doctor.dal.impl;

import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.VisitDao;
import by.kristev.doctor.dal.parser.EntityParser;
import by.kristev.doctor.exception.ConnectionException;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class VisitDaoImpl implements VisitDao {

    private static final String SELECT_ALL_QUERY = "SELECT * FROM doctor_home_visit_service.visit ORDER BY visit_date DESC;";
    private static final String SELECT_BY_APPOINTMENT_STATUS = "SELECT * FROM doctor_home_visit_service.visit where appointment_status = ?;";
    private static final String FIND_VISIT_BY_PATIENT_ID = "SELECT * FROM doctor_home_visit_service.visit where patient_id = ? ORDER BY visit_date DESC;";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM doctor_home_visit_service.visit where visit_id = ?;";
    private static final String INSERT_QUERY = "INSERT INTO `doctor_home_visit_service`.`visit` (`patient_id`, `visit_reason`, `visit_type`, `visit_date`, `doctor_id`, `tariff_id`, `notes`, `appointment_status`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String UPDATE_QUERY = "UPDATE `doctor_home_visit_service`.`visit` SET `patient_id` = ?, `visit_reason` = ?, `visit_type` = ?, `visit_date` = ?, `doctor_id` = ?, `tariff_id` = ?, `notes` = ?, `appointment_status` = ? WHERE (`visit_id` = ?);";
    private static final String DELETE_QUERY = "DELETE FROM doctor_home_visit_service.visit WHERE visit_id = ?";
    private static final String UPDATE_APPOINTMENT_STATUS = "UPDATE `doctor_home_visit_service`.`visit` SET `appointment_status` = ? WHERE (`visit_id` = ?);";
    private static final String SELECT_ALL_OUTSTANDING_VISITS = "SELECT * FROM doctor_home_visit_service.visit WHERE (appointment_status LIKE 'WAITING_FOR_CONFIRMATION' OR appointment_status LIKE 'ACCEPTED_BY_ADMINISTRATOR') ORDER BY visit_date DESC;";
    private static final String SELECT_ALL_VISITS_BY_DOCTOR_ID = "SELECT * FROM doctor_home_visit_service.visit WHERE doctor_id = ? ORDER BY visit_date DESC;";
    private static final String SELECT_ALL_OUTSTANDING_VISITS_BY_DOCTOR_ID = "SELECT * FROM doctor_home_visit_service.visit WHERE doctor_id = ? AND appointment_status LIKE 'ACCEPTED_BY_ADMINISTRATOR' ORDER BY visit_date DESC;";

    private static final Logger LOGGER = LogManager.getLogger(VisitDaoImpl.class);
    private final ConnectionManager connectionManager;
    private final EntityParser<Visit> visitEntityParser;

    public VisitDaoImpl(ConnectionManager connectionManager, EntityParser<Visit> visitEntityParser) {
        this.connectionManager = connectionManager;
        this.visitEntityParser = visitEntityParser;
    }

    @Override
    public Long save(Visit visit) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStmt = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            visitEntityParser.parseEntityToStatement(visit, insertStmt);
            insertStmt.executeUpdate();
            try (ResultSet generatedKeys = insertStmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    visit.setId(generatedKeys.getLong(1));
                } else {
                    LOGGER.error("Creating visit failed, no ID obtained.");
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Creating visit failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return visit.getId();
    }


    @Override
    public boolean update(Visit visit) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {

            visitEntityParser.parseEntityToStatement(visit, updateStmt);

            int parameterQuantity = updateStmt.getParameterMetaData().getParameterCount();
            updateStmt.setLong(parameterQuantity, visit.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Updating visit failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public boolean delete(Visit visit) throws DAOException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, visit.getId());
            return updateStmt.executeUpdate() > 0;
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Deleting visit failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
    }

    @Override
    public Visit getById(Long id) throws DAOException {
        AtomicReference<Visit> result = new AtomicReference<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Visit visit = visitEntityParser.createEntityFromResultSet(resultSet);
                    result.set(visit);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Getting by id failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result.get();
    }

    @Override
    public List<Visit> findAll() throws DAOException {
        return getVisits(SELECT_ALL_QUERY);
    }

    @Override
    public List<Visit> findVisitsByPatientId(Long patientId) throws DAOException {
        List<Visit> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(FIND_VISIT_BY_PATIENT_ID)) {
            selectStmt.setLong(1, patientId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Visit visit = visitEntityParser.createEntityFromResultSet(resultSet);
                    result.add(visit);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding by patient''s id failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public List<Visit> findVisitsByAppointmentStatusName(String appointmentStatus) throws DAOException {
        List<Visit> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_APPOINTMENT_STATUS)) {
            selectStmt.setString(1, appointmentStatus);
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Visit visit = visitEntityParser.createEntityFromResultSet(resultSet);
                    result.add(visit);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding by appointment status failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public boolean updateAppointmentStatus(Long id) throws DAOException {
        return false;
    }

    @Override
    public List<Visit> selectAllOutstandingVisits() throws DAOException {
        return getVisits(SELECT_ALL_OUTSTANDING_VISITS);
    }

    private List<Visit> getVisits(String selectAllOutstandingVisits) throws DAOException {
        List<Visit> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(selectAllOutstandingVisits)) {
            try (ResultSet resultSet = selectStmt.executeQuery()) {
                while (resultSet.next()) {
                    Visit visit = visitEntityParser.createEntityFromResultSet(resultSet);
                    result.add(visit);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public List<Visit> selectAllOutstandingVisitsByDoctorId(Long doctorId) throws DAOException {
        return getVisits(doctorId, SELECT_ALL_OUTSTANDING_VISITS_BY_DOCTOR_ID);
    }

    private List<Visit> getVisits(Long doctorId, String selectAllOutstandingVisitsByDoctorId) throws DAOException {
        List<Visit> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(selectAllOutstandingVisitsByDoctorId)) {
            selectStmt.setLong(1, doctorId);
            try (ResultSet resultSet = selectStmt.executeQuery()) {

                while (resultSet.next()) {
                    Visit visit = visitEntityParser.createEntityFromResultSet(resultSet);
                    result.add(visit);
                }
            }
        } catch (SQLException | ConnectionException exception) {
            LOGGER.error(MessageFormat.format("Finding failed.{0}", exception.getMessage()));
            throw new DAOException(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public List<Visit> selectAllVisitsByDoctorId(Long doctorId) throws DAOException {
        return getVisits(doctorId, SELECT_ALL_VISITS_BY_DOCTOR_ID);
    }
}

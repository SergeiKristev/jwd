package by.kristev.doctor;

public class ApplicationConstants {

    public static final String COMMAND_PARAM = "_command";

    public static final String DATASOURCE_PROPERTIES_PATH = "datasource.properties";
    public static final String DATASOURCE_DRIVER = "datasource.driver";
    public static final String DATASOURCE_URL = "datasource.url";
    public static final String DATASOURCE_USER = "datasource.userAccount";
    public static final String DATASOURCE_PASSWORD = "datasource.password";
    public static final String DATASOURCE_POOL_SIZE = "datasource.poolSize";

}

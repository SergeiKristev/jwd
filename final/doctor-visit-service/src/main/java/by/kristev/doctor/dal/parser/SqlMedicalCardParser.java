package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlMedicalCardParser implements EntityParser<MedicalCard> {

    private static final String MEDICAL_CARD_ID = "medical_card_id";
    private static final String PATIENT_ID = "patient_id";
    private static final String VISIT_ID = "visit_id";
    private static final String SYMPTOMS = "symptoms";
    private static final String MEDICAL_EXAMINATION = "medical_examination";
    private static final String CLINICAL_DIAGNOSIS = "clinical_diagnosis";
    private static final String ICD_10_ID = "icd_10_id";
    private static final String RECOMMENDATIONS = "recommendations";

    @Override
    public MedicalCard createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long medicalCardId = resultSet.getLong(MEDICAL_CARD_ID);
        Long patientId = resultSet.getLong(PATIENT_ID);
        Long visitId = resultSet.getLong(VISIT_ID);
        String symptoms = resultSet.getString(SYMPTOMS);
        String medicalExamination = resultSet.getString(MEDICAL_EXAMINATION);
        String clinicalDiagnosis = resultSet.getString(CLINICAL_DIAGNOSIS);
        Long icd10Id = resultSet.getLong(ICD_10_ID);
        String recommendations = resultSet.getString(RECOMMENDATIONS);
        Patient patient = new Patient();
        Visit visit = new Visit();
        Icd10 icd10Diagnosis = new Icd10();
        patient.setPatientId(patientId);
        visit.setId(visitId);
        icd10Diagnosis.setId(icd10Id);
        return new MedicalCard(medicalCardId, patient, visit, symptoms, medicalExamination, clinicalDiagnosis, icd10Diagnosis, recommendations);
    }

    @Override
    public PreparedStatement parseEntityToStatement(MedicalCard entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setLong(++i, entity.getPatient().getPatientId());
        statement.setLong(++i, entity.getVisit().getId());
        statement.setString(++i, entity.getSymptoms());
        statement.setString(++i, entity.getMedicalExamination());
        statement.setString(++i, entity.getClinicalDiagnosis());
        statement.setLong(++i, entity.getIcd10Diagnosis().getId());
        statement.setString(++i, entity.getRecommendations());
        return statement;
    }
}

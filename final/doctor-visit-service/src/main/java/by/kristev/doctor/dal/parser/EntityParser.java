package by.kristev.doctor.dal.parser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface EntityParser<T> {

    T createEntityFromResultSet(ResultSet resultSet) throws SQLException;

    PreparedStatement parseEntityToStatement(T entity, PreparedStatement statement) throws SQLException;


}

package by.kristev.doctor.model;

public enum Gender {

    MALE,
    FEMALE;
}

package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.Icd10;
import by.kristev.doctor.model.Tariff;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlIcd10Parser implements EntityParser<Icd10> {

    private static final String ICD10_ID = "icd10_id";
    private static final String NAME = "name";
    private static final String CODE = "code";
    private static final String PARENT_ID = "parent_id";
    private static final String PARENT_CODE = "parent_code";
    private static final String NODE_COUNT = "node_count";
    private static final String ADDITIONAL_INFO = "additional_info";

    @Override
    public Icd10 createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long icd10Id = resultSet.getLong(ICD10_ID);
        String name = resultSet.getString(NAME);
        String code = resultSet.getString(CODE);
        Long parentId = resultSet.getLong(PARENT_ID);
        String parentCode = resultSet.getString(PARENT_CODE);
        int nodeCount = resultSet.getInt(NODE_COUNT);
        String additionalInfo = resultSet.getString(ADDITIONAL_INFO);

        return new Icd10(icd10Id, name, code, parentId, parentCode, nodeCount, additionalInfo);
    }

    @Override
    public PreparedStatement parseEntityToStatement(Icd10 entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getName());
        statement.setString(++i, entity.getCode());
        statement.setLong(++i, entity.getParentId());
        statement.setString(++i, entity.getParentCode());
        statement.setInt(++i, entity.getNodeCount());
        statement.setString(++i, entity.getAdditionalInfo());

        return statement;
    }
}

package by.kristev.doctor.service;

import by.kristev.doctor.dal.*;
import by.kristev.doctor.connection.Transactional;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class VisitServiceImpl implements VisitService {

    private static final Logger LOGGER = LogManager.getLogger(VisitServiceImpl.class);

    private final PatientDao patientDao;
    private final DoctorDao doctorDao;
    private final VisitDao visitDao;
    private final TariffDao tariffDao;
    private final AppointmentDao appointmentDao;

    public VisitServiceImpl(PatientDao patientDao, DoctorDao doctorDao, VisitDao visitDao, TariffDao tariffDao, AppointmentDao appointmentDao) {
        this.patientDao = patientDao;
        this.doctorDao = doctorDao;
        this.visitDao = visitDao;
        this.tariffDao = tariffDao;
        this.appointmentDao = appointmentDao;
    }

    @Override
    public List<Visit> findAllVisits() throws ServiceException {
        List<Visit> visitList;
        try {
            visitList = visitDao.findAll();
            for (Visit visit : visitList) {
                buildVisit(visit);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit list getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitList;
    }

    @Transactional
    @Override
    public Long createNewVisit(Visit visit, Long appointmentId) throws ServiceException {
        Long visitId = 0L;
        try {
            appointmentDao.changeAppointmentStatus(appointmentId, 0);
            visitId = visitDao.save(visit);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit creating exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitId;
    }

    @Override
    public List<Appointment> findAppointmentsByDoctorIdAndAppointmentDate(Long doctorId, LocalDate appointmentDate) throws ServiceException {
        List<Appointment> intervals;
        LocalTime currentTime;
        if (appointmentDate.isEqual(LocalDate.now())) {
            currentTime = LocalTime.now().plusHours(1);
        } else {
            currentTime = LocalTime.of(0,0,0);
        }
        try {
            intervals = appointmentDao.findAppointmentsByDoctorIdAndAppointmentDate(doctorId, appointmentDate, currentTime);

        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Appointments finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return intervals;
    }

    @Override
    public Tariff getTariffByName(String tariffName) throws ServiceException {
        try {
            return tariffDao.getByTariffName(tariffName);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Tariff getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public Appointment getAppointmentById(Long timeIntervalId) throws ServiceException {
        try {
            return appointmentDao.getById(timeIntervalId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Time interval getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }


    @Override
    public boolean isBusyAppointment(Long appointmentId) throws ServiceException {
        try {
            return appointmentDao.isBusyTime(appointmentId);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Appointment changing exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Visit> findVisitsByPatientId(Long patientId) throws ServiceException {
        List<Visit> visitList = new ArrayList<>();
        try {
            visitList = visitDao.findVisitsByPatientId(patientId);
            for (Visit visit : visitList) {
                buildVisit(visit);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitList;
    }

    @Override
    public Visit getVisitById(Long visitId) throws ServiceException {
        Visit visit;
        try {
            visit = visitDao.getById(visitId);
            buildVisit(visit);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit getting exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visit;
    }

    @Transactional
    @Override
    public boolean updateVisit(Visit visit) throws ServiceException {
        try {
            visitDao.update(visit);
            return true;
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit updating exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }

    @Override
    public List<Visit> selectAllOutstandingVisits() throws ServiceException {
        List<Visit> visitList;
        try {
            visitList = visitDao.selectAllOutstandingVisits();
            for (Visit visit : visitList) {
                buildVisit(visit);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitList;
    }

    @Override
    public List<Visit> selectAllOutstandingVisitsByDoctorId(Long doctorId) throws ServiceException {
        List<Visit> visitList;
        try {
            visitList = visitDao.selectAllOutstandingVisitsByDoctorId(doctorId);
            for (Visit visit : visitList) {
                buildVisit(visit);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitList;
    }

    @Override
    public List<Visit> selectAllVisitsByDoctorId(Long doctorId) throws ServiceException {
        List<Visit> visitList;
        try {
            visitList = visitDao.selectAllVisitsByDoctorId(doctorId);
            for (Visit visit : visitList) {
                buildVisit(visit);
            }
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Visit finding exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
        return visitList;
    }

    @Transactional
    @Override
    public boolean releaseAppointment(Long doctorId, LocalTime startTime) throws ServiceException {
        try {
            Long appointmentId = appointmentDao.getAppointmentIdByDoctorIdAndAppointmentStatTime(doctorId, startTime);
            return appointmentDao.changeAppointmentStatus(appointmentId, 1);
        } catch (DAOException e) {
            LOGGER.error(MessageFormat.format("Appointment status updating exception: {0}", e.getMessage()));
            throw new ServiceException(e.getMessage(), e);
        }
    }


    private void buildVisit(Visit visit) throws DAOException {
        Patient patient = patientDao.getById(visit.getPatient().getPatientId());
        Doctor doctor = doctorDao.getById(visit.getDoctor().getDoctorId());
        Tariff tariff = tariffDao.getById(visit.getTariff().getId());
        visit.setPatient(patient);
        visit.setDoctor(doctor);
        visit.setTariff(tariff);
    }
}

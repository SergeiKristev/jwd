package by.kristev.doctor.command.doctor;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.MedicalCardService;
import by.kristev.doctor.service.VisitService;
import by.kristev.doctor.validation.MedicalCardValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class of the new doctor registration command
 *
 * @see Command,VisitService,MedicalCardService,MedicalCardValidator
 */
public class FillingMedicalCardCommand implements Command {


    private final VisitService visitService;
    private final MedicalCardService medicalCardService;
    private final MedicalCardValidator medicalCardValidator;

    /**
     * Initializing of the command
     *
     * @param visitService - service of the visit objects
     * @param medicalCardService - service of the medical card objects
     */
    public FillingMedicalCardCommand(VisitService visitService, MedicalCardService medicalCardService) {
        this.visitService = visitService;
        this.medicalCardService = medicalCardService;
        this.medicalCardValidator = MedicalCardValidator.getInstance();
    }

    /**
     * Executes the filling of patient's medical card. We get the data needed to create the medical card object from
     * the medical card form. After that we perform input data validation. If validation is successful,
     * we create <i>MedicalCard</i> object throw the <i>MedicalCardService</i> and update the visit status
     * to <i>COMPLETED</i> throw the <i>VisitService</i>. If there is invalid data, we set the attribute of the
     * <i>validationResult</i> object and redirect to the page with visit's list. Using the received ID of the new user,
     * we register the doctor through the doctor service.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to the page with all visit's list.  Name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Long visitId = Long.parseLong(request.getParameter("visit_id"));
        Visit visit = visitService.getVisitById(visitId);
        List<String> validationResult = new ArrayList<>();
        MedicalCard medicalCard = new MedicalCard();
        String icd10CodeName = request.getParameter("icd10_diagnosis");
        Icd10 icd10 = medicalCardService.getIcd10byCode(icd10CodeName);

        if (icd10==null) {
            validationResult.add("validation.medical_card.icd10_code_not_found");
        } else {
            medicalCard.setIcd10Diagnosis(icd10);
        }

        String symptoms = request.getParameter("symptoms");
        String medicalExamination = request.getParameter("examination");
        String clinicalDiagnosis = request.getParameter("clinical_diagnosis");
        String recommendations = request.getParameter("recommendations");

        medicalCard.setPatient(visit.getPatient());
        medicalCard.setVisit(visit);
        medicalCard.setSymptoms(symptoms);
        medicalCard.setMedicalExamination(medicalExamination);
        medicalCard.setClinicalDiagnosis(clinicalDiagnosis);
        medicalCard.setRecommendations(recommendations);

        validationResult.addAll(medicalCardValidator.validate(medicalCard));

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("visit", visit);
            request.setAttribute("pageName", "page.medical_card");
            request.setAttribute("medicalCard", medicalCard);
            return "medical_card_form";
        }

        visit.setAppointmentStatus(AppointmentStatus.COMPLETED);
        medicalCardService.fillMedicalCard(medicalCard);
        visitService.updateVisit(visit);

        request.setAttribute("medicalCard", medicalCard);
        request.setAttribute("pageName", "page.medical_card");

        return "redirect:?_command=" + CommandName.VIEW_ALL_VISITS + "&message=med_card_filled";
    }
}

package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;

public interface ConnectionPool {

    Connection getConnection() throws ConnectionException;

    void closeAll();

    int getPoolSize();
}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A class of the displaying the patient's data editing command
 *
 * @see Command,PatientService
 */
public class DisplayEditPatientFormCommand implements Command {

    private final PatientService patientService;

    /**
     * Initializing of the command
     *
     * @param patientService - service of the doctor objects
     */
    public DisplayEditPatientFormCommand(PatientService patientService) {
        this.patientService = patientService;
    }


    /**
     * Executes displaying the patient's data editing form.
     *
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long patientId = Long.parseLong(request.getParameter("patient.id"));
        Patient patient = patientService.getPatientById(patientId);
        request.setAttribute("patient", patient);
        request.setAttribute("pageName", "page.edit_patient");
        return "edit_patient";
    }
}

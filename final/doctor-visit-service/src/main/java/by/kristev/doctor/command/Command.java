package by.kristev.doctor.command;

import by.kristev.doctor.exception.CommandProcessException;
import by.kristev.doctor.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.BiFunction;

@FunctionalInterface
public interface Command extends BiFunction<HttpServletRequest, HttpServletResponse, String> {


    String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException;

    @Override
    default String apply(HttpServletRequest request, HttpServletResponse response) {

        try {
            return process(request, response);
        } catch (Exception e) {
            throw new CommandProcessException("Failed to execute command" + this.getClass().getName(), e);
        }
    }
}

package by.kristev.doctor.command.admin;

import by.kristev.doctor.SecurityContext;
import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.validation.DoctorValidator;
import by.kristev.doctor.validation.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * A class of the displaying the doctor's data editing command
 *
 * @see Command,DoctorService,UserValidator,DoctorValidator
 */
public class EditDoctorCommand implements Command {

    private final DoctorService doctorService;
    private final UserValidator userValidator;
    private final DoctorValidator doctorValidator;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     */
    public EditDoctorCommand(DoctorService doctorService) {
        this.doctorService = doctorService;
        this.userValidator = UserValidator.getInstance();
        this.doctorValidator = DoctorValidator.getInstance();
    }

    /**
     * Executes the editing doctor's data. We get the data needed to create the doctor object from the doctor data
     * editing form. After that we perform input data validation. If validation is successful,
     * we create <i>Doctor</i> object and update the data via the <i>DoctorService</i>. If we are logged in as
     * an administrator, a redirect occurs to the page displaying the list of doctors. If we are logged in as a doctor,
     * a redirect occurs to the personal account page.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to a page with a list of all doctors or to the personal account page. Name of the jsp view.
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ServiceException {

        User currentUser = SecurityContext.getInstance().getCurrentUser();

        Long doctorId = Long.parseLong(request.getParameter("doctor.id"));
        Doctor doctor = doctorService.getDoctorById(doctorId);
        String phone = request.getParameter("user.phone");
        String firstName = request.getParameter("user.first_name");
        String lastName = request.getParameter("user.last_name");
        Gender gender = Gender.valueOf(request.getParameter("user.gender"));
        LocalDate localDate = LocalDate.parse(request.getParameter("user.date_of_birth"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Speciality speciality = Speciality.valueOf(request.getParameter("doctor.speciality"));
        MedicalCategory category = MedicalCategory.valueOf(request.getParameter("doctor.medical_category"));
        int experience = Integer.parseInt(request.getParameter("doctor.experience"));

        doctor.setPhone(phone);
        doctor.setFirstName(firstName);
        doctor.setLastName(lastName);
        doctor.setGender(gender);
        doctor.setDateOfBirth(localDate);

        doctor.setSpeciality(speciality);
        doctor.setMedicalCategory(category);
        doctor.setExperience(experience);

        List<String> validationResult = userValidator.validate(doctor);

        List<String> validationDoctorResult = doctorValidator.validate(doctor);
        validationResult.addAll(validationDoctorResult);

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("doctor", doctor);
            request.setAttribute("pageName", "page.edit_doctor");
            return "edit_doctor";
        }

        doctorService.update(doctor);

        if (currentUser.getUserRole().equals(UserRole.ADMIN)) {
            return "redirect:?_command=" + CommandName.MANAGE_DOCTORS_LIST + "&message=doctor_edited";
        } else {
            return "redirect:?_command=" + CommandName.VIEW_PERSONAL_ACCOUNT + "&message=doctor_edited";
        }
    }
}

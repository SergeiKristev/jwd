package by.kristev.doctor.dal;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.model.Icd10;

import java.util.List;

public interface Icd10Dao extends CRUDDao<Icd10, Long> {

    Icd10 getIcd10byCode(String code) throws DAOException;
}

package by.kristev.doctor.service;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;

import java.util.List;

public interface UserService {

    List<User> findAllUsers() throws ServiceException;

    User getUserById(Long id) throws ServiceException;

    boolean delete(Long id) throws ServiceException;

    boolean update(User user) throws ServiceException;

    boolean disableUser(Long userId) throws ServiceException;

    Long registerUser(User user) throws ServiceException;

    public List<User> getAllUsersByRole(UserRole userRole) throws ServiceException;

    User findUserByEmail(String email) throws ServiceException;

}

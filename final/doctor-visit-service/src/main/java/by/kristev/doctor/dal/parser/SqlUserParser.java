package by.kristev.doctor.dal.parser;

import by.kristev.doctor.model.Gender;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SqlUserParser implements EntityParser<User> {

    private static final String USER_ID = "user_id";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PASSWORD = "user_password";
    private static final String USER_PHONE = "user_phone";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String GENDER = "gender";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String IS_ACTIVE = "is_active";
    private static final String USER_ROLE = "user_role";

    @Override
    public User createEntityFromResultSet(ResultSet resultSet) throws SQLException {
        Long userId = resultSet.getLong(USER_ID);
        String email = resultSet.getString(USER_EMAIL);
        String password = resultSet.getString(USER_PASSWORD);
        String phone = resultSet.getString(USER_PHONE);
        String firstName = resultSet.getString(FIRST_NAME);
        String lastName = resultSet.getString(LAST_NAME);
        Gender gender = Gender.valueOf(resultSet.getString(GENDER));
        LocalDate dateOfBirth = LocalDate.parse(resultSet.getString(DATE_OF_BIRTH), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        boolean isActive = resultSet.getBoolean(IS_ACTIVE);
        UserRole userRole = UserRole.valueOf(resultSet.getString(USER_ROLE));
        return new User(userId, email, password, phone, firstName, lastName, gender, dateOfBirth, isActive, userRole);
    }

    @Override
    public PreparedStatement parseEntityToStatement(User entity, PreparedStatement statement) throws SQLException {
        int i = 0;
        statement.setString(++i, entity.getEmail());
        statement.setString(++i, entity.getPassword());
        statement.setString(++i, entity.getPhone());
        statement.setString(++i, entity.getFirstName());
        statement.setString(++i, entity.getLastName());
        statement.setString(++i, entity.getGender().name());
        statement.setString(++i, entity.getDateOfBirth().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        statement.setBoolean(++i, entity.isActive());
        statement.setString(++i, entity.getUserRole().name());
        return statement;
    }
}

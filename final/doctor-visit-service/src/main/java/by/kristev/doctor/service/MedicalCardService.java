package by.kristev.doctor.service;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Icd10;
import by.kristev.doctor.model.MedicalCard;

public interface MedicalCardService {

    Long fillMedicalCard(MedicalCard medicalCard) throws ServiceException;

    Icd10 getIcd10byCode(String code) throws ServiceException;

    MedicalCard getMedicalCardByVisitId(Long visitId) throws ServiceException;
}

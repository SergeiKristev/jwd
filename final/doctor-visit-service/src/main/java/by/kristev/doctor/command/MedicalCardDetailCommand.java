package by.kristev.doctor.command;

import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.MedicalCard;
import by.kristev.doctor.model.Visit;
import by.kristev.doctor.service.MedicalCardService;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MedicalCardDetailCommand implements Command {

    private final MedicalCardService medicalCardService;
    private final VisitService visitService;

    public MedicalCardDetailCommand(MedicalCardService medicalCardService, VisitService visitService) {
        this.medicalCardService = medicalCardService;
        this.visitService = visitService;
    }

    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {
        Long visitId = Long.parseLong(request.getParameter("visit_id"));
        Visit visit = visitService.getVisitById(visitId);
        MedicalCard medicalCard = medicalCardService.getMedicalCardByVisitId(visitId);
        medicalCard.setVisit(visit);
        request.setAttribute("medicalCard", medicalCard);
        request.setAttribute("pageName", "page.medical_card");
        return "medical_card_detail";
    }
}

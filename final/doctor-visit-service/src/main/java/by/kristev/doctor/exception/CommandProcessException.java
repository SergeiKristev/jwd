package by.kristev.doctor.exception;

public class CommandProcessException extends RuntimeException {

    public CommandProcessException(String message) {
        super(message);
    }

    public CommandProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}

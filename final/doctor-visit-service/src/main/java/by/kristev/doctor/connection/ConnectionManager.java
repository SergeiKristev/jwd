package by.kristev.doctor.connection;

import by.kristev.doctor.exception.ConnectionException;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionManager {

    Connection getConnection() throws ConnectionException, SQLException;

}

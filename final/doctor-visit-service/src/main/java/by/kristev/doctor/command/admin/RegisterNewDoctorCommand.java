package by.kristev.doctor.command.admin;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.command.CommandName;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.service.UserService;
import by.kristev.doctor.validation.DoctorValidator;
import by.kristev.doctor.validation.UserValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class of the new doctor registration command
 *
 * @see Command,DoctorService,UserService,UserValidator,DoctorValidator
 */
public class RegisterNewDoctorCommand implements Command {

    private final UserService userService;
    private final DoctorService doctorService;
    private final UserValidator userValidator;
    private final DoctorValidator doctorValidator;

    /**
     * Initializing of the command
     *
     * @param userService - service of the user objects
     * @param doctorService - service of the doctor objects
     */
    public RegisterNewDoctorCommand(UserService userService, DoctorService doctorService) {
        this.userService = userService;
        this.doctorService = doctorService;
        this.userValidator = UserValidator.getInstance();
        this.doctorValidator = DoctorValidator.getInstance();
    }

    /**
     * Executes the new doctor registration. We get the data needed to create the doctor object from the doctor register
     * form. After that we perform input data validation. If validation is successful,
     * we create <i>User</i> throw the <i>UserService</i>. Getting the new user's ID. Creating and validating
     * a new doctor object. If there is invalid data, we set the attribute of the <i>validationResult</i> object and
     * forward to the doctor's registration page. Using the received ID of the new user, we register the doctor
     * through the doctor service.
     *
     * @param request  - request
     * @param response - response
     * @return redirect to the page with all doctor's list.  Name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ServiceException {

        String email = request.getParameter("user.email");
        String password = request.getParameter("user.password");
        String confirmPassword = request.getParameter("user.confirm_password");
        String phone = request.getParameter("user.phone");
        String firstName = request.getParameter("user.first_name");
        String lastName = request.getParameter("user.last_name");
        Gender gender = Gender.valueOf(request.getParameter("user.gender"));
        LocalDate localDate = LocalDate.parse(request.getParameter("user.date_of_birth"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        UserRole userRole = UserRole.DOCTOR;
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGender(gender);
        user.setDateOfBirth(localDate);
        user.setActive(true);
        user.setUserRole(userRole);

        Doctor doctor = new Doctor();
        Speciality speciality = Speciality.valueOf(request.getParameter("doctor.speciality"));
        MedicalCategory category = MedicalCategory.valueOf(request.getParameter("doctor.medical_category"));
        int experience = Integer.parseInt(request.getParameter("doctor.experience"));
        doctor.setSpeciality(speciality);
        doctor.setMedicalCategory(category);
        doctor.setExperience(experience);

        List<String> validationResult = userValidator.validate(user);

        List<User> users = userService.findAllUsers().stream()
                .filter(u -> u.getEmail().equalsIgnoreCase(email))
                .collect(Collectors.toList());

        if (!users.isEmpty()) {
            validationResult.add("validation.user.is_present");
        } else if(!password.equals(confirmPassword)) {
            validationResult.add("validation.user.password_confirm");
        }

        List<String> validationDoctorResult = doctorValidator.validate(doctor);
        validationResult.addAll(validationDoctorResult);

        if (!validationResult.isEmpty()) {
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("user", user);
            request.setAttribute("doctor", doctor);
            request.setAttribute("pageName", "page.doctor_register");
            return "registration_doctor";
        }

        Long userId = userService.registerUser(user);
        doctorService.registerDoctor(doctor, userId);


        request.setAttribute("validationResult", validationResult);
        request.setAttribute("user", user);
        request.setAttribute("doctor", doctor);
        request.setAttribute("pageName", "page.doctor_register");


        return "redirect:?_command=" + CommandName.MANAGE_DOCTORS_LIST + "&message=doctor_registered";
    }
}

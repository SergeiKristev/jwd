package by.kristev.doctor.command.patient;

import by.kristev.doctor.command.Command;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.*;
import by.kristev.doctor.service.DoctorService;
import by.kristev.doctor.service.VisitService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class for displaying the visit creating form.
 *
 * @see Command,VisitService,DoctorService
 */
public class VisitDisplayCommand implements Command {

    private final DoctorService doctorService;
    private final VisitService visitService;

    /**
     * Initializing of the command
     *
     * @param doctorService - service of the doctor objects
     * @param visitService - service of the visit objects
     */
    public VisitDisplayCommand(DoctorService doctorService, VisitService visitService) {
        this.doctorService = doctorService;
        this.visitService = visitService;
    }

    /**
     * Displays the form for creating a visit to the selected doctor on the selected date
     * @param request  - request
     * @param response - response
     * @return name of the jsp view
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ServiceException {

        Speciality speciality = Speciality.valueOf(request.getParameter("doctor.speciality"));

        LocalDate visitDate = LocalDate.parse(request.getParameter("visit_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<String> validationResult = new ArrayList<>(2);

        if (visitDate.isBefore(LocalDate.now()) || !visitDate.toString().matches("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])")) {
            validationResult.add("validation.user.date_of_visit");
            request.setAttribute("validationResult", validationResult);
            request.setAttribute("pageName", "page.create_visit");
            return "take_appointment";
        }

        List<Doctor> doctorList = doctorService.findAllFreeDoctorsByScheduleAndSpeciality(visitDate, speciality);

        Map<Doctor, List<Appointment>> doctorListHashMap = new HashMap<>();
        for (Doctor doctor: doctorList) {
            List<Appointment> appointmentsList = visitService.findAppointmentsByDoctorIdAndAppointmentDate(doctor.getDoctorId(), visitDate);
            doctorListHashMap.put(doctor, appointmentsList);
        }

        request.setAttribute("pageName", "page.visit_list");
        request.setAttribute("doctorsSpeciality", speciality.name());
        request.setAttribute("doctorsList", doctorListHashMap);
        request.setAttribute("visitDate", visitDate);
        return "visit_form";
    }
}

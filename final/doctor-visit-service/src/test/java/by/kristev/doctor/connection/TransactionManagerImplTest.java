package by.kristev.doctor.connection;

import by.kristev.doctor.ApplicationContext;
import by.kristev.doctor.exception.ConnectionException;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Proxy;
import java.sql.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class TransactionManagerImplTest extends TestCase {

    private ConnectionManager connectionManager;
    private static final int N_THREADS = 133;
    private static final int THREAD_SLEEP = 1_000;
    private static final int POOL_CAPACITY = 20;
    private static final Logger LOGGER = LogManager.getLogger(TransactionManagerImplTest.class);

    @Before
    public void setUp() throws Exception {
        ApplicationContext.getInstance().initialize();
        connectionManager = ApplicationContext.getInstance().getBean(ConnectionManager.class);

    }

    @Test
    public void testPoolSize() throws SQLException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(N_THREADS);
        Set<Integer> hashCodes = Collections.synchronizedSet(new HashSet<>());

        IntStream.range(0, N_THREADS).forEach(i -> executorService.submit(() -> {
            LOGGER.info("Try to get connection");
            try (Connection connection = connectionManager.getConnection()) {
                LOGGER.info("working with connection...");
                sleep();
                Assert.assertTrue(connection instanceof Proxy);
                int hashCode = connection.hashCode();
                hashCodes.add(hashCode);
                LOGGER.info("release connection: " + hashCode);
            } catch (SQLException | IllegalStateException | ConnectionException e) {
                LOGGER.error(e);
            }
        }));


        executorService.awaitTermination(N_THREADS / POOL_CAPACITY, TimeUnit.SECONDS);
        Assert.assertTrue(hashCodes.size() <= POOL_CAPACITY);
    }

    private void sleep() {
        try {
            Thread.sleep(Math.abs(new Random().nextInt(THREAD_SLEEP)));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @After
    public void closeConnection() {
        ApplicationContext.getInstance().destroy();
    }

}
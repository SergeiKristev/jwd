package by.kristev.doctor.validation;

import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Gender;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class UserValidatorTest {

    private static final Logger LOGGER = LogManager.getLogger(UserValidatorTest.class);
    private static UserValidator userValidator;

    @BeforeClass
    public static void beforeClass() throws Exception {
        userValidator = UserValidator.getInstance();

    }


    @Test
    public void shouldValidateUser() throws DAOException, ServiceException {
        //5 fields
        String invalidEmail = "testemail.com";
        String invalidPassword = "12345";
        String invalidPhone = "+12345678";
        String invalidFirstName = "12wqd";
        String invalidLastName = "12dwad32";

        String validEmail = "test@email.com";
        String validPassword = "Qwerty12345";
        String validPhone = "8(029)756-66-24";
        String validFirstName = "Ivan";
        String validLastName = "Ivanov";

        User invalidUser = new User();
        invalidUser.setEmail(invalidEmail);
        invalidUser.setPassword(invalidPassword);
        invalidUser.setPhone(invalidPhone);
        invalidUser.setFirstName(invalidFirstName);
        invalidUser.setLastName(invalidLastName);
        invalidUser.setGender(Gender.MALE);
        invalidUser.setDateOfBirth(LocalDate.parse("1991-01-20", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        invalidUser.setActive(true);
        invalidUser.setUserRole(UserRole.ADMIN);

        List<String> validationInvalidUser = userValidator.validate(invalidUser);
        Assert.assertEquals(5, validationInvalidUser.size());

        User validUser = new User();
        validUser.setEmail(validEmail);
        validUser.setPassword(validPassword);
        validUser.setPhone(validPhone);
        validUser.setFirstName(validFirstName);
        validUser.setLastName(validLastName);
        validUser.setGender(Gender.MALE);
        validUser.setDateOfBirth(LocalDate.parse("1991-01-20", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        validUser.setActive(true);
        validUser.setUserRole(UserRole.ADMIN);

        List<String> validationValidUser = userValidator.validate(validUser);
        Assert.assertEquals(0, validationValidUser.size());
    }
}
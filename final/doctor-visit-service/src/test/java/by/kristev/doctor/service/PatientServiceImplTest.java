package by.kristev.doctor.service;

import by.kristev.doctor.ApplicationContext;
import by.kristev.doctor.connection.ConnectionManager;
import by.kristev.doctor.dal.impl.CreateTablesUtil;
import by.kristev.doctor.exception.DAOException;
import by.kristev.doctor.exception.ServiceException;
import by.kristev.doctor.model.Gender;
import by.kristev.doctor.model.Patient;
import by.kristev.doctor.model.User;
import by.kristev.doctor.model.UserRole;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PatientServiceImplTest {

    private static final Logger LOGGER = LogManager.getLogger(PatientServiceImplTest.class);
    private static UserService userService;
    private static PatientService patientService;

    @Before
    public void before() throws Exception {
        ApplicationContext.getInstance().initialize();
        userService = ApplicationContext.getInstance().getBean(UserService.class);
        patientService = ApplicationContext.getInstance().getBean(PatientService.class);
        CreateTablesUtil createTablesUtil = new CreateTablesUtil();
        createTablesUtil.createDbTables();
    }

    @After
    public void after() throws Exception {
        ApplicationContext.getInstance().destroy();
    }

    @Test
    public void shouldRegisterPatient() throws DAOException, ServiceException {
        User user1 = new User();
        user1.setEmail("test@email.com");
        user1.setPassword("12345");
        user1.setPhone("+12345678");
        user1.setFirstName("Test first name");
        user1.setLastName("Test last name");
        user1.setGender(Gender.MALE);
        user1.setDateOfBirth(LocalDate.parse("1991-01-20", DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        user1.setActive(true);
        user1.setUserRole(UserRole.ADMIN);
        Long userId = userService.registerUser(user1);

        Patient patient = new Patient();
        patient.setHomeAddress("ул. Тестовая 3, кв 122");
        patient.setPolyclinic("12");
        patient.setInsurance("128-12234-43");

        patientService.registerPatient(patient, userId);

        List<Patient> patientList = patientService.findAllPatients();
        Assert.assertEquals(1L, patientList.size());
        LOGGER.info("Patient's list size: " + patientList.size());
    }


}
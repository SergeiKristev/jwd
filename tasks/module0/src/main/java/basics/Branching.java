package basics;

import basics.classes.MyPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import static basics.classes.MyPoint.calculateDistance;

public class Branching {
    public static void main(String[] args) {
        //Количество решенных задач [01]Branching: 28
        //Количество решенных задач [01]Cycles: 27
        //Количество решенных задач [01]Linear: 33
        task01(12.35, 45);
        task02(12.35, 45);
        task03(5);
        task04(45, 48);
        task05(25, 12);
        task06(25, 12);
        task07(- 25, 12, -45, 2.0);
        task08(14, 56);
        task09(5, 6, 5);
        task10(124, 48);
        task11(35, 65);
        task12(23, -12, 3);
        task13( 4, 4, 6, 4);
        task14(46, 65);
        task15(4, 6);
        task16(4, - 5);
        task17(45, 67);
        task18(-3,2, 4);
        task19(-3,2, 4);
        task20(3540, 464,456,20);
        task21();
        task22(24, 55);
        task23();
        task24(5);
        task25(64);
        task26(2, 4, 6);
        task27(2, 4, 6, 3);
        //28/29/30
        task31(6, 7, 3, 5, 4);
    }

    private static void task01(double num1,double num2){
        System.out.println("1. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран\n" +
                "цифру 7, в противном случае – цифру 8 \n");

        System.out.println("Введенные числа: " + num1 + ", " + num2);

        if (num1 < num2){
            System.out.println("7");
        } else {
            System.out.println("8");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task02(double num1,double num2){
        System.out.println("2. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран\n" +
                "слово «yes», в противном случае – слово «no» \n");

        System.out.println("Введенные числа: " + num1 + ", " + num2);

        if (num1 < num2){
            System.out.println("yes");
        } else {
            System.out.println("no");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task03(double num){
        System.out.println("3. Составить программу сравнения введенного числа а и цифры 3. Вывести на экран слово «yes»,\n" +
                "если число а меньше 3; если больше, то вывести слово «no».\n");

        System.out.println("Введенныое число: " + num);

        if (num < 3){
            System.out.println("yes");
        } else {
            System.out.println("no");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task04(double num1, double num2) {
        System.out.println("4. Составить программу: равны ли два числа а и b?\n");

        System.out.println("Введенные числа: " + num1 + ", " + num2);

        if (num1 == num2){
            System.out.println("Числа равны");
        } else {
            System.out.println("Числа не равны");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task05(double num1, double num2) {
        System.out.println("5. Составить программу: определения наименьшего из двух чисел а и b.\n");

        System.out.println("Введенные числа: " + num1 + ", " + num2);

        double min = (num1 < num2) ? num1 : num2;

        if (num1 == num2) {
            System.out.println(" Числа равны");
        } else {
            System.out.println("Наименьшее из двух чисел: " + min);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task06(double num1, double num2) {
        System.out.println("6. Составить программу: определения наибольшего из двух чисел а и b.\n");

        double max = (num1 > num2) ? num1 : num2;

        if (num1 == num2) {
            System.out.println(" Числа равны");
        } else {
            System.out.println("Наибольшее из двух чисел: " + max);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task07(double a, double b, double c, double x) {
        System.out.println("7. Составить программу нахождения модуля выражения a*x*x + b*x + c при заданных\n" +
                "значениях a, b, c и х\n");

        double result = a * x * x + b * x + c;

        result = Math.abs(result);

        System.out.println("Введенные числа: a = " + a + ", b = " + b + ", c = " + c + ", x = " + x);
        System.out.println("Модуль выражения равен " + result);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task08(double num1, double num2) {
        System.out.println("8. Составить программу нахождения наименьшего из квадратов двух чисел а и b.\n");

        double result = 0;

        if (num1 < num2){
            result = num1 * num1;
        } else {
            result = num2 * num2;
        }
        System.out.println("Наименьший из квадратов чисел а = " + num1 + " и b = "+ num2 + " равен " + result);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task09(double num1, double num2, double num3) {
        System.out.println("9. Составить программу, которая определит по трем введенным сторонам, является ли данный\n" +
                "треугольник равносторонним\n");

        if (num1 == num2 && num2 == num3){
            System.out.println("Треугольник со сторонами " + num1 + ", " + num2 + ", " + num3 + " является равносторонним");
        } else {
            System.out.println("Треугольник со сторонами " + num1 + ", " + num2 + ", " + num3 + " не является равносторонним");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task10(double area1, double area2) {
        System.out.println("10. Составить программу, которая определит площадь какого круга меньше\n");

        System.out.println("Введенные площади кругов: " + area1 + ", " +area2);

        double min = (area1 < area2) ? area1 : area2;

        if (area1 == area2) {
            System.out.println("Площади равны");
        } else {
            System.out.println("Наименьшая площадь: " + min);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task11(double area1, double area2) {
        System.out.println("11. Составить программу, которая определит площадь какого треугольника больше.\n");

        System.out.println("Введенные площади треугольников: " + area1 + ", " +area2);

        double max = (area1 < area2) ? area1 : area2;

        if (area1 == area2) {
            System.out.println("Площади равны");
        } else {
            System.out.println("Наибольшая площадь: " + max);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task12(double num1, double num2, double num3) {
        System.out.println("12. Даны три действительных числа. Возвести в квадрат те из них, значения которых\n" +
                "неотрицательны, и в четвертую степень — отрицательные.\n");
        System.out.println("Введенные числа: " + num1 + ", " + num2 + ", " + num3);

        num1 = (num1 >= 0) ? Math.pow(num1, 2) : Math.pow(num1, 4);
        num2 = (num2 >= 0) ? Math.pow(num2, 2) : Math.pow(num2, 4);
        num3 = (num3 >= 0) ? Math.pow(num3, 2) : Math.pow(num3, 4);

        System.out.println("Результат вычисления: " + num1 + ", " + num2 + ", " + num3);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task13(double x1, double y1, double x2, double y2) {
        System.out.println("13. Даны две точки А(х1, у1) и В(х2, у2). Составить алгоритм, определяющий, которая из точек\n" +
                "находится ближе к началу координат.\n");

        double min = 0;
        MyPoint startPoint = new MyPoint(0, 0);
        MyPoint point1 = new MyPoint(x1, y1);
        MyPoint point2 = new MyPoint(x2, y2);

        double distance1 = calculateDistance(point1, startPoint);
        double distance2 = calculateDistance(point2, startPoint);

        if (distance1 == distance2) {
            System.out.println("Расстояния равны");
        } else if (distance1 < distance2) {
            System.out.println("Ближе к началу координат расположена точка A(х1 = " + point1.getX() + " у1 = " + point1.getY() + ")\n");
        } else if (distance1 > distance2) {
            System.out.println("Ближе к началу координат расположена точка B(х2 = " + point2.getX() + " у2 = " + point2.getY() + ")\n");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task14(double angle1, double angle2) {
        System.out.println("14. Даны два угла треугольника (в градусах). Определить, существует ли такой треугольник,\n" +
                "и если да, то будет ли он прямоугольным.\n");

        System.out.println("Введенные углы: " + angle1 + ", " + angle2);

        double angle3 = 180 - angle1 - angle2;

        if (angle1 > 0 && angle2 > 0 && angle3 > 0) {
            System.out.println("Треугольник существует");
            if (angle1 == 90 || angle2 == 90 || angle3 == 90) {
                System.out.println("Треугольник прямоугольный");
            }
        } else {
            System.out.println("Треугольник не существует");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task15(double num1, double num2) {
        System.out.println("15. Даны действительные числа х и у, не равные друг другу. Меньшее из этих двух чисел\n" +
                "заменить половиной их суммы, а большее — их удвоенным произведением.\n");
        double newNum1 = 0;
        double newNum2 = 0;
        if (num1 != num2) {
            if (num1 < num2) {
                newNum1 = (num1 + num2) / 2;
                newNum2 = 2 * num1 * num2;
            } else {
                newNum2 = (num1 + num2) / 2;
                newNum1 = 2 * num1 * num2;
            }
            System.out.println("Для чисел " + num1 + " и " + num2 + " новые значения: " + newNum1 + " и " + newNum2);
        } else {
            System.out.println("Числа не могут быть равны!");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task16(double x,double y) {
        System.out.println("16. На плоскости ХОY задана своими координатами точка А. Указать, где она расположена\n" +
                "(на какой оси или в каком координатном угле).\n");
        MyPoint point = new MyPoint(x, y);

        if (point.getX() > 0 && point.getY() > 0) {
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится в 1 четверти");
        } else if (point.getX() < 0 && point.getY() > 0) {
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится во 2 четверти");
        } else if (point.getX() < 0 && point.getY() < 0) {
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится в 3 четверти");
        } else if (point.getX() > 0 && point.getY() < 0){
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится в 4 четверти");
        } else if (point.getX() == 0 && point.getY() != 0){
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится на оси y");
        } else if (point.getX() != 0 && point.getY() == 0){
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится на оси x");
        } else if (point.getX() == 0 && point.getY() == 0){
            System.out.println("Точка (" + point.getX() + ", " + point.getY() + ") находится в начале координат");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task17(int m,int n) {
        System.out.println("17. Даны целые числа m, n. Если числа не равны, то заменить каждое из них одним и тем же\n" +
                "числом, равным большему из исходных, а если равны, то заменить числа нулями.\n");
        System.out.println("Введены числа m =  " + m + ", и n = " + n);

        if (m != n) {
            if (m > n) {
                n = m;
            } else {
                m = n;
            }
        } else {
            m = 0;
            n = 0;
        }

        System.out.println("Полученные числа: " + m + ", " + n);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task18(double a,double b, double c) {
        System.out.println("18. Подсчитать количество отрицательных среди чисел а, b, с\n");
        System.out.println("Введены числа a =  " + a + ", b = " + b +", и c = " + c);
        int count = 0;

        if (a < 0) {
            count++;
        }
        if (b < 0) {
            count++;
        }
        if (c < 0) {
            count++;
        }

        System.out.println("Среди чисел " + a + ", " + b + ", " + c + " количество отрицательных: " + count);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task19(double a,double b, double c) {
        System.out.println("19. Подсчитать количество положительных среди чисел а, b, с\n");
        System.out.println("Введены числа a =  " + a + ", b = " + b +", и c = " + c);
        int count = 0;

        if (a > 0) {
            count++;
        }
        if (b > 0) {
            count++;
        }
        if (c > 0) {
            count++;
        }

        System.out.println("Среди чисел " + a + ", " + b + ", " + c + " количество положительных: " + count);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task20(double a,double b, double c, double k) {
        System.out.println("20. Определить, делителем каких чисел а, b, с является число k.\n");
        System.out.println("Введены числа a =  " + a + ", b = " + b +", c = " + c + ", k = " + k);

        if (a % k == 0) {
            System.out.println("Число " + k + " является делителем числа " + a);
        }
        else if (b % k == 0) {
            System.out.println("Число " + k + " является делителем числа " + b);
        }
        else if (c % k == 0) {
            System.out.println("Число " + k + " является делителем числа " + c);
        } else {
            System.out.println("Число " + k + " не является делителем введенных чисел ");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task21() {
        System.out.println("21. Программа — льстец. На экране высвечивается вопрос «Кто ты: мальчик или девочка?\n" +
                "Введи Д или М». В зависимости от ответа на экране должен появиться текст\n" +
                "«Мне нравятся девочки!» или «Мне нравятся мальчики!»\n");

        Scanner scanner = new Scanner(System.in);

        System.out.println("Кто ты: мальчик или девочка? Введи Д или М");
        String answer = scanner.nextLine();

        switch (answer) {
            case "Д":
                System.out.println("Мне нравятся девочки!");
                break;
            case "М":
                System.out.println("Мне нравятся мальчики!");
                break;
            default:
                System.out.println("Неверное значение!");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task22(double x,double y) {
        System.out.println("22. Перераспределить значения переменных х и у так, чтобы в х оказалось большее из\n" +
                "этих значений, а в y - меньшее.\n");
        System.out.println("Введены числа x =  " + x + ", y = " + y);
        double temp = 0;

        if (x < y) {
            temp = x;
            x = y;
            y = temp;
        }
        System.out.println("x =  " + x + ", y = " + y);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task23() {
        System.out.println("Определить правильность даты, введенной с клавиатуры (число — от 1 до 31,\n" +
                "месяц — от 1 до 12). Если введены некорректные данные, то сообщить об этом.\n");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int day = 0;
        int month = 0;

        try {
            System.out.println("Введите число месяца");
            day = Integer.parseInt(reader.readLine());
            System.out.println("Введите номер месяца");
            month = Integer.parseInt(reader.readLine());
            reader.close();

            if (day < 1 || day > 31 || month < 1 || month > 12) {
                System.out.println("Введены некорректные данные!");
            } else {
                System.out.println("День - " + day + ", месяц - " + month);
            }

        } catch (IOException | NumberFormatException e) {
            System.out.println("Что-то пошло не так!");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task24(int n) {
        System.out.println("24. Составить программу, определяющую результат гадания на ромашке — «любит—не любит»,\n" +
                "взяв за исходное данное количество лепестков n.\n");
        System.out.println("Количество лепестков: " + n);

        for (int i = 1; i <= n; i++) {
            if (i % 2 != 0) {
                System.out.println("Любит");
            } else {
                System.out.println("Не любит");
            }
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task25(int temp) {
        System.out.println("25. Написать программу — модель анализа пожарного датчика в помещении, которая выводит\n" +
                "сообщение «Пожароопасная ситуация », если температура в комнате превысила 60° С..\n");
        System.out.println("Текущая температура: " + temp + " C" );
        if (temp > 60) {
            System.out.println("Пожароопасная ситуация");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task26(double a, double b, double c) {
        System.out.println("26. Написать программу нахождения суммы большего и меньшего из трех чисел.\n");
        System.out.println("Введены числа a =  " + a + ", b = " + b +", c = " + c);
        double min = Double.MIN_VALUE;
        double max = Double.MAX_VALUE;
        double result = 0;
        double temp;
        double [] arr = {a, b, c};

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                temp = arr[i];
                arr[i] = arr[i-1];
                arr[i-1] = temp;
            }
        }
        result = arr[0] + arr[2];
        System.out.println("Минимальное число: " + arr[0] + ", большее число: " + arr[2] + ", сумма этих чисел: " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task27(double a, double b, double c, double d) {
        System.out.println("27. Найти max{min(a, b), min(c, d)}.\n");
        System.out.println("Введены числа a =  " + a + ", b = " + b +", c = " + c +", d = " + d);
        double minAB = 0;
        double minCD = 0;
        double result = 0;
        double [] arr = {a, b, c};

        if (a == b) {
            System.out.println("Числа а и b равны");
        } else if (a < b) {
            minAB = a;
        } else {
            minAB = b;
        }

        if (c == d) {
            System.out.println("Числа c и d равны");
        } else if (c < d) {
            minCD = c;
        } else {
            minCD = d;
        }

        result = (minAB > minCD) ? minAB : minCD;
        System.out.println("max{min(a, b), min(c, d)} = " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task31(double a, double b, double x, double y, double z) {
        System.out.println("31. Заданы размеры А, В прямоугольного отверстия и размеры х, у, z кирпича.\n" +
                "Определить, пройдет ли кирпич через отверстие.\n");

        System.out.println("Введены размеры отверстия А =" + a + ", B = " + b + " и размеры кирпича x =  " + x + ", y = " + y +", z = " + z);
        double max = 0;
        double temp;
        double [] arr = {x, y, z};

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[i - 1]) {
                temp = arr[i];
                arr[i] = arr[i-1];
                arr[i-1] = temp;
            }
        }
        max = arr[2];

        if (a > max && b > max ) {
            System.out.println("Кирпич пройдет");
        } else {
            System.out.println("Не пройдет!");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }
}

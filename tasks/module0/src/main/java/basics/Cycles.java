package basics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Cycles {
    public static void main(String[] args) {
        //Количество решенных задач [01]Branching: 28
        //Количество решенных задач [01]Cycles: 27
        //Количество решенных задач [01]Linear: 33
        task01();
        task02();
        task03();
        task04();
        task05();
        task06();
        task07(13,43,1);
        task08(13,19, 4, 1);
        task09();
        task10();
        task11();
        task12();
        task13();
        task14(7);
        task15();
        task16();
        task17(3.5, 8);
        task18(0.12, 42);
        task19(0.12, 42);
        task20(0.12, 42);
        task21(10, 25, 1.3);
        task22(10, 25, 1.3);
        task23(10, 25, 1.3);
        task24(123456789);
        task25();
        task26();
        task27();
    }

    private static void task01(){
        System.out.println("1. Необходимо вывести на экран числа от 1 до 5 \n");
        for (int i = 1; i <= 5; i++) {
            System.out.print(i +" ");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task02(){
        System.out.println("2. Необходимо вывести на экран числа от 5 до 1 \n");
        for (int i = 5; i >= 1; i--) {
            System.out.print(i +" ");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task03(){
        System.out.println("3. Необходимо вывести на экран таблицу умножения на 3\n");
        for (int i = 1; i <= 10; i++) {
            System.out.print(i * 3 +" ");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task04(){
        System.out.println("4. С помощью оператора while напишите программу вывода всех четных чисел в диапазоне\n" +
                " от 2 до 100 включительно\n");
        int count = 2;
        while (count <= 100) {
            if (count%2==0) {
                System.out.println(count);
            }
            count++;
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task05(){
        System.out.println("5. С помощью оператора while напишите программу определения суммы всех нечетных чисел в\n" +
                "диапазоне от 1 до 99 включительно\n");
        int count = 1;
        while (count < 100) {
            if (count%2 != 0) {
                System.out.println(count);
            }
            count++;
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task06(){
        System.out.println("6. Напишите программу, где пользователь вводит любое целое положительное число. А программа\n" +
                "суммирует все числа от 1 до введенного пользователем числа\n");
        System.out.println("Введите целое положительное число");
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int num = Integer.parseInt(reader.readLine());
            if (num > 0) {
                int result = 0;
                for (int i = 1; i < num; i++) {
                    result+= i;
                }
                System.out.println(result);
            } else {
                System.out.println("Вы ввели отрицательное число!");

            }
        } catch (IOException | NumberFormatException e) {
            System.out.println("Вы ввели не число!");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task07(double a, double b, double h){
        System.out.println("7. Вычислить значения функции на отрезке [а,b] c шагом h\n");
        double x;
        double y = 0;
        for (x = a; x <= b ; x += h ) {
            y = (x > 2) ? x : -x;
        }
        System.out.println("При х = " + x + " y = " + y);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task08(double a, double b, double c, double h){
        System.out.println("8. Вычислить значения функции на отрезке [а,b] c шагом h\n");
        double y;
        double x;
        for (x = a; x <= b; x += h) {
            if (x == 15) {
                y = (x + c) * 2;
            } else {
                y = (x - c) + 6;
            }
            System.out.println("При х = " + x + " у = " + y);
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task09(){
        System.out.println("9. Найти сумму квадратов первых ста чисел\n");
        int result = 0;
        for (int i = 1; i <= 100; i++) {
            result += i*i;
        }
        System.out.println("Сумма квадратов первых ста чисел: " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task10(){
        System.out.println("10. Составить программу нахождения произведения квадратов первых двухсот чисел\n");
        double result = 1;
        int count = 0;
        double number = 1.0;

        while (count <= 200) {
            result *= number * number;
            number += 0.01;
            count++;
        }
        System.out.println("Произведение квадратов первых ста чисел от 1 до 3 с шагом 0.01: " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task11() {
        System.out.println("11. Составить программу нахождения разности кубов первых двухсот чисел\n");
        double result = 1;
        int count = 0;
        double number = 1.0;

        while (count <= 200) {
            result -= Math.pow(number, 3);
            number += 0.01;
            count++;
        }
        System.out.println("Разность квадратов первых ста чисел: " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task12() {
        System.out.println("12. Последовательность аn строится так: а1 = 1, an =6+ аn-1 , для каждого n >1\n" +
                "Составьте программу нахождения произведения первых 10 членов этой последовательности\n");
        long result = 1L;
        int a = 1;

        for (int i = 2; i <= 10; i++) {
            a = 6 + a;
            result *= a;
        }
        System.out.println("Произведение первых 10 членов последовательности: " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task13() {
        System.out.println("13. Составить таблицу значений функции y = 5 - x^2/n на отрезке [-5; 5] с шагом 0.5\n");
        int a = -5;
        int b = 5;
        double h = 0.5;
        double y;

        for (double i = a; i <= b; i += h) {
            y = 5 - (i * i) / 2.0;
            System.out.println("y = " + y);
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task14(int n) {
        System.out.println("14. Дано натуральное n. вычислить: 1 + 1/2 + 1/3 + 1/4 + ... + 1/n\n");
        double result = 1.0;
        for (int i = 2; i < n; i++) {
            result += 1.0/i;
        }

        System.out.println("При n = " + n + " значение выражения равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task15() {
        System.out.println("15. Вычислить : 1+2+4+8+...+ 2 в 10 степени.\n");
        int result = 0;
        for (int i = 1; i < 10; i++) {
            result += Math.pow(2,i);
        }
        System.out.println(result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task16() {
        System.out.println("16. Вычислить: (1+2)*(1+2+3)*...*(1+2+...+10)\n");
        long result = 3;
        int sum = 3;
        for (int i = 3; i <= 10; i++) {
            sum += i;
            result *= sum;
        }
        System.out.println(result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task17(double a, int n) {
        System.out.println("17. Даны действительное (а) и натуральное (n). вычислить: a(a+1)...(a+n-1)\n");

        double result = 1;

        for (int i = 0; i < n; i++) {
            result *= a + i;
        }

        System.out.println("При а = " + a + ", n = " + n + " значение выражения равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task18(double e, int n) {
        System.out.println("18. Даны числовой ряд и некоторое число е. Найти сумму тех членов ряда, модуль которых\n" +
                "больше или равен заданному е. Общий член ряда имеет вид:...\n");

        double result = 0.0;
        double a = 0.0;

        for (int i = 1; i <= n; i++) {
            a = Math.pow(-1, i - 1) / i;
            if (Math.abs(a) >= e ){
                result += a;
            }
        }
        System.out.println("При e = " + e + " значение выражения равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task19(double e, int n) {
        System.out.println("19. Даны числовой ряд и некоторое число е. Найти сумму тех членов ряда, модуль которых\n" +
                "больше или равен заданному е. Общий член ряда имеет вид:...\n");

        double result = 0.0;
        double a = 0.0;

        for (int i = 0; i <= n; i++) {
            a = 1 / Math.pow(2, i) + 1 / Math.pow(3, i);
            if (Math.abs(a) >= e) {
                result += a;
            }
        }
        System.out.println("При e = " + e + " значение выражения равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task20(double e, int n) {
        System.out.println("20. Даны числовой ряд и некоторое число е. Найти сумму тех членов ряда, модуль которых\n" +
                "больше или равен заданному е. Общий член ряда имеет вид:...\n");

        double result = 0.0;
        double a = 0.0;

        for (int i = 0; i <= n; i++) {
            a = 1.0 / ((3 * i - 2) * (3 * i + 1));
            if (Math.abs(a) >= e ){
                result += a;
            }
        }
        System.out.println("При e = " + e + " значение выражения равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task21(double a, double b, double h) {
        System.out.println("21. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h.\n" +
                "Результат представить в виде таблицы, первый столбец которой – значения аргумента,\n" +
                "второй - соответствующие значения функции: F(x) = x − sin(x)\n");

        double result = 0.0;
        double f = 0;
        Map<Double, Double> map = new HashMap<>();

        for (double i = a; i <= b; i += h) {
            f = i - Math.sin(i);
            map.put(i, f);
        }
        System.out.println("На отрезке [" + a + ", " + b + "] с шагом " + h + " значение функции равно:\n");
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            System.out.printf("Аргумент - %.3f  Значение - %.3f\n", entry.getKey(), entry.getValue());
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task22(double a, double b, double h) {
        System.out.println("21. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h.\n" +
                "Результат представить в виде таблицы, первый столбец которой – значения аргумента,\n" +
                "второй - соответствующие значения функции: F(x) = sin^2(x)\n");

        double result = 0.0;
        double f = 0;
        Map<Double, Double> map = new HashMap<>();

        for (double i = a; i <= b; i += h) {
            f = Math.pow(Math.sin(i), 2);
            map.put(i, f);
        }
        System.out.println("На отрезке [" + a + ", " + b + "] с шагом " + h + " значение функции равно:\n");
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            System.out.printf("Аргумент - %.3f  Значение - %.3f\n", entry.getKey(), entry.getValue());
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task23(double a, double b, double h) {
        System.out.println("21. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h.\n" +
                "Результат представить в виде таблицы, первый столбец которой – значения аргумента,\n" +
                "второй - соответствующие значения функции: F(x) = ctg(x/3) + sin(x)/2\n");

        double result = 0.0;
        double f = 0;
        Map<Double, Double> map = new HashMap<>();

        for (double i = a; i <= b; i += h) {
            f = 1 / Math.tan(i / 3) + Math.sin(i) / 2;
            map.put(i, f);
        }
        System.out.println("На отрезке [" + a + ", " + b + "] с шагом " + h + " значение функции равно:\n");
        for (Map.Entry<Double, Double> entry : map.entrySet()) {
            System.out.printf("Аргумент - %.3f  Значение - %.3f\n", entry.getKey(), entry.getValue());
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task24(int num) {
        System.out.println("24. Вводится натуральное число. Найти сумму четных цифр, входящих в его состав. Преобразовать\n" +
                "его в другое число, цифры которого будут следовать в обратном порядке по сравнению с введенным\n" +
                "числом.\n");
        int result = 0;
        int numFromChar = 0;
        int numReverse =  0;
        StringBuilder sb = new StringBuilder();
        sb.append(num);
        numReverse = Integer.parseInt(sb.reverse().toString());

        String string = Integer.toString(num);
        for (int i = 1; i < string.length(); i+= 2){
            numFromChar = string.charAt(i) - 48; //or - '0'
            result += numFromChar;
        }
        System.out.println("Введенное число: " + num);
        System.out.println("Сумма четных цифр: " + result);
        System.out.println("Число с обратным порядком: " + numReverse);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task25() {
        System.out.println("25. Требуется определить факториал числа, которое ввел пользователь\n");
        long factotial = 1;
        int num = 0;

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите число: ");
            num = Integer.parseInt(reader.readLine());
            if (num < 0) {
                System.out.println("Число должно быть положительным!");
                throw new NumberFormatException();
            }
            if (num == 0){
                factotial = 1;
            } else {
                for (int i = 1; i <= num; i++) {
                    factotial *= i;
                }
            }
            System.out.println("Факториал числа " + num + " равен " + factotial);
        } catch (IOException | NumberFormatException e) {
            System.out.println("Что-то пошло не так...");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task26() {
        System.out.println("26. Вывести на экран соответствий между символами и их численными обозначениями в памяти\n" +
                "компьютера (Таблицу ASCII)\n");

        for (int i = 0; i < 255; i++) {
            System.out.printf("Численное обозначение %3d\t Символ %3s\n", i, (char)i);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task27() {
        System.out.println("27. Для каждого натурального числа в промежутке от m до n вывести все делители, кроме единицы и\n" +
                "самого числа. m и n вводятся с клавиатуры.\n");

        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите число m (начало промежутка)");
            int m = Integer.parseInt(reader.readLine());
            System.out.println("Введите число n (конец промежутка)");
            int n = Integer.parseInt(reader.readLine());

            while (m <= n) {
                System.out.print("\n число: " + m);
                System.out.print("\n делители: ");
                for (int i = 2; i <= m - 1; i++) {
                    if (m % i == 0) {
                        System.out.print(i + ",");
                    }
                }
                m += 1;
            }

        } catch (IOException | NumberFormatException e) {
            System.out.println("Что-то пошло не так!");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }
}

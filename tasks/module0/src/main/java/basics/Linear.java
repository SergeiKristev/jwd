package basics;

import basics.classes.MyPoint;
import basics.classes.Triangle;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static basics.classes.MyPoint.calculateDistance;

public class Linear {
    public static void main(String[] args) {
        //Количество решенных задач [01]Branching: 28
        //Количество решенных задач [01]Cycles: 27
        //Количество решенных задач [01]Linear: 33
        task01(23, 56);
        task02(23);
        task03(23, 45.6);
        task04(12,23,5);
        task05(23, 27);
        task06();
        task07(10);
        task08(12.4,324,3.45);
        task09(1.562,324,3.45, 556);
        task10(18,25);
        task11(5,8.8);
        task12(25, 3, 23, 4);
        task13(25, 3, 23, 4, 23, 9);
        task14(12);
        task15();
        task16(4562);
        task17(123, 56);
        task18(33);
        task19(32);
        task20(1234);
        task21(123.456);
        task22(1235456);
        task23(23, 14);
        task24(843, 674, 45);
        task25(3, 25, 13);
        task26(23, 25, 43);
        task27(2);
        task28(123);
        task29(54,57,13);
        task30(23,45,78);
        task31(25,14, 44, 54);
        //32...
        task33('B');
        task34(2043454543435L);
    }
    private static void task01(double num1,double num2){
        System.out.println("1. Даны два действительных числа х и у. Вычислить их сумму, разность, произведение и частное\n");
        double sum = num1 + num2;
        double difference = num1 - num2;
        double mult = num1 * num2;
        double division = num1 / num2;
        System.out.println("Сумма чисел x = " + num1 + " и y = " + num2 + " равна " + sum);
        System.out.println("Разность чисел x = " + num1 + " и y = " + num2 + " равна " + difference);
        System.out.println("Произведение чисел x = " + num1 + " и y = " + num2 + " равно " + mult);
        System.out.println("Частное чисел x = " + num1 + " и y = " + num2 + " равно " + division);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task02(double a){
        System.out.println("2. Найдите значение функции: с = 3 + а.\n");
        double c = 3 + a;
        System.out.println("При а = " + a + " значение с = " + c);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task03(double x, double y){
        System.out.println("3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5\n");
        double z =  2 * x +(y - 2) * 5;
        System.out.println("При x = " + x + " и y = " + y + " значение z = " + z);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task04(double a, double b, double c){
        System.out.println("4. Найдите значение функции: z = ( (a – 3 ) * b / 2) + c\n");
        double z = ((a - 3) * b / 2) + c ;
        System.out.println("При a = " + a + ", b = " + b + " и c = " + c + " значение z = " + z);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task05(double num1, double num2){
        System.out.println("5. Составить алгоритм нахождения среднего арифметического двух чисел\n");
        double avg = (num1 + num2) / 2 ;
        System.out.println("Среднее арифметическое чисел " + num1 + " и " + num2 + " = " + avg);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task06(){
        System.out.println("6. Написать код для решения задачи. В n малых бидонах 80 л молока.\n" +
                "   Сколько литров молока в m больших бидонах,\n" +
                "   если в каждом большом бидоне на 12 л. больше, чем в малом?\n");
        class Can {
            private int countLittleCan;
            private int countBigCan;
            private double volumeLittleCan;
            private double volumeBigCan;


            public double calculateTotalVolumeBigCan(double totalVolumeLittleCan, int countLittleCan, int countBigCan) {
                this.countBigCan = countBigCan;
                volumeLittleCan = (totalVolumeLittleCan / countLittleCan);
                volumeBigCan = volumeLittleCan + 12;
                return volumeBigCan * countBigCan;
            }
        }

        Can can1 = new Can();
        double result = can1.calculateTotalVolumeBigCan(80, 4, 3);
        System.out.println("В " + can1.countBigCan + " больших бидонах - " + result + " литров молока");

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task07(double lenth){
        System.out.println("7. Дан прямоугольник, ширина которого в два раза меньше длины. Найти площадь прямоугольника\n");
        double height = lenth / 2;
        double area = lenth * height;
        System.out.println("Площадь прямоугольника с шириной " + height + " равна " + area);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task08(double a, double b, double c){
        System.out.println("8. Вычислить значение выражения по формуле (все переменные принимают действительные значения)\n");
        double z = (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c) / (2 * a) - Math.pow(a, 3) * c + Math.pow(b, -3));

        System.out.println("При a = " + a + ", b = " + b + " и c = " + c + " значение z = " + z);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task09(double a, double b, double c, double d){
        System.out.println("9. Вычислить значение выражения по формуле (все переменные принимают действительные значения)\n");
        double result = (a / c) * (b / d) - (a * b -c) / (c * d);

        System.out.println("При a = " + a + ", b = " + b +", c = " + c + " и d = " + d + " значение формулы = " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task10(double x, double y){
        System.out.println("10. Вычислить значение выражения по формуле (все переменные принимают действительные значения)\n");
        double result = (Math.sin(x) + Math.cos(y)) / (Math.cos(x) - Math.sin(y)) * Math.tan(x * y);

        System.out.println("При x = " + x + " и y = " + y + " значение формулы = " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task11(double width, double height){
        System.out.println("11. Вычислить периметр и площадь прямоугольного треугольника по длинам а и b двух катетов\n");
        double perimeter = width * 2 + height * 2;
        double area = width * height;
        System.out.println("Периметр прямоугольного треугольника с катетами а = " + width + " и b = " + height + " равен " + perimeter);
        System.out.println("Площадь прямоугольного треугольника с катетами а = " + width + " и b = " + height + " равна " + area);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task12(double x1, double y1, double x2, double y2){
        System.out.println("12. Вычислить расстояние между двумя точками с данными координатами (х1, у1)и (x2, у2)\n");

        MyPoint point1 = new MyPoint(x1, y1);
        MyPoint point2 = new MyPoint(x2, y2);

        double distance = calculateDistance(point1, point2);

        System.out.println("Расстояние между двумя точками с данными координатами (х1 = " + point1.getX() + " у1 = " + point1.getY() + ")\n" +
                "и (x2 = " + point2.getX() + ", у2 = " + point2.getY() + ") равно " + distance);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    private static void task13(double x1, double y1, double x2, double y2, double x3, double y3){
        System.out.println("13. Заданы координаты трех вершин треугольника (х1 у2),(х2, у2) ),(х3, у3). \n" +
                "Найти его периметр и площадь\n");

        Triangle triangle = new Triangle(new MyPoint(x1,y1), new MyPoint(x2,y2), new MyPoint(x3,y3));
        double perimeter = triangle.calculatePerimeter();
        double area = triangle.calculateArea();

        System.out.println("Периметр треугольника с вершинами (х1 = " + x1 + " у1 = " + y1 + "), \n"
                + "(x2 = " + x2 + ", у2 = " + y2 + "), и (x3 = " + x3 + ", y3 =  " + y3 + " равен " + perimeter);
        System.out.println("Площадь треугольника с вершинами (х1 = " + x1 + " у1 = " + y1 + ")" +
                ", (x2 = " + x2 + ", у2 = " + y2 + "),\n" +
                " и (x3 = " + x3 + ", y3 = " + y3 + " равна " + area);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");

    }


    private static void task14(double radius){
        System.out.println("14. Вычислить длину окружности и площадь круга одного и того же заданного радиуса R\n");
        double circumference = 2 * Math.PI * radius;
        double area = Math.PI * Math.pow(radius, 2);
        System.out.println("Длина окружности с радиусом r = " + radius + " равна " + circumference);
        System.out.println("Площадь окружности с радиусом r = " + radius + " равна " + area);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task15() {
        System.out.println("15. Написать программу, которая выводит на экран первые четыре степени числа π\n");
        double result = 0;
        for (int i = 1; i < 5; i++) {
            result = Math.pow(Math.PI, i);
            System.out.print(result);
            System.out.println();
        }
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task16(int number) {
        System.out.println("16. Найти произведение цифр заданного четырехзначного числа\n");
        int data = number;
        int result = 1;
        while (number > 0){
            result = result * (number % 10);
            number = number / 10;
        }
        System.out.println("Произведение цифр числа " + data + " равно " + result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task17(double num1, double num2) {

        System.out.println("17. Даны два числа. Найти среднее арифметическое кубов этих чисел и среднее геометрическое\n" +
                " модулей этих чисел\n");

        double average = (Math.pow(num1, 3) + Math.pow(num2, 3)) / 2;
        double geometricModule = Math.sqrt(Math.abs(num1) * Math.abs(num2));

        System.out.println("Среднее арифметическое кубов чисел " + num1 + " и " + num2 + " равно " + average);
        System.out.println("Среднее геометрическое модулей чисел " + num1 + " и " + num2 + " равно " + geometricModule);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task18(double edge) {

        System.out.println("18. Дана длина ребра куба. Найти площадь грани, площадь полной поверхности и объем этого куба\n");

        double vergeArea = edge * edge;
        double surfaceArea = 6 * edge * 2;
        double volume = Math.pow(edge, 3);

        System.out.println("Площадь грани куба с ребром  " + edge + " равна " + vergeArea);
        System.out.println("Площадь полной поверхности куба с ребром  " + edge + " равна " + surfaceArea);
        System.out.println("Объем куба с ребром  " + edge + " равен " + volume);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task19(double edge) {

        System.out.println("19. Дана сторона равностороннего треугольника. Найти площадь этого треугольника,\n " +
                "его высоту, радиусы вписанной и описанной окружностей\n");

        double radiusInt =(Math.sqrt(3) * edge) / 6;
        double radiusOut = 2 * radiusInt;
        double height = 3 * radiusInt;
        double area = edge * height / 2;

        System.out.println("Площадь треугольника со стороной " + edge + " равна " + area);
        System.out.println("Высота треугольника со стороной " + edge + " равна " + height);
        System.out.println("Радиус вписанной окружности треугольника со стороной " + edge + " равен "+ radiusInt);
        System.out.println("Радиус описанной окружности треугольника со стороной " + edge + " равен " + radiusOut);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task20(double lenth) {

        System.out.println("20. Известна длина окружности. Найти площадь круга, ограниченного этой окружностью\n");

        double radius = lenth/(2*Math.PI);
        double area = Math.PI*Math.pow(radius, 2);

        System.out.println("Площадь круга, ограниченного окружностью длиной " + lenth + " равна " + area);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task21(double nnn_ddd) {

        System.out.println("21. Дано действительное число R вида nnn.ddd (три цифровых разряда в дробной и целой частях).\n" +
                "Поменять местами дробную и целую части числа и вывести полученное значение числа.\n");
        int nnn = (int) nnn_ddd;
        double ddd = (nnn_ddd - nnn) * 1000;
        double ddd_nnn = ddd + nnn/1000.0;

        BigDecimal bd = new BigDecimal(Double.toString(ddd_nnn));
        bd = bd.setScale(3, RoundingMode.HALF_UP);
        ddd_nnn = bd.doubleValue();

        System.out.println("Дано число " + nnn_ddd + ", результат " + ddd_nnn);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task22(long timeInSecunds) {

        System.out.println("22. Дано натуральное число Т, которое представляет длительность прошедшего времени в секундах.\n" +
                "Вывести данное значение длительности в часах, минутах и секундах в следующей форме: ННч ММмин SSc.\n");

        long hours = timeInSecunds /(60 * 60) % 24;
        long minuts = timeInSecunds /(60) % 60;
        long secunds = timeInSecunds % 60 ;
        System.out.println("Время в секундах: " + timeInSecunds);
        System.out.printf("Прошло %02dч %02dмин %02dс", hours, minuts, secunds);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task23(double radiusOut, double radiusInt) {

        System.out.println("23. Найти площадь кольца, внутренний радиус которого равен r, а внешний — R (R> r)\n");
        double result = 0;
        if (radiusOut>radiusInt) {
             result = Math.PI * (Math.pow(radiusOut, 2) - Math.pow(radiusInt, 2));
            System.out.println("Площадь кольца с внутренним радиусом " + radiusInt + " и внешним радиусом " + radiusOut +
                    " равна " + result );
        } else {
            System.out.println("Наружный радиус неможет быть меньше внутреннего!");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task24(int sideA, int sideB, float alpha) {

        System.out.println("24. Найти площадь равнобедренной трапеции с основаниями а и b и углом α при большем основании а\n");

        if (sideA>sideB) {
            double result = (sideA + sideB) * (sideA - sideB) * Math.tan(alpha)/4;
            System.out.println("Площадь равнобедренной трапеции с основаниями а = " + sideA + ", b = " + sideB + "" +
                    " и углом α = " + alpha + " равняется " + result);
        } else {
            System.out.println("Сторона а должна быть больше b");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task25(double a, double b, double c) {

        System.out.println("25. Вычислить корни квадратного уравнения ах2+ bх + с = 0 с заданными коэффициентами a, b и с" +
                " (предполагается, что а≠0 и что дискриминант уравнения неотрицателен)\n");

        double D;

        //calculate discriminant
        D = b * b - 4 * a * c;
        if (D > 0) {
            double x1, x2;
            x1 = (-b - Math.sqrt(D)) / (2 * a);
            x2 = (-b + Math.sqrt(D)) / (2 * a);
            System.out.println("Корни уравнения: x1 = " + x1 + ", x2 = " + x2);
        }
        else if (D == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Уравнение имеет единственный корень: x = " + x);
        }
        else {
            System.out.println("Уравнение не имеет действительных корней!");
        }


        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task26(double sideA, double sideB, double alpha) {

        System.out.println("26. Найти площадь треугольника, две стороны которого равны а и b, а угол между этими сторонами у\n");

        double alphaRad = alpha * Math.PI / 180.0;

        double result = 0.5 * sideA * sideB * Math.sin(alphaRad);

        System.out.println("Площадь треугольника со сторонами " + sideA + " и " + sideB + " и углом между ними " + alpha + " равна " + result);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task27(double a) {

        System.out.println("27. Дано значение a. Не используя никаких функций и никаких операций, кроме умножения,\n" +
                "получить значение а8^n за n три операции и а^n10 за четыре операции.\n");
        double aTemp = a;

        a = a*a; // 1 procedure
        a = a*a; // 2 procedure
        a = a*a; // 3 procedure

        System.out.println("Значение " + aTemp + " в степени 8 за 3 операции = " + a);

        double a2 = aTemp*aTemp;    // 1 procedure
        double a4=a2*a2;            // 2 procedure
        double a8=a4*a4;            // 3 procedure
        double a10=a8*a2;           // 4 procedure
        System.out.println("Значение " + aTemp + " в степени 10 за 4 операции = " + a10);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task28(double radian) {

        System.out.println("28. Составить программу перевода радианной меры угла в градусы, минуты и секунды\n");

        double graduses = radian*180/Math.PI;
        int gr = (int) graduses;
        gr = gr - 1;
        double minutes = gr*60;
        double seconds = minutes*60;
        System.out.printf("Радианная мера угла %.2f равна %.2f градусов, %.2f минут, %.2f секунд", radian, graduses, minutes, seconds);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task29(double a, double b, double c) {

        System.out.println("29. Найти (в радианах в градусах) все углы треугольника со сторонами а, b, с\n");

        double angleARad = Math.cos((b * b + c * c - a * a) / (2 * b * c));
        double angleADegr = angleARad * 180 / Math.PI;

        double angleBRad = Math.cos((a * a + c * c - b * b) / (2 * a * c));
        double angleBDegr = angleBRad * 180 / Math.PI;

        double angleCRad = Math.cos((a * a + b * b - c * c) / (2 * a * b));
        double angleCDegr = angleCRad * 180 / Math.PI;

        System.out.println("Треугольник со сторонами " + a + " " + b + " " + c + "\n");
        System.out.printf("Угол 1 = %.2f рад %.2f градусов \n", angleARad, angleADegr);
        System.out.printf("Угол 2 = %.2f рад %.2f градусов \n", angleBRad, angleBDegr);
        System.out.printf("Угол 3 = %.2f рад %.2f градусов \n", angleCRad, angleCDegr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task30(double resistor1, double resistor2, double resistor3) {

        System.out.println("30. Три сопротивления R1 R2, и R3 соединены параллельно. Найдите сопротивление соединения\n");

        double resistance = 1 / (1 / resistor1 + 1 / resistor2 + 1 / resistor3);

        System.out.printf("Параллельные сопротивления R1 = %.2f  R2 = %.2f  R3 = %.2f \n", resistor1, resistor2, resistor3);
        System.out.printf("Сопротивление соединения = %.2f", resistance);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task31(double v, double v1, double t1, double t2) {

        System.out.println("31. Составить программу для вычисления пути, пройденного лодкой, если ее скорость\n" +
                "в стоячей воде v км/ч, скорость течения реки v1 км/ч, время движения по озеру t1 ч,\n" +
                "а против течения реки — t2 ч\n");

        double distance = t1 * v + t2 * (v - v1);
        System.out.printf("Скорость = %.2f v1 = %.2f t1 = %.2f t2 = %.2f\n", v, v1, t1,t2);
        System.out.printf("Расстояние = %.2f", distance);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task33(char ch) {

        System.out.println("33. Ввести любой символ и определить его порядковый номер,\n" +
                "а также указать предыдущий и последующий символы\n");

        int ascii = ch;
        int prevCharInInt = ascii - 1;
        char prevChar = (char) prevCharInInt;

        int nextCharInInt = ascii + 1;
        char nextChar = (char) nextCharInInt;

        System.out.println("Символ: " + ch + ", порядковый номер: " + ascii);
        System.out.println("Предыдущий имвол: " + prevChar + ", порядковый номер: " + prevCharInInt);
        System.out.println("Следующий символ: " + nextChar + ", порядковый номер: " + nextCharInInt);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task34(double bytes) {

        System.out.println("34. Дана величина А, выражающая объем информации в байтах. Перевести А в более крупные\n" +
                "единицы измерения информации\n");
        double kBytes = bytes / 1000;
        double mBytes = kBytes / 1000;
        double gBytes = mBytes / 1000;
        double tBytes = gBytes / 1000;


        System.out.println("Размер в байтах: " + bytes);
        System.out.println("Размер в килобайтах: " + kBytes);
        System.out.println("Размер в мегабайтах: " + mBytes);
        System.out.println("Размер в гигабайтах: " + gBytes);
        System.out.println("Размер в терабайтах: " + tBytes);
        System.out.println("-------------------------------------------------------------------------------------------");
    }
}

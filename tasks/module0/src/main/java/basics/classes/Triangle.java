package basics.classes;

public class Triangle {
    MyPoint apexA;
    MyPoint apexB;
    MyPoint apexC;

    private double sideAB;
    private double sideBC;
    private double sideCA;

    public Triangle(MyPoint apexA, MyPoint apexB, MyPoint apexC) {
        this.apexA = apexA;
        this.apexB = apexB;
        this.apexC = apexC;
    }

    public void calculateAllSide () {
        sideAB = MyPoint.calculateDistance(apexA, apexB);
        sideBC = MyPoint.calculateDistance(apexB, apexC);
        sideCA = MyPoint.calculateDistance(apexC, apexA);
    }

    public double calculatePerimeter () {
        calculateAllSide();
        return sideAB+sideBC+sideCA;
    }

    //using Heron's formula
    public double calculateArea () {
        double s = (sideAB + sideBC + sideCA) / 2;
        return Math.sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideCA));
    }


    public MyPoint getApexA() {
        return apexA;
    }

    public MyPoint getApexB() {
        return apexB;
    }

    public MyPoint getApexC() {
        return apexC;
    }

    public double getSideAB() {
        return sideAB;
    }

    public double getSideBC() {
        return sideBC;
    }

    public double getSideCA() {
        return sideCA;
    }
}

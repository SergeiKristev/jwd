package calculator.oop;

public class Display {

    void staticDisplay() {
        System.out.println("    ООП КАЛЬКУЛЯТОР ЭЛЕКТРОНИКА ОКЭ-1     ");
        System.out.println("------------------------------------------");
        System.out.println("ДОСТУПНЫЕ КОМАНДЫ        | + | - | * | / |");
        System.out.println("ДЛЯ ПРОДОЛЖЕНИЯ РАБОТЫ ВВЕДИТЕ НОВОЕ ЧИСЛО");
        System.out.println("ДЛЯ ВЫХОДА ИЗ КАЛЬКУЛЯТОРА ВВЕДИТЕ       Q");
        System.out.println("------------------------------------------");
    }

    void printCommand(char command) {
        System.out.printf("|%-38s %s|\n", " ", command);
    }

    void printNumber(double num) {
        System.out.printf("|%40f|\n", num);
    }

    void printAnswer(double result) {
        System.out.printf("|ОТВЕТ: %33f|\n", result);
        System.out.println("ДЛЯ ПРОДОЛЖЕНИЯ НАЖМИТЕ ЛЮБУЮ КЛАВИШУ     ");
        System.out.println("ДЛЯ ВЫХОДА ИЗ КАЛЬКУЛЯТОРА ВВЕДИТЕ       Q");
        System.out.println("------------------------------------------");
    }
}

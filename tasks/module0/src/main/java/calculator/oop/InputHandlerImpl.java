package calculator.oop;

import java.util.Scanner;

public class InputHandlerImpl implements InputHandler {

    Scanner scanner = new Scanner(System.in);

    @Override
    public double readNumbers() {
        System.out.println("|ВВЕДИТЕ ЧИСЛО:                          |");
        double num;

        if(scanner.hasNextDouble()){
            num = scanner.nextDouble();
        } else {
            System.out.println("|НЕПРАВИЛЬНЫЙ ВВОД                       |");
            scanner.next();
            num = readNumbers();
        }
        return num;
    }

    @Override
    public char readUserCommands() {
        System.out.println("|ВВЕДИТЕ КОМАНДУ:                        |");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("|НЕПРАВИЛЬНЫЙ ВВОД                         |");
            scanner.next();
            operation = readUserCommands();
        }
        return operation;
    }
}

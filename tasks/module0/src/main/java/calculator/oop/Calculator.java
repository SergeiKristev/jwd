package calculator.oop;

import java.util.Scanner;

public class Calculator {
    private static double number1;
    private static double number2;
    private static double result;
    private static char command;
    String nextProcedure;

    Display display = new Display();
    InputHandlerImpl inputHandler = new InputHandlerImpl();
    MathSolverImpl mathSolver = new MathSolverImpl();


    void calculatorRunner() {
        display.staticDisplay();
        Scanner scanner = new Scanner(System.in);

        boolean isReady = true;
        while (isReady) {
            number1 = inputHandler.readNumbers();
            display.printNumber(number1);

            command = inputHandler.readUserCommands();

            display.printCommand(command);

            number2 = inputHandler.readNumbers();
            display.printNumber(number2);

            result = calculate(command, number1, number2);

            display.printAnswer(result);

            nextProcedure = scanner.nextLine();
            if (nextProcedure.equals("Q")) {
                System.exit(0);
            }
        }

    }

    double calculate (char command, double number1, double number2) {
        switch (command) {
            case '+':
                return mathSolver.add(number1, number2);
            case '-':
                return mathSolver.subtract(number1, number2);
            case '*':
                return mathSolver.multiply(number1, number2);
            case '/':
                return mathSolver.divide(number1, number2);
            default:
                System.out.println("НЕВЕРНАЯ ОПЕРАЦИЯ");
        }
        return 0;
    }


}

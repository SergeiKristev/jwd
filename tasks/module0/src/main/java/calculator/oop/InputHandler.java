package calculator.oop;

public interface InputHandler {

    double readNumbers();

    char readUserCommands();

}

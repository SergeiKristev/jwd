package calculator.oop;

public interface MathSolver {

    double add(double number1, double number2);

    double subtract(double number1, double number2);

    double multiply(double number1, double number2);

    double divide(double number1, double number2);
}

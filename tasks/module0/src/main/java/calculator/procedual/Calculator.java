package calculator.procedual;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        try {
            runCalculator();
            reader.close();
        } catch (IOException e) {
            System.out.println("ОШИБКА: " + e);        }
    }

    private static final String GET_FIRST_NUMBER = "ВВЕДИТЕ 1-е ЧИСЛО:";
    private static final String GET_SECOND_NUMBER = "ВВЕДИТЕ 2-е ЧИСЛО:";
    private static final String GET_COMMAND = "ВВЕДИТЕ КОМАНДУ:";
    private static final String GET_RESULT = "ОТВЕТ:";
    private static final String ADD = "+";
    private static final String SUBTRACT = "-";
    private static final String DIVIDE = "/";
    private static final String MULTIPLY = "*";

    private static double number1 = 0.00;
    private static double number2 = 0.00;
    private static double result = 0.00;

    private static boolean isRunning = true;

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    private static void displayStatic(String parametr) {
        clearDisplay();
        System.out.println("ПРОЦЕДУРНЫЙ КАЛЬКУЛЯТОР ЭЛЕКТРОНИКА  ПКЭ-1");
        System.out.println("------------------------------------------");
        displayExecute(parametr);
        System.out.println("------------------------------------------");
        System.out.println("ДОСТУПНЫЕ КОМАНДЫ        | + | - | * | / |");
        System.out.println("ДЛЯ ПРОДОЛЖЕНИЯ РАБОТЫ ВВЕДИТЕ НОВОЕ ЧИСЛО");
        System.out.println("ДЛЯ ВЫХОДА ИЗ КАЛЬКУЛЯТОРА ВВЕДИТЕ   ВЫХОД");
    }

    private static void displayExecute(String displayExecute){
        switch (displayExecute) {
            case GET_COMMAND:
                System.out.printf("|%-18s %21f|\n", GET_COMMAND, number1);
                break;
            case GET_SECOND_NUMBER:
                System.out.printf("|%-18s %21f|\n", GET_SECOND_NUMBER, number1);
                break;
            case GET_RESULT:
                System.out.printf("|%-18s %21f|\n", GET_RESULT, result);
                break;
            case ADD:
                System.out.printf("|%-18s %21s|\n", GET_SECOND_NUMBER, ADD);
                break;
            case SUBTRACT:
                System.out.printf("|%-18s %21s|\n", GET_SECOND_NUMBER, SUBTRACT);
                break;
            case DIVIDE:
                System.out.printf("|%-18s %21s|\n", GET_SECOND_NUMBER, DIVIDE);
                break;
            case MULTIPLY:
                System.out.printf("|%-18s %21s|\n", GET_SECOND_NUMBER, MULTIPLY);
                break;
                default:
                    System.out.printf("|%-18s %21f|\n", GET_FIRST_NUMBER, 0.00);

        }
    }

    private static void clearDisplay(){
        for (int i = 0; i < 20; i++) {
            System.out.println(" ");
        }
    }

    private static void readUserCommands() throws IOException {
        boolean isNotString = true;
        String operator = "";
        while (isNotString) {
            try {

                operator = reader.readLine();

                if (operator.equals("ВЫХОД")) {
                    System.out.println("ПРОЦЕДУРНЫЙ КАЛЬКУЛЯТОР ЭЛЕКТРОНИКА\n" +
                            "ПКЭ-1 ЗАВЕРШАЕТ РАБОТУ");
                    System.exit(0);
                }

                switch (operator) {
                    case ADD:
                    case SUBTRACT:
                    case MULTIPLY:
                    case DIVIDE:
                        break;
                    default:
                        System.out.println("НЕПРАВИЛЬНЫЙ ВВОД");
                }
                        isNotString = false;
            } catch (IOException | NumberFormatException e) {
                System.out.println("НЕПРАВИЛЬНЫЙ ВВОД");
            }
        }
        commands(operator);
        clearDisplay();
    }

    private static double readNumbers() throws IOException {
        boolean isString = true;
        double getNumber = 0;
        String s = null;
        while (isString) {
        try {
            s = reader.readLine();
            if (s.equals("ВЫХОД")) {
                System.out.println("ПРОЦЕДУРНЫЙ КАЛЬКУЛЯТОР ЭЛЕКТРОНИКА\n" +
                        "ПКЭ-1 ЗАВЕРШАЕТ РАБОТУ");
                System.exit(0);
            }
            getNumber = Double.parseDouble(s);

            isString = false;
        } catch (IOException | NumberFormatException e) {
            System.out.println("НЕПРАВИЛЬНЫЙ ВВОД");
            }
        }
        return getNumber;
    }


    private static void runCalculator() throws IOException {
        boolean isRunning = true;
        displayStatic(GET_FIRST_NUMBER);

        while (isRunning) {
            number1 = readNumbers();
            clearDisplay();
            displayStatic(GET_COMMAND);
            readUserCommands();
            clearDisplay();
            displayStatic(GET_SECOND_NUMBER);
            clearDisplay();
            displayStatic(GET_RESULT);
            number1 = 0.0;
            number2 = 0.0;
            result = 0;
            }
    }

    private static void commands(String command) throws IOException {
            switch (command) {
                case "+":
                    add();
                    break;
                case "-":
                    subtract();
                    break;
                case "*":
                    multiply();
                    break;
                case "/":
                    divide();
                    break;
                case "ВЫХОД":
                    isRunning = false;
                    break;
                    default:
                        System.out.println("НЕПРАВИЛЬНАЯ КОМАНДА");

        }
    }

    private static void add() throws IOException {
        displayStatic(ADD);
        number2 = readNumbers();
        result = number1 + number2;

    }

    private static void subtract() throws IOException {
        displayStatic(SUBTRACT);
        number2 = readNumbers();
        result = number1 - number2;
    }

    private static void multiply() throws IOException {
        displayStatic(MULTIPLY);
        number2 = readNumbers();
        result = number1 * number2;
    }

    private static void divide() throws IOException {
        displayStatic(DIVIDE);
        number2 = readNumbers();
        result = number1 / number2;
    }



}

package decomposition;

public class Task14 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы), определяющий, в каком из данных двух чисел больше цифр\n");

        runProgram(1235, 12341234);
    }

    private static int getNumberLength(int number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static void lengthCompare(int number1, int number2) {
        int result = 0;
        int length1 = getNumberLength(number1);
        int length2 = getNumberLength(number2);

        result = length1 - length2;

        if (result > 0) {
            System.out.println("Больше цифр содержится в числе: " + number1);
        } else if (result < 0) {
            System.out.println("Больше цифр содержится в числе: " + number2);
        } else {
            System.out.println("Числа равны по длине");
        }
    }

    public static void runProgram(int num1, int num2) {
        System.out.println("Даны числа " + num1 + " и " + num2);
        lengthCompare(num1, num2);
    }
}

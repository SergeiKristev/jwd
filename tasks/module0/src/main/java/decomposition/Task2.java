package decomposition;

public class Task2 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего\n" +
                "кратного двух натуральных чисел:\n");

        runProgram(16, 4);
    }

    private static long greatestCommonDivisor(long number1, long number2) {
        return number2 == 0 ? number1 : greatestCommonDivisor(number2, number1 % number2);
    }

    private static long leastCommonMultiple(long number1, long number2) {
        return number1 / greatestCommonDivisor(number1, number2) * number2;
    }

    public static void runProgram(long num1, long num2) {

        long resultGCD = greatestCommonDivisor(num1,num2);
        long resultLCM = leastCommonMultiple(num1,num2);

        System.out.println("Наибольший общеий делитель чисел " + num1 + " и " + num2 + " равен " + resultGCD);
        System.out.println("Наименьшее общее кратное чисел " + num1 + " и " + num2 + " равно " + resultLCM);
    }
}

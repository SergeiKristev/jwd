package decomposition;

import java.util.Random;

public class Task11 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4]\n" +
                "+D[5] +D[6]. Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных\n" +
                "элементов массива с номерами от k до m\n");

        runProgram(3, 9, 10);
    }

    private static int[] createArray(int n) {
        return new int[n];
    }


    private static void arrayRandomFilling(int[] arr, int bound) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(bound);
        }
    }

    private static int getSum(int[] arr, int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += arr[i];
        }
        return sum;
    }

    private static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length-1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.print(arr[arr.length-1] + "]\n");
    }

    public static void runProgram(int k, int m, int arrSize) {
        int[] arr = createArray(arrSize);
        arrayRandomFilling(arr, 100);
        printArray(arr);
        int result = getSum(arr, k, m);

        System.out.println("Сумма трех последовательно расположенных элементов массива с номерами от k = " + k + " до m = " + m + ": " + result);
    }
}

package decomposition;

public class Task12 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления\n" +
                "его площади, если угол между сторонами длиной X и Y— прямой\n");

        runProgram(5, 7, 7, 12);

    }

    private static double calculateDiameter(int x, int y) {
        return Math.sqrt(x * x + y * y);
    }

    private static double calculateRightTriangleArea(int x, int y) {
        return (double) (x * y) / 2;
    }

    //using Heron's formula
    private static double calculateTriangleArea (int x, int y, int z, int t) {
        double diameter = calculateDiameter(x, y);
        double s = (z + t + diameter) / 2;
        return Math.sqrt(s * (s - z) * (s - t) * (s - diameter));
    }

    private static double calculateQuadrangleArea(int x, int y, int z, int t) {
        double rightTriangleArea = calculateRightTriangleArea(x, y);
        double triangleArea = calculateTriangleArea(x, y, z, t);
        return rightTriangleArea + triangleArea;
    }

    public static void runProgram(int x, int y, int z, int t) {

        double result = calculateQuadrangleArea(x, y, z, t);
        System.out.println("Площадь четырехугольника со сторонами X = " + x + ", Y = " + y + ", Z = " + z +", T = " + t + " : " + result);
    }
}

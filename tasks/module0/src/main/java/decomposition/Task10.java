package decomposition;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9\n");

        runProgram(1, 9);

    }

    private static int calculateFactorial(int n){
        int result = 1;
        for (int i = 1; i <=n; i ++){
            if (isOdd(i)) {
                result = result * i;
            }
        }
        return result;
    }

    private static long calculateSumFactorial(int first, int last) {
        long sum = 0;
        for (int i = first; i <= last; i++) {
            sum += calculateFactorial(i);
        }
        return sum;
    }

    private static boolean isOdd(int num) {
        return num % 2 != 0;
    }

    public static void runProgram(int num1, int num2) {
        long result = calculateSumFactorial(num1, num2);

        System.out.println("Сумма факториалов всех нечетных чисел от " + num1 + " до " + num2 + ": " + result);
    }
}

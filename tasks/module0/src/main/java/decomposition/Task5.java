package decomposition;

public class Task5 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы) для нахождения суммы большего и меньшего из трех чисел\n");

        double num1 = 24;
        double num2 = 35.9;
        double num3 = 22.6;

        runProgram(24, 35.9, 22.6);

    }

    private static double findMinimum(double number1, double number2,double number3) {
        double min = Double.MAX_VALUE;
        if (number1 < min) {
            min = number1;
        }
        if (number2 < min) {
            min = number2;
        }
        if (number3 < min) {
            min = number3;
        }
        return min;
    }

    private static double findMaximum(double number1, double number2,double number3) {
        double max = Double.MIN_VALUE;
        if (number1 > max) {
            max = number1;
        }
        if (number2 > max) {
            max = number2;
        }
        if (number3 > max) {
            max = number3;
        }
        return max;
    }

    private static double getSumMinAndMax (double number1, double number2,double number3) {
        return findMinimum(number1, number2, number3) + findMaximum(number1, number2, number3);
    }

    public static void runProgram(double num1, double num2, double num3) {
        double sum = getSumMinAndMax(num1, num2, num3);
        System.out.println("Сумма большего и меньшего из чисел " + num1 + ", " + num2 + ", " + num3 + " равна " + sum);
    }
}

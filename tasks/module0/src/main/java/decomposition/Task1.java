package decomposition;

public class Task1 {
    public static void main(String[] args) {

        System.out.println("Условие:");
        System.out.println("Треугольник задан координатами своих вершин. Написать метод для вычисления его площади\n");

        runProgram(2, 4, 2, 8, 4, 6);

    }

    private static int pointAx;
    private static int pointAy;
    private static int pointBx;
    private static int pointBy;
    private static int pointCx;
    private static int pointCy;

    private static double sideAB;
    private static double sideBC;
    private static double sideCA;


    private static void setPointA(int x, int y) {
        pointAx = x;
        pointAy = y;
    }

    private static void setPointB(int x, int y) {
        pointBx = x;
        pointBy = y;
    }

    private static void setPointC(int x, int y) {
        pointCx = x;
        pointCy = y;
    }


    private static double calculateDistance(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    private static void calculateAllSide() {
        sideAB = calculateDistance(pointAx, pointAy, pointBx, pointBy);
        sideBC = calculateDistance(pointBx, pointBy, pointCx, pointCy);
        sideCA = calculateDistance(pointCx, pointCy, pointAx, pointBy);
    }

    //using Heron's formula
    private static double calculateArea () {
        double s = (sideAB + sideBC + sideCA) / 2;
        return Math.sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideCA));
    }

    public static void runProgram(int x1, int y1, int x2, int y2, int x3, int y3) {
        setPointA(x1,y1);
        setPointB(x2,y2);
        setPointC(x3,y3);
        calculateAllSide();
        double area = calculateArea();
        System.out.println("Площадь треугольника с вершинами (х1 = " + pointAx + " у1 = " + pointAy + ")" +
                ", (x2 = " + pointBx + ", у2 = " + pointBy + "),\n" +
                "и (x3 = " + pointCx + ", y3 = " + pointCy + ") равна " + area);
    }
}

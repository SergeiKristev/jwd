package decomposition;

import java.util.Arrays;
import java.util.Random;

public class Task13 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Дано натуральное число N. Написать метод(методы) для формирования массива, элементами\n" +
                "которого являются цифры числа N.\n");

        runProgram(23554457);
    }

    private static int[] createArray(int n) {
        return new int[n];
    }

    private static int getNumberLength(int number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static int[] getNumbers(int num) {
        int length = getNumberLength(num);
        int[] numbers = createArray(length);
        int result = 1;
        for (int i = numbers.length-1; i > 0; i--) {
            if (num <= 100) {
                numbers[1] = num % 10;
                numbers[0] = num / 10;
            } else {
                numbers[i] = num % 10;
                num = num / 10;
            }
        }
        return numbers;
    }

    private static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length-1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.print(arr[arr.length-1] + "]\n");
    }

    public static void runProgram(int num) {
        System.out.println("Массив цифр числа N = " + num);
        int[] result = getNumbers(num);
        printArray(result);
    }

}

package decomposition;

import java.util.Random;

public class Task7 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("На плоскости заданы своими координатами n точек. Написать метод(методы), определяющие,\n" +
                "между какими из пар точек самое большое расстояние. Указание. Координаты точек занести в массив\n");

        runProgram(10, 10);

    }

    private static int maxXpositionA = 0;
    private static int maxYpositionA = 0;
    private static int maxXpositionB = 0;
    private static int maxYpositionB = 0;

    private static int[][] createArrayWithPoints(int n) {
        return new int[n][2];
    }

    private static void pointArrayRandomFilling(int[][] arr, int bound) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
                arr[i][0] = rand.nextInt(bound);
                arr[i][1] = rand.nextInt(bound);
        }
    }

    private static double calculateDistance(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    private static double getMaxDistance(int[][] arr) {
        double maxDistance = Double.MIN_VALUE;
        double currentDistance = 0;
        for (int i = 1; i <= arr.length-1; i++) {
            currentDistance = calculateDistance(arr[i-1][0], arr[i-1][1], arr[i][0], arr[i][1]);
            if(currentDistance > maxDistance) {
                maxDistance = currentDistance;
                maxXpositionA = arr[i-1][0];
                maxYpositionA = arr[i-1][1];
                maxXpositionA = arr[i][0];
                maxYpositionA = arr[i][1];
            }
        }
        return maxDistance;
    }

    private static void printMultidimensionalArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
                System.out.print("X: " + arr[i][0] + "\t");
                System.out.print("Y: " + arr[i][1] + "\t");
                System.out.println();
        }
        System.out.println();
    }

    public static void runProgram(int n, int bound) {

        int[][] test = createArrayWithPoints(n);
        pointArrayRandomFilling(test, bound);

        System.out.println("Полученный массив:\n");
        printMultidimensionalArray(test);

        double distance = getMaxDistance(test);

        System.out.println("Максимальное расстояние между двумя точками с координатами (X1 = " + maxXpositionA + " Y1 = " +maxYpositionA + ")\n" +
                "и (X2 = " + maxXpositionB + ", Y2 = " + maxYpositionB + ") равно " + distance);
    }

}

package decomposition;

import java.util.Random;

public class Task8 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Составить программу, которая в массиве A[N] находит второе по величине число (вывести\n" +
                "на печать число, которое меньше максимального элемента массива, но больше всех других элементов)\n");

        runProgram(10, 100);
    }

    private static int[] createArray(int n) {
        return new int[n];
    }


    private static void arrayRandomFilling(int[] arr, int bound) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(bound);
        }
    }

    private static int[] arrayBubbleSort(int[] arr) {
        int temp = 0;
        for (int i = arr.length-1; i > 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j+1]) {
                    temp = arr[j+1];
                    arr[j+1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    private static void printArray(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length-1; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.print(arr[arr.length-1] + "]\n");
    }

    private static int findPrevMax(int[] arr) {
        arrayBubbleSort(arr);
        if (arr.length > 1) {
            return arr[arr.length - 2];
        }
        return arr[0];
    }

    public static void runProgram(int n, int bound) {

        int[] arr = createArray(n);
        arrayRandomFilling(arr, bound);
        System.out.println("Полученный массив:\n");
        printArray(arr);

        int result = findPrevMax(arr);

        System.out.println("Второе по величине число в массиве: " + result);
    }
}

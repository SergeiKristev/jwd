package decomposition;

import java.util.ArrayList;
import java.util.List;

public class Task15 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Даны натуральные числа К и N. Написать метод(методы) формирования массива А, элементами\n" +
                "которого являются числа, сумма цифр которых равна К и которые не большее N.\n");

        runProgram(4, 1200);

    }

    private static int getSum(int number) {
        int result = 0;
        while (number > 0){
            result = result +  (number % 10);
            number = number / 10;
        }
        return result;
    }

    private static List<Integer> fillingArray(int k, int n) {
        List<Integer> list = new ArrayList<>();
        int data = 0;
        int count = 0;
        for (int i = 0; i < n; i++) {
            data = getSum(i);
            if (data == k) {
                list.add(i);
                count++;
            }
        }
        return list;
    }

    public static void runProgram(int k, int n) {
        System.out.println("Даны числа " + k + " и " + n);
        List<Integer> list = fillingArray(k, n);
        System.out.println("Полученный массив:");
        System.out.println(list);
    }
}

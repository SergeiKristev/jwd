package decomposition;

public class Task17 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его\n" +
                "цифр, возведенная в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения\n" +
                "задачи использовать декомпозицию\n");

        printArmstrongsNumbers(9999);
    }

    private static int getNumberLength(long number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static int getNumberInPow(long number, int length) {
        return (int)Math.pow(number, length);
    }

    private static boolean checkArmstrongsNumber(long number) {
        long startNumber = number;
        int length = getNumberLength(number);
        long result = 0;
        long sum = 0;
        while (number > 0){
            result = number % 10;
            number = number / 10;
            sum += getNumberInPow(result, length);
        }
        return sum == startNumber;
    }

    public static void printArmstrongsNumbers(long k) {
        for (long i = 1; i <= k ; i++) {
            if (checkArmstrongsNumber(i))
                System.out.println(i);
        }
    }


}

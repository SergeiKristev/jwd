package decomposition;

public class Task3 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел\n");

        runProgram(16, 4 ,2 ,9);
    }

    private static long greatestCommonDivisor(long number1, long number2) {
        return number2 == 0 ? number1 : greatestCommonDivisor(number2, number1 % number2);
    }

    private static long greatestCommonDivisorFour (long number1, long number2, long number3, long number4) {
        long gcd1 = greatestCommonDivisor(number1, number2);
        long gcd2 = greatestCommonDivisor(number3, number4);
        return gcd2 == 0 ? gcd1 : greatestCommonDivisor(gcd2, gcd1 % gcd2);
    }

    public static void runProgram(long num1, long num2, long num3, long num4) {

        long resultGCD = greatestCommonDivisorFour(num1, num2, num3, num4);
        System.out.println("Наибольший общеий делитель чисел " + num1 + ", " + num2 + ", " + num3 + ", " +num4 + " равен " + resultGCD);
    }
}

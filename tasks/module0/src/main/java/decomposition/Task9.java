package decomposition;

public class Task9 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми\n");

        runProgram(5, 42, 33);
    }
    private static long greatestCommonDivisor(long number1, long number2) {
        return number2 == 0 ? number1 : greatestCommonDivisor(number2, number1 % number2);
    }


    private static boolean checkGCD(long number1, long number2, long number3) {
        long gcd1 = greatestCommonDivisor(number1, number2);
        long gcd2 = greatestCommonDivisor(gcd1, number3);
        return gcd2 == 1;
    }

    public static void runProgram(long num1, long num2, long num3) {

        boolean result = checkGCD(num1, num2, num3);
        System.out.println("Являются ли числа " + num1 + ", " + num2 + ", " + num3 + " взаимно простыми: " + result);
    }

}

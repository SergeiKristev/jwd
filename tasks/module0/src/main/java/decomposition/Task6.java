package decomposition;

public class Task6 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Вычислить площадь правильного шестиугольника со стороной а, используя метод вычисления\n" +
                "площади треугольника\n");

        runProgram(24);
    }

    private static double calculateRightTriangleArea(double side) {
        return (Math.sqrt(3) * Math.pow(side, 2)) / 4;
    }

    private static double calculateHexagonArea(double side) {
        return calculateRightTriangleArea(side) * 6;
    }

    public static void runProgram(double side) {
        double area = calculateHexagonArea(side);
        System.out.println("Площадь правильного шестиугольниа со стороной " + side + " равна " + area);
    }
}

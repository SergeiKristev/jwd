package decomposition;

public class Task18 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую\n" +
                "последовательность (например, 1234, 5789). Для решения задачи использовать декомпозицию\n");

        printNumbers(9999);
    }

    private static int getNumberLength(int number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static int[] getNumbers(int num) {
        int length = getNumberLength(num);
        int[] numbers = new int[length];
        int result = 1;
        for (int i = numbers.length-1; i > 0; i--) {
            if (num <= 100) {
                numbers[1] = num % 10;
                numbers[0] = num / 10;
            } else {
                numbers[i] = num % 10;
                num = num / 10;
            }
        }
        return numbers;
    }

    private static boolean checkNumber(int[] arr) {
        for (int i = 1; i < arr.length ; i++) {
            if (arr[i] <= arr[i-1]) {
                return false;
            }
        } return true;
    }

    public static void printNumbers(int number) {
        for (int i = 0; i < number; i++) {
            int[] result = getNumbers(i);
            if (checkNumber(result)) {
                System.out.println(i);
            }
        }
    }

}

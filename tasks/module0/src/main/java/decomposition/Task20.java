package decomposition;

public class Task20 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д.\n" +
                "Сколько таких действий надо произвести, чтобы получился нуль? Для решения задачи использовать\n" +
                "декомпозицию..\n");
        runProgram(44);
    }

    private static int getNumberLength(int number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static int[] getNumbers(int num) {
        int length = getNumberLength(num);
        int[] numbers = new int[length];
        int result = 1;

        if (num < 10) {
            numbers[0] = num;
        }

        for (int i = numbers.length-1; i > 0; i--) {
            if (num <= 100) {
                numbers[1] = num % 10;
                numbers[0] = num / 10;
            } else {
                numbers[i] = num % 10;
                num = num / 10;
            }
        }
        return numbers;
    }

    private static int getSumNumbers(int[] number) {
        int count = 0;
        int sum = 0;
        for (int i = 0; i < number.length; i++) {
                sum += number[i];
        } return sum;
    }

    private static void runProgram(int number) {
        int currentNumber =0;
        int prevNumber = number;
        int sumNumbers = 0;
        int count = 0;
        while (prevNumber > 0) {
            int[] arrNumbers = getNumbers(prevNumber);
            sumNumbers = getSumNumbers(arrNumbers);
            currentNumber = prevNumber - sumNumbers;
            prevNumber = currentNumber;
            count++;
        }
        System.out.println("Сумма действий для числа " + number + ": " + count);
    }
}

package decomposition;

public class Task19 {

    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры.\n" +
                "Определить также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.\n");

        runProgram(999);
    }

    private static int getNumberLength(int number) {
        String num = String.valueOf(number);
        return num.length();
    }

    private static int[] getNumbers(int num) {
        int length = getNumberLength(num);
        int[] numbers = new int[length];
        int result = 1;
        for (int i = numbers.length-1; i > 0; i--) {
            if (num <= 100) {
                numbers[1] = num % 10;
                numbers[0] = num / 10;
            } else {
                numbers[i] = num % 10;
                num = num / 10;
            }
        }
        return numbers;
    }

    private static boolean checkNumber(int[] arr) {
        for (int i = 1; i < arr.length ; i++) {
            if (arr[i] % 2 ==0) {
                return false;
            }
        } return true;
    }


    private static int getSumEvenNumbers(int sum) {
        int count = 0;
        int[] sumArr = getNumbers(sum);
        for (int i = 1; i < sumArr.length; i++) {
            if (sumArr[i] % 2 == 0) {
                count++;
            }
        } return count;
    }


    public static void runProgram(int number) {
        int sum = 0;
        int countOdd = 0;
        for (int i = 0; i < number; i++) {
            int[] result = getNumbers(i);
            if (checkNumber(result)) {
                sum += i;
            }
        }
        countOdd = getSumEvenNumbers(sum);

        System.out.println("Сумма n - значных чисел, содержащих только нечетные цифры в диапазонеот 0 до " +
                number + " равна " + sum + ", количество четных чисел в сумме равно " + countOdd);
    }
}

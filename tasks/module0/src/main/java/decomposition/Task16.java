package decomposition;

public class Task16 {
    public static void main(String[] args) {
        System.out.println("Условие:");
        System.out.println("Два простых числа называются «близнецами», если они отличаются друг от друга на 2\n" +
                "(например, 41 и 43). Найти и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное\n" +
                "натуральное число больше 2. Для решения задачи использовать декомпозицию\n");

        printTwins(100);
    }

    public static boolean checkSimple(int i){
        if (i <= 1)
            return false;
        else if (i <=3 )
            return true;
        else if (i % 2 == 0 || i % 3 == 0)
            return false;
        int n = 5;
        while (n * n <= i){
            if (i % n == 0 || i % (n + 2) == 0)
                return false;
            n = n + 6;
        }
        return true;
    }

    private static boolean checkTwins(int number1, int number2) {
        return Math.abs(number1 - number2) == 2;
    }

    public static void printTwins(int n) {
        if (n < 2) {
            System.out.println("Число n должно быть больше 2");
        } else {
            for(int i = n; i <= n*2; ++i) {
                if(checkSimple(i)) {
                    for(int j = i; j <= n*2; ++j) {
                        if(checkSimple(j) && checkTwins(j, i))
                            System.out.println(i + " " + j);
                    }
                }
            }
        }

    }
}

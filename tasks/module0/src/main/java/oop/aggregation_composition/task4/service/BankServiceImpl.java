package oop.aggregation_composition.task4.service;

import oop.aggregation_composition.task4.model.Account;

import java.util.Comparator;
import java.util.List;

public class BankServiceImpl implements BankService {

    public String getBalance(List<Account> accounts, long id) {
        double balance = 0;
        for (Account account : accounts) {
            if (account.getId() == id)
            balance = account.getBalance();
        }
        return "Account`s balance id=" + id + ": " + balance;
    }

    public String getBalancePositiveAccounts(List<Account> accounts) {
        double balance = 0;
        for (Account account : accounts) {
            if (account.getBalance() > 0) {
                balance += account.getBalance();
            }

        }
        return "Account`s balance: " + balance;
    }

    public String getBalanceNegativeAccounts(List<Account> accounts) {
        double balance = 0;
        for (Account account : accounts) {
            if (account.getBalance() < 0) {
                balance += account.getBalance();
            }

        }
        return "Account`s balance: " + balance;
    }

    public void sortByBalance(List<Account> accounts) {
        accounts.sort(Comparator.comparing(Account::getBalance));
    }

    public Account getAccount (List<Account> accounts, long id) {
        for (Account client : accounts) {
            if (client.getId() == id) {
                return client;
            }
        }
        return null;
    }

    public void openAccount(Account account, List<Account> accounts) {
        accounts.add(account);
    }

    public void blockAccaunt (List<Account> accounts, long id) {
        for (Account account : accounts) {
            if(account.getId() == id) {
                account.setOpen(false);
            }
        }
    }

    public void unlockAccaunt (List<Account> accounts, long id) {
        for (Account account : accounts) {
            if(account.getId() == id) {
                account.setOpen(true);
            }
        }
    }

    public String getInformation (List<Account> accounts) {
        String information = "";
        for (Account account : accounts) {
            information += String.format("#%d, balance: %.2f, status: %s\n", account.getId(), account.getBalance(), account.isOpen());
        }
        return information;
    }

}

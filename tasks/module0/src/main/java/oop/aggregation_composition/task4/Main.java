package oop.aggregation_composition.task4;

import oop.aggregation_composition.task4.model.Account;
import oop.aggregation_composition.task4.model.Client;
import oop.aggregation_composition.task4.service.BankService;
import oop.aggregation_composition.task4.service.BankServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Client client1 = new Client("Ivan", "Ivanov", "MC123456");
        Client client2 = new Client("Petr", "Petrovov", "MC123457");

        client1.getAccounts().add(new Account(123456789, 10000));
        client1.getAccounts().add(new Account(245676789, 2000));
        client1.getAccounts().add(new Account(124343534, -20));

//        List<Account> client2Accounts = new ArrayList<>();
//        client2Accounts.add(new Account(685654675, 1450));
//        client2Accounts.add(new Account(324234344, 3400));
//        client2Accounts.add(new Account(123435445, -20));

        BankService bankService = new BankServiceImpl();
        System.out.println(client1.getAccounts());

        bankService.blockAccaunt(client1.getAccounts(), 123456789);
        System.out.println(bankService.getAccount(client1.getAccounts(), 123456789));
        System.out.println(bankService.getBalance(client1.getAccounts(), 123456789));
        System.out.println(bankService.getBalanceNegativeAccounts(client1.getAccounts()));
        bankService.sortByBalance(client1.getAccounts());
        System.out.println(client1.getAccounts());

    }

}

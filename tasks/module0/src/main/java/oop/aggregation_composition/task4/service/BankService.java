package oop.aggregation_composition.task4.service;

import oop.aggregation_composition.task4.model.Account;

import java.util.Comparator;
import java.util.List;

public interface BankService {

    String getBalance(List<Account> accounts, long id);

    String getBalancePositiveAccounts(List<Account> accounts);

    String getBalanceNegativeAccounts(List<Account> accounts);

    void sortByBalance(List<Account> accounts);

    Account getAccount (List<Account> accounts, long id);

    void openAccount (Account account, List<Account> accounts);

    void blockAccaunt (List<Account> accounts, long id);

    void unlockAccaunt (List<Account> accounts, long id);

    String getInformation (List<Account> accounts);
}

package oop.aggregation_composition.task5.model;

import java.util.Objects;

public class TourPackage {
    private String country;
    private String city;
    private TourPackageType type;
    private Transport transport;
    private TypeOfFood food;
    private int numberOfDays;
    private double price;

    public TourPackage(String country, String city, TourPackageType type, Transport transport, TypeOfFood food, int numberOfDays, double price) {
        this.country = country;
        this.city = city;
        this.type = type;
        this.transport = transport;
        this.food = food;
        this.numberOfDays = numberOfDays;
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public TourPackageType getType() {
        return type;
    }

    public void setType(TourPackageType type) {
        this.type = type;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public TypeOfFood getFood() {
        return food;
    }

    public void setFood(TypeOfFood food) {
        this.food = food;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TourPackage that = (TourPackage) o;

        if (numberOfDays != that.numberOfDays) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (!Objects.equals(country, that.country)) return false;
        if (!Objects.equals(city, that.city)) return false;
        if (type != that.type) return false;
        if (transport != that.transport) return false;
        return food == that.food;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = country != null ? country.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (transport != null ? transport.hashCode() : 0);
        result = 31 * result + (food != null ? food.hashCode() : 0);
        result = 31 * result + numberOfDays;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "TourPackage{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", type=" + type +
                ", transport=" + transport +
                ", food=" + food +
                ", numberOfDays=" + numberOfDays +
                ", price=" + price +
                '}';
    }
}

package oop.aggregation_composition.task5.model;

import java.util.ArrayList;
import java.util.List;

public class TourAggregator {

    private String nameOfTourOperator;
    private List<TourPackage> tourPackages;
    private List<Client> clients;

    public TourAggregator(String nameOfTourOperator, List<TourPackage> tourPackages, List<Client> clients) {
        this.nameOfTourOperator = nameOfTourOperator;
        this.tourPackages = tourPackages;
        this.clients = clients;
    }

    public String getNameOfTourOperator() {
        return nameOfTourOperator;
    }

    public void setNameOfTourOperator(String nameOfTourOperator) {
        this.nameOfTourOperator = nameOfTourOperator;
    }

    public List<TourPackage> getTourPackages() {
        return tourPackages;
    }

    public void setTourPackages(List<TourPackage> tourPackages) {
        this.tourPackages = tourPackages;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
}

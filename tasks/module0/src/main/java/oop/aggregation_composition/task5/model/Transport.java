package oop.aggregation_composition.task5.model;

public enum Transport {

    PLANE,
    BUS,
    TRAIN,
    SHIP,
    CAR;

}

package oop.aggregation_composition.task5.service;

import oop.aggregation_composition.task5.model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TravelAggregator implements TravelPackageService {


    public List<TourPackage> selectByMoreOption (List<TourPackage> tourPackages, Transport transport, TypeOfFood food, int days){
        List<TourPackage> packages = new ArrayList<>();
        for (TourPackage tour : tourPackages) {
            if (tour.getTransport().equals(transport) && tour.getFood().equals(food) && tour.getNumberOfDays() == days) {
                packages.add(tour);
            }
        }
        return packages;
    }

    public List<TourPackage> selectByMoreOption (List<TourPackage> tourPackages, int days){
        List<TourPackage> packages = new ArrayList<>();
        for (TourPackage tour : tourPackages) {
            if (tour.getNumberOfDays() == days) {
                packages.add(tour);
            }
        }
        return packages;
    }

    public void printTours (List<TourPackage> packages) {
        if (packages.size() == 0) {
            System.out.println("TourAggregator not found");
            return;
        }
        for (TourPackage tour : packages) {
            System.out.println(tour);
        }
    }

    public void sortByPrice(List<TourPackage> tourPackages) {
        tourPackages.sort(Comparator.comparing(TourPackage::getPrice));
    }

    public void sortByNumberOfDays(List<TourPackage> tourPackages) {
        tourPackages.sort(Comparator.comparing(TourPackage::getNumberOfDays));
    }

}

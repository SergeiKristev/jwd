package oop.aggregation_composition.task5.service;

import oop.aggregation_composition.task5.model.*;

import java.util.List;

public interface TravelPackageService {

    List<TourPackage> selectByMoreOption (List<TourPackage> tourPackages, Transport transport, TypeOfFood food, int days);

    List<TourPackage> selectByMoreOption (List<TourPackage> tourPackages, int days);

    void printTours (List<TourPackage> packages);

    void sortByPrice(List<TourPackage> tourPackages);

    void sortByNumberOfDays(List<TourPackage> tourPackages);
}

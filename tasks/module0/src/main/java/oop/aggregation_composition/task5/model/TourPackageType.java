package oop.aggregation_composition.task5.model;

public enum TourPackageType {

    RECREATION,
    EXCURSION,
    TREATMENT,
    SHOPPING,
    CRUISE;
}

package oop.aggregation_composition.task5;

import oop.aggregation_composition.task5.model.*;
import oop.aggregation_composition.task5.service.TravelAggregator;
import oop.aggregation_composition.task5.service.TravelPackageService;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        TravelPackageService service = new TravelAggregator();

        List<TourPackage> tourPackages = new ArrayList<>();

        tourPackages.add(new TourPackage("Italy", "Milan", TourPackageType.CRUISE,
                Transport.PLANE, TypeOfFood.ALL_INCLUSIVE, 30, 5000));
        tourPackages.add(new TourPackage("Russia", "Adler", TourPackageType.TREATMENT,
                Transport.TRAIN, TypeOfFood.BREAKFAST, 14, 500));

        service.printTours(tourPackages);
        System.out.println();

        System.out.println(service.selectByMoreOption(tourPackages, 14));
        System.out.println();

        service.sortByNumberOfDays(tourPackages);
        service.printTours(tourPackages);

    }
}

package oop.aggregation_composition.task2;

import java.util.Arrays;
import java.util.Objects;

public class Car {

    private String model;
    private int price;
    private int yearOfManufacture;
    private Engine engine;
    private Wheel[] wheels;

    public Car(String model, int price, int yearOfManufacture, Engine engine, Wheel[] wheels) {
        this.model = model;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
        this.engine = engine;
        this.wheels = wheels;
    }

    public void printModel() {
        System.out.println("Model " + this.getModel());
    }

    public void go(){
        System.out.println(model + " is going");
    }

    public void refuel() {

        System.out.println(model + " is refueling");
    }

    public void changeWheel(int wheal) {
        if (wheal > 0 && wheal <= wheels.length ) {
            System.out.println(model + " is changing wheal № " + wheal);
        } else {
            System.out.println("Are you serious?");
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (price != car.price) return false;
        if (yearOfManufacture != car.yearOfManufacture) return false;
        if (!Objects.equals(model, car.model)) return false;
        if (!Objects.equals(engine, car.engine)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(wheels, car.wheels);

    }

    @Override
    public int hashCode() {
        int result = model != null ? model.hashCode() : 0;
        result = 31 * result + price;
        result = 31 * result + yearOfManufacture;
        result = 31 * result + (engine != null ? engine.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(wheels);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", yearOfManufacture=" + yearOfManufacture +
                ", engine=" + engine +
                ", wheels=" + Arrays.toString(wheels) +
                '}';
    }
}

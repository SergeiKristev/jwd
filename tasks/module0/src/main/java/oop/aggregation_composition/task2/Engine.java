package oop.aggregation_composition.task2;

public abstract class Engine {

    String manufacturer;

    public Engine(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}

package oop.aggregation_composition.task2;

public class Main {
    public static void main(String[] args) {

        Car electroCar = new Car("Tesla", 100000, 2020,
                new ElectricEngine("Tesla Motors", 85), new Wheel[4]);

        Car gasCar = new Car("BMW", 60000, 2019,
                new GasEngine("BMW motors", 250), new Wheel[4]);

        electroCar.printModel();
        gasCar.printModel();

        electroCar.go();
        gasCar.go();

        electroCar.refuel();
        gasCar.refuel();

        electroCar.changeWheel(2);
        gasCar.changeWheel(5);

    }
}

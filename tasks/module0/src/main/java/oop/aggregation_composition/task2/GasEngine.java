package oop.aggregation_composition.task2;

public class GasEngine extends Engine {

    private int horsePower;

    public GasEngine(String manufacturer, int horsePower) {
        super(manufacturer);
        this.horsePower = horsePower;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public void checkFuelSystem() {
        System.out.println("Fuel system checked");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GasEngine gasEngine = (GasEngine) o;

        return horsePower == gasEngine.horsePower;

    }

    @Override
    public int hashCode() {
        return horsePower;
    }
}

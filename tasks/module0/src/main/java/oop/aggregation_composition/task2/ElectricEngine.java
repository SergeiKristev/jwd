package oop.aggregation_composition.task2;

public class ElectricEngine extends Engine {

    private int powerInkilowatts;

    public ElectricEngine(String manufacturer, int powerInkilowatts) {
        super(manufacturer);
        this.powerInkilowatts = powerInkilowatts;
    }

    public int getPowerInkilowatts() {
        return powerInkilowatts;
    }

    public void setPowerInkilowatts(int powerInkilowatts) {
        this.powerInkilowatts = powerInkilowatts;
    }

    public void checkBattеry() {
        System.out.println("Battery checked");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ElectricEngine that = (ElectricEngine) o;

        return powerInkilowatts == that.powerInkilowatts;

    }

    @Override
    public int hashCode() {
        return powerInkilowatts;
    }

    @Override
    public String toString() {
        return "ElectricEngine{" +
                "powerInkilowatts=" + powerInkilowatts +
                '}';
    }
}

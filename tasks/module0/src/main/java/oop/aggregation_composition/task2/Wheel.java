package oop.aggregation_composition.task2;

public class Wheel {

    private int diameter;

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wheel wheel = (Wheel) o;

        return diameter == wheel.diameter;

    }

    @Override
    public int hashCode() {
        return diameter;
    }
}

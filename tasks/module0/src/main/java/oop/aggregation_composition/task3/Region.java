package oop.aggregation_composition.task3;

import java.util.List;

public class Region {

    private String regionCentreName;
    private int districtCount;
    private List<District> districtList;

    public Region(String regionCentreName, int districtCount, List<District> districtList) {
        this.regionCentreName = regionCentreName;
        this.districtCount = districtCount;
        this.districtList = districtList;
    }

    public String getRegionCentreName() {
        return regionCentreName;
    }

    public void setRegionCentreName(String regionCentreName) {
        this.regionCentreName = regionCentreName;
    }

    public int getDistrictCount() {
        return districtCount;
    }

    public void setDistrictCount(int districtCount) {
        this.districtCount = districtCount;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Region region = (Region) o;

        if (districtCount != region.districtCount) return false;
        if (regionCentreName != null ? !regionCentreName.equals(region.regionCentreName) : region.regionCentreName != null)
            return false;
        return districtList != null ? districtList.equals(region.districtList) : region.districtList == null;

    }

    @Override
    public int hashCode() {
        int result = regionCentreName != null ? regionCentreName.hashCode() : 0;
        result = 31 * result + districtCount;
        result = 31 * result + (districtList != null ? districtList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Region{" +
                "regionCentreName='" + regionCentreName + '\'' +
                ", districtCount=" + districtCount +
                ", districtList=" + districtList +
                '}';
    }
}

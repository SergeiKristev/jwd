package oop.aggregation_composition.task3;

import java.util.List;

public class State {

    private String capital;
    private double area;
    private List<Region> regionList;

    public State(String capital, double area, List<Region> regionList) {
        this.capital = capital;
        this.area = area;
        this.regionList = regionList;
    }

    public void printCapital() {
        System.out.println("The capital is " + getCapital());
    }

    public void printRegionsCount() {
        System.out.println("Regions count: " + regionList.size());
    }

    public void printStateArea() {
        System.out.println("State area is: " + getArea());
    }

    public void printRegionsCentres() {
        for (Region center: regionList) {
            System.out.println(center.getRegionCentreName());
        }
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public List<Region> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<Region> regionList) {
        this.regionList = regionList;
    }


}

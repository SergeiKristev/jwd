package oop.aggregation_composition.task3;

import java.util.List;

public class District {

    private String districtCentreName;
    private List<City> cityList;

    public District(String districtCentreName, List<City> cityList) {
        this.districtCentreName = districtCentreName;
        this.cityList = cityList;
    }

    public String getDistrictCentreName() {
        return districtCentreName;
    }

    public void setDistrictCentreName(String districtCentreName) {
        this.districtCentreName = districtCentreName;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        District district = (District) o;

        if (districtCentreName != null ? !districtCentreName.equals(district.districtCentreName) : district.districtCentreName != null)
            return false;
        return cityList != null ? cityList.equals(district.cityList) : district.cityList == null;

    }

    @Override
    public int hashCode() {
        int result = districtCentreName != null ? districtCentreName.hashCode() : 0;
        result = 31 * result + (cityList != null ? cityList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "District{" +
                "districtCentreName='" + districtCentreName + '\'' +
                ", cityList=" + cityList +
                '}';
    }
}

package oop.aggregation_composition.task3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<City> minskDistrictCities = new ArrayList<>();
        List<City> borisovDistrictCities = new ArrayList<>();

        List<District> minskDistrictsList = new ArrayList<>();
        List<District> brestDistrictsList = new ArrayList<>();

        List<Region> belarusRegionsList = new ArrayList<>();

        /*
        create minsk district
         */
        City minsk = new City("Minsk", 2_000_000);
        City fanipol = new City("Fanipol", 90_000);
        City borisov = new City("Borisov", 140_000);

        minskDistrictCities.add(minsk);
        minskDistrictCities.add(fanipol);

        District minskDistrict = new District("Minsk", minskDistrictCities);
        District borisovDistrict = new District("Borisov", borisovDistrictCities);

        minskDistrictsList.add(minskDistrict);
        minskDistrictsList.add(borisovDistrict);

        /*
        create minsk region
         */
        Region minskRegion = new Region("Minsk", 18, minskDistrictsList);
        Region breastRegion = new Region("Brest", 16, brestDistrictsList); //empty list

        belarusRegionsList.add(minskRegion);
        belarusRegionsList.add(breastRegion);

        /*
        Create State
         */

        State belarus = new State("Minsk", 3242334534534.23, belarusRegionsList);

        belarus.printCapital();
        belarus.printRegionsCount();
        belarus.printRegionsCentres();
        belarus.printStateArea();

    }
}

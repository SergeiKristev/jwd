package oop.aggregation_composition.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Sentence {

    private List<Word> wordList;

    public Sentence() {
        wordList = new ArrayList<>();
    }


   public void appendWord(Word word) {
        wordList.add(word);
   }

    public void appendWord(List<Word> list) {
        wordList.addAll(list);
    }

    public void printSentence () {
        for (Word word: wordList) {
            System.out.print(word + " ");
        }
    }


    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sentence sentence = (Sentence) o;

        return Objects.equals(wordList, sentence.wordList);

    }

    @Override
    public int hashCode() {
        return wordList != null ? wordList.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "wordList=" + wordList +
                '}';
    }
}

package oop.aggregation_composition.task1;

import java.util.ArrayList;
import java.util.List;

public class Text {

    private String header = "No name";
    private String author = "No author";
    private List<Sentence> body;

    public Text() {
    }

    public Text(String header) {
        this.header = header;
        this.body = new ArrayList<>();
    }


    public Text(String header, String author) {
        this.header = header;
        this.author = author;
        this.body = new ArrayList<>();
    }

    public void appendText(Word word) {
        Sentence sentence = new Sentence();
        sentence.appendWord(word);
        body.add(sentence);
    }

    public void appendText(Sentence sentence) {
        body.add(sentence);
    }

    public void appendText(List<Sentence> list) {
        body.addAll(list);
    }

    public void printHeader() {
        System.out.println("Text: " + header);
        System.out.println("Author: " + author);
    }

    public void printText() {
        for (Sentence s : body) {
            s.printSentence();
            System.out.println();
        }
    }

    @Override
    public String toString() {
        return "Text{" +
                "header='" + header + '\'' +
                ", author='" + author + '\'' +
                ", body=" + body +
                '}';
    }
}

package oop.aggregation_composition.task1;

/*
    1. Создать объект класса Текст, используя классы Предложение, Слово. Методы: дополнить текст, вывести на консоль текст,
    заголовок текста.
 */

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Word> wordList = new ArrayList<>();
        List<Sentence> sentenceList = new ArrayList<>();

        Word word1 = new Word("I");
        Word word2 = new Word("am");
        Word word3 = new Word("console");
        Word word4 = new Word("viewer");
        Word word5 = new Word("I");
        Word word6 = new Word("do");
        Word word7 = new Word("not");
        Word word8 = new Word("work");
        Word word9 = new Word("well");

        wordList.add(word1);
        wordList.add(word2);
        wordList.add(word3);

        Sentence sentence1 = new Sentence();
        Sentence sentence2 = new Sentence();

        sentence1.appendWord(wordList);
        sentence1.appendWord(word4);

        sentence2.appendWord(word5);
        sentence2.appendWord(word6);
        sentence2.appendWord(word7);
        sentence2.appendWord(word8);

        sentenceList.add(sentence1);
        sentenceList.add(sentence2);

        Text text = new Text("Some text", "Some author");
        text.appendText(sentenceList);
        text.appendText(word9);

        text.printHeader();
        text.printText();
    }
}

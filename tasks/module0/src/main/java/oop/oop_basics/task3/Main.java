package oop.oop_basics.task3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        int year = 2020;
        Calendar calendar = new Calendar(year);
        calendar.addWeekend();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        calendar.addHoliday(LocalDate.parse("01.01." + year, dtf), "New Year");
        calendar.addHoliday(LocalDate.parse("08.03." + year, dtf), "Women’s Day");
        calendar.addHoliday(LocalDate.parse("09.05." + year, dtf), "Victory Day");
        calendar.addHoliday(LocalDate.parse("03.07." + year, dtf), "Independence Day");

        calendar.printHolidays();

        Calendar.Holiday holiday = calendar.removeHoliday("Women’s Day");
        System.out.println("\nDeleted: " + holiday);
        System.out.println();

        calendar.printHolidays();
    }
}

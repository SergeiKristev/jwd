package oop.oop_basics.task3;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class Calendar {
    private int year;
    private ArrayList<Holiday> holidays;

    public Calendar(int year) {
        this.year = year;
        this.holidays = new ArrayList<>();
    }

    public void addWeekend() {
        LocalDate date = LocalDate.of(year, 1, 1);
        while (date.isBefore(LocalDate.of(year+1, 1, 1))) {
            if (date.getDayOfWeek() == DayOfWeek.SATURDAY) {
                holidays.add(new Holiday( "Saturday", date));
            } else if(date.getDayOfWeek() == DayOfWeek.SUNDAY) {
                holidays.add(new Holiday("Sunday", date));
            }
                date = date.plusDays(1);
        }
    }


    public void addHoliday(LocalDate holidayDate, String nameOfHoliday) {
        holidays.add(new Holiday(nameOfHoliday, holidayDate));
        holidays.sort(Comparator.comparing(Holiday::getHoliday).thenComparing(Holiday::getNameOfHoliday));
    }

    public Holiday removeHoliday(String nameOfHoliday) {
        Holiday removedHoliday = null;
        Iterator<Holiday> holidayIterator = holidays.iterator();
        while (holidayIterator.hasNext()) {
            Holiday nextHoliday = holidayIterator.next();
            if (nextHoliday.getNameOfHoliday().equals(nameOfHoliday)) {
                removedHoliday = nextHoliday;
                holidayIterator.remove();
            }
        }
        return removedHoliday;
    }

    public void printHolidays() {
        for (Holiday holiday : holidays) {
            System.out.println(holiday);
        }
    }

    class Holiday {

        private String nameOfHoliday;
        private LocalDate holiday;

        public Holiday(String nameOfHoliday, LocalDate holiday) {
            this.nameOfHoliday = nameOfHoliday;
            this.holiday = holiday;
        }


        public LocalDate getHoliday() {
            return holiday;
        }

        public String getNameOfHoliday() {
            return nameOfHoliday;
        }


        @Override
        public String toString() {
            return "Holiday{" +
                    "nameOfHoliday='" + nameOfHoliday + '\'' +
                    ", holiday=" + holiday.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) +
                    '}';
        }
    }
}

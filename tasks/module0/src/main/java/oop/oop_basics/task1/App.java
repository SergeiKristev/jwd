package oop.oop_basics.task1;

import oop.oop_basics.task1.model.Directory;
import oop.oop_basics.task1.model.TextFile;
import oop.oop_basics.task1.service.DirectoryService;
import oop.oop_basics.task1.service.TextFileService;

public class App {

    //test App without controller and dao

    public static void main(String[] args) {

        TextFileService textFileService = new TextFileService();
        DirectoryService directoryService = new DirectoryService();

        TextFile file = new TextFile("temp");
        String context1 = "This is first line";
        String context2 = "This is second line";
        String context3 = "This is third line";

        Directory directory =  directoryService.create("temp/");

        textFileService.addContext(file, context1);
        textFileService.addContext(file, context2);
        textFileService.addContext(file, context3);

        textFileService.print(file);
        directoryService.putFile(directory, file);

        System.out.println(directory);


    }
}

package oop.oop_basics.task1.service;

import oop.oop_basics.task1.model.File;

public interface FileSystemManager<T> {
    void rename(T t, String name);
    T create(String name);
    <F extends File> void putFile(T t, F f);
    void remove(T t);
}

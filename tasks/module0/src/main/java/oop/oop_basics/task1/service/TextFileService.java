package oop.oop_basics.task1.service;

import oop.oop_basics.task1.model.TextFile;

public class TextFileService implements TextFileManager<TextFile>{

    @Override
    public void rename(TextFile textFile, String name) {
        textFile.setFileName(name);
    }

    @Override
    public TextFile create(String name) {
        return new TextFile(name);
    }

    @Override
    public void print(TextFile textFile) {
        for (String s: textFile.getTextContent()) {
            System.out.println(s);
        }
    }

    @Override
    public void addContext(TextFile textFile, String context) {
        textFile.getTextContent().add(context);
    }

    @Override
    public void remove(TextFile textFile) {
        textFile = null;
    }
}

package oop.oop_basics.task1.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TextFile extends File {

    private List<String> textContent;


    public TextFile(String fileName) {
        super(fileName);
        textContent = new ArrayList<>();
        setFilePermission(".txt");
    }


    public List<String> getTextContent() {
        return textContent;
    }

    public void setTextContent(List<String> textContent) {
        this.textContent = textContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextFile textFile = (TextFile) o;
        return Objects.equals(textContent, textFile.textContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(textContent);
    }

    @Override
    public String toString() {
        return "TextFile{" +
                "textContent=" + textContent +
                '}';
    }
}

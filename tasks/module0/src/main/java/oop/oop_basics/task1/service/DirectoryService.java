package oop.oop_basics.task1.service;

import oop.oop_basics.task1.model.Directory;
import oop.oop_basics.task1.model.File;

public class DirectoryService implements FileSystemManager<Directory> {

    @Override
    public void rename(Directory directory, String name) {
        directory.setDirectoryName(name);
    }

    @Override
    public Directory create(String name) {
        return new Directory(name);
    }

    @Override
    public <F extends File> void putFile(Directory directory, F f) {
        directory.getFiles().add(f);
    }

    @Override
    public void remove(Directory directory) {
        directory = null;
    }
}

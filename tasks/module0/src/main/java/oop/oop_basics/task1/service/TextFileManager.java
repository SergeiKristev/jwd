package oop.oop_basics.task1.service;

public interface TextFileManager<T> {
    void rename(T t, String name);
    T create(String name);
    void print(T t);
    void addContext(T t, String context);
    void remove(T t);
}

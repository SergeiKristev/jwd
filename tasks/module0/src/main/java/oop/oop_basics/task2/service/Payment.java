package oop.oop_basics.task2.service;

import oop.oop_basics.task2.model.Currency;
import oop.oop_basics.task2.model.Item;

import java.util.List;

public class Payment {

    private Currency currency;

    private List<Item> items;

    public Payment(Currency currency, List<Item> items) {
        this.currency = currency;
        this.items = items;
    }


    public void makePayment(Currency currency) {
        Receipt receipt = new Receipt(items);
        receipt.printReceipt(currency);
        double totalCoast = receipt.getTotalPrice();
        System.out.printf("Total cost %18.2f %s\n", totalCoast, currency);
    }

    class Receipt {
        private List<Item> items;
        private double totalCost;

        public Receipt(List<Item> items) {
            this.items = items;
            totalCost = getTotalPrice();
        }

        public void printReceipt(Currency currency) {
            int count = 1;
            System.out.println("\t********************");
            System.out.printf("Item \t%-15s\t%10s\n\n", "Item name", "Price");
            for (Item item: items) {
                System.out.printf("Item %d\t%-15s\t%10.2f\n", count, item.getName(), item.getPrice() * currency.getRate());
                count++;
            }
            System.out.println();
        }

        public double getTotalPrice() {
            double totalPrice = 0;
            for (Item item: items) {
                totalPrice += item.getPrice();
            }
            return totalPrice;
        }

        public void addItem(Item item) {
            items.add(item);
        }

        public void removeItem(Item item){
            items.remove(item);
        }
    }
}

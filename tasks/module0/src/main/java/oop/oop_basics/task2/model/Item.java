package oop.oop_basics.task2.model;

public class Item {
    private String name;
    private int article;
    private double price;

    public Item(String name, int article, double price) {
        this.name = name;
        this.article = article;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", article=" + article +
                ", price=" + price +
                '}';
    }
}


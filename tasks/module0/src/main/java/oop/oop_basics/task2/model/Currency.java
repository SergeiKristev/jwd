package oop.oop_basics.task2.model;

public enum Currency {
    BYN( 2.24),
    USD(1.0),
    EUR(0.89),
    RUB( 67.52);

    private double rate;

    Currency( double rate) {
        this.rate = rate;
    }


    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

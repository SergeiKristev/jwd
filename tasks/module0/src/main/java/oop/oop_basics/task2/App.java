package oop.oop_basics.task2;

import oop.oop_basics.task2.model.Currency;
import oop.oop_basics.task2.model.Item;
import oop.oop_basics.task2.service.Payment;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        Item item1 = new Item("Milk", 231, 1.33);
        Item item2 = new Item("Chocolate", 1322, 2.3);
        Item item3 = new Item("Nuts", 7445, 8.12);
        Item item4 = new Item("Meal", 2334, 15.66);
        Item p5 = new Item("Something", 34445, 150.60);

        List<Item> list = new ArrayList<>();
        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);

        Payment payment1 = new Payment(Currency.USD, list);
        payment1.makePayment(Currency.USD);
        payment1.makePayment(Currency.BYN);

    }
}

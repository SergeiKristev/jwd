package oop.oop_basics.task4.controller;

import oop.oop_basics.task4.service.TreasuresHundler;

public class GetExpensiveCommand implements AppCommand {

    private TreasuresHundler service;

    public GetExpensiveCommand(TreasuresHundler service) {
        this.service = service;
    }

    @Override
    public void execute(String userData) {
        System.out.println("Самое дорогое сокровище в пещере: ");
        service.chooseMostExpensiveTreasure();
        System.out.println();
    }
}

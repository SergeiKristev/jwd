package oop.oop_basics.task4.controller;

import oop.oop_basics.task4.service.TreasuresHundler;

public class ExitCommand implements AppCommand {

    private TreasuresHundler service;

    public ExitCommand(TreasuresHundler service) {
        this.service = service;
    }


    @Override
    public void execute(String userData) {
        System.out.println("Дракон проснулся и съел Вас!");
        System.exit(0);
    }
}

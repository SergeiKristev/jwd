package oop.oop_basics.task4.dao;

import oop.oop_basics.task4.model.Treasure;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements oop.oop_basics.task4.dao.DAO<Treasure> {

    private static final String PATH = "tasks/module0/src/main/java/oop/oop_basics/task4/resources/treasures.txt";

    @Override
    public List<Treasure> getAll() {
        List<Treasure> treasureList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(PATH))) {

            while (reader.ready()) {
                String[] arr = reader.readLine().split("=");
                treasureList.add(new Treasure(Integer.parseInt(arr[0]), Double.parseDouble(arr[1]), arr[2]));
            }
        } catch (IOException e) {
            System.out.println("Exception " + e.getMessage());
        }
        return treasureList;

    }
}

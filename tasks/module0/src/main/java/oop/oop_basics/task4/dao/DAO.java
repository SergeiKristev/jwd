package oop.oop_basics.task4.dao;

import java.util.List;

public interface DAO<T> {

    List<T> getAll();
}

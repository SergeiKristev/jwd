package oop.oop_basics.task4.controller;

public interface AppCommand {

    void execute(String userData);
}

package oop.oop_basics.task4.controller;

import oop.oop_basics.task4.dao.DAO;
import oop.oop_basics.task4.dao.UserDAO;
import oop.oop_basics.task4.model.Treasure;
import oop.oop_basics.task4.service.TreasuresHundler;
import java.util.List;
import java.util.Scanner;

public class SelectTreasuresCommand implements AppCommand{

    private TreasuresHundler service;
    private DAO<Treasure> dao = new UserDAO();

    public SelectTreasuresCommand(TreasuresHundler service) {
        this.service = service;
    }

    @Override
    public void execute(String userData) {
        Scanner scanner = new Scanner(System.in); //fixme to BufferedReader
        System.out.println("Пока Дракон спит,вы можете украсть сокровища на любую сумму!");
        System.out.println("Введите желаемую стоимость сокровищ");
        double amount = scanner.nextDouble();
        service.showAllTreasures();

        System.out.println("Для покупки введите через пробел номера сокровища:");

        List<Treasure> list = dao.getAll();
        Scanner scanner2 = new Scanner(System.in); //fixme to BufferedReader
        String s = scanner2.nextLine();

        String[] treasuresId = s.split(" ");

        Treasure[] getTreasures = new Treasure[treasuresId.length];
            int count = 0;
            for (int i = 0; i < treasuresId.length; i++) {
                for (Treasure tr: list) {  //fixme to Iterator
                    if (Integer.parseInt(treasuresId[i]) == tr.getId()) {
                        getTreasures[count] = tr;
                        count++;
                    }
                }
            }
            service.selectForGivenAmount(amount, getTreasures);
        }
}

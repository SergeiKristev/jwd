package oop.oop_basics.task4.controller;

public interface AppCommandFactory {

    AppCommand getCommand(String commandName);

}

package oop.oop_basics.task4;

import oop.oop_basics.task4.controller.*;
import oop.oop_basics.task4.dao.UserDAO;
import oop.oop_basics.task4.model.Treasure;
import oop.oop_basics.task4.presentation.ConsoleViewer;
import oop.oop_basics.task4.service.TreasuresHundler;
import oop.oop_basics.task4.service.TreasuresHandlerService;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        UserDAO dao = new UserDAO();
        TreasuresHandlerService hundler = new TreasuresHandlerService();

        List<Treasure> treasures = dao.getAll();

        TreasuresHundler service = new TreasuresHandlerService();

        ConsoleViewer consoleViewer = new ConsoleViewer();

        AppCommand showAll = new ShowAllCommand(service);
        AppCommand getExpensive = new GetExpensiveCommand(service);
        AppCommand getTreasures = new SelectTreasuresCommand(service);
        AppCommand exitCommand = new ExitCommand(service);

        Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.SHOW_ALL, showAll);
        commands.put(AppCommandName.GET_EXPENSIVE, getExpensive);
        commands.put(AppCommandName.GET_TREASURES, getTreasures);
        commands.put(AppCommandName.EXIT, exitCommand);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);

        boolean isRunning = true;

        while (isRunning) {
            consoleViewer.printMainMenu();
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            controller.handleUserData(command);
        }
    }
}

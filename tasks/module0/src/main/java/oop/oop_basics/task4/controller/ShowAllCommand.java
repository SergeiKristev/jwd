package oop.oop_basics.task4.controller;

import oop.oop_basics.task4.service.TreasuresHundler;

public class ShowAllCommand implements AppCommand {

    private TreasuresHundler service;

    public ShowAllCommand(TreasuresHundler service) {
        this.service = service;
    }

    @Override
    public void execute(String userData) {
        service.showAllTreasures();
    }
}

package oop.oop_basics.task4.presentation;

import oop.oop_basics.task4.controller.AppCommandName;

import java.util.Scanner;

public class ConsoleViewer {

    Scanner scanner = new Scanner(System.in);

    public void printMainMenu() {
        System.out.println("Добро пожаловать в пещеру Дракона-Клептомана!");
        System.out.println("Доступные команды:");
        System.out.println(AppCommandName.SHOW_ALL.getShortCommand() + "\tПросмотр всех сокровищ");
        System.out.println(AppCommandName.GET_EXPENSIVE.getShortCommand() + "\tПросмотр самого дорогого сокровища");
        System.out.println(AppCommandName.GET_TREASURES.getShortCommand() + "\tВыбор сокровищ на заданную сумму");
        System.out.println(AppCommandName.EXIT.getShortCommand() + "\tВыход");
    }

    public void showTreasures(){
        System.out.println("Все сокровища в пещере:\n");
    }

    public void selectoMostExpensive(){
        System.out.println("Самое дорогое сокровище в пещере: ");
    }

    public void selectTreasures(){
        System.out.println("Пока Дракон спит,вы можете украсть сокровища на любую сумму!");
        System.out.println("Введите желаемую стоимость сокровищ");
        double amount = scanner.nextDouble();
        System.out.println("Для покупки введите через пробел номера сокровища:");
    }

}

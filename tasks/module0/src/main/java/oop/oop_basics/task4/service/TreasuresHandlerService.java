package oop.oop_basics.task4.service;

import oop.oop_basics.task4.dao.DAO;
import oop.oop_basics.task4.dao.UserDAO;
import oop.oop_basics.task4.model.Treasure;

import java.util.*;

public class TreasuresHandlerService implements TreasuresHundler {

    public DAO<Treasure> getDao() {
        return dao;
    }


    DAO<Treasure> dao = new UserDAO();

    @Override
    public void showAllTreasures() {
        List<Treasure> list = dao.getAll();
        for (Treasure tr: list) {
            System.out.printf("№ %3d, цена %5.2f, сокровище: %s\n", tr.getId(), tr.getPrice(), tr.getTreasureName());
        }
    }

    @Override
    public void chooseMostExpensiveTreasure() {
        List<Treasure> list = dao.getAll();
        list.sort(Comparator.comparing(Treasure::getPrice).reversed());
            System.out.printf("№ %3d, цена %5.2f, сокровище: %s\n", list.get(0).getId(), list.get(0).getPrice(), list.get(0).getTreasureName());
    }

    @Override
    public void selectForGivenAmount(double amount, Treasure...treasure) {
        List<Treasure> cart = new ArrayList<>();
        Collections.addAll(cart, treasure);
        double totalPrice = 0;
        for (Treasure tr: cart) {
            totalPrice += tr.getPrice();
        }
        if (totalPrice > amount) {
                System.out.println("В вашей корзине сокровищ на " + totalPrice + " монет.");
                System.out.println("Вы превысили лимит! Попробуйте ещё.\n");
        } else {
            System.out.println("В вашей корзине сокровищ на " + totalPrice + " монет. Ваша сдача " + (amount - totalPrice) + " монет.\n");
        }
    }
}

package oop.oop_basics.task4.model;

import java.util.Objects;

public class Treasure {

    private int id;
    private double price;
    private String treasureName;

    public Treasure(int id, double price, String treasureName) {
        this.id = id;
        this.price = price;
        this.treasureName = treasureName;
    }

    public String getTreasureName() {
        return treasureName;
    }

    public void setTreasureName(String treasureName) {
        this.treasureName = treasureName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Treasure treasure = (Treasure) o;

        if (id != treasure.id) return false;
        if (Double.compare(treasure.price, price) != 0) return false;
        return Objects.equals(treasureName, treasure.treasureName);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = treasureName != null ? treasureName.hashCode() : 0;
        result = 31 * result + id;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "id= " + id + ", price= " + price + ", treasure= " + treasureName;
    }
}

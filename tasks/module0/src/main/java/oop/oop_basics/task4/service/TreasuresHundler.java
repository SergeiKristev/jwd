package oop.oop_basics.task4.service;

import oop.oop_basics.task4.dao.DAO;
import oop.oop_basics.task4.model.Treasure;

public interface TreasuresHundler {

    DAO<Treasure> getDao();
    void showAllTreasures();
    void chooseMostExpensiveTreasure();
    void selectForGivenAmount(double amount, Treasure...treasure);

}

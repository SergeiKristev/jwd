package oop.oop_basics.task4.controller;

public enum AppCommandName {


    SHOW_ALL("-s"),
    GET_EXPENSIVE("-ge"),
    GET_TREASURES("-gt"),
    EXIT("-ex");

    private final String shortCommand;

    public String getShortCommand() {
        return shortCommand;
    }

    AppCommandName(String s) {
        this.shortCommand = s;
    }

    public static AppCommandName fromString(String name) {

        final AppCommandName[] values = AppCommandName.values();
        for (AppCommandName commandName : values) {
            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
                return commandName;
            }
        }
        return null;
    }
}

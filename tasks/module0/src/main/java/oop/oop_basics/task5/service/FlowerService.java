package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Flower;
import java.util.Map;

public interface FlowerService {

    void getAll(Map<Integer, Flower> flowers);
}

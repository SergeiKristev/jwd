package oop.oop_basics.task5.model;

import java.util.Objects;

public class FlowerComposition {

    private String flowers;
    private String wrappers;

    public FlowerComposition(String flowers, String wrappers) {
        this.flowers = flowers;
        this.wrappers = wrappers;
    }

    public String getFlowers() {
        return flowers;
    }

    public void setFlowers(String flowers) {
        this.flowers = flowers;
    }

    public String getWrappers() {
        return wrappers;
    }

    public void setWrappers(String wrappers) {
        this.wrappers = wrappers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlowerComposition that = (FlowerComposition) o;

        if (!Objects.equals(flowers, that.flowers)) return false;
        return Objects.equals(wrappers, that.wrappers);

    }

    @Override
    public int hashCode() {
        int result = flowers != null ? flowers.hashCode() : 0;
        result = 31 * result + (wrappers != null ? wrappers.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FlowerComposition{" +
                "flowers='" + flowers + '\'' +
                ", wrappers='" + wrappers + '\'' +
                '}';
    }
}

package oop.oop_basics.task5.controller;

public class AppController {

    private final AppCommandFactory commandFactory;

    public AppController(AppCommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public void handleUserData(String data) {

        final AppCommand command = commandFactory.getCommand(data);
        command.execute(data);
    }
}

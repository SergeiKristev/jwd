package oop.oop_basics.task5.controller;

public interface AppCommandFactory {

    AppCommand getCommand(String commandName);
}

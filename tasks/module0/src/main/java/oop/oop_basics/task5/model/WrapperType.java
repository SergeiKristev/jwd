package oop.oop_basics.task5.model;

public enum WrapperType {
    CRAFT_PAPER,
    TRANSPARENCIES,
    FELT,
    ORGANZA;
}

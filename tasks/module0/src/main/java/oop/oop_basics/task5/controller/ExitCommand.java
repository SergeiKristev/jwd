package oop.oop_basics.task5.controller;

import oop.oop_basics.task5.service.FlowerServiceImpl;

public class ExitCommand implements AppCommand {

    private FlowerServiceImpl service; //fixme: to interface

    public ExitCommand(FlowerServiceImpl service) {
        this.service = service;
    }

    @Override
    public void execute(String userData) {
        System.out.println("Магазин закрывается");
        System.exit(0);
    }
}

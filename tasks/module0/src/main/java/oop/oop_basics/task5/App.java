package oop.oop_basics.task5;
import oop.oop_basics.task5.controller.*;
import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.model.FlowerType;
import oop.oop_basics.task5.model.Wrapper;
import oop.oop_basics.task5.model.WrapperType;
import oop.oop_basics.task5.presentation.ConsoleViewer;
import oop.oop_basics.task5.resources.Resources;
import oop.oop_basics.task5.service.*;

import java.util.*;

public class App {

    List<Flower> flowers = new ArrayList<>();

    public static void main(String[] args) {

        FlowerServiceImpl flowerService = new FlowerServiceImpl();
        WrapperServiceImpl wrapperService = new WrapperServiceImpl();
        FlowerCompositionServiceImpl flowerCompositionService = new FlowerCompositionServiceImpl();

        Resources.getFlowers().put(1, new Flower(FlowerType.ROSE, "red", 20));
        Resources.getFlowers().put(2, new Flower(FlowerType.TULIP, "orange", 12));
        Resources.getFlowers().put(3, new Flower(FlowerType.ALSTORMERIA, "red", 15));
        Resources.getWrappers().put(1, new Wrapper(WrapperType.CRAFT_PAPER, "green"));
        Resources.getWrappers().put(2, new Wrapper(WrapperType.CRAFT_PAPER, "blue"));
        Resources.getWrappers().put(3, new Wrapper(WrapperType.CRAFT_PAPER, "white"));


        ConsoleViewer consoleViewer = new ConsoleViewer();

        AppCommand showAllFlower = new ShowAllFlowerCommand(flowerService);
        AppCommand showAllWrapper = new ShowAllWrappersCommand(wrapperService);
        AppCommand createComposition = new CreateCompositionCommand(flowerCompositionService);
        AppCommand exitCommand = new ExitCommand(flowerService);


        Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
        commands.put(AppCommandName.SHOW_ALL_FLOWER, showAllFlower);
        commands.put(AppCommandName.SHOW_ALL_WRAPPERS, showAllWrapper);
        commands.put(AppCommandName.CREATE_COMPOSITION, createComposition);
        commands.put(AppCommandName.EXIT, exitCommand);

        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commands);
        AppController controller = new AppController(commandFactory);
//
        boolean isRunning = true;

        while (isRunning) {
            consoleViewer.printMainMenu();
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            controller.handleUserData(command);
        }
    }
}

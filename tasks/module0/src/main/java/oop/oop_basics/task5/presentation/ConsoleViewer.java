package oop.oop_basics.task5.presentation;

import oop.oop_basics.task5.controller.AppCommandName;

import java.util.Scanner;

public class ConsoleViewer {

    Scanner scanner = new Scanner(System.in);

    public void printMainMenu() {
        System.out.println("Добро пожаловать в цветочный магазин!");
        System.out.println("Доступные команды:");
        System.out.println(AppCommandName.SHOW_ALL_FLOWER.getShortCommand() + "\tВыбор цветов");
        System.out.println(AppCommandName.SHOW_ALL_WRAPPERS.getShortCommand() + "\tВыбор упаковок");
        System.out.println(AppCommandName.CREATE_COMPOSITION.getShortCommand() + "\tПросмтор композиции");
        System.out.println(AppCommandName.EXIT.getShortCommand() + "\tВыход");
    }

    public void printSecondMenu() {
        System.out.println("Добро пожаловать в цветочный магазин!");
        System.out.println("Доступные команды:");
        System.out.println(AppCommandName.SHOW_ALL_WRAPPERS.getShortCommand() + "\tПросмотр всех упаковок");
        System.out.println(AppCommandName.EXIT.getShortCommand() + "\tВыход");
    }
}

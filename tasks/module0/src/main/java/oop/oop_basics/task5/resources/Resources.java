package oop.oop_basics.task5.resources;

import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.model.Wrapper;

import java.util.HashMap;
import java.util.Map;

public class Resources {
    private static Map<Integer, Flower> flowers = new HashMap<>();
    private static Map<Integer, Wrapper> wrappers = new HashMap<>();

    public static Map<Integer, Flower> getFlowers() {
        return flowers;
    }

    public static void setFlowers(Map<Integer, Flower> flowers) {
        Resources.flowers = flowers;
    }

    public static Map<Integer, Wrapper> getWrappers() {
        return wrappers;
    }

    public static void setWrappers(Map<Integer, Wrapper> wrappers) {
        Resources.wrappers = wrappers;
    }
}

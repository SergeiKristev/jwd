package oop.oop_basics.task5.model;

public class Wrapper {
    private WrapperType wrapperType;
    private String color;

    public Wrapper(WrapperType wrapperType, String color) {
        this.wrapperType = wrapperType;
        this.color = color;
    }

    public WrapperType getWrapperType() {
        return wrapperType;
    }

    public void setWrapperType(WrapperType wrapperType) {
        this.wrapperType = wrapperType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wrapper wrapper = (Wrapper) o;

        if (wrapperType != wrapper.wrapperType) return false;
        return color != null ? color.equals(wrapper.color) : wrapper.color == null;

    }

    @Override
    public int hashCode() {
        int result = wrapperType != null ? wrapperType.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Wrapper{" +
                "wrapperType=" + wrapperType +
                ", color='" + color + '\'' +
                '}';
    }
}

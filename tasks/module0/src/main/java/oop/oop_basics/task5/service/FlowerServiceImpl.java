package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Flower;
import java.util.Map;

public class FlowerServiceImpl implements FlowerService{


    @Override
    public void getAll(Map<Integer, Flower> flowers) {
        for (Map.Entry<Integer, Flower> entry : flowers.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

}

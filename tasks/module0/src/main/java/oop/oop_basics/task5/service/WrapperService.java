package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Wrapper;
import java.util.Map;

public interface WrapperService {

    void getAll(Map<Integer, Wrapper> wrappers);
}

package oop.oop_basics.task5.controller;

import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.resources.Resources;
import oop.oop_basics.task5.service.FlowerCompositionServiceImpl;
import oop.oop_basics.task5.service.FlowerServiceImpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

public class ShowAllFlowerCommand implements AppCommand {

    private FlowerServiceImpl service;

    public ShowAllFlowerCommand(FlowerServiceImpl service) {
        this.service = service;
    } //fixme: to interface

    @Override
    public void execute(String userData) {
        System.out.println("Список всех цветов:");
        service.getAll(Resources.getFlowers());
        System.out.println("Выберите цветы");
        System.out.println("Введите через пробел номера цветов:");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        String[] flowers = s.split(" ");

        Flower[] getFlowersId = new Flower[flowers.length];
        int count = 0;
        for (int i = 0; i < getFlowersId.length; i++) {
            for (Map.Entry<Integer, Flower> entry : Resources.getFlowers().entrySet()) {
               if (entry.getKey().equals(Integer.parseInt(flowers[i]))) {
                  getFlowersId[count] =  entry.getValue();
                  count++;
                }
            }
        }
        Collections.addAll(FlowerCompositionServiceImpl.getFlowerList(), getFlowersId);

    }
}

package oop.oop_basics.task5.controller;

import java.util.Map;

public class SimpleAppCommandFactory implements AppCommandFactory {

    private final Map<AppCommandName, AppCommand> commandMap;

    public SimpleAppCommandFactory(Map<AppCommandName, AppCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public AppCommand getCommand(String commandName) {

        final AppCommandName appCommandName = AppCommandName.fromString(commandName);
        final AppCommand command = commandMap.getOrDefault(appCommandName, (userData) -> System.out.println("BAD COMMAND"));
        return command;
    }
}

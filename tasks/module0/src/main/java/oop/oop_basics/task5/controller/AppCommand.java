package oop.oop_basics.task5.controller;

public interface AppCommand {

    void execute(String userData);
}

package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.model.Wrapper;

import java.util.List;

public interface FlowerCompositionService {

    void createFlowerComposition(List<Flower> flowers, List<Wrapper> wrappers);

}

package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.model.FlowerComposition;
import oop.oop_basics.task5.model.Wrapper;

import java.util.ArrayList;
import java.util.List;

public class FlowerCompositionServiceImpl implements FlowerCompositionService {

    private static List<Flower> flowerList = new ArrayList<>();
    private static List<Wrapper> wrapperList = new ArrayList<>();

    public static List<Flower> getFlowerList() {
        return flowerList;
    }

    public static List<Wrapper> getWrapperList() {
        return wrapperList;
    }

    @Override
    public void createFlowerComposition(List<Flower> flowers, List<Wrapper> wrappers) {
        System.out.println(new FlowerComposition(flowers.toString(), wrappers.toString()));
    }
}

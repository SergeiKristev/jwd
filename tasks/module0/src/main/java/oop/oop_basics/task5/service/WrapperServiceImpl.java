package oop.oop_basics.task5.service;

import oop.oop_basics.task5.model.Wrapper;
import java.util.Map;

public class WrapperServiceImpl implements WrapperService {


    @Override
    public void getAll(Map<Integer, Wrapper> wrappers) {
        for (Map.Entry<Integer, Wrapper> entry : wrappers.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

}

package oop.oop_basics.task5.controller;

import oop.oop_basics.task5.service.FlowerCompositionServiceImpl;

public class CreateCompositionCommand implements AppCommand {

    private FlowerCompositionServiceImpl service; //fixme: to interface

    public CreateCompositionCommand(FlowerCompositionServiceImpl service) {
        this.service = service;
    }

    @Override
    public void execute(String userData) {
        System.out.println("Ваша композиция:");
        service.createFlowerComposition(FlowerCompositionServiceImpl.getFlowerList(), FlowerCompositionServiceImpl.getWrapperList());
        System.out.println("Приходите еще!");
        System.exit(0);
    }
}

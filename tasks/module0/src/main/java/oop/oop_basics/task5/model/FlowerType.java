package oop.oop_basics.task5.model;

public enum FlowerType {
    ROSE,
    TULIP,
    ALSTORMERIA,
    SCARLET_FLOWER,
    LILY;
}

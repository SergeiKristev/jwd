package oop.oop_basics.task5.model;

import java.util.Objects;

public class Flower {

    private FlowerType flowerType;
    private String color;
    private int size;

    public Flower(FlowerType flowerType, String color, int size) {
        this.flowerType = flowerType;
        this.color = color;
        this.size = size;
    }

    public FlowerType getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(FlowerType flowerType) {
        this.flowerType = flowerType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flower flower = (Flower) o;

        if (size != flower.size) return false;
        if (flowerType != flower.flowerType) return false;
        return Objects.equals(color, flower.color);

    }

    @Override
    public int hashCode() {
        int result = flowerType != null ? flowerType.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + size;
        return result;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "flowerType=" + flowerType +
                ", color='" + color + '\'' +
                ", size=" + size +
                '}';
    }
}

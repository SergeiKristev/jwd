package oop.oop_basics.task5.controller;

import oop.oop_basics.task5.model.Flower;
import oop.oop_basics.task5.model.Wrapper;
import oop.oop_basics.task5.resources.Resources;
import oop.oop_basics.task5.service.FlowerCompositionServiceImpl;
import oop.oop_basics.task5.service.WrapperServiceImpl;

import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

public class ShowAllWrappersCommand implements AppCommand {

    private WrapperServiceImpl service;

    public ShowAllWrappersCommand(WrapperServiceImpl service) {
        this.service = service;
    } //fixme: to interface

    @Override
    public void execute(String userData) {
        System.out.println("Список всех упаковок:");
        service.getAll(Resources.getWrappers());

        System.out.println("Выберите упаковку");
        System.out.println("Введите через пробел номера упаковок:");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        String[] wrappers = s.split(" ");

        Wrapper[] getWrapperId = new Wrapper[wrappers.length];
        int count = 0;
        for (int i = 0; i < getWrapperId.length; i++) {
            for (Map.Entry<Integer, Wrapper> entry : Resources.getWrappers().entrySet()) {
                if (entry.getKey().equals(Integer.parseInt(wrappers[i]))) {
                    getWrapperId[count] =  entry.getValue();
                    count++;
                }
            }
        }
        Collections.addAll(FlowerCompositionServiceImpl.getWrapperList(), getWrapperId);
    }
}

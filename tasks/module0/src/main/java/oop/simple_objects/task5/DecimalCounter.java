package oop.simple_objects.task5;

public class DecimalCounter {

    private int currentValue;
    private int lowLimit;
    private int highLimit;

    public int getCurrentValue() {
        return currentValue;
    }

    public DecimalCounter(int lowLimit, int highLimit) {
        currentValue = lowLimit;
        this.lowLimit = lowLimit;
        this.highLimit = highLimit;
    }

    public DecimalCounter(int value, int lowLimit, int highLimit) {
        currentValue = value;
        this.lowLimit = lowLimit;
        this.highLimit = highLimit;
    }

    public int increaseValue() {
        if (currentValue < highLimit) {
            return currentValue++;
        }
        else return currentValue;
    }

    public int decreaseValue() {
        if (currentValue > lowLimit) {
            return currentValue--;
        }
        else return currentValue;
    }

}

package oop.simple_objects.task5;

/*
   5. Опишите класс, реализующий десятичный счетчик, который может увеличивать или уменьшать свое значение на единицу в
   заданном диапазоне. Предусмотрите инициализацию счетчика значениями по умолчанию и произвольными значениями. Счетчик
   имеет методы увеличения и уменьшения состояния, и метод позволяющее получить его текущее состояние. Написать код,
   демонстрирующий все возможности класса.
 */
public class Main {
    public static void main(String[] args) {

        DecimalCounter counter1 = new DecimalCounter(10, 100);
        DecimalCounter counter2 = new DecimalCounter(15, 10, 100);

        System.out.println("Значение счетчика 1: " + counter1.getCurrentValue());
        System.out.println("Значение счетчика 2: " + counter2.getCurrentValue());

        counter1.increaseValue();
        counter1.increaseValue();

        counter2.decreaseValue();

        System.out.println(counter1.getCurrentValue());
        System.out.println(counter2.getCurrentValue());

    }
}

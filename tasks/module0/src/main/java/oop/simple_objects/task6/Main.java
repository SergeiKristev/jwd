package oop.simple_objects.task6;

public class Main {

    public static void main(String[] args) {
        Clock clock = new Clock();
        clock.setHour(15);
        clock.setMinute(45);
        clock.setSecond(43);

        System.out.print("Current time: ");
        System.out.println(clock.toString());
        System.out.println();
        clock.increaseHour(25);
        System.out.println("Time increased by 25 h: " + clock.toString());
        clock.increaseMinute(61);
        System.out.println("Time increased by 61 min: " +clock.toString());
        clock.increaseSecond(95);
        System.out.println("Time increased by 95 sec: " +clock.toString());
        System.out.println();


        clock.decreaseSecond(95);
        System.out.println("Time decreased by 95 sec: " +clock.toString());
        clock.decreaseMinute(61);
        System.out.println("Time decreased by 61 min: " +clock.toString());
        clock.decreaseHour(25);
        System.out.println("Time decreased by 25 h: " + clock.toString());
        System.out.println();
        System.out.print("Current time: ");
        System.out.println(clock.toString());
    }
}

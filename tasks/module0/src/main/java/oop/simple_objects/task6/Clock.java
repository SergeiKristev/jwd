package oop.simple_objects.task6;
/*
   6. Составьте описание класса для представления времени. Предусмотрте возможности установки времени и изменения его
   отдельных полей (час, минута, секунда) с проверкой допустимости вводимых значений. В случае недопустимых значений
   полей поле устанавливается в значение 0. Создать методы изменения времени на заданное количество часов, минут и секунд.
 */
public class Clock {

    private int hour;
    private int minute;
    private int second;

    public Clock() {
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute <=60) {
            this.minute = minute;
        } else {
            this.minute = 0;
        }
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        if (second >= 0 && second <=60) {
            this.second = second;
        } else {
            this.second = 0;
        }
    }

    public void increaseHour(int hourIncrease) {
        if (hourIncrease > 24) {
            hourIncrease = hourIncrease % 24;
        }
        if ((hour + hourIncrease) >=24) {
            hour = Math.abs(24 - hourIncrease);
        } else {
            hour += hourIncrease;
        }
    }

    public void increaseMinute(int minutesIncrease) {
        int hourIncrease = 0;
        if (minutesIncrease > 60) {
            hourIncrease = minutesIncrease / 60;
            minutesIncrease = minutesIncrease % 60;
        }
        increaseHour(hourIncrease);
        if (minutesIncrease == 60) {
            hour++;
        }
        else if ((minute + minutesIncrease) >= 60) {
            minute = Math.abs(60 - minutesIncrease);
            hour++;
        } else {
            minute += minutesIncrease;
        }
    }

    public void increaseSecond(int secondsIncrease) {
        int minutesIncrease = 0;
        if (secondsIncrease > 60) {
            minutesIncrease = secondsIncrease / 60;
            secondsIncrease = secondsIncrease % 60;
        }
        increaseMinute(minutesIncrease);
        if ((second + secondsIncrease) >=60) {
            second = Math.abs(60 - second - secondsIncrease);
        } else {
            second += secondsIncrease;
        }

    }

    public void decreaseHour(int hourDecrease) {
        if (hourDecrease > 24) {
            hourDecrease = hourDecrease % 24;
        }
        if (24 <= (hour + hourDecrease)) {
            hour = Math.abs(hourDecrease - hour);
        } else {
            hour -= hourDecrease;
        }
    }

    public void decreaseMinute(int minutesDecrease) {
        int hourDecrease = 0;
        if (minutesDecrease > 60) {
            hourDecrease = minutesDecrease / 60;
            minutesDecrease = minutesDecrease % 60;
        }
        decreaseHour(hourDecrease);
        if (minutesDecrease == 60) {
            hour--;
        }
        else if ((minute - minutesDecrease) < 0) {
            minute = 60 - Math.abs(minutesDecrease - minute);
            hour--;
        } else {
            minute -= minutesDecrease;
        }
    }

    public void decreaseSecond(int secondsDecrease) {
        int minutesDecrease = 0;
        if (secondsDecrease > 60) {
            minutesDecrease = secondsDecrease / 60;
            secondsDecrease = secondsDecrease % 60;
        }
        decreaseMinute(minutesDecrease);
        if (secondsDecrease == 60) {
            minute--;
        } else if ((second - secondsDecrease) < 0) {
            second = 60 - Math.abs(secondsDecrease - second);
        } else {
            second -= secondsDecrease;
        }

    }

    @Override
    public String toString() {
        return hour + "hh " + minute + "min " + second + "sec";
    }
}

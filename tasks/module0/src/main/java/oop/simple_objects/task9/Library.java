package oop.simple_objects.task9;

public class Library {

    private Book[] library;

    Library(Book[] library) {
        this.library = library;
    }

    Book[] getLibrary() {
        return library;
    }

    public void setLibrary(Book[] library) {
        this.library = library;
    }

    static void showBooksByAuthor(Book[] library, String author) {
        for (Book book : library) {
            if (book.getAuthor().equals(author)) {
                System.out.println(book.toString());
            }
        }
    }

    static void showBooksByPublishing(Book[] library, String publishing) {
        for (Book book : library) {
            if (book.getPublishing().equals(publishing)) {
                System.out.println(book.toString());
            }
        }
    }

    static void showBooksPublishedAfter(Book[] library, int year) {
        for (Book book : library) {
            if (book.getYearPublication() > year) {
                System.out.println(book.toString());
            }
        }
    }

}

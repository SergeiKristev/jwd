package oop.simple_objects.task9;

public enum TypeBinding {
    SEWN,
    GLUED,
    PUR_GLUED,
    LAY_FLAT,
    SPIRAL,
    WIRE_O,
    SADDLE_STITCHED
}

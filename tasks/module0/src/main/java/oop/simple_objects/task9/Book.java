package oop.simple_objects.task9;

import java.util.Objects;

public class Book {
    private static int count = 1;
    private int id;
    private String title;
    private String author;
    private String publishing;
    private int yearPublication;
    private int numberPages;
    private double price;
    private TypeBinding typeBinding;

    public Book(String title, String author, String publishing, int yearPublication, int numberPages, double price, TypeBinding typeBinding) {
        this.id = id + count;
        count++;
        this.title = title;
        this.author = author;
        this.publishing = publishing;
        this.yearPublication = yearPublication;
        this.numberPages = numberPages;
        this.price = price;
        this.typeBinding = typeBinding;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublishing() {
        return publishing;
    }

    public void setPublishing(String publishing) {
        this.publishing = publishing;
    }

    public int getYearPublication() {
        return yearPublication;
    }

    public void setYearPublication(int yearPublication) {
        this.yearPublication = yearPublication;
    }

    public int getNumberPages() {
        return numberPages;
    }

    public void setNumberPages(int numberPages) {
        this.numberPages = numberPages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TypeBinding getTypeBinding() {
        return typeBinding;
    }

    public void setTypeBinding(TypeBinding typeBinding) {
        this.typeBinding = typeBinding;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (yearPublication != book.yearPublication) return false;
        if (numberPages != book.numberPages) return false;
        if (Double.compare(book.price, price) != 0) return false;
        if (!Objects.equals(title, book.title)) return false;
        if (!Objects.equals(author, book.author)) return false;
        if (!Objects.equals(publishing, book.publishing)) return false;
        return typeBinding == book.typeBinding;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (publishing != null ? publishing.hashCode() : 0);
        result = 31 * result + yearPublication;
        result = 31 * result + numberPages;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (typeBinding != null ? typeBinding.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publishing='" + publishing + '\'' +
                ", yearPublication=" + yearPublication +
                ", numberPages=" + numberPages +
                ", price=" + price +
                ", typeBinding=" + typeBinding +
                '}';
    }
}

package oop.simple_objects.task9;

import static oop.simple_objects.task9.Library.*;

/*
    9. Создать класс Book, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод
    toString(). Создать второй класс, агрегирующий массив типа Book, с подходящими конструкторами и методами. Задать
    критерии выбора данных и вывести эти данные на консоль.
    Book: id, название, автор(ы), издательство, год издания, количество страниц, цена, тип переплета.

    Найти и вывести:
        a) список книг заданного автора;
        b) список книг, выпущенных заданным издательством;
        c) список книг, выпущенных после заданного года.
 */

public class Main {

    public static void main(String[] args) {


        Book book1 = new Book( "The Colobok", "folk",
                "ACT", 2014, 22, 5.5, TypeBinding.LAY_FLAT);
        Book book2 = new Book( "Harry Potter and the Philosopher's Stone", "J. K. Rowling",
                "Pottermore", 2005, 223, 54.5, TypeBinding.SEWN);
        Book book3 = new Book( "The Chronicles of Narnia", "C. S. Lewis",
                "HarperCollins", 2010, 255, 18.6, TypeBinding.SEWN);
        Book book4 = new Book( "War and Peace", "Leo Tolstoy",
                "ACT", 2006, 1225, 15, TypeBinding.PUR_GLUED);
        Book book5 = new Book( "Harry Potter and the Cursed Child", "J. K. Rowling",
                "Pottermore", 2006, 228, 51.5, TypeBinding.SEWN);

        Library myLibrary = new Library(new Book[]{book1, book2, book3, book4, book5});

        for (Book book: myLibrary.getLibrary()) {
            System.out.println(book);
        }
        System.out.println();

        showBooksByAuthor(myLibrary.getLibrary(), "J. K. Rowling");
        System.out.println();

        showBooksByPublishing(myLibrary.getLibrary(), "ACT");
        System.out.println();

        showBooksPublishedAfter(myLibrary.getLibrary(), 2010);


    }
}

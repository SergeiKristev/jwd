package oop.simple_objects.task2;

/*
    2. Создйте класс Test2 двумя переменными. Добавьте конструктор с входными параметрами. Добавьте конструктор,
    инициализирующий члены класса по умолчанию. Добавьте set- и get- методы для полей экземпляра класса.
 */

public class Main {

    public static void main(String[] args) {
        Test2 test1 = new Test2();
        Test2 test2 = new Test2(45, 54);

        System.out.println(test1);
        System.out.println(test2);
    }
}

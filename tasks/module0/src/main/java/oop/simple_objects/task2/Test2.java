package oop.simple_objects.task2;

import java.util.Objects;

public class Test2 {

    private int number1;
    private int number2;

    public Test2() {
        number1 = 24;
        number2 = 35;
    }

    public Test2(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test2 task2 = (Test2) o;
        return number1 == task2.number1 &&
                number2 == task2.number2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number1, number2);
    }

    @Override
    public String toString() {
        return "Test2{" +
                "number1=" + number1 +
                ", number2=" + number2 +
                '}';
    }
}

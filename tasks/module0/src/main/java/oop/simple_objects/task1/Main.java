package oop.simple_objects.task1;

/*
    1. Создайте класс Test1 двумя переменными. Добавьте метод вывода на экран и методы изменения этих переменных. Добавьте
    метод, который находит сумму значений этих переменных, и метод, который находит наибольшее значение из этих двух переменных.
 */

public class Main {
    public static void main(String[] args) {

        Test1 test = new Test1();
        test.setNumber1(26);
        test.setNumber2(34);

        test.printNumbers();

        System.out.println("Наибольшее значение: " + test.getMax());

        System.out.println("Сумма чисел: " + test.getSum());

    }
}

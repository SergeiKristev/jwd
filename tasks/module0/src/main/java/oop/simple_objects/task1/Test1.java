package oop.simple_objects.task1;

public class Test1 {

    private int number1;
    private int number2;

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public  void printNumbers() {
        System.out.println("number1: " + number1 + ", number2: " + number2);
    }

    public int getSum() {
        return number1 + number2;
    }

    public int getMax() {
        if (number1 > number2) {
            return number1;
        } else if (number2 > number1) {
            return  number2;
        } else {
            System.out.println("Числа равны");
        } return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Test1 test1 = (Test1) o;

        if (number1 != test1.number1) return false;
        return number2 == test1.number2;

    }

    @Override
    public int hashCode() {
        int result = number1;
        result = 31 * result + number2;
        return result;
    }
}

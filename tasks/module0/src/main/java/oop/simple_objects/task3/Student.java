package oop.simple_objects.task3;

import java.util.Arrays;
import java.util.Objects;

public class Student {

    private String surname;
    private String initials;
    private int groupId;
    private int[] academicPerformance;

    public Student(String surname, String initials, int groupId, int[] academicPerformance) {
        this.surname = surname;
        this.initials = initials;
        this.groupId = groupId;
        this.academicPerformance = academicPerformance;
    }

    public String getSurname() {
        return surname;
    }

    public String getInitials() {
        return initials;
    }

    public int getGroupId() {
        return groupId;
    }

    public int[] getAcademicPerformance() {
        return academicPerformance;
    }

    public static void filterByMarks(Student[] group, int markFrom, int markTo) {
        for (int i = 0; i < group.length; i++) {
            int[] marks = group[i].getAcademicPerformance();
            int count = 0;
            for (int j = 0; j < marks.length; j++) {
                if (marks[j] >= markFrom && marks[j] <= markTo) {
                    count++;
                }
                if (count == marks.length) {
                    System.out.println("Student: " + group[i].getSurname() + ", " + group[i].getInitials() +
                            ", groupID " + group[i].getGroupId());
                }
            } count = 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return groupId == student.groupId &&
                Objects.equals(surname, student.surname) &&
                Objects.equals(initials, student.initials) &&
                Arrays.equals(academicPerformance, student.academicPerformance);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(surname, initials, groupId);
        result = 31 * result + Arrays.hashCode(academicPerformance);
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "surname='" + surname + '\'' +
                ", initials='" + initials + '\'' +
                ", groupId=" + groupId +
                ", academicPerformance=" + Arrays.toString(academicPerformance) +
                '}';
    }
}

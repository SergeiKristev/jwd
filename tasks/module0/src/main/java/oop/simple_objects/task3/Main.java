package oop.simple_objects.task3;

import static oop.simple_objects.task3.Student.filterByMarks;

/*
    3. Создайте класс с именем Student, содержащий поля: фамилия и инициалы, номер группы, успеваемость (массив из пяти
    элементов). Создайте массив из десяти элементов такого типа. Добавьте возможность вывода фамилий и номеров групп
    студентов, имеющих оценки, равные только 9 или 10.
 */

public class Main {

    public static void main(String[] args) {

        Student student1 = new Student("Jackson", "S.L.", 8, new int[]{8, 9, 10, 7, 4});
        Student student2 = new Student("Russel", "K.J.", 8, new int[]{5, 9, 10, 8, 6});
        Student student3 = new Student("Jason", "J.L.", 8, new int[]{10, 9, 10, 9, 9});
        Student student4 = new Student("Goggins", "W.J.", 8, new int[]{7, 9, 10, 7, 4});
        Student student5 = new Student("Bichir", "D.J.", 8, new int[]{9, 9, 10, 7, 4});
        Student student6 = new Student("Roth", "T.J.", 8, new int[]{8, 9, 10, 7, 4});
        Student student7 = new Student("Dern", "B.J.", 8, new int[]{8, 9, 10, 7, 4});
        Student student8 = new Student("Madsen", "M.J.", 8, new int[]{8, 9, 10, 7, 4});
        Student student9 = new Student("Parks", "J.J.", 9, new int[]{9, 9, 10, 9, 9});
        Student student10 = new Student("Gourrier", "D.J.", 9, new int[]{8, 9, 10, 7, 4});

        Student [] group1 = {student1, student2, student3, student4, student5, student6, student7, student8, student9, student10};

        filterByMarks(group1, 9, 10);


    }
}

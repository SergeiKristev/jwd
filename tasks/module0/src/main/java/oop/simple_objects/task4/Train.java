package oop.simple_objects.task4;

import java.util.*;

public class Train {

//    Comparator<Train> compareByNumber = Comparator.comparingInt(Train::getTrainNumber);
//
//    Comparator<Train> compareByDestination = Comparator.comparing(Train::getDestination).thenComparing(Train::getDepartureTime);


    private int trainNumber;
    private String destination;
    private Calendar departureTime;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Calendar getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Calendar departureTime) {
        this.departureTime = departureTime;
    }

    public Train(int trainNumber, String destination, Calendar departureTime) {
        this.trainNumber = trainNumber;
        this.destination = destination;
        this.departureTime = departureTime;
    }

    public static void trainGetInfo(Train[] trains, int trainNumber) {
        for (Train train: trains) {
            if (train.getTrainNumber() == trainNumber) {
                System.out.println(train.toString());
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return trainNumber == train.trainNumber &&
                Objects.equals(destination, train.destination) &&
                Objects.equals(departureTime, train.departureTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, trainNumber, departureTime);
    }

    @Override
    public String toString() {
        return "Train{" +
                "trainNumber=" + trainNumber +
                ", destination='" + destination + '\'' +
                ", departureTime=" + departureTime.getTime() +
                '}';
    }
}

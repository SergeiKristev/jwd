package oop.simple_objects.task4;

import java.util.Arrays;
import java.util.Comparator;

public class TrainComparator {

    public static Train[] compareByTrainNumber(Train[] arr) {
        Arrays.sort(arr, Comparator.comparingInt(Train::getTrainNumber));
        return arr;
    }

    public static Train[] compareByDestinationAndDeparture(Train[] arr) {
        Arrays.sort(arr, Comparator.comparing(Train::getDestination).thenComparing(Train::getDepartureTime));
        return arr;
    }

}

package oop.simple_objects.task4;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static oop.simple_objects.task4.Train.trainGetInfo;

/*
    4. Создайте класс Train, содержащий поля: название пункта назначения, номер поезда, время отправления. Создайте данные
    в массив из пяти элементов типа Train, добавьте возможность сортировки элементов массива по номерам поездов.
    Добавьте возможность вывода информации о поезде, номер которого введен пользователем. Добавьте возможность сортировки
    массив по пункту назначения, причем поезда с одинаковыми пунктами назначения должны быть упорядочены по времени
    отправления.
 */
public class Main {

    public static void main(String[] args) {
        Train train1 = new Train(116, "Brest",
                new GregorianCalendar(2020, Calendar.MARCH, 3, 14, 55));
        Train train2 = new Train(118, "Moscow",
                new GregorianCalendar(2020, Calendar.MARCH, 3, 18, 5));
        Train train3 = new Train(119, "Brest",
                new GregorianCalendar(2020, Calendar.MARCH, 1, 4, 30));
        Train train4 = new Train(114, "Vilnius",
                new GregorianCalendar(2020, Calendar.MARCH, 5, 16, 10));
        Train train5 = new Train(113, "London",
                new GregorianCalendar(2020, Calendar.MARCH, 7, 1, 8));

        Train[] trains = {train1, train2, train3, train4, train5};

        TrainComparator.compareByTrainNumber(trains);
        System.out.println(Arrays.toString(trains));

        TrainComparator.compareByDestinationAndDeparture(trains);
        System.out.println(Arrays.toString(trains));

        trainGetInfo(trains, 113);

    }
}

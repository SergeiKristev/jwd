package oop.simple_objects.task10;

public enum AirplaneType {
    BOEING_737_300,
    BOEING_737_500,
    BOEING_737_800,
    EMBRAER_E_175,
    EMBRAER_E_195,
    CRJ_200
}

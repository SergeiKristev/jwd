package oop.simple_objects.task10;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;

public class Airline {

    private String destination;
    private String flightNumber;
    private AirplaneType airplaneType;
    private Calendar timeOfDeparture;
    private DaysOfWeek[] daysOfWeek;

    public Airline(String destination, String flightNumber, AirplaneType airplaneType, Calendar timeOfDeparture, DaysOfWeek[] daysOfWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.timeOfDeparture = timeOfDeparture;
        this.daysOfWeek = daysOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public Calendar getTimeOfDeparture() {
        return timeOfDeparture;
    }

    public void setTimeOfDeparture(Calendar timeOfDeparture) {
        this.timeOfDeparture = timeOfDeparture;
    }

    public DaysOfWeek[] getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(DaysOfWeek[] daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Airline airline = (Airline) o;

        if (!Objects.equals(destination, airline.destination)) return false;
        if (!Objects.equals(flightNumber, airline.flightNumber))
            return false;
        if (airplaneType != airline.airplaneType) return false;
        if (!Objects.equals(timeOfDeparture, airline.timeOfDeparture))
            return false;
        return daysOfWeek == airline.daysOfWeek;

    }

    @Override
    public int hashCode() {
        int result = destination != null ? destination.hashCode() : 0;
        result = 31 * result + (flightNumber != null ? flightNumber.hashCode() : 0);
        result = 31 * result + (airplaneType != null ? airplaneType.hashCode() : 0);
        result = 31 * result + (timeOfDeparture != null ? timeOfDeparture.hashCode() : 0);
        result = 31 * result + (daysOfWeek != null ? daysOfWeek.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "destination='" + destination + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", airplaneType=" + airplaneType +
                ", timeOfDeparture=" + timeOfDeparture.getTime() +
                '}';
    }
}

package oop.simple_objects.task10;

import java.util.Calendar;
import java.util.GregorianCalendar;

/*
    10. Создать класс Airline, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод
    toString(). Создать второй класс, агрегирующий массив типа Airline, с подходящими конструкторами и методами.
    Задать критерии выбора данных и вывести эти данные на консоль.
    Airline: пункт назначения, номер рейса, тип самолета, время вылета, дни недели.

    Найти и вывести:
        a) список рейсов для заданного пункта назначения;
        b) список рейсов для заданного дня недели;
        c) список рейсов для заданного дня недели, время вылета для которых больше заданного.
 */

public class Main {

    public static void main(String[] args) {

        //Using LocalDate is more preferable than Calendar

        Airline airline1 = new Airline("Milan", "B2 882", AirplaneType.EMBRAER_E_195,
                new GregorianCalendar(2020, Calendar.MARCH, 5, 16,20),
                new DaysOfWeek[]{DaysOfWeek.MONDAY, DaysOfWeek.THURSDAY, DaysOfWeek.SATURDAY});

        Airline airline2 = new Airline("Moscow", "B2 956", AirplaneType.BOEING_737_800,
                new GregorianCalendar(2020, Calendar.MARCH, 5, 21,25),
                new DaysOfWeek[]{DaysOfWeek.MONDAY, DaysOfWeek.THURSDAY});

        Airline airline3 = new Airline("Moscow", "B2 972", AirplaneType.BOEING_737_500,
                new GregorianCalendar(2020, Calendar.MARCH, 6, 8,50),
                new DaysOfWeek[]{DaysOfWeek.MONDAY, DaysOfWeek.THURSDAY, DaysOfWeek.TUESDAY, DaysOfWeek.SATURDAY});

        Airline airline4 = new Airline("Tallinn", "B2 806", AirplaneType.EMBRAER_E_175,
                new GregorianCalendar(2020, Calendar.MARCH, 6, 12,24),
                new DaysOfWeek[]{DaysOfWeek.MONDAY, DaysOfWeek.THURSDAY, DaysOfWeek.TUESDAY, DaysOfWeek.SATURDAY});

        Airline airline5 = new Airline("Istanbul", "B2 784", AirplaneType.EMBRAER_E_195,
                new GregorianCalendar(2020, Calendar.MARCH, 7, 3,38),
                new DaysOfWeek[]{DaysOfWeek.MONDAY, DaysOfWeek.THURSDAY, DaysOfWeek.TUESDAY,DaysOfWeek.SATURDAY});

        Belavia avaiapark = new Belavia(new Airline[]{airline1,airline2, airline3, airline4, airline5});


        Belavia.showFlightsByDestination(avaiapark.getAirline(), "Moscow");
        System.out.println();

        Belavia.showFlightsByDayOfWeek(avaiapark.getAirline(), DaysOfWeek.THURSDAY);
        System.out.println();

        Belavia.showFlightsByDayOfWeek(avaiapark.getAirline(), DaysOfWeek.MONDAY,
               new GregorianCalendar(2020, Calendar.MARCH, 6, 15, 0));

    }
}

package oop.simple_objects.task10;

import java.util.Calendar;

public class Belavia {

    private Airline[] airline;

    Belavia(Airline[] airline) {
        this.airline = airline;
    }

    Airline[] getAirline() {
        return airline;
    }

    public void setAirline(Airline[] airline) {
        this.airline = airline;
    }

    static void showFlightsByDestination(Airline[] belavia, String destination) {
        System.out.println("List of flights scheduled on " + destination);
        for (Airline flight : belavia) {
                if (flight.getDestination().equalsIgnoreCase(destination)) {
                    System.out.println(flight.toString());
            }
        }
    }


    static void showFlightsByDayOfWeek(Airline[] belavia, DaysOfWeek daysOfWeeks) {
        System.out.println("List of flights scheduled on " + daysOfWeeks);
        for (Airline flight : belavia) {
            for (DaysOfWeek day: flight.getDaysOfWeek()) {
                if (day == daysOfWeeks) {
                    System.out.println(flight.toString());
                }
            }
        }
    }

    static void showFlightsByDayOfWeek(Airline[] belavia, DaysOfWeek daysOfWeeks, Calendar timeDeparture) {
        System.out.println("List of flights scheduled in " + daysOfWeeks + " after " + timeDeparture.getTime());
        for (Airline flight : belavia) {
            if (flight.getTimeOfDeparture().after(timeDeparture)) {
                for (DaysOfWeek day: flight.getDaysOfWeek()) {
                    if(day == daysOfWeeks) {
                        System.out.println(flight.toString());
                    }
                }
            }
        }
    }

}

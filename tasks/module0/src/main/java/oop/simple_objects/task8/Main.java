package oop.simple_objects.task8;

/*
    8. Создать класс Customer, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод
    toString(). Создать второй класс, агрегирующий массив типа Customer, с подходящими конструкторами и методами.
    Задать критерии выбора данных и вывести эти данные на консоль.
    Класс Customer: id, фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.

    Найти и вывести:
    a) список покупателей в алфавитном порядке;
    b) список покупателей, у которых номер кредитной карточки находится в заданном интервале
 */


import static oop.simple_objects.task8.RegularCustomers.showCustomers;
import static oop.simple_objects.task8.RegularCustomers.showCustomersByCreditCard;

public class Main {

    public static void main(String[] args) {

        Customer customer1 = new Customer("Vader", "Darth","O.", "Death Star",
                4444_5555, 123_456_789);
        Customer customer2 = new Customer( "Skywalker","Luke", "J.", "Tatooine",
                4444_5567, 123_456_799);
        Customer customer3 = new Customer(  "Organa","Leia", "J.", "Alderaan",
                4444_5588, 123_456_785);
        Customer customer4 = new Customer( "Solo","Han","O.", "Hoth",
                4444_5512, 123_456_784);
        Customer customer5 = new Customer(  "Kenobi","Obi-Wan","O.", "Mustafar",
                4444_5578, 123_456_788);

        RegularCustomers regularCustomers = new RegularCustomers(new Customer[]{customer1, customer2, customer3, customer4, customer5});

        for (Customer customer : regularCustomers.getCustomers()) {
            System.out.println(customer.toString());
        }
        System.out.println();

        showCustomers(regularCustomers.getCustomers());
        System.out.println();

        showCustomersByCreditCard(regularCustomers.getCustomers(), 4444_5560, 4444_5580);

    }
}

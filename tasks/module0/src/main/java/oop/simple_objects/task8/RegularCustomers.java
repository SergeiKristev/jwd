package oop.simple_objects.task8;

import java.util.Arrays;
import java.util.Comparator;

public class RegularCustomers {

    private Customer[] customers;

    Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    RegularCustomers(Customer[] customers) {
        this.customers = customers;
    }

    static void showCustomers(Customer[] customers) {
        Arrays.sort(customers, Comparator.comparing(Customer::getLastName));
        for (Customer c : customers) {
            System.out.println(c.toString());
        }
    }


    static void showCustomersByCreditCard(Customer[] customers, long lowLimit, long highLimit) {
        for (Customer customer : customers) {
            if (customer.getCreditCardNumber() > lowLimit && customer.getCreditCardNumber() < highLimit) {
                System.out.println(customer.toString());
            }
        }
    }

}

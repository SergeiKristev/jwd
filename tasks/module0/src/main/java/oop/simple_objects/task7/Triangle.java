package oop.simple_objects.task7;
import basics.classes.MyPoint;

import java.util.Objects;

public class Triangle {
    MyPoint apexA;
    MyPoint apexB;
    MyPoint apexC;

    private double sideAB;
    private double sideBC;
    private double sideCA;

    public Triangle(MyPoint apexA, MyPoint apexB, MyPoint apexC) {
        this.apexA = apexA;
        this.apexB = apexB;
        this.apexC = apexC;
    }

    public void calculateAllSide () {
        sideAB = MyPoint.calculateDistance(apexA, apexB);
        sideBC = MyPoint.calculateDistance(apexB, apexC);
        sideCA = MyPoint.calculateDistance(apexC, apexA);
    }

    public double calculatePerimeter () {
        calculateAllSide();
        return sideAB+sideBC+sideCA;
    }

    //using Heron's formula
    public double calculateArea () {
        double s = (sideAB + sideBC + sideCA) / 2;
        return Math.sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideCA));
    }

    // Calculate median


    public MyPoint getApexA() {
        return apexA;
    }

    public MyPoint getApexB() {
        return apexB;
    }

    public MyPoint getApexC() {
        return apexC;
    }

    public double getSideAB() {
        return sideAB;
    }

    public double getSideBC() {
        return sideBC;
    }

    public double getSideCA() {
        return sideCA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.sideAB, sideAB) == 0 &&
                Double.compare(triangle.sideBC, sideBC) == 0 &&
                Double.compare(triangle.sideCA, sideCA) == 0 &&
                Objects.equals(apexA, triangle.apexA) &&
                Objects.equals(apexB, triangle.apexB) &&
                Objects.equals(apexC, triangle.apexC);
    }

    @Override
    public int hashCode() {
        return Objects.hash(apexA, apexB, apexC, sideAB, sideBC, sideCA);
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "apexA=" + apexA +
                ", apexB=" + apexB +
                ", apexC=" + apexC +
                ", sideAB=" + sideAB +
                ", sideBC=" + sideBC +
                ", sideCA=" + sideCA +
                '}';
    }
}

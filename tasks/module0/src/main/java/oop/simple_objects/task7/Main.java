package oop.simple_objects.task7;

import basics.classes.MyPoint;
/*
    7.  Описать класс, представляющий треугольник. Предусмотреть методы для создания объектов, вычисления площади,
    периметра и точки пересечения медиан.
 */
public class Main {

    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(new MyPoint(10, 4), new MyPoint(23, 5), new MyPoint(67, 78));

        triangle1.calculateAllSide();
        System.out.println(triangle1);

        System.out.println("Area: " + triangle1.calculateArea());
        System.out.println("Perimeter: " + triangle1.calculatePerimeter());
    }
}

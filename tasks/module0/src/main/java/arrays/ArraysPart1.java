package arrays;

import java.util.Arrays;

public class ArraysPart1 {
    public static void main(String[] args) {
        //Количество решенных задач [02]Arrays: 19
        //Количество решенных задач [02]Multidimensional_arrays: 28

        task01(4, 20);
        task02(20);
        task03(20);
        task04(20);
        task05(20);
        task06(20);
        task07(44, 20);
        task08(20, 20);
        task09(20);
        task10(10, 20);
        task11(4, 20);
        task12(20);
        task13(20, 3, 2, 58);
        task14(20);
        task15(20, 15, 29);
        task16(20);
        task17(20);
        //task18
        task19(20);
        task20(20);

    }

    private static void task01(int k, int arrSize) {
        System.out.println("1. В массив A [N] занесены натуральные числа. Найти сумму тех элементов, которые кратны\n " +
                "данному К\n");

        int[] arr = new int[arrSize];
        int sum = 0;
        ArraysUtility.arrayRandomFilling(arr, 100);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % k == 0) {
                sum += arr[i];
            }
        }
        System.out.println("Сумма элементов, кратных K = " + k + " равна " + sum);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task02(int arrSize) {
        System.out.println("2. В целочисленной последовательности есть нулевые элементы. Создать массив из номеров\n" +
                "этих элементов\n");

        int[] arr = new int[arrSize];
        int count = 0;
        int arrResultPosition = 0;
        ArraysUtility.arrayRandomFilling(arr, 100);

//        arr[2] = 0;
//        arr[16] = 0;
//        arr[22] = 0;
//        arr[77] = 0;


        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                count++;
            }
        }

        if (count > 0) {
            int[] arrResult = new int[count];

            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == 0) {
                    arrResult[arrResultPosition] = i;
                    arrResultPosition++;
                }
            }
            System.out.println("Массив создан");
            ArraysUtility.printArray(arrResult);
        } else {
            System.out.println("В массиве отсутствуют нулевые элменты");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task03(int arrSize) {
        System.out.println("3. Дана последовательность целых чисел а1 а2,..., аn . Выяснить, какое число встречается\n" +
                "раньше - положительное или отрицательное.\n");

        int[] arr = new int[20];
        ArraysUtility.arrayRandomFilling(arr, 100);

        //arr[0] = -4;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                System.out.println("Раньше встречается положительное число");
                break;
            } else if (arr[i] < 0) {
                System.out.println("Раньше встречается отрицательное число");
                break;
            }
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task04(int arrSize) {
        System.out.println("4. Дана последовательность действительных чисел а1 а2 ,..., аn . Выяснить, будет ли она\n" +
                "возрастающей\n");

        boolean isIncreasing = true;
        int[] arr = new int[arrSize];
        ArraysUtility.arrayRandomFilling(arr, 100);

        //ArraysUtility.arrayBubbleSort(arr);

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] >= arr[i - 1]) {
                continue;
            } else {
                isIncreasing = false;
                break;
            }
        }
        if (isIncreasing) {
            System.out.println("Последовательность является возрастающей");
        } else {
            System.out.println("Последовательность не является возрастающей");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task05(int arrSize) {
        System.out.println("5. Дана последовательность натуральных чисел а1 , а2 ,..., ап. Создать массив из четных\n" +
                "чисел этой последовательности. Если таких чисел нет, то вывести сообщение об этом факте\n");

        int[] arr = new int[arrSize];
        int count = 0;
        int arrResultPosition = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                count++;
            }
        }

        if (count > 0) {
            int[] arrayEven = new int[count];

            for (int i = 0; i < arr.length; i++) {
                if (arr[i] % 2 == 0) {
                    arrayEven[arrResultPosition] = arr[i];
                    arrResultPosition++;
                }
            }
            System.out.println("Массив создан");
            System.out.println(Arrays.toString(arrayEven));
        } else {
            System.out.println("В массиве отсутствуют четные элменты");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task06(int arrSize) {
        System.out.println("6. Дана последовательность чисел а1 ,а2 ,..., ап. Указать наименьшую длину числовой оси,\n" +
                "содержащую все эти числа.\n");

        int[] arr = new int[arrSize];
        ArraysUtility.arrayRandomFilling(arr, 100);
        int min = 0;

        if (arr.length == 0) {
            System.out.println("Массив пустой");
        }
        if (arr.length == 1) {
            min = arr[0];
        }
        if (arr.length > 0) {
            ArraysUtility.arrayBubbleSort(arr);
        }
        min = arr[1] - arr[0];

        System.out.println("Наименьшая длина числовой оси: " + min);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task07(int z, int arrSize) {
        System.out.println("7. Дана последовательность действительных чисел а1 ,а2 ,..., ап. Заменить все ее члены,\n" +
                "большие данного Z, этим числом. Подсчитать количество замен\n");

        int[] arr = new int[arrSize];
        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > z) {
                arr[i] = z;
            }
        }
        System.out.println("Массив чисел, не больше числа z = " + z);
        ArraysUtility.printArray(arr);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task08(int n, int arrSize) {
        System.out.println("8. Дан массив действительных чисел, размерность которого N. Подсчитать, сколько в нем\n" +
                "отрицательных, положительных и нулевых элементов\n");

        int[] arr = new int[arrSize];
        int positiveElements = 0;
        int negativeElements = 0;
        int nullElements = 0;
        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                positiveElements++;
            } else if (arr[i] < 0) {
                negativeElements++;
            } else {
                nullElements++;
            }
        }

        System.out.println("Количество положительных элементов -  " + positiveElements);
        System.out.println("Количество отрицательных элементов -  " + negativeElements);
        System.out.println("Количество нулевых элементов -  " + nullElements);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task09(int arrSize) {
        System.out.println("9. Даны действительные числа а1 ,а2 ,..., аn . Поменять местами наибольший и наименьший\n" +
                "элементы\n");

        int[] arr = new int[arrSize];
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int indexMax = 0;
        int indexMin = 0;
        int temp = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexMax = i;
            }
            if (arr[i] < min) {
                min = arr[i];
                indexMin = i;
            }
        }

        temp = arr[indexMax];
        arr[indexMax] = arr[indexMin];
        arr[indexMin] = temp;

        System.out.println("Массив после замены:");
        ArraysUtility.printArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task10(int i, int arrSize) {
        System.out.println("10. Даны целые числа а1 ,а2 ,..., аn . Вывести на печать только те числа, для которых аi > i\n");

        int[] arr = new int[arrSize];

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);
        System.out.println("Числа больше i = " + i);
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] > i) {
                System.out.print(arr[j] + " ");
            }
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task11(int m, int arrSize) {
        System.out.println("11. Даны натуральные числа а1 ,а2 ,..., аn . Указать те из них, у которых остаток от деления\n" +
                "на М равен L (0 < L < М-1).\n");

        int[] arr = new int[arrSize];
        int count = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);
        System.out.println("Числа, у которых остаток от деления на М = " + m + " равен L (0 < L < М-1): ");

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % m > 0 && arr[i] % m < m - 1) {
                System.out.print(arr[i] + " ");
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Таких чисел нет");
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task12(int arrSize) {
        System.out.println("12. Задана последовательность N вещественных чисел. Вычислить сумму чисел, порядковые номера\n" +
                "которых являются простыми числами\n");

        int[] arr = new int[arrSize];
        int sum = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (ArraysUtility.checkSimple(i)) {
                sum += arr[i];
            }
        }

        System.out.println("Сумма чисел, порядковые номера которых являются простыми числами: " + sum);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task13(int arrSize, int m, int l, int n) {
        System.out.println("13. Определить количество элементов последовательности натуральных чисел, кратных числу М и\n" +
                "заключенных в промежутке от L до N\n");

        int[] arr = new int[arrSize];
        int count = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % m == 0 && arr[i] > l && arr[i] < n) {
                count++;
            }
        }

        System.out.println("Количество элементов последовательности натуральных чисел, кратных числу М = " + m + " и\n" +
                "заключенных в промежутке от L = " + l + " до N = " + n + " равняется " + count);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task14(int arrSize) {
        System.out.println("14. Дан одномерный массив A[N]. Найти: max(a2,a4,...,a2k) + min(a1,a3,...,a2k+1)\n");

        int[] arr = new int[arrSize];
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int maxSum = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);


        for (int i = 1; i < arr.length / 2; i++) {
            max = Integer.max(max, arr[2 * i]);
            min = Integer.min(min, arr[2 * i - 1]);
        }
        maxSum = max + min;


        System.out.println("max(a2,a4,...,a2k) + min(a1,a3,...,a2k+1) = " + maxSum);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task15(int arrSize, int c, int d) {
        System.out.println("15. Дана последовательность действительных чисел a1,a2,...,an . Указать те ее элементы,\n" +
                "которые принадлежат отрезку [с, d]\n");

        int[] arr = new int[arrSize];
        int count = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);
        System.out.println("Отрезку [" + c + ", " + d + "] принадлежат элементы: ");

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= c && arr[i] <= d) {
                System.out.print(arr[i] + " ");
                count++;
            }
        }
        if (count == 0) {
            System.out.println("элементы отсутсвуют");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task16(int arrSize) {
        System.out.println("16. Даны действительные числа a1,a2,...,an . Найти max(a1 + a2n,a2 + a2n−1,...,an + an+1)\n");

        int[] arr = new int[arrSize];

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        int maxSum = arr[0] + arr[arr.length - 1];

        for (int i = 1; i < arr.length / 2; i++) {
            if ((arr[i] + arr[arr.length - i - 1]) > maxSum) {
                maxSum = arr[i] + arr[arr.length - i - 1];
            }
        }

        System.out.println("max(a1 + a2n,a2 + a2n−1,...,an + an+1) = " + maxSum);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task17(int arrSize) {
        System.out.println("17. Дана последовательность целых чисел a1,a2,...,an . Образовать новую последовательность,\n" +
                "выбросив из исходной те члены, которые равны min(a1,a2,...,an)\n");

        int[] arr = new int[arrSize];
        int min = Integer.MAX_VALUE;
        int resultPosition = 0;
        int count = 0;

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);


        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
                count = 1;
            } else if (arr[i] == min) {
                count++;
            }
        }

        int[] arr2 = new int[arr.length - count];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != min) {
                arr2[resultPosition] = arr[i];
                resultPosition++;
            }
        }

        System.out.println("Минимальный элемент: " + min);
        System.out.println("Новая последовательность: ");
        ArraysUtility.printArray(arr2);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task19(int arrSize) {
        System.out.println("19. В массиве целых чисел с количеством элементов n найти наиболее часто встречающееся число.\n" +
                "Если таких чисел несколько, то определить наименьшее из них\n");

        int[] arr = new int[arrSize];

        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        ArraysUtility.arrayBubbleSort(arr);

        int maxFrequency = 0;
        int MaxFrequencyCounter = 0;
        int counter = 0;
        maxFrequency = arr[1];
        MaxFrequencyCounter = 1;

        int count = 0;
        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j < arr.length; j++) {
                if (arr[i] == arr[j])
                    counter++;
                count++;
            }
            if (((MaxFrequencyCounter == counter) && (maxFrequency > arr[i])) || (MaxFrequencyCounter < counter)) {
                maxFrequency = arr[i];
                MaxFrequencyCounter = counter;
            }
            counter = 0; // сбрасываем счётчик
            count = 0;
        }

        System.out.println("Наиболее часто встречающееся число = " + maxFrequency);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task20(int arrSize) {
        System.out.println("20. Дан целочисленный массив с количеством элементов п. Сжать массив, выбросив из него\n" +
                "каждый второй элемент (освободившиеся элементы заполнить нулями). Примечание. Дополнительный массив\n" +
                "не использоват\n");

        int[] arr = new int[arrSize];
        ArraysUtility.arrayRandomFilling(arr, 100);
        System.out.println("Исходный массив: ");
        ArraysUtility.printArray(arr);

        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0)
                arr[i] = 0;
        }

        System.out.println("Новый массив:");
        ArraysUtility.printArray(arr);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }
}

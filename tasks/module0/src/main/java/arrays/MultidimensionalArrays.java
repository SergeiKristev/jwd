package arrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

public class MultidimensionalArrays {
    public static void main(String[] args) throws IOException {
        //Количество решенных задач [02]Arrays: 19
        //Количество решенных задач [02]Multidimensional_arrays: 28
        task01();
        task02();
        task03(10);
        task04(10);
        task05(10);
        task06(10);
        task07(10);
        task08(5, 5, 10);
        task09(5, 5, 10);
        task10(2, 3, 10);
        task11(5, 5, 10);
        task12(5, 5, 10);
        task13(6, 6, 10);
        task14(6, 6, 10);
        task15(6, 6, 10);
        task16(6, 6, 10);
        task26();
        task27(5, 5, 100);
        task28(5, 5, 10);
        task29(5, 10);
        task30(10, 20, 15);
        task31(10, 10, 1000);
        task32(10, 10, 100);
        task33(10, 10, 100);
        task34(10, 10, 2);
        task35(10, 10, 100);
        task38(10, 10, 100);
        task39(10, 10, 10);

    }

    private static void task01() {
        System.out.println("1. Cоздать матрицу 3 x 4, заполнить ее числами 0 и 1 так, чтобы в одной строке была ровно\n" +
                "одна единица, и вывести на экран.\n");

        int[][] arr = {{0, 0, 1},
                {0, 1, 0},
                {1, 0, 0}};

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task02() {
        System.out.println("2. Создать и вывести на экран матрицу 2 x 3, заполненную случайными числами из [0, 9].\n");

        int[][] arr = new int[2][3];

        ArraysUtility.multiArrayRandomFilling(arr, 10);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task03(int bound) {
        System.out.println("3. Дана матрица. Вывести на экран первый и последний столбцы.\n");

        int[][] arr = new int[5][10];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();
        System.out.println("Первый столбец: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][0]);
        }
        System.out.println("Последний столбец: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][arr[i].length - 1]);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task04(int bound) {
        System.out.println("4. Дана матрица. Вывести на экран первую и последнюю строки.\n");

        int[][] arr = new int[5][10];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();
        System.out.println("Первая строка: ");
        ArraysUtility.printArray(arr[0]);
        System.out.println("Последняя строка: ");
        ArraysUtility.printArray(arr[arr.length - 1]);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task05(int bound) {
        System.out.println("5. Дана матрица. Вывести на экран все четные строки, то есть с четными номерами.\n");

        int[][] arr = new int[6][10];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();
        System.out.println("Четные строки: ");
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) { //even index != even string
                ArraysUtility.printArray(arr[i]);
            }
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task06(int bound) {
        System.out.println("6. Дана матрица. Вывести на экран все нечетные столбцы, у которых первый элемент больше\n" +
                "последнего.\n");

        int[][] arr = new int[3][10];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();

        System.out.println("Нечетные столбцы: ");

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j % 2 == 0 && arr[0][j] > arr[arr.length - 1][j]) {
                    System.out.print(arr[i][j] + "\t");
                }
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task07(int bound) {
        System.out.println("7. Дан двухмерный массив 5×5. Найти сумму модулей отрицательных нечетных элементов.\n");

        int[][] arr = new int[5][5];
        int sum = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);
        arr[1][3] = -5;
        arr[3][3] = -5;

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] < 0 && arr[i][j] % 2 != 0) {
                    sum += Math.abs(arr[i][j]);
                }
            }
        }
        System.out.println("Сумма модулей отрицательных нечетных элементов: " + sum);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task08(int n, int m, int bound) {
        System.out.println("8. Дан двухмерный массив n×m элементов. Определить, сколько раз встречается число 7 среди\n" +
                "элементов массива\n");

        int[][] arr = new int[n][m];
        int count = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == 7) {
                    count++;
                }
            }
        }
        System.out.println("Число 7 встречается " + count + " раз(а)");

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task09(int n, int m, int bound) {
        System.out.println("9. Дана квадратная матрица. Вывести на экран элементы, стоящие на диагонали.\n");

        int[][] arr = new int[n][m];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();

        System.out.println("Элементы стоящие на диагонали:");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i][i] + " ");
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task10(int k, int p, int bound) {
        System.out.println("10. Дана матрица. Вывести k-ю строку и p-й столбец матрицы.\n");

        int[][] arr = new int[5][5];

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();

        System.out.println(k + " строка:\n");
        ArraysUtility.printArray(arr[k - 1]);
        System.out.println();
        System.out.println(p + " столбец:\n");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][p - 1]);
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task11(int m, int n, int bound) {
        System.out.println("11. Дана матрица размера m x n. Вывести ее элементы в следующем порядке: первая строка\n" +
                "справа налево, вторая строка слева направо, третья строка справа налево и так далее.\n");

        int[][] arr = new int[m][n];
        int count = 1;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();
        System.out.println("Инверсия строк:");
        for (int i = 0; i < arr.length; i++) {
            if (count % 2 != 0) {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    System.out.print(arr[i][j] + "\t");
                }
            } else {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + "\t");
                }
            }
            count++;
            System.out.println();
        }
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task12(int m, int n, int bound) {
        System.out.println("12. Получить квадратную матрицу порядка n\n");

        int[][] arr = new int[m][n];
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == i) {
                    arr[i][j] = count;
                    count++;
                } else {
                    arr[i][j] = 0;
                }
            }
        }

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task13(int m, int n, int bound) {
        System.out.println("13. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное)\n");

        int[][] arr = new int[m][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (i % 2 == 0) {
                    arr[i][j] = j + 1;
                } else {
                    arr[i][j] = n - j;
                }
            }
        }

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task14(int n, int m, int bound) {
        System.out.println("14. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное)\n");

        int[][] arr = new int[n][m];
        int count = 1;

        for (int i = 0; i < arr.length; i++) {
            for (int j = arr[i].length - 1; j >= 0; j--) {
                if (j == arr[i].length - 1 - i) {
                    arr[i][j] = count;
                    count++;
                } else {
                    arr[i][j] = 0;
                }
            }
        }

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task15(int n, int m, int bound) {
        System.out.println("15. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное)\n");

        int[][] arr = new int[n][m];
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == i) {
                    arr[i][j] = n - j;
                } else {
                    arr[i][j] = 0;
                }
            }
        }

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task16(int n, int m, int bound) {
        System.out.println("16. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное)\n");

        int[][] arr = new int[n][m];
        int count = 1;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == i) {
                    arr[i][j] = count * (count + 1);
                    count++;
                } else {
                    arr[i][j] = 0;
                }
            }
        }

        System.out.println("Полученная матрица:");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task26() throws IOException {
        System.out.println("26. С клавиатуры вводится двумерный массив чисел размерностью nxm. Выполнить с массивом\n" +
                "следующие действия:\n" +
                "а) вычислить сумму отрицательных элементов в каждой строке;\n" +
                "б) определить максимальный элемент в каждой строке;\n" +
                "в) переставить местами максимальный и минимальный элементы матрицы.\n");

        int firstArraySize;
        int secondArraySize;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите размер 1-го массива");
        firstArraySize = Integer.parseInt(reader.readLine());
        System.out.println("Введите размер 2-го массива");
        secondArraySize = Integer.parseInt(reader.readLine());

        int[][] arr = new int[firstArraySize][secondArraySize];
        System.out.println("Вводите числа через Enter");

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.println("Массив 1, индекс - " + i + " Массив 2, индекс - " + j);
                try {
                    arr[i][j] = Integer.parseInt(reader.readLine());
                } catch (IOException | NumberFormatException e) {
                    System.out.println("Неверный ввод");
                }
            }
        }

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        reader.close();

        //а) вычислить сумму отрицательных элементов в каждой строке
        // по модулю?
        int negativeSum = 0;
        int lineNumber = 0;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] < 0) {
                    lineNumber = i + 1;
                    negativeSum += Math.abs(arr[i][j]);
                }
            }
            System.out.println("Сумма отрицательных элементов строки - " + lineNumber + " равна " + negativeSum);
            lineNumber = 0;
            negativeSum = 0;
        }
        System.out.println();
        //б) определить максимальный элемент в каждой строке

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                lineNumber = i + 1;

                if (arr[i][j] > max) {
                    max = arr[i][j];
                }
            }
            System.out.println("Максимальный элемент строки - " + lineNumber + " равен " + max);
            lineNumber = 0;
            max = Integer.MIN_VALUE;
        }
        System.out.println();
        //в) переставить местами максимальный и минимальный элементы матрицы

        max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int maxI = 0;
        int maxJ = 0;
        int minI = 0;
        int minJ = 0;
        int temp = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                if (arr[i][j] > max) {
                    max = arr[i][j];
                    maxI = i;
                    maxJ = j;
                }

                if (arr[i][j] < min) {
                    min = arr[i][j];
                    minI = i;
                    maxJ = j;
                }
            }

        }

        System.out.println("Максимальный элемент равен " + max + ", минимальный элемент равен " + min);
        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        temp = arr[maxI][maxJ];
        arr[maxI][maxJ] = arr[minI][minJ];
        arr[minI][minJ] = temp;

        System.out.println("Массив после замены: ");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task27(int n, int m, int bound) {
        System.out.println("27. В числовой матрице поменять местами два столбца любых столбца, т. е. все элементы одного\n" +
                "столбца поставить на соответствующие им позиции другого, а его элементы второго переместить в первый.\n" +
                "Номера столбцов вводит пользователь с клавиатуры.\n");
        int firstColumn;
        int secondColumn;
        int temp = 0;

        int[][] arr = new int[n][m];
        ArraysUtility.multiArrayRandomFilling(arr, 100);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите номер 1-й колонки");
        firstColumn = scanner.nextInt();
        //validate

        System.out.println("Введите размер 2-й колонки");
        secondColumn = scanner.nextInt();
        // validate

        if (firstColumn < 0 || firstColumn > m || secondColumn < 0 || secondColumn > m) {
            System.out.println("Некорректное значение");
        }

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (j == firstColumn - 1) {
                    temp = arr[i][j];
                    arr[i][j] = arr[i][secondColumn - 1];
                    arr[i][secondColumn - 1] = temp;

                }
            }
        }

        System.out.println("Массив после замены: ");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task28(int n, int m, int bound) {
        System.out.println("28. Задана матрица неотрицательных чисел. Посчитать сумму элементов в каждом столбце.\n" +
                "Определить, какой столбец содержит максимальную сумму.\n");

        int sum = 0;
        int columnNumber = 0;
        int maxColumn = 0;
        int maxColumnPosition = 0;
        int[] result = new int[m];
        int[][] arr = new int[n][m];
        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int k = 0; k < m; k++) { //column number iteration
            for (int i = 0; i < arr.length; i++) {
                sum += arr[i][k];
            }
            result[k] = sum;
            sum = 0;
        }
        System.out.println("Сумма элементов в каждом столбце: ");
        ArraysUtility.printArray(result);

        for (int i = 0; i < result.length; i++) {
            if (result[i] > maxColumn) {
                maxColumn = result[i];
                maxColumnPosition = i;
            }
        }
        maxColumnPosition++;
        System.out.println("Максимальную сумму содержит столбец № : " + maxColumnPosition);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task29(int n, int bound) {
        System.out.println("29. Найти положительные элементы главной диагонали квадратной матрицы\n");

        int[][] arr = new int[n][n];
        int[] result = new int[n];
        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][i] > 0) {
                    result[i] = arr[i][i];
                }
            }
        }

        System.out.println("Положительные элементы главной диагонали квадратной матрицы: ");
        ArraysUtility.printArray(result);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task30(int n, int m, int bound) {
        System.out.println("30. Матрицу 10x20 заполнить случайными числами от 0 до 15. Вывести на экран саму матрицу и\n" +
                "номера строк, в которых число 5 встречается три и более раз\n");

        int[][] arr = new int[n][m];
        int count = 0;
        int columnNumber = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);


        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == 5) {
                    count++;
                }
                if (count == 3) {
                    columnNumber = i + 1;
                    System.out.println("Число 5 встречается три и более раз в строке " + columnNumber);
                    break;
                }
            }
            columnNumber = 0;
            count = 0;
        }

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task31(int n, int m, int bound) {
        System.out.println("31. Сформировать матрицу из чисел от 0 до 999, вывести ее на экран. Посчитать количество\n" +
                "двузначных чисел в ней.\n");

        int[][] arr = new int[n][m];
        int count = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] >= 10 && arr[i][j] < 100) {
                    count++;
                }
            }
        }

        System.out.println("Количество двузначных чисел в матрице " + count);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task32(int n, int m, int bound) {
        System.out.println("32. Отсортировать строки матрицы по возрастанию и убыванию значений элементов\n");

        int[][] arr = new int[n][m];
        int temp = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int i = 0; i < arr.length; i++) {
            ArraysUtility.arrayBubbleSort(arr[i]);
        }

        System.out.println("Матрица после сортировки строк по возрастанию: ");
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println("Матрица после сортировки строк по убыванию: ");

        for (int i = 0; i < arr.length; i++) {
            ArraysUtility.arrayReverseBubbleSort(arr[i]);
        }
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task33(int n, int m, int bound) {
        System.out.println("33. Отсотрировать столбцы матрицы по возрастанию и убыванию значений эементов\n");

        int[][] arr = new int[n][m];
        int temp = 0;

        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int k = 0; k < m; k++) {
            for (int i = arr.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (arr[j][k] > arr[j + 1][k]) {
                        temp = arr[j + 1][k];
                        arr[j + 1][k] = arr[j][k];
                        arr[j][k] = temp;
                    }
                }
            }
        }
        System.out.println("Матрица после сортировки столбцов по возрастанию: ");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println("Матрица после сортировки столбцов по убыванию: ");
        for (int k = 0; k < m; k++) {
            for (int i = arr.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (arr[j][k] < arr[j + 1][k]) {
                        temp = arr[j + 1][k];
                        arr[j + 1][k] = arr[j][k];
                        arr[j][k] = temp;
                    }
                }
            }
        }
        ArraysUtility.printMultidimensionalArray(arr);
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task34(int m, int n, int bound) {
        System.out.println("34. Сформировать случайную матрицу m x n, состоящую из нулей и единиц, причем в каждом\n" +
                "столбце число единиц равно номеру столбца\n");

        int[][] arr = new int[m][n];
        int count = 0;

        Random rand = new Random();

        for (int k = 0; k < n; k++) { //column number iteration
            for (int i = 0; i < arr.length; i++) {
                arr[i][k] = rand.nextInt(bound);
                if (arr[i][k] == 1) {
                    count++;
                }
                if (count < k + 1) {
                    arr[i][k] = 1;
                }
                if (count > k + 1) {
                    arr[i][k] = 0;
                }
            }
            count = 0;
        }

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task35(int m, int n, int bound) {
        System.out.println("35. Найдите наибольший элемент матрицы и заменить все нечетные элементы на него\n");

        int[][] arr = new int[m][n];
        int max = Integer.MIN_VALUE;
        ArraysUtility.multiArrayRandomFilling(arr, bound);

        System.out.println("Полученная матрица: ");
        ArraysUtility.printMultidimensionalArray(arr);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] > max) {
                    max = arr[i][j];
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] % 2 != 0) {
                    arr[i][j] = max;
                }
            }
        }
        System.out.println("Матрица после замены: ");
        ArraysUtility.printMultidimensionalArray(arr);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task38(int m, int n, int bound) {
        System.out.println("38. Найдите сумму двух матриц\n");

        int[][] arr1 = new int[m][n];
        int[][] arr2 = new int[m][n];
        int[][] arrSum = new int[m][n];

        ArraysUtility.multiArrayRandomFilling(arr1, bound);
        ArraysUtility.multiArrayRandomFilling(arr2, bound);

        System.out.println("Полученная матрица 1: ");
        ArraysUtility.printMultidimensionalArray(arr1);

        System.out.println("Полученная матрица 2: ");
        ArraysUtility.printMultidimensionalArray(arr2);

        System.out.println("Сумма матриц: ");
        for (int i = 0; i < arrSum.length; i++) {
            for (int j = 0; j < arrSum[i].length; j++) {
                arrSum[i][j] = arr1[i][j] + arr2[i][j];
            }
        }
        ArraysUtility.printMultidimensionalArray(arrSum);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }

    private static void task39(int m, int n, int bound) {
        System.out.println("39. Найдите произведение двух матриц.\n");

        int[][] arr1 = new int[m][n];
        int[][] arr2 = new int[m][n];
        int[][] arrMultiply = new int[m][n];

        ArraysUtility.multiArrayRandomFilling(arr1, bound);
        ArraysUtility.multiArrayRandomFilling(arr2, bound);

        System.out.println("Полученная матрица 1: ");
        ArraysUtility.printMultidimensionalArray(arr1);

        System.out.println("Полученная матрица 2: ");
        ArraysUtility.printMultidimensionalArray(arr2);

        System.out.println("Произведение матриц: ");
        for (int i = 0; i < arrMultiply.length; i++) {
            for (int j = 0; j < arrMultiply[i].length; j++) {
                arrMultiply[i][j] = arr1[i][j] * arr2[i][j];
            }
        }

        ArraysUtility.printMultidimensionalArray(arrMultiply);

        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------------");
    }
}

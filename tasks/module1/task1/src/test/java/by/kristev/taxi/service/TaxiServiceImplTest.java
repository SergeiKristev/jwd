package by.kristev.taxi.service;

import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.dal.impl.ByTaxiIdSpecifications;
import by.kristev.taxi.dal.impl.TaxiRepository;
import by.kristev.taxi.model.*;
import by.kristev.taxi.service.impl.TaxiServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TaxiServiceImplTest {

    Repository<Taxi> repository;
    TaxiService service;
    List<Taxi> list;

    @Before
    public void setUp() throws Exception {
        repository = new TaxiRepository();
        {
            Taxi taxi1 = new TaxiCab(100, TaxiType.PASSENGER, CarModel.KIA, CarColor.BLACK, 2018, 80_000,
                    11000, CarBodyType.HATCHBACK, 4, true);
            repository.create(taxi1);

            Taxi taxi2 = new TaxiCab(101, TaxiType.PASSENGER, CarModel.VOLKSWAGEN, CarColor.YELLOW, 2019, 15_000,
                    10500, CarBodyType.SEDAN, 4, true);
            repository.create(taxi2);

            Taxi taxi3 = new TaxiCab(102, TaxiType.PASSENGER, CarModel.LADA, CarColor.ORANGE, 2016, 95_000,
                    8000, CarBodyType.MINIVAN, 6, true);
            repository.create(taxi3);
        }
        service = new TaxiServiceImpl(repository);
        list = repository.findAll();
    }

    @Test
    public void shouldViewAllTaxi() {
        Assert.assertEquals(3, list.size());
    }

    @Test
    public void sortDescByIdTest() {
        list = service.sortTaxiList("DESC", "ID");
        Assert.assertTrue(list.get(0).getId() > list.get(1).getId());
    }

    @Test
    public void sortDescByCarModelTest() {
        list = service.sortTaxiList("DESC", "CAR_MODEL");
        Assert.assertEquals("VOLKSWAGEN", list.get(0).getCarModel().name());
    }

    @Test
    public void findByIdTest() {
        long expectedID = 100;
        Taxi expectedTaxi = repository.read(expectedID);
        Specification<Taxi> specification = new ByTaxiIdSpecifications(expectedID);
        Assert.assertTrue(specification.match(expectedTaxi));
    }

    @Test
    public void deleteByIdTest() {
        long actualID = list.get(0).getId();
        service.deleteById(actualID);
        Assert.assertNull(repository.read(actualID));
    }

    @Test
    public void getTotalCostTest() {
        double expectedCost = 29_500;
        double actualCost = service.getTotalCost();
        Assert.assertEquals(expectedCost, actualCost, 1e-9);
    }
}

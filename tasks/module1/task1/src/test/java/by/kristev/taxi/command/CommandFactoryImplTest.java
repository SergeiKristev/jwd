package by.kristev.taxi.command;

import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.impl.TaxiRepository;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import by.kristev.taxi.service.impl.TaxiServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

public class CommandFactoryImplTest {
    Map<CommandType, Command> commands;

    @Before
    public void setUp() throws Exception {
        commands = new EnumMap<>(CommandType.class);
        Repository<Taxi> repository = new TaxiRepository();
        TaxiService taxiService = new TaxiServiceImpl(repository);

        Command viewTaxiCommand = new ViewAllTaxiCommand(taxiService);
        Command viewDetailCommand = new ViewDetailCommand(taxiService);
        Command defaultCommand = new DefaultCommand(taxiService);

        commands.put(CommandType.VIEW_ALL, viewTaxiCommand);
        commands.put(CommandType.VIEW_DETAIL, viewDetailCommand);
        commands.put(CommandType.DEFAULT_COMMAND, defaultCommand);

    }

    @Test
    public void commandShouldHaveDefaultCommand() {
        CommandType actualCommandName = CommandType.of("TEST_COMMAND");
        Command actualCommand = commands.get(actualCommandName);

        CommandType expectedCommandName = CommandType.DEFAULT_COMMAND;
        Command expectedCommand = commands.get(expectedCommandName);

        Assert.assertEquals(expectedCommand, actualCommand);
    }

    @Test
    public void commandShouldNotReturnNull() {
        CommandType actualCommandName = CommandType.of("");
        Command actualCommand = commands.get(actualCommandName);

        Assert.assertNotNull(actualCommand);
    }
}
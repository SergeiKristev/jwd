package by.kristev.taxi;

import by.kristev.taxi.command.*;
import by.kristev.taxi.controller.TaxiAppHandler;
import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.impl.TaxiRepository;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import by.kristev.taxi.service.impl.TaxiServiceImpl;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.*;

import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaxiAppTest {
    static Repository<Taxi> repository;
    static HttpHandler httpHandler;
    static CommandFactory commandFactory;
    static HttpServer server;

    @BeforeClass
    public static void setUp() throws Exception {
        InetSocketAddress localhost = new InetSocketAddress("localhost", 49070);
        server = HttpServer.create(localhost, 0);

        repository = new TaxiRepository();

        TaxiService taxiService = new TaxiServiceImpl(repository);

        Command viewTaxiCommand = new ViewAllTaxiCommand(taxiService);
        Command viewDetailCommand = new ViewDetailCommand(taxiService);
        Command readFromFileCommand = new ReadFromFileCommand(taxiService);
        Command getTotalCostCommand = new GetTotalCostCommand(taxiService);
        Command deleteTaxiById = new DeleteTaxiCommand(taxiService);
        Command sortCommand = new SortCommand(taxiService);
        Command defaultCommand = new DefaultCommand(taxiService);

        Map<CommandType, Command> commands = new EnumMap<CommandType, Command>(CommandType.class);
        commands.put(CommandType.VIEW_ALL, viewTaxiCommand);
        commands.put(CommandType.VIEW_DETAIL, viewDetailCommand);
        commands.put(CommandType.READ_FROM_FILE, readFromFileCommand);
        commands.put(CommandType.GET_TOTAL_COST, getTotalCostCommand);
        commands.put(CommandType.SORT, sortCommand);
        commands.put(CommandType.DELETE_TAXI_BY_ID, deleteTaxiById);
        commands.put(CommandType.DEFAULT_COMMAND, defaultCommand);

        commandFactory = new CommandFactoryImpl(commands);

        httpHandler = new TaxiAppHandler(commandFactory);

        //create repository
        Map<String, String> requestMap = new ConcurrentHashMap<>();
        requestMap.put("filePath", "src/test/resources/taxi.csv");
        Command command = commandFactory.getCommand(CommandType.READ_FROM_FILE.getCommand());
        command.process(requestMap);

        HttpContext serverContext = server.createContext("/test", httpHandler);

        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();
    }


    @Test
    public void testRepositorySize() {
        List<Taxi> actualList = repository.findAll();
        Assert.assertEquals(16, actualList.size());
    }

    @Test
    public void commandShouldHaveDefaultCommand() {
        Map<String, String> requestMap = new ConcurrentHashMap<>();
        requestMap.put("", "");
        Command actualCommand = commandFactory.getCommand("");
        actualCommand.process(requestMap);

        Command expectedCommand = commandFactory.getCommand(CommandType.DEFAULT_COMMAND.getCommand());

        Assert.assertEquals(expectedCommand, actualCommand);
    }

    @Test
    public void commandShouldDeleteTaxi() {
        List<Taxi> actualList = repository.findAll();
        Map<String, String> requestMap = new ConcurrentHashMap<>();
        requestMap.put(ApplicationConstants.COMMAND_ARGUMENT_NAME, CommandType.DELETE_TAXI_BY_ID.getCommand());
        requestMap.put("ID", "100");

        int actualListSize = actualList.size();
        Command command = commandFactory.getCommand("DELETE_TAXI_BY_ID");
        command.process(requestMap);

        List<Taxi> newList = repository.findAll();
        int expectedListSize = newList.size();
        Assert.assertEquals(expectedListSize, actualListSize-1);
    }


    @After
    public void tearDown() throws Exception {
        server.stop(1);
    }
}
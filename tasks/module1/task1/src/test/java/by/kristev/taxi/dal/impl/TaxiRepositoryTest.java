package by.kristev.taxi.dal.impl;

import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TaxiRepositoryTest {

    Repository<Taxi> repository;

    @Before
    public void setUp() throws Exception {
        repository = new TaxiRepository();
        {
            Taxi taxi1 = new TaxiCab(100, TaxiType.PASSENGER, CarModel.KIA, CarColor.BLACK, 2018, 80_000,
                    11000, CarBodyType.HATCHBACK, 4, true);
            repository.create(taxi1);

            Taxi taxi2 = new TaxiCab(101, TaxiType.PASSENGER, CarModel.VOLKSWAGEN, CarColor.YELLOW, 2019, 15_000,
                    10500, CarBodyType.SEDAN, 4, true);
            repository.create(taxi2);

            Taxi taxi3 = new TaxiCab(102, TaxiType.PASSENGER, CarModel.LADA, CarColor.ORANGE, 2016, 95_000,
                    8000, CarBodyType.MINIVAN, 6, true);
            repository.create(taxi3);
        }
    }

    @Test
    public void repositoryShouldNotReturnNull() {
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void repositoryShouldNotBeEmpty() {
        List<Taxi> actualList = repository.findAll();
        Assert.assertFalse(actualList.isEmpty());
    }

    @Test
    public void testRepositorySize() {
        List<Taxi> actualList = repository.findAll();
        Assert.assertEquals(3, actualList.size());
    }
}
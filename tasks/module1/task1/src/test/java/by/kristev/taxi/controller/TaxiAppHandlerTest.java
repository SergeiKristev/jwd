package by.kristev.taxi.controller;

import by.kristev.taxi.ApplicationConstants;
import by.kristev.taxi.command.CommandFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class TaxiAppHandlerTest {

    CommandFactory factory;
    TaxiAppHandler taxiAppHandler;

    @Before
    public void setUp() throws Exception {
        taxiAppHandler = new TaxiAppHandler(factory);
    }

    @Test
    public void responsePageShouldNotBeEmpty() throws IOException {

        String content = "";
        String response = "";
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        content = bufferedReader.lines().collect(Collectors.joining());

        response = MessageFormat.format(content, "");

        Assert.assertFalse(response.isEmpty());
    }

    @Test
    public void testRequestParsing() throws UnsupportedEncodingException {
        String testQuery = "_command=GET_TOTAL_COST";
        String request = URLDecoder.decode(testQuery, StandardCharsets.UTF_8.name());

        Map<String, String> testParameters = new ConcurrentHashMap<>();

        String[] parameters = request.split("&");
        for (String data : parameters) {
            testParameters.put(data.split("=")[0], data.split("=")[1]);
        }

        String expectedCommandName = "GET_TOTAL_COST";
        String actualCommandName = testParameters.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
        Assert.assertEquals(expectedCommandName, actualCommandName);
    }
}
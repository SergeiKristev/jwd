package by.kristev.taxi.dal;

import by.kristev.taxi.model.Taxi;

import java.util.List;

public interface Repository<T> extends CRUDOperation<T> {

    List<T> find(Specification<T> spec);

    List<T> findAll();

}

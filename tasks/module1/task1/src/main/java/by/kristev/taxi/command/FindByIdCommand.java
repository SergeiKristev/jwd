package by.kristev.taxi.command;

import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.dal.impl.ByTaxiIdSpecifications;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public class FindByIdCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(FindByIdCommand.class);


    private TaxiService taxiService;

    public FindByIdCommand(TaxiService taxiSearchService) {
        this.taxiService = taxiSearchService;
    }

    @Override
    public String process(Map<String, String> params) {
        String response = "";
        try {
            long id = Long.parseLong(params.get("id").toUpperCase());
            Specification<Taxi> specification = new ByTaxiIdSpecifications(id);
            List<Taxi> taxiList = taxiService.findTaxi(specification);
            if (!taxiList.isEmpty()) {
                response = CommandUtil.getResponseForm(taxiList);
            } else {
                response = "Taxi with ID = " + id + " not found";
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Taxi found exception%s", e));
            response = "Taxi not found. Invalid input data.";
        }

        return "<p><h2>SEARCHING RESULT</h2></p>" + response;
    }

}

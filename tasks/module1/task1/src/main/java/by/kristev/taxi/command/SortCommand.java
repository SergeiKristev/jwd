package by.kristev.taxi.command;

import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;

import java.util.List;
import java.util.Map;

public class SortCommand implements Command {

    private TaxiService taxiService;

    public SortCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {
        String sortType = "";
        String sortValue = "";

        sortType = params.get("sort_type").toUpperCase();
        sortValue = params.get("sort_value").toUpperCase();

        List<Taxi> result = taxiService.sortTaxiList(sortType, sortValue);
        return "<p><h2>SORTED LIST</h2></p>" + CommandUtil.getResponseForm(result);
    }
}

package by.kristev.taxi.command;

public interface CommandFactory {

    Command getCommand(String commandName);
}

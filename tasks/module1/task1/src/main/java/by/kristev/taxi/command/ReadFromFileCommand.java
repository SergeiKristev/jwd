package by.kristev.taxi.command;

import by.kristev.taxi.model.*;
import by.kristev.taxi.service.TaxiService;
import by.kristev.taxi.validator.FilePathValidator;
import by.kristev.taxi.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class ReadFromFileCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(ReadFromFileCommand.class);

    private Validator<Path> validator;
    private TaxiService taxiService;

    public ReadFromFileCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
        validator = new FilePathValidator();
    }

    @Override
    public String process(Map<String, String> params) {

        StringBuilder response = new StringBuilder();
        Path path = Paths.get(params.get("filePath"));
        LOGGER.info(String.format("File path: %s", path));

        if (validator.isValid(path)) {
            response.append(loadRepository(path));
            response.append("<h3>The repository loaded</h3>");
        } else {
            response.append("<h3>File path is invalid. CSV file not found.</h3>");
        }
        return response.toString();
    }

    public String loadRepository(Path path) {

        StringBuilder validationResponse = new StringBuilder();
        int lineCounter = 1;

        try (BufferedReader reader = new BufferedReader(new FileReader(String.valueOf(path)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                validationResponse.append(validContext(line, lineCounter));
                lineCounter++;
            }

        } catch (Exception e) {
            LOGGER.error(String.format("Repository didn't load. Exception: %s", e));
            validationResponse.append("<h3>Repository didn't load. Exception: ").append(e.getMessage()).append("</h3>");
        }
        return validationResponse.toString();
    }

    public String validContext(String line, int lineNumber) {

        StringBuilder validationResponse = new StringBuilder();

        String[] values = line.split(";");
        Taxi taxi = null;
        try {
            long id = Long.parseLong(values[0]);
            TaxiType taxiType = TaxiType.valueOf(values[1]);
            CarModel carModel = CarModel.valueOf(values[2]);
            CarColor carColor = CarColor.valueOf(values[3]);
            int yearManufacturer = Integer.parseInt(values[4]);
            int mileage = Integer.parseInt(values[5]);
            double cost = Double.parseDouble(values[6]);

            if (values[1].equalsIgnoreCase("PASSENGER")) {

                CarBodyType bodyType = CarBodyType.valueOf(values[7]);
                int numberOfSeats = Integer.parseInt(values[8]);
                boolean containsBabySeat = Boolean.parseBoolean(values[9]);

                taxi = new TaxiCab(id, taxiType, carModel, carColor, yearManufacturer,
                        mileage, cost, bodyType, numberOfSeats, containsBabySeat);

            } else if (values[1].equalsIgnoreCase("CARGO")) {

                int tonnage = Integer.parseInt(values[7]);
                taxi = new CargoTaxi(id, taxiType, carModel, carColor, yearManufacturer,
                        mileage, cost, tonnage);
            }

            boolean taxiIsNotExist = taxiService.createTaxi(taxi);

            if (taxiIsNotExist) {
                validationResponse.append("<p>Taxi with ID ")
                        .append(id).append(" added to repository.</p>");
            } else {
                validationResponse.append("<p>Line ").append(lineNumber).append(": taxi didn't add to list. Taxi with ID ")
                        .append(id).append(" is already exist.</p>");
                LOGGER.info("Taxi is already exist");

            }

        } catch (Exception e) {
            validationResponse.append("<p>Line ").append(lineNumber).append(": taxi didn't add to list. Exception: ")
                    .append(e.getMessage()).append("</p>");
            LOGGER.error(String.format("Taxi didn't add to list. Exception: %s", e));
        }
        return validationResponse.toString();
    }
}

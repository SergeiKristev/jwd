package by.kristev.taxi.command;

import by.kristev.taxi.service.TaxiService;

import java.util.Map;

public class GetTotalCostCommand implements Command {

    private TaxiService taxiService;

    public GetTotalCostCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {
        return "<h3>Total cost taxi park: " + taxiService.getTotalCost() + "</h3>";
    }
}

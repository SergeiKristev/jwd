package by.kristev.taxi.command;

import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.dal.impl.ByTaxiModelSpecifications;
import by.kristev.taxi.model.CarModel;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public class FindByModelCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(FindByModelCommand.class);

    private TaxiService taxiService;

    public FindByModelCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {

        String response = "";
        try {
            CarModel model = CarModel.valueOf(params.get("model").toUpperCase());
            Specification<Taxi> specification = new ByTaxiModelSpecifications(model);
            List<Taxi> taxiList = taxiService.findTaxi(specification);
            if (!taxiList.isEmpty()) {
                response = CommandUtil.getResponseForm(taxiList);
            } else {
                response = "Taxi with MODEL = " + model + " not found";
            }
        } catch (Exception e) {
            LOGGER.error(String.format("Taxi found exception %s", e));
            response = "Taxi not found. Invalid input data.";
        }

        return "<p><h2>SEARCHING RESULT</h2></p>" + response;
    }
}

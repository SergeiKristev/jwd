package by.kristev.taxi.validator;

public interface Validator<T> {

    boolean isValid(T t);
}

package by.kristev.taxi.model;

import java.util.Objects;

public class CargoTaxi extends Taxi {
    private int tonnage;

    public CargoTaxi(long id, TaxiType taxiType, CarModel carModel, CarColor carColor, int yearManufacturer,
                     int mileage, double cost, int tonnage) {
        super(id, taxiType, carModel, carColor, yearManufacturer, mileage, cost);
        this.tonnage = tonnage;
    }

    public int getTonnage() {
        return tonnage;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = tonnage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CargoTaxi cargoTaxi = (CargoTaxi) o;
        return tonnage == cargoTaxi.tonnage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tonnage);
    }

    @Override
    public String toString() {
        return super.toString() +
                "<p><b>tonnage:</b> " + tonnage + "</p>";
    }
}

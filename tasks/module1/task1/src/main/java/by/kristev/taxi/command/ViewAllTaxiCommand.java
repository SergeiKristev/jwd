package by.kristev.taxi.command;

import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;

import java.util.List;
import java.util.Map;

public class ViewAllTaxiCommand implements Command {

    private TaxiService taxiService;

    public ViewAllTaxiCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {

        List<Taxi> taxi = taxiService.viewAllTaxi();
        return "<p><h2>TAXI LIST</h2></p>" + CommandUtil.getResponseForm(taxi);

    }

}

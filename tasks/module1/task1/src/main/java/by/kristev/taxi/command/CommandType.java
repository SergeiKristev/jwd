package by.kristev.taxi.command;

public enum CommandType {
    VIEW_ALL("VIEW_ALL"),
    VIEW_DETAIL("VIEW_DETAIL"),
    FIND_BY_ID("FIND_BY_ID"),
    FIND_BY_MODEL("FIND_BY_MODEL"),
    GET_TOTAL_COST("GET_TOTAL_COST"),
    SORT("SORT"),
    READ_FROM_FILE("READ_FROM_FILE"),
    CREATE_TAXI("CREATE_TAXI"),
    DELETE_TAXI_BY_ID("DELETE_TAXI_BY_ID"),
    DEFAULT_COMMAND("DEFAULT_COMMAND");

    private final String command;

    CommandType(String s) {
        this.command = s;
    }

    public static CommandType of(String name) {

        final CommandType[] values = CommandType.values();
        for (CommandType commandType : values) {
            if (commandType.getCommand().equals(name) || commandType.name().equals(name)) {
                return commandType;
            }
        }
        return DEFAULT_COMMAND;
    }

    public String getCommand() {
        return command;
    }
}

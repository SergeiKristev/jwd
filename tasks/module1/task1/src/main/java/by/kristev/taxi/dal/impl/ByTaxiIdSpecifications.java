package by.kristev.taxi.dal.impl;

import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.model.Taxi;

public class ByTaxiIdSpecifications implements Specification<Taxi> {

    private long searchId;

    public ByTaxiIdSpecifications(long searchId) {
        this.searchId = searchId;
    }

    @Override
    public boolean match(Taxi bean) {
        return this.searchId == bean.getId();
    }
}

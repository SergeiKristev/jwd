package by.kristev.taxi.command;

import by.kristev.taxi.ApplicationConstants;
import by.kristev.taxi.model.Taxi;

import java.util.List;

public class CommandUtil {

    private CommandUtil() {
    }

    public static String getResponseForm(List<Taxi> taxi) {
        StringBuilder response = new StringBuilder();
        response.append("<div>\n" +
                "    <table width=\"100%\" cellpadding=\"2\" cellspacing=\"2\" border=\"2\">\n" +
                "        <thead>\n" +
                "        <tr>\n" +
                "            <th width=\"5%\">ID</th>\n" +
                "            <th width=\"20%\">Taxi type</th>\n" +
                "            <th width=\"20%\">Car model</th>\n" +
                "            <th width=\"20%\">Car color</th>\n" +
                "            <th width=\"15%\">Year of manufacture</th>\n" +
                "            <th width=\"10%\">Mileage</th>\n" +
                "            <th width=\"5%\">Cost</th>\n" +
                "            <th width=\"5%\">Detail</th>\n" +
                "        </tr>\n" +
                "        </thead>\n" +
                "    </table>\n" +
                "</div>\n" +
                "<div>\n" +
                "    <table width=\"100%\" cellpadding=\"2\" cellspacing=\"2\" border=\"2\">\n" +
                "        <tbody>");

        for (Taxi t : taxi) {
            response.append("<tr>")
                    .append("<td width=\"5%\">")
                    .append(t.getId())
                    .append("</td>")
                    .append("<td width=\"20%\">")
                    .append(t.getTaxiType())
                    .append("</td>")
                    .append("<td width=\"20%\">")
                    .append(t.getCarModel())
                    .append("</td>")
                    .append("<td width=\"20%\">")
                    .append(t.getCarColor())
                    .append("</td>")
                    .append("<td width=\"15%\">")
                    .append(t.getYearManufacturer())
                    .append("</td>")
                    .append("<td width=\"10%\">")
                    .append(t.getMileage())
                    .append("</td>")
                    .append("<td width=\"5%\">")
                    .append(t.getCost())
                    .append("</td>")
                    .append("<td width=\"5%\">")
                    .append("<form action=\"/taxi\" method=\"get\">\n" + "\t<input type=\"hidden\" name=\"")
                    .append(ApplicationConstants.COMMAND_ARGUMENT_NAME + "\" value=\"VIEW_DETAIL\">\n")
                    .append("\t<input type=\"hidden\" name=\"ID\" value=\"" + t.getId() + "\">\n")
                    .append("\t<input type=\"submit\" value=\"DETAIL\" />\n")
                    .append("</form>")
                    .append("</td>")
                    .append("</tr>");

        }
        response.append("        </tbody>\n" +
                "    </table>\n" +
                "</div>");
        return response.toString();
    }
}

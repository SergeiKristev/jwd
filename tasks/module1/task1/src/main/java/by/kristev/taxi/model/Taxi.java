package by.kristev.taxi.model;

import java.util.Objects;

public abstract class Taxi {
    private long id;
    private TaxiType taxiType;
    private CarModel carModel;
    private CarColor carColor;
    private int yearManufacturer;
    private int mileage;
    private double cost;

    public Taxi(long id, TaxiType taxiType, CarModel carModel, CarColor carColor, int yearManufacturer, int mileage, double cost) {
        this.id = id;
        this.taxiType = taxiType;
        this.carModel = carModel;
        this.carColor = carColor;
        this.yearManufacturer = yearManufacturer;
        this.mileage = mileage;
        this.cost = cost;
    }

    public TaxiType getTaxiType() {
        return taxiType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public CarColor getCarColor() {
        return carColor;
    }

    public void setCarColor(CarColor carColor) {
        this.carColor = carColor;
    }

    public int getYearManufacturer() {
        return yearManufacturer;
    }

    public void setYearManufacturer(int yearManufacturer) {
        this.yearManufacturer = yearManufacturer;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public void setTaxiType(TaxiType taxiType) {
        this.taxiType = taxiType;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taxi taxi = (Taxi) o;
        return id == taxi.id &&
                yearManufacturer == taxi.yearManufacturer &&
                mileage == taxi.mileage &&
                Double.compare(taxi.cost, cost) == 0 &&
                taxiType == taxi.taxiType &&
                carModel == taxi.carModel &&
                carColor == taxi.carColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taxiType, carModel, carColor, yearManufacturer, mileage, cost);
    }

    @Override
    public String toString() {
        return "<p><b>Taxi ID:</b> " + id + "</p>" +
                "<p><b>taxi type:</b> " + taxiType + "</p>" +
                "<p><b>car model:</b> " + carModel + "</p>" +
                "<p><b>car color:</b> " + carColor + "</p>" +
                "<p><b>year of manufacturer:</b> " + yearManufacturer + "</p>" +
                "<p><b>mileage:</b> " + mileage + "</p>" +
                "<p><b>cost:</b> " + cost + "</p>";
    }
}

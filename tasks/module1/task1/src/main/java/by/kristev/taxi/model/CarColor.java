package by.kristev.taxi.model;

public enum CarColor {
    BLACK,
    WHITE,
    RED,
    YELLOW,
    ORANGE;
}

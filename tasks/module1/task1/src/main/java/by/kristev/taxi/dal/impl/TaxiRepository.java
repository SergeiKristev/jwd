package by.kristev.taxi.dal.impl;

import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.model.Taxi;

import java.util.ArrayList;
import java.util.List;

public class TaxiRepository implements Repository<Taxi> {

    private List<Taxi> taxi = new ArrayList<>();


    @Override
    public List<Taxi> find(Specification<Taxi> spec) {

        List<Taxi> result = new ArrayList<>();
        for (Taxi t : taxi) {
            if (spec.match(t)) {
                result.add(t);
            }
        }
        return result;
    }

    @Override
    public List<Taxi> findAll() {
        return new ArrayList<>(taxi);
    }


    @Override
    public boolean create(Taxi entity) {
        for (Taxi t : taxi) {
            if (entity.getId() == t.getId()) {
                return false;
            }
        }
        taxi.add(entity);
        return true;
    }

    @Override
    public Taxi read(Long entityId) {
        for (Taxi t : taxi) {
            if (t.getId() == entityId) {
                return t;
            }
        }
        return null;
    }

    @Override
    public void update(Taxi entity) {
        taxi.add(entity);
    }

    @Override
    public void delete(Taxi entity) {
        taxi.remove(entity);
    }
}

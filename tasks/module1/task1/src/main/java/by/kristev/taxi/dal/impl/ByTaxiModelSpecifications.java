package by.kristev.taxi.dal.impl;

import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.model.CarModel;
import by.kristev.taxi.model.Taxi;

public class ByTaxiModelSpecifications implements Specification<Taxi> {

    private CarModel searchCarModel;

    public ByTaxiModelSpecifications(CarModel searchCarModel) {
        this.searchCarModel = searchCarModel;
    }

    @Override
    public boolean match(Taxi bean) {
        return searchCarModel.equals(bean.getCarModel());
    }
}

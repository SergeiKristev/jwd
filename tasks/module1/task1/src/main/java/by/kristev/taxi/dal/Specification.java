package by.kristev.taxi.dal;

public interface Specification<T> {

    boolean match(T bean);

}

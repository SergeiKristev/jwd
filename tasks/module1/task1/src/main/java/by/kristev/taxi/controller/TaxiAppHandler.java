package by.kristev.taxi.controller;

import by.kristev.taxi.ApplicationConstants;
import by.kristev.taxi.command.Command;
import by.kristev.taxi.command.CommandFactory;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class TaxiAppHandler implements HttpHandler {
    private static final Logger LOGGER = LogManager.getLogger(TaxiAppHandler.class);
    private final CommandFactory commandFactory;

    public TaxiAppHandler(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        URI requestURI = exchange.getRequestURI();
        LOGGER.info(String.format("Received incoming request: %s URI %s", requestMethod, requestURI.toString()));

        Map<String, String> requestData = getRequestParams(exchange);

        String view = handleRequest(requestData);

        String response = getResponsePage(view);
        OutputStream outputStream = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(200, response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
        } catch (Exception e) {
            exchange.sendResponseHeaders(500, response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
            LOGGER.error(String.format("Response exception: %s", e.getMessage()));
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                LOGGER.error("Impossible close outputStream ");
            }

        }
    }

    private Map<String, String> getRequestParams(HttpExchange exchange) {

        Map<String, String> requestData = new ConcurrentHashMap<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))) {

            String request = null;
            String query = exchange.getRequestURI().getQuery();
            LOGGER.info(String.format("Query: %s", query));


            if ("GET".equals(exchange.getRequestMethod()) && query != null) {
                request = URLDecoder.decode(query, StandardCharsets.UTF_8.name());
            } else if ("POST".equals(exchange.getRequestMethod())) {
                request = URLDecoder.decode(reader.lines().collect(Collectors.joining()), StandardCharsets.UTF_8.name());
                LOGGER.info(String.format("Request: %s", request));

            }

            if (request != null) {
                String[] parameters = request.split("&");
                for (String data : parameters) {
                    requestData.put(data.split("=")[0], data.split("=")[1]);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Parse exception: ", e);
        }

        return requestData;
    }


    public String handleRequest(Map<String, String> requestData) {
        String view = "";

        Command command;
        try {
            String commandName = requestData.get(ApplicationConstants.COMMAND_ARGUMENT_NAME);
            command = commandFactory.getCommand(commandName);
            view = command.process(requestData);

        } catch (Exception e) {
            LOGGER.error("Command exception: ", e);
        }

        return view;
    }

    public String getResponsePage(String view) {
        String content = "";
        String response = "";

        InputStream resourceAsStream = this.getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        content = bufferedReader.lines().collect(Collectors.joining());

        response = MessageFormat.format(content, view);

        return response;
    }
}

package by.kristev.taxi.model;

public enum TaxiType {
    PASSENGER,
    CARGO;
}

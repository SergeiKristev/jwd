package by.kristev.taxi;

public class ApplicationConstants {

    private ApplicationConstants() {
    }

    public static final String COMMAND_ARGUMENT_NAME = "_command";

}

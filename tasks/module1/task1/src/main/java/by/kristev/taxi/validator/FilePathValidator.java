package by.kristev.taxi.validator;

import java.nio.file.Files;
import java.nio.file.Path;

public class FilePathValidator implements Validator<Path> {

    @Override
    public boolean isValid(Path path) {
        String fileName = String.valueOf(path.getFileName()).toLowerCase();
        return Files.exists(path) && fileName.endsWith(".csv");
    }
}

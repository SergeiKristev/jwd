package by.kristev.taxi.model;

public enum CarBodyType {
    SEDAN,
    WAGON,
    HATCHBACK,
    MINIVAN,
    MINIBUS,
    CARGO;
}

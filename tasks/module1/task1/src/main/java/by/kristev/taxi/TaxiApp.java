package by.kristev.taxi;

import by.kristev.taxi.command.*;
import by.kristev.taxi.controller.TaxiAppHandler;
import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.impl.TaxiRepository;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import by.kristev.taxi.service.impl.TaxiServiceImpl;
import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaxiApp {

    private static final Logger LOGGER = LogManager.getLogger(TaxiApp.class);

    public static void main(String[] args) throws IOException {


        InetSocketAddress localhost = new InetSocketAddress("localhost", 49060);
        HttpServer server = null;
        try {
            server = HttpServer.create(localhost, 0);
        } catch (IOException e) {
            LOGGER.error(String.format("Server can't continue work. %s", e));
            throw new IOException();
        }

        Repository<Taxi> repository = new TaxiRepository();
        LOGGER.info("Repository loaded");

        TaxiService taxiService = new TaxiServiceImpl(repository);

        Command viewTaxiCommand = new ViewAllTaxiCommand(taxiService);
        Command viewDetailCommand = new ViewDetailCommand(taxiService);
        Command readFromFileCommand = new ReadFromFileCommand(taxiService);
        Command findByIdCommand = new FindByIdCommand(taxiService);
        Command findByModelCommand = new FindByModelCommand(taxiService);
        Command getTotalCostCommand = new GetTotalCostCommand(taxiService);
        Command deleteTaxiById = new DeleteTaxiCommand(taxiService);
        Command sortCommand = new SortCommand(taxiService);
        Command defaultCommand = new DefaultCommand(taxiService);

        Map<CommandType, Command> commands = new EnumMap<>(CommandType.class);
        commands.put(CommandType.VIEW_ALL, viewTaxiCommand);
        commands.put(CommandType.VIEW_DETAIL, viewDetailCommand);
        commands.put(CommandType.READ_FROM_FILE, readFromFileCommand);
        commands.put(CommandType.FIND_BY_ID, findByIdCommand);
        commands.put(CommandType.FIND_BY_MODEL, findByModelCommand);
        commands.put(CommandType.GET_TOTAL_COST, getTotalCostCommand);
        commands.put(CommandType.SORT, sortCommand);
        commands.put(CommandType.DELETE_TAXI_BY_ID, deleteTaxiById);
        commands.put(CommandType.DEFAULT_COMMAND, defaultCommand);
        LOGGER.info("Registry commands");


        CommandFactory commandFactory = new CommandFactoryImpl(commands);

        server.createContext("/taxi", new TaxiAppHandler(commandFactory));

        LOGGER.info("Created context /taxi");

        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        LOGGER.info(String.format("Server started on %s", server.getAddress().toString()));
    }
}

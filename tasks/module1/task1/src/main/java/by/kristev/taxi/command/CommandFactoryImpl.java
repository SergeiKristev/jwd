package by.kristev.taxi.command;


import java.util.Map;

public class CommandFactoryImpl implements CommandFactory {

    private final Map<CommandType, Command> commands;

    public CommandFactoryImpl(Map<CommandType, Command> commands) {
        this.commands = commands;
    }

    @Override
    public Command getCommand(String commandName) {

        final CommandType commandType = CommandType.of(commandName);
        return commands.get(commandType);
    }
}

package by.kristev.taxi.command;

import by.kristev.taxi.ApplicationConstants;
import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.dal.impl.ByTaxiIdSpecifications;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public class ViewDetailCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(ViewDetailCommand.class);


    private TaxiService taxiService;

    public ViewDetailCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {

        StringBuilder response = new StringBuilder();

        try {
            long id = Long.parseLong(params.get("ID"));
            Specification<Taxi> specification = new ByTaxiIdSpecifications(id);
            List<Taxi> taxiList = taxiService.findTaxi(specification);
            response.append("<h2>DETAIL INFORMATION</h2>");
            for (Taxi t : taxiList) {
                response.append("<div>").
                        append(t.toString()).
                        append("<form action=\"/taxi\" method=\"POST\">\n" + "\t<input type=\"hidden\" name=\"").
                        append(ApplicationConstants.COMMAND_ARGUMENT_NAME + "\" value=\"DELETE_TAXI_BY_ID\">\n").
                        append("\t<input type=\"hidden\" name=ID value=\"").
                        append(t.getId()).
                        append("\">\n").
                        append("\t<input type=\"submit\" value=\"DELETE TAXI\" />\n").
                        append("</form>").
                        append("</div>");

            }
        } catch (Exception e) {
            LOGGER.error(String.format("Taxi found exception %s", e));
            response.append("Invalid ID");
        }

        return response.toString();
    }
}

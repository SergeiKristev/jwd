package by.kristev.taxi.service.impl;

import by.kristev.taxi.dal.Repository;
import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.model.Taxi;
import by.kristev.taxi.service.TaxiService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

public class TaxiServiceImpl implements TaxiService {

    private static final Logger LOGGER = LogManager.getLogger(TaxiServiceImpl.class);


    private Repository<Taxi> repository;

    public TaxiServiceImpl(Repository<Taxi> repository) {
        this.repository = repository;
    }

    @Override
    public Double getTotalCost() {
        List<Taxi> taxi = repository.findAll();
        double totalCost = 0;
        for (Taxi t : taxi) {
            totalCost += t.getCost();
        }
        return totalCost;
    }

    @Override
    public List<Taxi> viewAllTaxi() {
        return repository.findAll();
    }

    @Override
    public Taxi getTaxi(long id) {

        return repository.read(id);
    }

    @Override
    public boolean createTaxi(Taxi taxi) {
        return repository.create(taxi);

    }

    @Override
    public Taxi read(long id) {
        return repository.read(id);
    }

    @Override
    public void update(Taxi taxi) {
        repository.update(taxi);
    }

    @Override
    public void deleteById(long id) {
        List<Taxi> taxiList = repository.findAll();
        for (Taxi t : taxiList) {
            if (t.getId() == id) {
                repository.delete(t);
            }
        }
    }

    @Override
    public List<Taxi> findTaxi(Specification<Taxi> specification) {
        return repository.find(specification);
    }

    @Override
    public List<Taxi> sortTaxiList(String sortType, String sortValue) {

        List<Taxi> taxiList = repository.findAll();

        if (sortType.equals("ASC")) {
            switch (sortValue) {
                case ("ID"):
                    taxiList.sort(Comparator.comparing(Taxi::getId));
                    break;
                case ("COST"):
                    taxiList.sort(Comparator.comparing(Taxi::getCost));
                    break;
                case ("CAR_MODEL"):
                    taxiList.sort(Comparator.comparing((Taxi t) -> t.getCarModel().name()));
                    break;
                case ("CAR_MODEL_AND_COST"):
                    taxiList.sort(Comparator.comparing((Taxi t) -> t.getCarModel().name()).thenComparing(Taxi::getCost));
                    break;
                default:
                    LOGGER.error(String.format("Unknown sort value: %s", sortValue));
            }
        } else if (sortType.equals("DESC")) {
            switch (sortValue) {
                case ("ID"):
                    taxiList.sort(Comparator.comparing(Taxi::getId).reversed());
                    break;
                case ("COST"):
                    taxiList.sort(Comparator.comparing(Taxi::getCost).reversed());
                    break;
                case ("CAR_MODEL"):
                    taxiList.sort(Comparator.comparing((Taxi t) -> t.getCarModel().name()).reversed());
                    break;
                case ("CAR_MODEL_AND_COST"):
                    taxiList.sort(Comparator.comparing((Taxi t) -> t.getCarModel().name()).thenComparing(Taxi::getCost).reversed());
                    break;
                default:
                    LOGGER.error(String.format("Unknown sort value: %s", sortValue));
            }
        }

        return taxiList;
    }

}

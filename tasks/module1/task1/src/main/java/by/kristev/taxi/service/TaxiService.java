package by.kristev.taxi.service;

import by.kristev.taxi.dal.Specification;
import by.kristev.taxi.model.Taxi;

import java.util.List;

public interface TaxiService {

    List<Taxi> viewAllTaxi();

    Taxi getTaxi(long id);

    boolean createTaxi(Taxi taxi);

    Taxi read(long id);

    void update(Taxi taxi);

    public void deleteById(long id);

    List<Taxi> findTaxi(Specification<Taxi> specification);


    Double getTotalCost();

    List<Taxi> sortTaxiList(String sortType, String sortValue);


}

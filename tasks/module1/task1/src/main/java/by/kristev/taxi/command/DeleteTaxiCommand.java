package by.kristev.taxi.command;

import by.kristev.taxi.service.TaxiService;

import java.util.Map;

public class DeleteTaxiCommand implements Command {

    private TaxiService taxiService;

    public DeleteTaxiCommand(TaxiService taxiService) {
        this.taxiService = taxiService;
    }

    @Override
    public String process(Map<String, String> params) {
        long id = 0;
        try {
            id = Long.parseLong(params.get("id".toUpperCase()));
            taxiService.deleteById(id);
            return "<h3>Taxi with id: " + id + " deleted</h3>";
        } catch (Exception e) {
            return "<h3>Taxi can't be deleted</h3>";
        }
    }
}

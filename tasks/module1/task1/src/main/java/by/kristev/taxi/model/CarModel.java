package by.kristev.taxi.model;

public enum CarModel {
    VOLKSWAGEN,
    HYUNDAI,
    SKODA,
    KIA,
    LADA,
    MERCEDES,
    IVECO;
}

package by.kristev.taxi.model;

import java.util.Objects;

public class TaxiCab extends Taxi {

    private CarBodyType bodyType;
    private int numberOfSeats;
    private boolean containsBabySeat;

    public TaxiCab(long id, TaxiType taxiType, CarModel carModel, CarColor carColor,
                   int yearManufacturer, int mileage, double cost, CarBodyType bodyType, int numberOfSeats, boolean containsBabySeat) {
        super(id, taxiType, carModel, carColor, yearManufacturer, mileage, cost);
        this.bodyType = bodyType;
        this.numberOfSeats = numberOfSeats;
        this.containsBabySeat = containsBabySeat;
    }

    public CarBodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(CarBodyType bodyType) {
        this.bodyType = bodyType;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public boolean isContainsBabySeat() {
        return containsBabySeat;
    }

    public void setContainsBabySeat(boolean containsBabySeat) {
        this.containsBabySeat = containsBabySeat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TaxiCab taxiCab = (TaxiCab) o;
        return numberOfSeats == taxiCab.numberOfSeats &&
                containsBabySeat == taxiCab.containsBabySeat &&
                bodyType == taxiCab.bodyType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bodyType, numberOfSeats, containsBabySeat);
    }

    @Override
    public String toString() {
        return super.toString() +
                "<p><b>body type:</b> " + bodyType + "\n" +
                "<p><b>number of seats:</b> " + numberOfSeats + "\n" +
                "<p><b>contains baby seat:</b> " + containsBabySeat + "\n";
    }
}

package by.kristev.taxi.command;

import java.util.Map;

public interface Command {

    String process(Map<String, String> params);
}

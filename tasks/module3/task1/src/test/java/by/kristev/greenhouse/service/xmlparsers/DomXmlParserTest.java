package by.kristev.greenhouse.service.xmlparsers;

import by.kristev.greenhouse.controller.xmlparsers.DomXmlParser;
import by.kristev.greenhouse.model.Plant;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class DomXmlParserTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void SaxXmlParserTest() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        DomXmlParser domXmlParser = new DomXmlParser();
        List<Plant> plants = domXmlParser.parse(inputStream);
        int expectedListSize = 2;
        Assert.assertEquals(expectedListSize, plants.size());
    }
}
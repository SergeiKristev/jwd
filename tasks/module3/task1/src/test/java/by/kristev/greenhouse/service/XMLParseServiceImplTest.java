package by.kristev.greenhouse.service;

import by.kristev.greenhouse.controller.xmlparsers.DomXmlParser;
import by.kristev.greenhouse.controller.xmlparsers.STAXXmlParser;
import by.kristev.greenhouse.controller.xmlparsers.SaxXMLParser;
import by.kristev.greenhouse.dal.PlantsRepository;
import by.kristev.greenhouse.dal.Repository;
import by.kristev.greenhouse.model.Plant;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class XMLParseServiceImplTest {

    Repository<Plant> repository;

    @Before
    public void setUp() throws Exception {
        repository = new PlantsRepository();
    }

    @Test
    public void listFromSaxParserShouldBeCreated() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        SaxXMLParser saxXMLParser = new SaxXMLParser();
        List<Plant> plants = saxXMLParser.parse(inputStream);
        repository.addAll(plants);
        Assert.assertFalse(repository.findAll().isEmpty());
    }

    @Test
    public void listFromSaxtParserShouldBeCreated() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        STAXXmlParser staxXmlParser = new STAXXmlParser();
        List<Plant> plants = staxXmlParser.parse(inputStream);
        repository.addAll(plants);
        Assert.assertFalse(repository.findAll().isEmpty());    }

    @Test
    public void listFromDomParserShouldBeCreated() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        DomXmlParser domXmlParser = new DomXmlParser();
        List<Plant> plants = domXmlParser.parse(inputStream);
        repository.addAll(plants);
        Assert.assertFalse(repository.findAll().isEmpty());    }
}
package by.kristev.greenhouse.controller;

import by.kristev.greenhouse.ApplicationContext;
import by.kristev.greenhouse.commnad.AppCommandProvider;
import by.kristev.greenhouse.commnad.AppCommandProviderImpl;
import by.kristev.greenhouse.dal.PlantsRepository;
import by.kristev.greenhouse.dal.Repository;
import by.kristev.greenhouse.model.Plant;
import by.kristev.greenhouse.controller.xmlparsers.SaxXMLParser;
import by.kristev.greenhouse.controller.xmlparsers.XMLParser;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

public class PlantServletTest {

    Repository<Plant> repository;

    @Before
    public void setUp() throws Exception {
        repository = new PlantsRepository();
        ApplicationContext.getInstance().initialize();
        AppCommandProvider commandProvider = ApplicationContext.getInstance().getBean(AppCommandProviderImpl.class);
    }


    @Test
    public void shouldParseValidFile() throws FileNotFoundException {
        InputStream inputStream = getClass().getResourceAsStream("/flowers_valid.xml");
        XMLParser saxXMLParser = new SaxXMLParser();
        List<Plant> plants = saxXMLParser.parse(inputStream);
        repository.addAll(plants);
        List<Plant> actualList = repository.findAll();
        Assert.assertEquals(2, actualList.size());
    }

    @Test



    @After
    public void tearDown() throws Exception {
        ApplicationContext.getInstance().destroy();
    }
}
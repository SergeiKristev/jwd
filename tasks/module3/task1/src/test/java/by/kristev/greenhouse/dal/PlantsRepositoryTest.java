package by.kristev.greenhouse.dal;

import by.kristev.greenhouse.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PlantsRepositoryTest {

    Repository<Plant> repository;

    @Before
    public void setUp() throws Exception {
        repository = new PlantsRepository();
        {
            Plant plant1 = new Flower(102L, "Rose", Soil.GROUND, Origin.EUROPE, new VisualParameters(Color.GREEN, Color.ORANGE,
                    23.5), new GrowingTips(28, true, 30),
                    Multiplying.SEED, 20, 50, true);

            Plant plant2 = new Flower(105L, "Rose2", Soil.PODZOL, Origin.AUSTRALIA, new VisualParameters(Color.ROSE, Color.PURPLE,
                    50), new GrowingTips(22, true, 50),
                    Multiplying.SEED, 10, 40, true);

            repository.create(plant1);
            repository.create(plant2);
        }
    }

    @Test
    public void repositoryShouldNotReturnNull() {
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void repositoryShouldNotBeEmpty() {
        List<Plant> actualList = repository.findAll();
        Assert.assertFalse(actualList.isEmpty());
    }

    @Test
    public void testRepositorySize() {
        List<Plant> actualList = repository.findAll();
        Assert.assertEquals(2, actualList.size());
    }
}
package by.kristev.greenhouse.service.xmlparsers;

import by.kristev.greenhouse.controller.xmlparsers.SaxXMLParser;
import by.kristev.greenhouse.model.Plant;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class SaxXMLParserTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void SaxXmlParserTest(){
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        SaxXMLParser saxXMLParser = new SaxXMLParser();
        List<Plant> plants = saxXMLParser.parse(inputStream);
        int expectedListSize = 2;
        Assert.assertEquals(expectedListSize, plants.size());
    }
}
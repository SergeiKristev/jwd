package by.kristev.greenhouse.service.xmlparsers;

import by.kristev.greenhouse.controller.xmlparsers.STAXXmlParser;
import by.kristev.greenhouse.model.Plant;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

public class STAXXmlParserTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void STAXXXmlParserTest(){
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("flowers_valid.xml");
        STAXXmlParser staxXmlParser = new STAXXmlParser();
        List<Plant> plants = staxXmlParser.parse(inputStream);
        int expectedListSize = 2;
        Assert.assertEquals(expectedListSize, plants.size());
    }
}
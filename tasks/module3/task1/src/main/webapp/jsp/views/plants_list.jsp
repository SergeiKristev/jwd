<%@ page import="by.kristev.greenhouse.dal.PlantsRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

        <c:if test="${not empty plantsList}">
            <h1 class="navbar-content">Plants list</h1>
            <table class="table is-striped is-hoverable is-fullwidth">
                <thead>
                <th>ID</th>
                <th>NAME</th>
                <th>SOIL</th>
                <th>ORIGIN</th>
                <th>LEAF COLOR</th>
                <th>SIZE</th>
                </thead>
                <tbody>
                <c:forEach items="${plantsList}" var="plant">
                    <tr>
                        <td><c:out value="${plant.id}"/></td>
                        <td><c:out value="${plant.name}"/></td>
                        <td><c:out value="${plant.soil}"/></td>
                        <td><c:out value="${plant.origin}"/></td>
                        <td><c:out value="${plant.visualParameters.leafColor}"/></td>
                        <td><c:out value="${plant.size}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>


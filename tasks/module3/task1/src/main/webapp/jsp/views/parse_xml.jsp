<%@ page import="by.kristev.greenhouse.ApplicationConstants" %>
<%@ page import="by.kristev.greenhouse.commnad.CommandName" %>
<%@ page import="by.kristev.greenhouse.controller.validator.ValidationResult" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container is-fluid">
    <div class="notification">
        <h2>Upload XML file</h2>

        <div>
            <form method="POST" enctype="multipart/form-data">
                <div class="field">
                    <div id="file-js-example" class="file has-name is-primary">
                        <label class="file-label">
                            <input type="hidden" value="${CommandName.UPLOAD_XML}"
                                   name="${ApplicationConstants.COMMAND_PARAM}">
                            <input class="file-input" type="file" name="fileName"/>
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Choose a file…
                                </span>
                            </span>
                            <span class="file-name">
                                No file uploaded
                             </span>
                        </label>

                        <div class="select">
                            <select name="PARSER">
                                <option>SELECT PARSER TYPE</option>
                                <option value="DOM">DOM</option>
                                <option value="SAX">SAX</option>
                                <option value="STAX">STAX</option>
                            </select>
                        </div>

                        <input class="button" type="submit" name="parse_button" value="PARSE"/>

                    </div>
                </div>
            </form>
        </div>
        <div>
            <c:if test="${not empty validationResult}">
                <h1>Parsing error (in progress...)</h1>
                <article class="message is-danger is-small">
                    <div class="message-body">
                        <c:out value="${validationResult}"/>
                    </div>
                </article>
            </c:if>
        </div>
    </div>
</div>
<script>
    const fileInput = document.querySelector('#file-js-example input[type=file]');
    fileInput.onchange = () => {
        if (fileInput.files.length > 0) {
            const fileName = document.querySelector('#file-js-example .file-name');
            fileName.textContent = fileInput.files[0].name;
        }
    }
</script>
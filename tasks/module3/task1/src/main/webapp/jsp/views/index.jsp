<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 5/15/20
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="has-background-warning">
    <c:if test="${not empty indexPageMessage}">
        <article class="message is-danger is-small">
            <div class="message-body">
                <c:out value="${indexPageMessage}"/>
            </div>
        </article>
    </c:if>
</div>
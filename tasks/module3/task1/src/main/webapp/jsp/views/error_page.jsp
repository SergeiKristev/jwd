
<%@ page import="by.kristev.greenhouse.controller.validator.ValidationResult" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach items="${validationResult}" var="error">
    <article class="message is-danger is-small">
        <div class="message-body">
                ${error}
        </div>
    </article>
</c:forEach>

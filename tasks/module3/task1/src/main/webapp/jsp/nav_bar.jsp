<%--
  Created by IntelliJ IDEA.
  User: sergeikristev
  Date: 5/15/20
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.kristev.greenhouse.commnad.CommandName" %>
<aside class="menu">
    <p class="menu-label">
        Menu
    </p>
    <ul class="menu-list">
        <li>
            <a href="?_command=${CommandName.DEFAULT_COMMAND}">Index page</a>
        </li>
        <li>
            <a href="?_command=${CommandName.VIEW_PLANTS_LIST}">View all plants</a>
        </li>
        <li>
            <a href="?_command=${CommandName.UPLOAD_XML}">Upload XML file</a>
        </li>
        <li>
            <a href="?_command=${CommandName.DOWNLOAD_XML}">Download XML file</a>
        </li>

    </ul>
</aside>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page import="by.kristev.greenhouse.commnad.CommandName" %>
<%@ page import="by.kristev.greenhouse.ApplicationConstants" %>
<%@ page import="by.kristev.greenhouse.controller.validator.ValidatorXSD" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Green House App</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bulma.css">

</head>
<body>


<section class="hero is-dark">
    <div class="hero-head">

    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                Green House App
            </h1>
            <h2 class="subtitle">
                plants list
            </h2>
        </div>
    </div>
</section>

<%--<section class="is-fullheight is-medium">--%>
<%--    <div class="column is-one-quarter">--%>
<%--        <jsp:include page="nav_bar.jsp"/>--%>
<%--    </div>--%>
<%--</section>--%>

<section class="is-fullheight is-medium">

    <div class="columns">
        <div class="column is-one-fifth">

            <jsp:include page="nav_bar.jsp"/>
        </div>
        <div class="column is-6">
            <div class="container">
                <div class="content">
            <jsp:include page="views/${viewName}.jsp"/>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="content has-text-centered">
        GreenHouse App, 2020
    </div>
</section>


</body>
</html>

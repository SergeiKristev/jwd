package by.kristev.greenhouse.controller;

import by.kristev.greenhouse.ApplicationContext;
import by.kristev.greenhouse.commnad.*;
import by.kristev.greenhouse.exception.AppCommandException;
import by.kristev.greenhouse.exception.FileParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/", loadOnStartup = 1)
@MultipartConfig(fileSizeThreshold = 6291456, // 6 MB
        maxFileSize = 10485760L, // 10 MB
        maxRequestSize = 20971520L // 20 MB
)
public class PlantServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(PlantServlet.class);


    private AppCommandProvider commandProvider;

    @Override
    public void init() throws ServletException {
        commandProvider = ApplicationContext.getInstance().getBean(AppCommandProviderImpl.class);
    }



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        String commandName = CommandUtil.getCommandFromRequest(req);
        try {
            String viewName = commandProvider.get(CommandName.of(commandName)).execute(req, resp);
            if (viewName.startsWith("redirect:")) {
                String redirect = viewName.replace("redirect:", "");
                resp.sendRedirect(redirect);
            } else {
                req.setAttribute("viewName", viewName);
                req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
            }

        } catch (AppCommandException e) {
            LOGGER.error("Command exception ", e);
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        } catch (Exception e) {
            LOGGER.error("Servlet exception ", e);
            req.setAttribute("viewName", "error_page");
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}

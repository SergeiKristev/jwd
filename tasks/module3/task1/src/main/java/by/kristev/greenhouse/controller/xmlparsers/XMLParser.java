package by.kristev.greenhouse.controller.xmlparsers;

import java.io.InputStream;
import java.util.List;

public interface XMLParser<T> {

    List<T> parse(InputStream is);

}

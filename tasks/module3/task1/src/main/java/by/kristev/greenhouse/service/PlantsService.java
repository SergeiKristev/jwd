package by.kristev.greenhouse.service;

import by.kristev.greenhouse.dal.Specification;
import by.kristev.greenhouse.model.Plant;

import java.util.List;

public interface PlantsService {

    List<Plant> viewAllPlants();

    void addAllPlants(List<Plant> plants);

    Plant getPlant(Long id);

    boolean createPlant(Plant plant);

    Plant read(Long id);

    void update(Plant taxi);

    public void deleteById(long id);

    List<Plant> findPlant(Specification<Plant> specification);
}

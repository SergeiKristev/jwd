package by.kristev.greenhouse.controller.xmlparsers;

public interface ParserFactory<T> {

    XMLParser<T> getParser(String parserType);
}

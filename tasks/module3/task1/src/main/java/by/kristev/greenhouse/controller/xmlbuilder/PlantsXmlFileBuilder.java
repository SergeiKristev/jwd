package by.kristev.greenhouse.controller.xmlbuilder;

import by.kristev.greenhouse.model.*;
import java.util.List;

public class PlantsXmlFileBuilder implements XMLStringBuilder<List<Plant>> {

    private final XMLStringBuilder<Flower> flowerXMLStringBuilder;

    public PlantsXmlFileBuilder() {
        this.flowerXMLStringBuilder = new FlowerXmlBuilder();
    }

    @Override
    public String build(List<Plant> plantList) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<flowers xsi:schemaLocation=\"http://www.example.com/plants flowers.xsd\"\n" +
                "        xmlns=\"http://www.example.com/plants\"\n" +
                "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                ">\n");
        for (Plant plant : plantList) {
            if (plant.getClass().equals(Flower.class)) {
                stringBuilder.append(flowerXMLStringBuilder.build((Flower) plant));
            } else {
                // create other plant
            }
        }
        stringBuilder.append("</flowers>");
        return stringBuilder.toString();
    }
}

package by.kristev.greenhouse.controller.xmlparsers;

public enum ParserTypes {

    SAX("SAX"),
    STAX("STAX"),
    DOM("DOM");

    private final String parser;

    ParserTypes(String s) {
        this.parser = s;
    }

    public static ParserTypes fromString(String name) {

        final ParserTypes[] values = ParserTypes.values();
        for (ParserTypes parserTypes : values) {
            if (parserTypes.getParser().equals(name) || parserTypes.name().equals(name)) {
                return parserTypes;
            }
        }
        return null;
    }


    public String getParser() {
        return parser;
    }
}

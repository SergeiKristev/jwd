package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.model.*;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static by.kristev.greenhouse.model.XMLConstants.*;

public class DomBuilder {

    Plant plant;
    VisualParameters visualParameters;
    GrowingTips growingTips;

    public Plant buildPlant(Element plantElement) {

        plant = new Flower();
        visualParameters = new VisualParameters();
        growingTips = new GrowingTips();

        buildPlantInheritor(plantElement);

        plant.setId(Long.valueOf(plantElement.getAttribute(ID.getValue()).replace("fl", "")));
        String plantName = plantElement.getAttribute(NAME.getValue());
        if (plantName != null) {
            plant.setName(plantName);
        } else {
            plant.setName("Default Plant");
        }

        String soil = getValueFromElement(plantElement, SOIL.getValue());
        plant.setSoil(Soil.fromValue(soil));

        String origin = getValueFromElement(plantElement, ORIGIN.getValue());
        plant.setOrigin(Origin.fromValue(origin));

        Element vParameter = (Element) plantElement.getElementsByTagName(VISUAL_PARAMETER.getValue()).item(0);
        String stalkColor = getValueFromElement(vParameter, STALK_COLOR.getValue());
        visualParameters.setStalkColor(Color.fromValue(stalkColor));

        String leafColor = getValueFromElement(vParameter, LEAF_COLOR.getValue());
        visualParameters.setLeafColor(Color.fromValue(leafColor));

        visualParameters.setAverageSize(Double.parseDouble(getValueFromElement(vParameter, AVERAGE_SIZE.getValue())));

        Element growingTipsElement = (Element) plantElement.getElementsByTagName(GROWING_TIPS.getValue()).item(0);
        growingTips.setTemperature(Double.parseDouble(getValueFromElement(growingTipsElement, TEMPERATURE.getValue())));

        growingTips.setPhotophilous(Boolean.parseBoolean(getValueFromElement(growingTipsElement, IS_PHOTOPHILOUS.getValue())));

        growingTips.setWaterAmount(Integer.parseInt(getValueFromElement(growingTipsElement, WATER_AMOUNT.getValue())));

        String multiplying = getValueFromElement(plantElement, MULTIPLYING.getValue());
        plant.setMultiplying(Multiplying.fromValue(multiplying));

        return plant;
    }


    private void buildPlantInheritor(Element plantElement) {

        if (FLOWER.getValue().equals(plantElement.getTagName())) {
            plant = new Flower();
            visualParameters = new VisualParameters();
            growingTips = new GrowingTips();

            ((Flower) plant).setPetalsQuantity(Integer.parseInt(getValueFromElement(plantElement, PETALS_QUANTITY.getValue())));

            String size = getValueFromElement(plantElement, SIZE.getValue());
            ((Flower) plant).setSize(Double.parseDouble(size));

            String isPoison = getValueFromElement(plantElement, IS_POISON.getValue());
            ((Flower) plant).setPoison(Boolean.parseBoolean(isPoison));

        }
        plant.setVisualParameters(visualParameters);
        plant.setGrowingTips(growingTips);
    }


    private String getValueFromElement(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }
}

package by.kristev.greenhouse.controller.xmlbuilder;

import by.kristev.greenhouse.model.GrowingTips;

public class GrowingTipsXmlBuilder implements XMLStringBuilder<GrowingTips> {

    @Override
    public String build(GrowingTips growingTips) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t<growing-tips>\n")
                .append("\t\t<temperature>")
                .append(growingTips.getTemperature())
                .append("</temperature>\n")
                .append("\t\t<is-photophilous>")
                .append(growingTips.isPhotophilous())
                .append("</is-photophilous>\n")
                .append("\t\t<water-amount>")
                .append(growingTips.getWaterAmount())
                .append("</water-amount>\n")
                .append("\t</growing-tips>\n");
        return stringBuilder.toString();
    }
}

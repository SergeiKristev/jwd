package by.kristev.greenhouse.commnad;

import by.kristev.greenhouse.exception.AppCommandException;
import by.kristev.greenhouse.model.Plant;
import by.kristev.greenhouse.service.PlantsService;
import by.kristev.greenhouse.controller.validator.ValidatorXSD;
import by.kristev.greenhouse.controller.xmlparsers.ParserFactory;
import by.kristev.greenhouse.controller.xmlparsers.SimpleParseFactory;
import by.kristev.greenhouse.controller.xmlparsers.XMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.List;

public class UploadXMLCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(UploadXMLCommand.class);
    private final ValidatorXSD validatorXSD = new ValidatorXSD();
    private final ParserFactory parserFactory;


    private final PlantsService plantsService;

    public UploadXMLCommand(PlantsService plantsService) {
        this.plantsService = plantsService;
        parserFactory = new SimpleParseFactory();
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {


        if (req.getParameter("PARSER") != null) {
            String parsingType = req.getParameter("PARSER");
            String filePath = CommandUtil.getFilePathFromRequest(req);

            LOGGER.info(MessageFormat.format("Parser: {0}", parsingType));
            try (InputStream inputStream = new FileInputStream(filePath)) {
                validatorXSD.getValidationResult().getMessages().clear();
                validatorXSD.validate(filePath);
                if (validatorXSD.getValidationResult().isValid()) {

                    XMLParser parser = parserFactory.getParser(parsingType);
                    List<Plant> plantList = parser.parse(inputStream);
                    plantsService.addAllPlants(plantList);

                } else {
                    LOGGER.error(MessageFormat.format("File is not valid {0}", validatorXSD.getValidationResult()));
                    req.setAttribute("validationResult", validatorXSD.getValidationResult().getMessages());
                    return "error_page";
                }

            } catch (Exception e) {
                LOGGER.error("App command exception ");
            }
            return "redirect:?_command=" + CommandName.VIEW_PLANTS_LIST;
        } else {
            return "parse_xml";
        }
    }

}

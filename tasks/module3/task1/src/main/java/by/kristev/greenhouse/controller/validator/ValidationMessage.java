package by.kristev.greenhouse.controller.validator;

import java.util.List;

public class ValidationMessage {

    private final String fieldName;
    private final String fieldValue;
    private final List<String> errors;

    public ValidationMessage(String fieldName, String fieldValue, List<String> errors) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.errors = errors;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public List<String> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "Field name: " + fieldName +
                ", field value: " + fieldValue  +
                ", error: " + errors + ".";
    }
}

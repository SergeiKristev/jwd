package by.kristev.greenhouse.model;

public enum Soil {
    PODZOL("podzol"),
    GROUND("ground"),
    SOD_PODZOLIC("sod-podzolic");

    private final String value;

    Soil(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Soil fromValue(String value) {

        final Soil[] values = Soil.values();
        for (Soil soil : values) {
            if (soil.getValue().equals(value) || soil.name().equals(value)) {
                return soil;
            }
        }
        throw new IllegalArgumentException(value);
    }
}

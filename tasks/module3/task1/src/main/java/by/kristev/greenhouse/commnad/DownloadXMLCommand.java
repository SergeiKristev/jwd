package by.kristev.greenhouse.commnad;

import by.kristev.greenhouse.exception.AppCommandException;
import by.kristev.greenhouse.model.Plant;
import by.kristev.greenhouse.service.PlantsService;
import by.kristev.greenhouse.controller.xmlbuilder.PlantsXmlFileBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.util.List;

public class DownloadXMLCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(DownloadXMLCommand.class);

    private final PlantsService plantsService;

    public DownloadXMLCommand(PlantsService plantsService) {
        this.plantsService = plantsService;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {
        List<Plant> plants = plantsService.viewAllPlants();
        if (!plants.isEmpty()) {
            PlantsXmlFileBuilder fileBuilder = new PlantsXmlFileBuilder();
            String xmlResponse = fileBuilder.build(plants);
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(resp.getOutputStream()))) {

                resp.setHeader("Content-Disposition", "attachment;filename=" + "flowers.xml");
                resp.setContentType("text/xml");

                writer.write(xmlResponse);
                writer.flush();

                //java.lang.IllegalStateException: Cannot forward after response has been committed ?

            } catch (IOException e) {
                LOGGER.error(MessageFormat.format("File downloading exception {0}", e.getMessage()));
            }
            LOGGER.info("XML file downloaded");
        } else {
            req.setAttribute("indexPageMessage", "Repository is empty!");
            LOGGER.info("XML file didn't download. Repository is empty!");
        }
        return "index";
    }
}

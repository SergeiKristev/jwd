package by.kristev.greenhouse.exception;

public class AppCommandException extends Exception {

    public AppCommandException(String message) {
        super(message);
    }

    public AppCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}

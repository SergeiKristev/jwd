package by.kristev.greenhouse.model;

public enum Color {

    WHITE("white"),
    BLACK("black"),
    BLUE("blue"),
    PURPLE("purple"),
    ROSE("rose"),
    GREEN("green"),
    RED("red"),
    YELLOW("yellow"),
    ORANGE("orange");

    private final String value;

    Color(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Color fromValue(String value) {

        final Color[] values = Color.values();
        for (Color color : values) {
            if (color.getValue().equals(value) || color.name().equals(value)) {
                return color;
            }
        }
        throw new IllegalArgumentException(value);
    }
}

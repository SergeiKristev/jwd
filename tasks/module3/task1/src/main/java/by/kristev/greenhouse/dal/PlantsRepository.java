package by.kristev.greenhouse.dal;

import by.kristev.greenhouse.model.Plant;

import java.util.ArrayList;
import java.util.List;

public class PlantsRepository implements Repository<Plant> {

    private final List<Plant> plants = new ArrayList<>();

    @Override
    public List<Plant> find(Specification<Plant> spec) {
        //
        return null;
    }

    @Override
    public boolean create(Plant entity) {
        for (Plant p : plants) {
            if (entity.getId().equals(p.getId())) {
                return false;
            }
        }
        plants.add(entity);
        return true;
    }

    @Override
    public Plant read(Long entityId) {
        for (Plant p : plants) {
            if (p.getId().equals(entityId)) {
                return p;
            }
        }
        return null;
    }

    @Override
    public void update(Plant entity) {
        plants.add(entity);
    }

    @Override
    public void delete(Plant entity) {
        plants.remove(entity);
    }

    @Override
    public List<Plant> findAll() {
        return new ArrayList<>(plants);
    }

    @Override
    public void addAll(List<Plant> list) {
        for (Plant plant : list) {
            create(plant);
        }
    }
}

package by.kristev.greenhouse.controller.validator;

import by.kristev.greenhouse.ApplicationConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


public class ValidatorXSD implements XmlValidator{
    private static final Logger LOGGER = LogManager.getLogger(ValidatorXSD.class);
    private final ValidationResult validationResult;

    public ValidatorXSD() {
        this.validationResult = new ValidationResult();
    }

    @Override
    public ValidationResult validate(String filePath) {
        File file = new File(getClass().getClassLoader().getResource(ApplicationConstants.XSD_SCHEMA_FILE_PATH).getFile());
        String pathToSchemaXSD = file.getAbsolutePath();

        try {

            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(pathToSchemaXSD));
            Validator validator = schema.newValidator();
            Source source = new StreamSource(new File(filePath));
            validator.validate(source);


            LOGGER.info("File XML is valid");

        } catch (SAXException | IOException | NullPointerException e) {
            LOGGER.error(String.format("File XML is invalid %s", e.getMessage()));
            validationResult.addMessage("File: ", filePath, e.getMessage());
            return validationResult;
        }
        return validationResult;
    }



    public ValidationResult getValidationResult() {
        return validationResult;
    }
}

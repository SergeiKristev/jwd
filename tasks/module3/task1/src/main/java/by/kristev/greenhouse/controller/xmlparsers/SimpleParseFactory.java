package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.model.Plant;

import java.util.EnumMap;
import java.util.Map;

public class SimpleParseFactory implements ParserFactory<Plant> {

    private final Map<ParserTypes, XMLParser<Plant>> parsers;

    public SimpleParseFactory() {
        this.parsers = new EnumMap<>(ParserTypes.class);
        parsers.put(ParserTypes.DOM, new DomXmlParser());
        parsers.put(ParserTypes.SAX, new SaxXMLParser());
        parsers.put(ParserTypes.STAX, new STAXXmlParser());
    }

    @Override
    public XMLParser<Plant> getParser(String parserType) {
        final ParserTypes parserTypes = ParserTypes.fromString(parserType);
        return parsers.get(parserTypes);
    }

    public Map<ParserTypes, XMLParser<Plant>> getParsers() {
        return parsers;
    }
}

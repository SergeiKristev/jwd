package by.kristev.greenhouse.commnad;

public enum CommandName {

    UPLOAD_XML("UPLOAD_XML"),
    DOWNLOAD_XML("DOWNLOAD_XML"),
    VIEW_PLANTS_LIST("VIEW_PLANTS_LIST"),
    DEFAULT_COMMAND("DEFAULT_COMMAND");

    private final String command;

    CommandName(String s) {
        this.command = s;
    }

    public static CommandName of(String name) {

        final CommandName[] values = CommandName.values();
        for (CommandName commandName : values) {
            if (commandName.getCommand().equals(name) || commandName.name().equals(name)) {
                return commandName;
            }
        }
        return DEFAULT_COMMAND;
    }


    public String getCommand() {
        return command;
    }
}

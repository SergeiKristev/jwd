package by.kristev.greenhouse.model;

import java.util.Objects;

public class Flower extends Plant {

    private int petalsQuantity;
    private double size;
    private boolean isPoison;


    public Flower(Long id, String name, Soil soil, Origin origin, VisualParameters visualParameters,
                  GrowingTips growingTips, Multiplying multiplying, int petalsQuantity,
                  double size, boolean isPoison) {
        super(id, name, soil, origin, visualParameters, growingTips, multiplying);
        this.petalsQuantity = petalsQuantity;
        this.size = size;
        this.isPoison = isPoison;
    }

    public Flower() {
    }

    public Flower(Long id, String name, Soil soil, Origin origin, VisualParameters visualParameters, GrowingTips growingTips, Multiplying multiplying) {
        super(id, name, soil, origin, visualParameters, growingTips, multiplying);
    }

    public int getPetalsQuantity() {
        return petalsQuantity;
    }

    public void setPetalsQuantity(int petalsQuantity) {
        this.petalsQuantity = petalsQuantity;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public boolean isPoison() {
        return isPoison;
    }

    public void setPoison(boolean poison) {
        isPoison = poison;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return petalsQuantity == flower.petalsQuantity &&
                Double.compare(flower.size, size) == 0 &&
                isPoison == flower.isPoison;
    }

    @Override
    public int hashCode() {
        return Objects.hash(petalsQuantity, size, isPoison);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "petalsQuantity=" + petalsQuantity +
                ", size=" + size +
                ", isPoison=" + isPoison +
                "} " + super.toString();
    }
}

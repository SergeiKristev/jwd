package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.exception.XMLParseException;
import by.kristev.greenhouse.model.Plant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static by.kristev.greenhouse.model.XMLConstants.FLOWER;

public class DomXmlParser implements XMLParser<Plant> {
    private static final Logger LOGGER = LogManager.getLogger(DomXmlParser.class);
    private final DomBuilder domBuilder;
    private final DocumentBuilderFactory dbf;

    public DomXmlParser() {
        dbf = DocumentBuilderFactory.newInstance();
        domBuilder = new DomBuilder();
    }

    @Override
    public List<Plant> parse(InputStream is) {
        List<Plant> plants = new ArrayList<>();

        try {
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document document = documentBuilder.parse(is);
            Element root = document.getDocumentElement();
            NodeList plantList = root.getElementsByTagName(FLOWER.getValue());
            for (int i = 0; i < plantList.getLength(); i++) {
                Element element = (Element) plantList.item(i);
                Plant plant = domBuilder.buildPlant(element);
                plants.add(plant);
            }
            LOGGER.info("Parsing file was successful");
        } catch (IOException | SAXException | ParserConfigurationException e) {
            LOGGER.error("Streaming Exception in dom Parser", e);
            throw new XMLParseException("Xml Steam Exception in DOM parser");
        }

        return plants;
    }

}

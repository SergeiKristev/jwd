package by.kristev.greenhouse.commnad;

import java.util.EnumMap;
import java.util.Map;

public class AppCommandProviderImpl implements AppCommandProvider {

    private final Map<CommandName, Command> commands = new EnumMap<>(CommandName.class);

    @Override
    public void register(CommandName commandName, Command command) {
        this.commands.put(commandName, command);
    }

    @Override
    public void remove(CommandName commandName) {
        this.commands.remove(commandName);
    }

    @Override
    public Command get(CommandName commandName) {
        return this.commands.get(commandName);
    }

    public Map<CommandName, Command> getCommands() {
        return commands;
    }
}

package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.exception.FileParseException;
import by.kristev.greenhouse.exception.XMLParseException;
import by.kristev.greenhouse.model.Plant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SaxXMLParser implements XMLParser<Plant> {

    private static final Logger LOGGER = LogManager.getLogger(SaxXMLParser.class);
    private final PlantHandler plantHandler;

    public SaxXMLParser() {
        plantHandler = new PlantHandler();
    }

    @Override
    public List<Plant> parse(InputStream inputStream) {
        List<Plant> plants = new ArrayList<>();
        try {
            SAXParserFactory parserFactor = SAXParserFactory.newInstance();
            SAXParser parser = parserFactor.newSAXParser();
            parser.parse(inputStream, plantHandler);
            plants = plantHandler.getPlantList();
            LOGGER.info("Parsing file was successful");
        } catch (IOException e) {
            LOGGER.error("Exception streaming xml reader", e.getCause());
        } catch (SAXException | ParserConfigurationException e) {
            LOGGER.error("Exception parsing xml file", e.getCause());
            throw new XMLParseException("Xml Steam Exception in Sax parser");
        }
        return plants;
    }


}

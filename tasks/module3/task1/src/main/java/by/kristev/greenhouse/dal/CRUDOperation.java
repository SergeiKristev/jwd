package by.kristev.greenhouse.dal;

public interface CRUDOperation<T> {

    boolean create(T entity);

    T read(Long entityId);

    void update(T entity);

    void delete(T entity);
}

package by.kristev.greenhouse.commnad;

import by.kristev.greenhouse.ApplicationConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

public class CommandUtil {

    private static final Logger LOGGER = LogManager.getLogger(CommandUtil.class);


    private CommandUtil() {
    }

    public static String getCommandFromRequest(HttpServletRequest request) {

        return request.getAttribute(ApplicationConstants.COMMAND_PARAM) != null ? String.valueOf(request.getAttribute(
                ApplicationConstants.COMMAND_PARAM)) :
                request.getParameter(ApplicationConstants.COMMAND_PARAM);
    }

    public static String getFilePathFromRequest(HttpServletRequest req) {
        String fileName;
        String uploadPath = req.getServletContext().getRealPath("");
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        try {
            Part filePart = req.getPart("fileName");
            fileName = getFileNameFromPart(filePart);
            if (fileName != null) {
                uploadPath = uploadDir + File.separator + fileName;
                LOGGER.info(String.format("File path: %s", uploadPath));
                filePart.write(uploadPath);
            }
        } catch (IOException | ServletException e) {
            LOGGER.error("Exception getting filename ", e);
        }
        return uploadPath;
    }


    public static String getFileNameFromPart(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename"))
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
        }
        return null;
    }
}

package by.kristev.greenhouse;

public class ApplicationConstants {

    private ApplicationConstants() {
    }

    public static final String COMMAND_PARAM = "_command";
    public static final String XSD_SCHEMA_FILE_PATH = "flowers.xsd";

}

package by.kristev.greenhouse.controller.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValidationResult {

    private final List<ValidationMessage> messages = new ArrayList<>();

    public void addMessage(String field, String value, String...errors) {
        messages.add(new ValidationMessage(field, value, Arrays.asList(errors)));
    }

    public boolean isValid(){
        return this.messages.isEmpty();
    }

    public List<ValidationMessage> getMessages() {
        return messages;
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "messages=" + messages +
                '}';
    }
}

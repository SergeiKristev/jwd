package by.kristev.greenhouse.controller.xmlbuilder;

import by.kristev.greenhouse.model.*;

public class FlowerXmlBuilder implements XMLStringBuilder<Flower> {

    private final XMLStringBuilder<VisualParameters> visualParametersBuilder;
    private final XMLStringBuilder<GrowingTips> growingTipsBuilder;

    public FlowerXmlBuilder() {
        this.visualParametersBuilder = new VisualParametersXmlBuilder();
        this.growingTipsBuilder = new GrowingTipsXmlBuilder();
    }


    @Override
    public String build(Flower flower) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<flower ")
                .append("id=\"fl")
                .append(flower.getId())
                .append("\" name=\"")
                .append(flower.getName())
                .append("\">\n")
                .append("\t<soil>")
                .append(flower.getSoil().getValue())
                .append("</soil>\n")
                .append("\t<origin>")
                .append(flower.getOrigin().getValue())
                .append("</origin>\n")

                .append(visualParametersBuilder.build(flower.getVisualParameters()))

                .append((growingTipsBuilder.build(flower.getGrowingTips())))

                .append("\t<multiplying>")
                .append(flower.getMultiplying().getValue())
                .append("</multiplying>\n")
                .append("\t<petals-quantity>")
                .append(flower.getPetalsQuantity())
                .append("</petals-quantity>\n")
                .append("\t<size>")
                .append(flower.getSize())
                .append("</size>\n")
                .append("\t<is-poison>")
                .append(flower.isPoison())
                .append("</is-poison>\n")
                .append("</flower>\n");
        return stringBuilder.toString();
    }

}


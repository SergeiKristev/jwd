package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.exception.FileParseException;
import by.kristev.greenhouse.exception.XMLParseException;
import by.kristev.greenhouse.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static by.kristev.greenhouse.model.XMLConstants.*;
import static by.kristev.greenhouse.model.XMLConstants.FLOWER;

public class STAXXmlParser implements XMLParser<Plant> {
    private static final Logger LOGGER = LogManager.getLogger(STAXXmlParser.class);
    private final XMLInputFactory inputFactory;
    private Plant plant;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public STAXXmlParser() {
        inputFactory = XMLInputFactory.newInstance();
    }

    @Override
    public List<Plant> parse(InputStream inputStream) {
        List<Plant> plants = new ArrayList<>();
        String name;
        try {
            XMLStreamReader streamReader = inputFactory.createXMLStreamReader(inputStream);
            while (streamReader.hasNext()) {
                int type = streamReader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = streamReader.getLocalName();
                    if (FLOWER.getValue().equals(name)) {
                        plant = buildPlant(streamReader, name);
                        plants.add(plant);
                    }
                }
            }
            LOGGER.info("Parsing file was successful");
        } catch (XMLStreamException e) {
            LOGGER.error("Xml Steam Exception in Stax parser", e);
            throw new XMLParseException("Xml Steam Exception in Stax parser");
        }

        return plants;
    }

    public Plant buildPlant(XMLStreamReader reader, String plantType) throws XMLStreamException {

        if (FLOWER.getValue().equalsIgnoreCase(plantType)) {
            plant = new Flower();
            visualParameters = new VisualParameters();
            growingTips = new GrowingTips();
        } else {
            LOGGER.error("Unknown plant's type");
        }
        String id = reader.getAttributeValue(null, ID.getValue()).replace("fl", "");
        plant.setId(Long.valueOf(id));
        plant.setName(reader.getAttributeValue(null, NAME.getValue()));

        String currentElement;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    currentElement = reader.getLocalName();
                    switch (Objects.requireNonNull(getFromValue(currentElement))) {
                        case SOIL:
                            String soil = getValueFromElement(reader);
                            plant.setSoil(Soil.fromValue(soil));
                            break;
                        case ORIGIN:
                            String origin = getValueFromElement(reader);
                            plant.setOrigin(Origin.fromValue(origin));
                            break;
                        case STALK_COLOR:
                            String stalkColor = getValueFromElement(reader);
                            visualParameters.setStalkColor(Color.fromValue(stalkColor));
                            break;
                        case LEAF_COLOR:
                            String leafColor = getValueFromElement(reader);
                            visualParameters.setLeafColor(Color.fromValue(leafColor));
                            break;
                        case AVERAGE_SIZE:
                            visualParameters.setAverageSize(Double.parseDouble(getValueFromElement(reader)));
                            break;
                        case TEMPERATURE:
                            growingTips.setTemperature(Double.parseDouble(getValueFromElement(reader)));
                            break;
                        case IS_PHOTOPHILOUS:
                            growingTips.setPhotophilous(Boolean.parseBoolean(getValueFromElement(reader)));
                            break;
                        case WATER_AMOUNT:
                            growingTips.setWaterAmount(Integer.parseInt(getValueFromElement(reader)));
                            break;
                        case MULTIPLYING:
                            String multiplying = getValueFromElement(reader);
                            plant.setMultiplying(Multiplying.fromValue(multiplying));
                            break;
                        case PETALS_QUANTITY:
                            String petalsQuantity = getValueFromElement(reader);
                            ((Flower) plant).setPetalsQuantity(Integer.parseInt(petalsQuantity));
                            break;
                        case SIZE:
                            String size = getValueFromElement(reader);
                            ((Flower) plant).setSize(Double.parseDouble(size));
                            break;
                        case IS_POISON:
                            String isPoison = getValueFromElement(reader);
                            ((Flower) plant).setPoison(Boolean.parseBoolean(isPoison));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    currentElement = reader.getLocalName();
                    if (FLOWER.equals(getFromValue(currentElement))) {
                        plant.setVisualParameters(visualParameters);
                        plant.setGrowingTips(growingTips);
                        return plant;
                    }
                    break;
            }
        }
        throw new XMLStreamException("There is no such elements");
    }


    private String getValueFromElement(XMLStreamReader reader) throws XMLStreamException {
        String text = "";
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}

package by.kristev.greenhouse.model;

import java.util.Objects;

public class VisualParameters {

    private Color stalkColor;
    private Color leafColor;
    private double averageSize;

    public VisualParameters() {
    }

    public VisualParameters(Color stalkColor, Color leafColor, double averageSize) {
        this.stalkColor = stalkColor;
        this.leafColor = leafColor;
        this.averageSize = averageSize;
    }

    public Color getStalkColor() {
        return stalkColor;
    }

    public void setStalkColor(Color stalkColor) {
        this.stalkColor = stalkColor;
    }

    public Color getLeafColor() {
        return leafColor;
    }

    public void setLeafColor(Color leafColor) {
        this.leafColor = leafColor;
    }

    public double getAverageSize() {
        return averageSize;
    }

    public void setAverageSize(double averageSize) {
        this.averageSize = averageSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisualParameters that = (VisualParameters) o;
        return Double.compare(that.averageSize, averageSize) == 0 &&
                stalkColor == that.stalkColor &&
                leafColor == that.leafColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stalkColor, leafColor, averageSize);
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stalkColor=" + stalkColor +
                ", leafColor=" + leafColor +
                ", averageSize=" + averageSize +
                '}';
    }
}

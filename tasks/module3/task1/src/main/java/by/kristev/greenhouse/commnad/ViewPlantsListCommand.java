package by.kristev.greenhouse.commnad;

import by.kristev.greenhouse.exception.AppCommandException;
import by.kristev.greenhouse.model.Plant;
import by.kristev.greenhouse.service.PlantsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ViewPlantsListCommand implements Command {

    private final PlantsService plantsService;

    public ViewPlantsListCommand(PlantsService plantsService) {
        this.plantsService = plantsService;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {
        try {
            List<Plant> plants = plantsService.viewAllPlants();
            if (!plants.isEmpty()) {
                req.setAttribute("plantsList", plants);
            } else {
                req.setAttribute("indexPageMessage", "Repository is empty!");
                return "index";
            }
        } catch (Exception e) {
            throw new AppCommandException("AppCommand exception", e);
        }
        return "plants_list";
    }
}

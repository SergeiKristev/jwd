package by.kristev.greenhouse.model;

public enum Multiplying {
    LEAVES("leaves"),
    CUTTING("cutting"),
    SEED("seed");

    private final String value;

    Multiplying(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Multiplying fromValue(String value) {

        final Multiplying[] values = Multiplying.values();
        for (Multiplying multiplying : values) {
            if (multiplying.getValue().equals(value) || multiplying.name().equals(value)) {
                return multiplying;
            }
        }
        throw new IllegalArgumentException(value);
    }
}

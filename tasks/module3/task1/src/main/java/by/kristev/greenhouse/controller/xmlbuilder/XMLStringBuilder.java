package by.kristev.greenhouse.controller.xmlbuilder;

public interface XMLStringBuilder<T> {

    String build(T t);
}

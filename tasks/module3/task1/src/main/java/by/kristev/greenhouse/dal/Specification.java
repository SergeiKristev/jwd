package by.kristev.greenhouse.dal;

public interface Specification<T> {

    boolean match(T bean);

}

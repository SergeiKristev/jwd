package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.controller.validator.ValidationResult;

import java.util.List;

public class ParseResult<T> {

    private final List<T> parseResultList;
    private final List<ValidationResult> validationResults;

    public ParseResult(List<T> parseResult, List<ValidationResult> validationResults) {
        this.parseResultList = parseResult;
        this.validationResults = validationResults;
    }

    public List<T> getParseResultList() {
        return parseResultList;
    }

    public List<ValidationResult> getValidationResults() {
        return validationResults;
    }
}

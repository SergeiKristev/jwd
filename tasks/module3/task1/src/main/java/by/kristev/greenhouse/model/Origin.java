package by.kristev.greenhouse.model;

public enum Origin {

    EUROPE("Europe"),
    ASIA("Asia"),
    AFRICA("Africa"),
    NORTH_AMERICA("North America"),
    SOUTH_AMERICA("South America"),
    AUSTRALIA("Australia");

    final String value;

    Origin(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Origin fromValue(String value) {

        final Origin[] values = Origin.values();
        for (Origin origin : values) {
            if (origin.getValue().equals(value) || origin.name().equals(value)) {
                return origin;
            }
        }
        throw new IllegalArgumentException(value);
    }
}

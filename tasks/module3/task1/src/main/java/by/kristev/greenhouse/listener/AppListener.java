package by.kristev.greenhouse.listener;

import by.kristev.greenhouse.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().initialize();
        LOGGER.info(String.format("Context initialized%s", servletContextEvent.toString()));
        ServletContext servletContext = servletContextEvent.getServletContext();
        servletContext.setAttribute("configMessage", "Message from servlet config");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().destroy();
        LOGGER.info(String.format("Context destroyed%s", servletContextEvent.toString()));
    }
}

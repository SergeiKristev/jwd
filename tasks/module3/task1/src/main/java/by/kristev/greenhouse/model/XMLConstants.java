package by.kristev.greenhouse.model;

public enum XMLConstants {

    FLOWERS("flowers"),
    FLOWER("flower"),
    VISUAL_PARAMETER("visual-parameter"),
    GROWING_TIPS("growing-tips"),

    //constants with text
    ID("id"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    STALK_COLOR("stalk-color"),
    LEAF_COLOR("leaf-color"),
    AVERAGE_SIZE("average-size"),
    TEMPERATURE("temperature"),
    IS_PHOTOPHILOUS("is-photophilous"),
    WATER_AMOUNT("water-amount"),
    MULTIPLYING("multiplying"),
    PETALS_QUANTITY("petals-quantity"),
    SIZE("size"),
    IS_POISON("is-poison");

    private String value;

    XMLConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static XMLConstants getFromValue(String value) {
        for (XMLConstants constants : values()) {
            if (constants.getValue().equalsIgnoreCase(value)) {
                return constants;
            }
        }
        return null;
    }

}

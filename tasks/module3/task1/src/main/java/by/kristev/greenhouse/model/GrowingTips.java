package by.kristev.greenhouse.model;

import java.util.Objects;

public class GrowingTips {

    private double temperature;
    private boolean isPhotophilous;
    private int waterAmount;

    public GrowingTips() {
    }

    public GrowingTips(double temperature, boolean isPhotophilous, int waterAmount) {
        this.temperature = temperature;
        this.isPhotophilous = isPhotophilous;
        this.waterAmount = waterAmount;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public boolean isPhotophilous() {
        return isPhotophilous;
    }

    public void setPhotophilous(boolean photophilous) {
        isPhotophilous = photophilous;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(int waterAmount) {
        this.waterAmount = waterAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowingTips that = (GrowingTips) o;
        return Double.compare(that.temperature, temperature) == 0 &&
                isPhotophilous == that.isPhotophilous &&
                waterAmount == that.waterAmount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature, isPhotophilous, waterAmount);
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", isPhotophilous=" + isPhotophilous +
                ", waterAmount=" + waterAmount +
                '}';
    }
}

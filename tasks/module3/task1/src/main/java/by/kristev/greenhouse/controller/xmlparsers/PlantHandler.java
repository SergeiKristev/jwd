package by.kristev.greenhouse.controller.xmlparsers;

import by.kristev.greenhouse.model.*;

import static by.kristev.greenhouse.model.XMLConstants.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class PlantHandler extends DefaultHandler {

    private static final Logger LOGGER = LogManager.getLogger(PlantHandler.class);
    private final List<Plant> plantList;
    private XMLConstants xmlConstants = null;
    private Plant plant = null;
    private final EnumSet<XMLConstants> withText;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public PlantHandler() {
        plantList = new ArrayList<>();
        withText = EnumSet.range(XMLConstants.ID, XMLConstants.IS_POISON);
    }

    public List<Plant> getPlantList() {
        return plantList;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (FLOWER.getValue().equalsIgnoreCase(qName)) {
            plant = new Flower();
            visualParameters = new VisualParameters();
            growingTips = new GrowingTips();
            plant.setId(Long.valueOf(attributes.getValue(0).replace("fl", "")));
            if (attributes.getLength() == 2) {
                plant.setName(attributes.getValue(1));
            } else {
                plant.setName("Default Plant");
            }
            plant.setVisualParameters(visualParameters);
            plant.setGrowingTips(growingTips);
        } else {
            XMLConstants temp = XMLConstants.getFromValue(qName);
            if (withText.contains(temp)) {
                xmlConstants = temp;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (FLOWER.getValue().equalsIgnoreCase(qName)) {
            plantList.add(plant);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String data = new String(ch, start, length).trim();
        if (xmlConstants != null) {
            switch (xmlConstants) {
                case SOIL:
                    plant.setSoil(Soil.fromValue(data));
                    break;
                case ORIGIN:
                    plant.setOrigin(Origin.fromValue(data));
                    break;
                case STALK_COLOR:
                    visualParameters.setStalkColor(Color.fromValue(data));
                    break;
                case LEAF_COLOR:
                    visualParameters.setLeafColor(Color.fromValue(data));
                    break;
                case AVERAGE_SIZE:
                    visualParameters.setAverageSize(Double.parseDouble(data));
                    break;
                case TEMPERATURE:
                    growingTips.setTemperature(Double.parseDouble(data));
                    break;
                case IS_PHOTOPHILOUS:
                    growingTips.setPhotophilous(Boolean.parseBoolean(data));
                    break;
                case WATER_AMOUNT:
                    growingTips.setWaterAmount(Integer.parseInt(data));
                    break;
                case MULTIPLYING:
                    plant.setMultiplying(Multiplying.fromValue(data));
                    break;
                case PETALS_QUANTITY:
                    ((Flower) plant).setPetalsQuantity(Integer.parseInt(data));
                    break;
                case SIZE:
                    ((Flower) plant).setSize(Double.parseDouble(data));
                    break;
                case IS_POISON:
                    ((Flower) plant).setPoison(Boolean.parseBoolean(data));
                    break;
                default:
                    LOGGER.error("No such element");
            }
        }
        xmlConstants = null;
    }
}

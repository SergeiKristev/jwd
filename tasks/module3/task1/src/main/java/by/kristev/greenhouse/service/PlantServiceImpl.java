package by.kristev.greenhouse.service;

import by.kristev.greenhouse.dal.Repository;
import by.kristev.greenhouse.dal.Specification;
import by.kristev.greenhouse.model.Plant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class PlantServiceImpl implements PlantsService {

    private static final Logger LOGGER = LogManager.getLogger(PlantServiceImpl.class);


    private final Repository<Plant> repository;

    public PlantServiceImpl(Repository<Plant> repository) {
        this.repository = repository;
    }

    @Override
    public List<Plant> viewAllPlants() {

        return repository.findAll();
    }

    @Override
    public void addAllPlants(List<Plant> plants) {
        LOGGER.info("All plants added to repository");
        repository.addAll(plants);
    }

    @Override
    public Plant getPlant(Long id) {
        //
        return null;
    }

    @Override
    public boolean createPlant(Plant plant) {
        //
        return false;
    }

    @Override
    public Plant read(Long id) {
        return null;
    }

    @Override
    public void update(Plant plant) {
        //
    }

    @Override
    public void deleteById(long id) {
        //
    }

    @Override
    public List<Plant> findPlant(Specification<Plant> specification) {
        //
        return null;
    }
}

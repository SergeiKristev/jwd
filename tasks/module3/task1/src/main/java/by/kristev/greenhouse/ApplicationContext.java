package by.kristev.greenhouse;

import by.kristev.greenhouse.commnad.*;
import by.kristev.greenhouse.dal.PlantsRepository;
import by.kristev.greenhouse.dal.Repository;
import by.kristev.greenhouse.model.Plant;
import by.kristev.greenhouse.service.PlantServiceImpl;
import by.kristev.greenhouse.service.PlantsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);

    private static final ApplicationContext INSTANCE = new ApplicationContext();

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    private ApplicationContext() {
    }

    public void initialize() {
        Repository<Plant> repository = new PlantsRepository();
        LOGGER.info("Repository loaded");

        PlantsService plantsService = new PlantServiceImpl(repository);
        LOGGER.info("Services loaded");

        Command viewPlantsListCommand = new ViewPlantsListCommand(plantsService);
        Command uploadXMLCommand = new UploadXMLCommand(plantsService);
        Command downloadXMLCommand = new DownloadXMLCommand(plantsService);
        Command defaultCommand = new DefaultCommand();

        AppCommandProvider appCommandProvider = new AppCommandProviderImpl();

        appCommandProvider.register(CommandName.DEFAULT_COMMAND, defaultCommand);
        appCommandProvider.register(CommandName.VIEW_PLANTS_LIST, viewPlantsListCommand);
        appCommandProvider.register(CommandName.UPLOAD_XML, uploadXMLCommand);
        appCommandProvider.register(CommandName.DOWNLOAD_XML, downloadXMLCommand);


        LOGGER.info("Registry commands");

        beans.put(plantsService.getClass(), plantsService);
        beans.put(appCommandProvider.getClass(), appCommandProvider);

    }

    public void destroy() {
        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }
}

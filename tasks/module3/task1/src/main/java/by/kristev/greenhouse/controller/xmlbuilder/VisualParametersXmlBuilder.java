package by.kristev.greenhouse.controller.xmlbuilder;

import by.kristev.greenhouse.model.VisualParameters;


public class VisualParametersXmlBuilder implements XMLStringBuilder<VisualParameters> {

    @Override
    public String build(VisualParameters visualParameters) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\t<visual-parameter>\n")
                .append("\t\t<stalk-color>")
                .append(visualParameters.getStalkColor().getValue())
                .append("</stalk-color>\n")
                .append("\t\t<leaf-color>")
                .append(visualParameters.getLeafColor().getValue())
                .append("</leaf-color>\n")
                .append("\t\t<average-size>")
                .append(visualParameters.getAverageSize())
                .append("</average-size>\n")
                .append("\t</visual-parameter>\n");
        return stringBuilder.toString();
    }

}

package by.kristev.greenhouse.commnad;

import by.kristev.greenhouse.exception.AppCommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultCommand implements Command {

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {
        return "index";
    }
}

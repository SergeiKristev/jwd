package by.kristev.greenhouse.dal;

import java.util.List;

public interface Repository<T> extends CRUDOperation<T> {

    List<T> find(Specification<T> spec);

    List<T> findAll();

    void addAll(List<T> list);
}

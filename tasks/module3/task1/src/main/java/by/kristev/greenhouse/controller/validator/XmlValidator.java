package by.kristev.greenhouse.controller.validator;


public interface XmlValidator {

    ValidationResult validate(String filePath);
}
